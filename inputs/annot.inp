 &input
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% nelc = number of electrons. This is actually one less than the total number of
% electrons in your system as we are adding an electron to make an N+1 electron
% system. So here for singly ionised carbon, nelc=4
%
% nz = atomic number (of the neutral) 
%
% lrgle2 = maximum angular momentum of the total (N+1) electron system
%
% nset = number of symmetries included (see below)
%
% maxorb/inorb = number of orbitals included 
%
% ncfg = total number of configurations
%
% nrang2 = total number of splines to be used in the inner region
%
% ra = r matrix radius
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
	nelc=4, nz=6, lrgle1=0, lrgle2=7, lrgld1=20, lrgld2=-1, lrgslo=2,
	lrgsup=2, nset=3, maxorb=10, inorb=10, ncfg=22, nrang2=50, ek2max=2.5504287,
        ihbug(9)=0, ihbug(2)=0, irbug(6) = 0, bsto=0.000, nexp=0, ibop=0, nix=-2, 
        mpol = 1, ra=20.0, buttle=.true., farm=.true., xdr=.false., ndiag=1,
        restart=.false., lfixn =20
 ,/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Symmetry section:
% The four columns signify
% 1: The angular momentum number l, ie: 0=s, 1=p etc
% 2: Singlet or Triplet state
% 3: Even or odd
% 4: How many of each state.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 0   1   0   1
% means s singlet even one 
 1   3   1   1
% means p triplet odd one
 1   1   1   1

% This is then a list of the orbitals in pairs
% 1: The principal number n
% 2: The angular momentum number l
%
 1   0   2   0   2   1   3   0   3   1   3   2   4   0   4   1   4   2   4   3 
% 
% reads 1s 2s 2p 3s 3p 3d 4s 4p 4d 4f
%
% Leave these zeros alone!
%
0 0 0
% 
% tells how many elements in each row following
 2  3  2  3  3  3  3  3  3  3  3  3  3  2  2  2  3  3  2  2  2  2 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lists all possible configurations of the electrons two rows per configuration
% first row is the orbitals corresponding to the list above (so ' 1   2'  is the
% 1s and 2s orbitals). Second row is the occupancy of those orbitals. So the
% first two rows here designate the 1s^2 2s^2 configuration.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   1    2     
   2    2      
   1    2    3    
   2    1    1
   1    3       
   2    2
   1    2    4   
   2    1    1
   1    2    5   
   2    1    1
   1    2    7
   2    1    1
   1    2    8
   2    1    1
   1    3    4
   2    1    1
   1    3    5
   2    1    1
   1    3    6
   2    1    1
   1    3    7
   2    1    1
   1    3    8
   2    1    1
   1    3    9
   2    1    1
   1    4
   2    2
   1    5
   2    2
   1    6
   2    2
   1    4    5
   2    1    1
   1    5    6
   2    1    1
   1    7    
   2    2   
   1    8
   2    2
   1    9
   2    2
   1   10
   2    2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
% Two rows
% max n of bound L orbitals. 
% max n for each core l
% in this case we have up to 4f, so the highest s, p, d and f is n=4
% If we had up to 4s we would have 4 3 3, as the highest s is n=4, and the highest
% p and d is n=3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   4  4  4  4
   4  4  4  4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Orbital data:Orbitals are Slater Type 
%
% N . r^n . exp( -a . r )
%
% and this is encoded in four lines: 
%
% 1. The number of terms in successive lines 
% 2. The coefficient of the exponent (a in above equation)
% 3. The exponent of r (n in above equation)
% 4. The term coefficient (N in above equation)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     5
     1    1    2    2    2 
    5.29120    9.12780    2.02900     4.68910    1.75380   
   22.76900    4.95170    0.10082    -1.29310   -0.05258   
     6
     1    2    1    2    1    2  
    9.50000    9.50000    5.40000     5.40000    2.15000    2.15000   
   -5.02230   -8.88523   -1.19810   -13.31300   -1.19950    9.68670       
     6
     1    2    3    1    2    3   
    4.65000    4.65000    4.65000     1.19000    1.19000    1.19000    
    5.07300    3.47810    7.62870    -1.93300   -0.96969    1.34690
     4
     1    2    3    4
    5.30000    2.00000    1.20000     1.20000
    7.51170   -9.09940    3.67380    -1.01550
     4
     2    2    2    2
    2.49810    3.87000    1.85000     8.25000
    0.34816    1.75780    5.00720     1.09930
     6
     2    3    4    2    3    4 
    3.57000    3.57000    3.57000     1.09000    1.09000    1.09000    
   -2.42050   -0.07938   -2.38080    -1.57050    1.09630   -0.00768
     3
     2    3    4
    1.11850    1.11850    1.11850
    4.99350   -4.93250    0.96089
     4
     3    4    3    4
    2.00000    2.00000    1.03000     1.03000 
   -0.26074   -0.00357   -0.45344     0.00128  
     2
     3    4
    1.91410    1.91410
   11.15700   -4.78430
     1
     4
    1.50300
    0.70504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% The ground state occupancy. Each row is an angular momentum (s, p, d). Each
% column is the n value
% so here we designate that the ground state is 1s^2 2s^2. 
 2 2 0 0 
 0 0 0 
 0 0
 0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% To designate a ground state of 1s^2 2s^2 2p^5 we would have
% 2  2  0
% 6  0  
% 0
% assuming that we had included all orbitals up to 3d.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




