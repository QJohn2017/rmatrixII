module imbc_data
! Time-stamp: "2005-02-04 07:59:32 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use error_prt, only: alloc_error
  implicit none

  integer, allocatable, save  :: imbci(:,:)
  integer, allocatable, save  :: imbc(:,:,:)

  private
  public alloc_mbc
  public imbci, imbc

contains

  subroutine alloc_mbc (nset, jbufiv2, npcset)
    use symmetry_check, only: npset
    use filehand, only: ireadb, ireadc
    integer, intent(in)       :: nset
    integer, intent(in)       :: jbufiv2
    integer, intent(in)       :: npcset(npset)
    integer                   :: npcx, status, i

    npcx = MAXVAL(npcset(:npset))

    if (allocated(imbc)) then
       deallocate (imbc, imbci, stat=status)
       if (status /= 0) call alloc_error (status, 'alloc_mbc', 'd')
    end if
    allocate (imbci(nset,npset), imbc(npcx,nset,npset), stat=status)
    if (status /= 0) call alloc_error (status, 'alloc_mbc', 'a')
    do i = 1, npset
       print *,'Before IN ', imbci(1,i)
       call ireadb (jbufiv2, imbci(:,i), 1, nset)
       print *,'After IN ', imbci(1,i)
    end do
    do i = 1, npset
       call ireadc (jbufiv2, imbc(:,:,i), 1, npcset(i), 1, nset, &
            npcx, 0)
    end do
  end subroutine alloc_mbc
end module imbc_data
