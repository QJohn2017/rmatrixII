module debug
! Time-stamp: "03/03/03 07:17:52 cjn"
! Flags for printing diagnostic information
  implicit none

  integer, save :: bug1, bug2, bug3, bug4, bug5, bug6, bug7, bug8, bug9

  private
  public set_debug_switches
  public bug1, bug2, bug3, bug4, bug5, bug6, bug7, bug8, bug9

contains
  subroutine set_debug_switches (bugs)
    integer, intent(in)  :: bugs(9) ! input debug switches

    bug1 = bugs(1)
    bug2 = bugs(2)
    bug3 = bugs(3)
    bug4 = bugs(4)
    bug5 = bugs(5)
    bug6 = bugs(6)
    bug7 = bugs(7)
    bug8 = bugs(8)
    bug9 = bugs(9)
  end subroutine set_debug_switches

end module debug
