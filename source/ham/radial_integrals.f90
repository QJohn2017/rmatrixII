module radial_integrals
! read radial integrals
! Time-stamp: "02/01/25 07:52:24 cjn"
  use precisn, only: wp
  use io_units, only: itape1, jbuff1
  implicit none

  real(wp), allocatable, save :: rmbc(:)
  real(wp), allocatable, save :: rmcc(:)
  integer, allocatable, save  :: ibcpol(:,:)
  integer, allocatable, save  :: iccpol(:,:)

  private
  public rd_radial_integrals, filestart
  public ibcpol, iccpol, rmbc, rmcc

contains

  subroutine rd_radial_integrals (lrang1, lrang2, nbcpol, nccpol)
! read radial integral indexes and matrix elements
    use error_prt, only: alloc_error
    use filehand, only: readb
    integer, intent(in)      :: lrang1
    integer, intent(in)      :: lrang2
    integer, intent(in)      :: nbcpol
    integer, intent(in)      :: nccpol
    integer                  :: status, ival , ipos

    allocate (ibcpol(lrang1,lrang2), iccpol(lrang2,lrang2), stat=status)
    if (status /= 0) call alloc_error (status, 'rd_radial_integrals',&
         'a')
    read (itape1) ibcpol
    read (itape1) iccpol

! read radial integrals files
    ival = 0
    ipos = 1
    call filestart (ibcpol(1,1), ival, ipos, jbuff1)
    allocate (rmbc(nbcpol), rmcc(nccpol), stat=status)
    if (status /= 0) call alloc_error (status, 'stmmat', 'a')
    call readb (jbuff1, rmbc, 1, nbcpol)
    call readb (jbuff1, rmcc, 1, nccpol)
  end subroutine rd_radial_integrals

  subroutine filestart (icat, ival, ipos, jbuf)
! use filehand to position a file for data read-in
    use filehand, only: lookup, moveb, movef, startr
    integer, intent(in)    :: icat ! catologue number
    integer, intent(in)    :: ival ! reqd posn label
    integer, intent(inout) :: ipos ! current position label
    integer, intent(in)    :: jbuf ! unit number of data file
    integer                :: nsrec, nsbuf

    call lookup (icat, nsrec, nsbuf) ! cat entry -> rec, posn
    if (ival > ipos) then
       call movef (jbuf, nsrec, nsbuf) ! skip forward
    else
       call moveb (jbuf, nsrec, nsbuf) ! skip back
    end if
    call startr (jbuf, nsrec, nsbuf) ! initiate read at rec, posn
    ipos = ival
  end subroutine filestart

end module radial_integrals
