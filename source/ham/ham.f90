program ham
! Time-stamp: "2005-08-09 15:31:03 cjn"
  use precisn, only: wp
  use io_units, only: fo, jbuff1, jbuff2, jbuff4, jbuff5, jbufd, &
       jbufiv, jbuffv, jbufie, jbuffe, itape1, jbufev,           &
       jibb, jfbb, jibc, jfbc
  use error_prt, only: alloc_error
  use rm_data, only: lrgle1, lrgle2, lrgld1, lrgld2, lrgslo, lrgsup,&
       lrgp, mpol, neprnt, ndiag, lamax, nrang2, tgt_mmts, prm, farm
  use rad_data, only: lrang1, lrang2
  use set_data, only: nast, nstat, isplo, ispup
  use states, only: ll, lspn, lpty, enat
  use filehand, only: initda, open, newfil, endw
  use sc_ham, only: bindx, npset
  use channels, only: setchn, def_channels
  use l_data, only: cusetl, lrgl, npty, def_l_data
  use rdhmat_mod, only: rdhmat
  use direct_Hamiltonian, only: sthmat
  use exchange_Hamiltonian, only: stxmat
  use crlmat_mod, only: crlmat
  use stghrd_mod, only: stghrd, lrgllo, lrglup, minim, ecore
  use stmmat_mod, only: stmmat
  use dipoles, only: stdip
  use prmat_interface, only: wrtmom
  use farm_interface, only: write1, open_farm_file, close_farm_file
  use symmetry_check, only: gen_np1_symms
  implicit none
  integer, allocatable     :: ivec(:)
  character(len=7)         :: filea, stats
  integer                  :: mvely, lbufd, nbbpol, nbcpol, nccpol
  integer                  :: ndummy, iexch, more, lrgplo, ilpi, lrgpup, isp
  integer                  :: isnp, l1bc, status
  logical                  :: real_typ
  integer                  :: c0, c1, cr
  real(wp)                 :: t0, t1


  call cpu_time(t0)
  call system_clock (count=c0)
  call initda

  call stghrd
  mvely = 0
  if (mpol == 1) then
     write (fo,'(/,a,/)') 'Dipoles are to be calculated'
     mvely = 1
  end if

  if (ndiag < 0) then ! no scattering calculation
     close (jbufiv)
     close (jbuff1)
     close (jbuff4)
     close (itape1)
     close (jibb)
     close (jfbb)
     write (fo,'(a,/)') 'end of HAM target calculation'
     call cpu_time (t1)
     write (fo,'(a,f16.4,a)') 'CPU time     = ', t1 - t0, ' secs'
     call system_clock (count=c1, count_rate=cr)
     write (fo,'(a,f16.4,a)') 'Elapsed time = ', REAL(c1-c0,wp) / &
          REAL(cr,wp), ' secs'
     stop
  end if

! open cc angular files, all radial, hamiltonian and asymptotic files
  lbufd = MAX(nrang2*nrang2+lamax, 4*(ispup-isplo+1))
  call files (ndiag, lbufd)
  if (farm .or. prm /= 0) call open_farm_file

  call crlmat (mpol, mvely, nbbpol, nbcpol, nccpol)
  if (tgt_mmts) call wrtmom  ! target moments for use in PRMAT
! CHANGE - HUGO
! if (farm .or. prm /=0) call write1 (ecore)
! call bindx (minim)
! END CHANGE
  if (mpol > 0) then
     call stmmat (mpol, lamax, mvely, lrgllo, lrglup, lrgp,   &
          lrgslo, lrgsup, nbcpol, nccpol)
     print *,'exit from stmmat'
! open scratch file for eigenvectors storage
     if (mpol == 1) then
        jbufev = 47
        stats = 'unknown'
        filea = 'hvec'
        real_typ = .true.
        call open (jbufev, filea, stats, real_typ)
        call newfil (jbufev, ndummy)
     end if
  end if
! CHANGE HUGO
! write asymptotic file
  if (farm .or. prm /= 0) call write1 (ecore)

!  (not uncommented) call gen_np1_symms
  call bindx (minim)
! END CHANGE
  allocate (ivec(npset), stat=status)
  if (status /= 0) call alloc_error (status, 'ang', 'a')
  l1bc = MIN(lrang1, lrang2)

  isnp = 0
  ilpi = 0
  call def_l_data
  call def_channels
  open(unit=38,file='AbsorbBound',form='unformatted')
  open(unit=97,file='Splinewaves',form='unformatted')
  lrgl_loop: do lrgl = lrgllo, lrglup
     if (lrgl >= lrgle1 .and. lrgl <= lrgle2) then
        iexch = 1
     else
        iexch = 0
     end if
     if (lrgp < 0) then   ! both N+1 parities required
        lrgplo = 0
        lrgpup = 1
     else
        lrgplo = lrgp
        lrgpup = lrgplo
     end if
     npty_loop: do npty = lrgplo, lrgpup
        ilpi = ilpi + 1
        call cusetl
        call setchn (iexch)
        call sthmat (ilpi, lbufd)
        more = 2
        if (lrgl >= lrgld1 .and. lrgl <= lrgld2) then
           do isp = isplo, ispup, 2
              if (lrgl == lrglup .and. npty == lrgpup .and. isp &
                   == ispup .and. iexch == 0) more = 0
! read asymptotic coefficients and hamiltonian from DA file jbufd.
! calculate energy levels, surface amplitudes.
! write asymptotic file blocks for this lrgl, npty and target spin.
              call rdhmat (-isp, 0, lbufd, more, ecore, mpol, ivec)
           end do
        end if
        if (iexch == 1) then
           call stxmat (ilpi, lbufd)
           do isp = lrgsup, lrgslo, -2
              isnp = isnp + 1
              if (isnp > npset) then
                 write (fo,'(a,i5)') 'ham: set must be > ', isnp
                 exit lrgl_loop
              endif
              if (lrgl == lrglup .and. npty == lrgpup .and. &
                   isp == lrgslo)  more = 0
              call rdhmat (isp, isnp, lbufd, more, ecore, mpol, ivec)
           end do
        end if
     end do npty_loop
  end do lrgl_loop
  close(unit=38)
  close(unit=97)

! dipole matrix elements
  if (mpol == 1) then
     write (fo,'(//,a,/)') 'Dipole restructuring:'
     call endw (jbufev)
     close (jbufev)
     close (itape1)
     if (farm .or. prm /= 0) call close_farm_file
     call stdip (lrgslo, lrgsup, lrgllo, lrglup, mvely, ivec)
  end if

  close (jbuffv)
  close (jbufie)
  close (jbuffe)
  close (jbuff2)
  close (jbuff5)
  close (jibc)
  close (jfbc)
  close (jbufd, status='delete')
  if (mpol /= 1) then
     if (farm .or. prm /= 0) call close_farm_file
     close (itape1)
  end if
  close (jbufiv)
  close (jbuff1)
  close (jbuff4)
  close (jibb)
  close (jfbb)
  close(39)
  write (fo,'(/,a,/)') 'end of HAM'
  call cpu_time (t1)
  write (fo,'(a,f16.4,a)') 'CPU time     = ', t1 - t0, ' secs'
  call system_clock (count=c1, count_rate=cr)
  write (fo,'(a,f16.4,a)') 'Elapsed time = ', REAL(c1-c0,wp) / &
       REAL(cr,wp), ' secs'

  stop
contains

  subroutine files (ndiag, lbufd)
! opens all cc angular files, all radial, Hamiltonian asymptotic files.
    use stghrd_mod, only: lrglup
    use rad_data, only: lrang1, lrang2
    use set_data, only: lrang3
    use io_units, only: jbuffv, jbufie, jbuffe, jbuff1, jbuff4, &
         jbuff2, jbuff5, jbufd
    integer, intent(in)     :: ndiag
    integer, intent(in)     :: lbufd
    character(len=7)        :: file, stats
    integer                 :: lr4, int_size, real_size
    integer, save           :: lbuff2
    integer, save           :: idim2
    integer, save           :: lbuff5
    real(wp)                :: x = 1.0_wp
    logical                 :: real_typ

    lr4 = lrglup+1
    lbuff2 = lrang1*lrang1*(2*lrang1-1)
    idim2 = lr4 + lrang3 - 1
    lbuff5 = lrang1*lrang1*((lrang1+lrang2)/2)

 ! open cc angular files
    stats = 'old'
    file = 'accdr'
    real_typ = .true.
    call open (jbuffv, file, stats, real_typ)
    file = 'accei'
    real_typ = .false.
    call open (jbufie, file, stats, real_typ)
    real_typ = .true.
    file = 'accer'
    call open (jbuffe, file, stats, real_typ)

! open radial files
    if (ndiag == 0) then
       file = 'RAD1'
       real_typ = .true.
       call open (jbuff1, file, stats, real_typ)
       file = 'RAD4'
       call open (jbuff4, file, stats, real_typ)
    end if
    inquire (iolength = int_size) jbuff2   ! integer size in io-units
    open (unit=jbuff2, file='RAD2', access='direct', status='old', &
         form='unformatted', recl=int_size*lbuff2)
    open (unit=jbuff5, file='RAD5', access='direct', status='unknown', &
         form='unformatted', recl=int_size*lbuff5)

! open hamiltonian and asymptotic files
    inquire (iolength = real_size) x  ! real size in io units
    open (unit=jbufd, file='HAM1', access='direct', status='new', &
         form='unformatted', recl=real_size*lbufd)
  end subroutine files

end program ham
