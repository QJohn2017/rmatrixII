module accdi
! read accdi file
! Time-stamp: "2005-02-04 11:13:15 dlcjn1"
  use precisn, only: wp
  use io_units, only: fo, jbufiv
  use filehand, only: open, readix, ireada, ireadb, ireadc
  use debug, only: bug2
  use error_prt, only: alloc_error
  implicit none

  integer, allocatable, save   :: lamij(:,:)
  integer, allocatable, save   :: ivcont(:)
  integer, allocatable, save   :: ncset(:)

  private
  public lamij, ivcont, ncset
  public rd_accdi

contains

  subroutine rd_accdi (nset)
! open accdi file and read its index
    use symmetry_check, only: compare_tgt_symms
    integer, intent(in)        :: nset
    integer                    :: ns, ns2, status
    character (len=7)          :: stats, file
    logical                    :: real_typ
    integer, allocatable       :: lseta(:), ipseta(:), isseta(:)

    stats = 'old'
    file = 'accdi'
    real_typ = .false.
    call open (jbufiv, file, stats, real_typ)
    call readix (jbufiv)
    call ireada (jbufiv, ns)
    if (ns /= nset) then
       write (fo,'(a,i3,a,i3)') 'rd_accdi error: ns = ', ns, &
            ' nset = ', nset
       stop
    end if
    ns2 = (ns * (ns+1)) / 2 + 1
    allocate (ncset(ns), lamij(ns,ns), ivcont(ns2), &
         stat=status)
    call ireadb (jbufiv, ncset, 1, ns)
    call ireadc (jbufiv, lamij, 1, ns, 1, ns, ns, 0)
    call ireadb (jbufiv, ivcont, 1, ns2)

! Check consistency of target files by comparing HAM and ANG symmetries
    allocate (lseta(ns), ipseta(ns), isseta(ns), stat=status)
    if (status /= 0) call alloc_error (status, 'accdi', 'a')
    call ireadb (jbufiv, lseta, 1, ns)
    call ireadb (jbufiv, ipseta, 1, ns)
    call ireadb (jbufiv, isseta, 1, ns)
    call compare_tgt_symms (ns, lseta, ipseta, isseta)
    deallocate (lseta, ipseta, isseta, stat=status)
    if (status /= 0) call alloc_error (status, 'accdi', 'd')

    if (bug2 > 0) then
       write (fo,'(a,i5,a,i5)') 'accdi index: ns = ', ns
       write (fo,'(a6,9(2x,i6)/(6x,9(2x,i6)))') 'ncset', ncset
       write (fo,'(a6,9(2x,i6)/(6x,9(2x,i6)))') 'lamij', lamij
       write (fo,'(a6,9(2x,i6)/(6x,9(2x,i6)))') 'ivcont', ivcont
    end if
  end subroutine rd_accdi

end module accdi
