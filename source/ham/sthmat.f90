module direct_Hamiltonian
! Time-stamp: "2005-02-04 10:34:32 dlcjn1"
! Direct Hamiltonian Matrix
  use precisn, only: wp
  use io_units, only: fo, jbufd, jbuff1, jbuff2, jbuff4, jbufiv, &
       jbuffv, itape1
  use error_prt, only: alloc_error
  use consts, only: eps
  implicit none

  integer, allocatable, save   :: irhsgl(:)   ! ang index data
  real(wp), allocatable, save  :: vsh(:)      ! ang coefficients
  real(wp), pointer, save      :: rkcc(:)     ! two-electron integrals

  private
  public sthmat

contains

  subroutine sthmat (ilpi, lbufd)
! calculates direct terms in the hamiltonian matrix
! and stores them on disc in records nrang2 x nrang2
    use rm_data, only: nrang2, lamax
    use accdi, only: lamij, ivcont, ncset
    use channels, only: istch, nchset, iconch, nschan, nsprec, krech,&
         lenham
    use rad_data, only: lrang1, lrang2, nvary
    use states, only: nsetst, aij, enat
    use set_data, only: nset, lset, isset, isplo, ispup, &
         nstat, llpdim, lrang3, nss
    use l_data, only: lrgl, lval, nsetl, nscol, ltot, npty
    use crlmat_mod, only: crl
    use filehand, only: lookup, movef, moveb, startr, readb, ireadb
    use angular_momentum, only: dracah, rme
    integer, allocatable, save  :: ist3(:)
    integer, allocatable, save  :: iccdst(:,:)
    integer, allocatable, save  :: nccd(:,:)
    integer, intent(in)         :: ilpi
    integer, intent(in)         :: lbufd
    real(wp)                    :: hsmat(nrang2*nrang2*nss*nss)
    real(wp)                    :: cmats(nrang2*nrang2), cmat(nrang2*nrang2)
    real(wp)                    :: buf1d(nrang2*nrang2+lamax)
    real(wp)                    :: buf2d(nrang2*nrang2+lamax)
    real(wp)                    :: r1(nrang2*nrang2), rt(nrang2*nrang2)
    real(wp)                    :: zb(lrang3)
    integer                     :: ictccd(lrang1,lrang1,2*lrang1-1)
    real(wp)                    :: cf(nss*nss,lamax)
    integer                     :: lrglpi, status, iforr, ifor1, nrech
    integer                     :: i, minus, kl, lrgl2, l, l2, nra1
    integer                     :: nra12, nrec1, nbuf1, nrec, nbuf
    integer                     :: ir, n3, n, n2, np, klp, lp
    integer                     :: lp2, nra2, nrasqr, icclng, ldif
    integer                     :: nrecic, iccdif, ifora, is, ichan1
    integer                     :: lcfg, lcfg2, isp, nch, nsrec, jlo, j
    integer                     :: js, jchan1, lcfgp, lcfgp2, lamcfg
    integer                     :: jsp, kis, kjs, lamsin, lamlu, lamlo
    integer                     :: lamup, lamst, lamfin, kisjs, icont1
    integer                     :: icont2, nvshel, nreca, nbufa, ich
    integer                     :: icount, lam, lam2, iz, icfs, lams
    integer                     :: izcont, lamf, istat, jstlo, jstat
    integer                     :: kjstat, kistat, kicsup, kjcsup, kics
    integer                     :: jcslo, kjcs, ind, ics, jcs, ihs
    integer                     :: ichann, jchann, in, jn, jch
    integer                     :: ichan2, jchan2, ichcon, jchlo, ihs1
    integer                     :: jchcon, ijrec, ib, n1, nrkd, ist, jst
    real(wp)                    :: rir, rac, ai, ai2, aj, aiaj, cin, e

    lrglpi = lrgl + lrgl + npty

    if (ilpi == 1) then
       allocate (ist3(lrang2), nccd(llpdim,lrang2), &
            iccdst(llpdim,lrang2), stat=status)
       if (status /= 0) then
          write (fo,'(a,i4)') 'sthmat: allocation error = ', status
          stop
       end if
       read (itape1) ist3, nccd, iccdst ! pointer arrays for rk integrals
       iforr = 1
       ifor1 = 0
    else
       iforr = -1
       ifor1 = -1
    end if

    nrech = krech(lrglpi)
    write (jbufd, rec=nrech) (i, nschan(i), nsprec(i), &
         lenham(i), i = isplo, ispup) ! ptr block for Hamiltonian for lrgl, parity
    if (ltot == 0) return

    minus = 1
    if (MOD(lrgl+npty,2) == 1) minus = -1   ! parity factor
    lrgl2 = lrgl + lrgl

    ki: do kl = 1, ltot
       l = lval(kl)        ! l_i
       l2 = l + l
       nra1 = nvary(l+1)   ! # cont b.f.
       nra12 = (nra1 * (nra1 + 1)) / 2   ! # one-electron integrals
!      call rd_rt
       call lookup (ist3(l+1), nrec1, nbuf1) ! locate 1-elec integral
      nrec = nrec1
      nbuf = nbuf1
      if (ifor1 > 0) then
         call movef (jbuff4, nrec, nbuf)
      else if (ifor1 < 0) then
         call moveb (jbuff4, nrec, nbuf)
      else
         call startr (jbuff4, nrec, nbuf)
      end if
      call readb (jbuff4, rt, 1, nra12)
      ifor1 = 1


       ir = 0          ! r1 array -> square matrix
       n3 = - nra1
       do n = 1, nra1
          n3 = n3 + nra1
          n2 = n - nra1
          do np = 1, n
             ir = ir + 1      ! triangle ptr
             n1 = n3 + np
             n2 = n2 + nra1
             rir = rt(ir)
             r1(n1) = rir
             r1(n2) = rir
          end do
       end do

       kj: do klp = kl, ltot
          lp = lval(klp)        ! l_j
          lp2 = lp + lp
          nra2 = nvary(lp+1)    ! n'
          nrasqr = nra1 * nra2
!         call rd_rk ! rd cc RK-integrals for l,lp
      if (l == lp) then
         icclng = nra12
      else
         icclng = nrasqr
      end if
      ldif = lp - l + 1
      nrecic = l * llpdim + ldif
      read (jbuff2, rec=nrecic) &
           ictccd(1:lrang1,1:lrang1,1:lrang1+lrang1-1)
      iccdif = iccdst(ldif,l+1)
      nrkd = nccd(ldif,l+1)
      call lookup (iccdif, nrec1, nbuf1)
      nrec = nrec1
      nbuf = nbuf1
      if (nrec < 2) then
         write (fo,'(a,2i3)')  'sthmat: error attempting to read rk&
              &integrals for l, lp = ', l, lp
         stop
      end if
      if (iforr > 0) then
         call movef (jbuff1, nrec, nbuf)
      else if (iforr == 0) then
         call startr (jbuff1, nrec, nbuf)
      else
         call moveb (jbuff1, nrec, nbuf)
      end if
      allocate (rkcc(nrkd), stat=status)
      if (status /= 0) call alloc_error (status, 'rd_rk', 'a')
      call readb (jbuff1, rkcc, 1, nrkd)
      iforr = 1
      ifora = 0


          tgt_i_symms: do i = 1, nscol(kl) ! sets coupled to l, lp
             is = nsetl(i,kl)
             ichan1 = nchset(is,kl)
             lcfg = lset(is)       ! L_i
             lcfg2 = lcfg + lcfg
             isp = isset(is)      ! i spin
             nch = nschan(isp)     ! # channels in Hamiltonian
             nsrec = nsprec(isp) - 1  ! starting record

             jlo = 1
             if (l == lp) jlo = i

             tgt_j_syms: do j = jlo, nscol(klp)
                js = nsetl(j,klp)
                jchan1 = nchset(js,klp)
                lcfgp = lset(js)      ! l_j
                lcfgp2 = lcfgp + lcfgp
                lamcfg = lcfg + lcfgp  ! L_i + L_j
                jsp = isset(js)       ! j spin

! lamsin is interaction indicator: = -999 if sets is, js cannot interact
                lamsin = 0
                if (is > js) then !if is > js sets must be reversed
                   kis = js
                   kjs = is
                else
                   kis = is
                   kjs = js
                end if
                lamlu = lamij(kis,kjs)
                if (isp /= jsp) cycle tgt_j_syms !spins differ
! test if sets cannot couple for some other reason. if can, lamij
! contains limits on lambda imposed by shell cplg between cfgs in sets
                if (lamlu == -999) then
                   lamsin = -999
                end if
                if (lamsin /= -999) then
                   lamlo = lamlu / 100
                   lamup = MOD(lamlu, 100)
                   lamst = MAX(ABS(l-lp), lamlo) ! lambda limits
                   lamfin = MIN(l+lp , lamup)
                   if (lamst > lamfin) then
                      lamsin = -999
                   end if
                end if

                if (lamsin /= -999) then
!                  call rd_ang_data
      kisjs = nset * (kis-1) - (kis*(kis-1))/2 + kjs
      icont1 = ivcont(kisjs)
      icont2 = ivcont(kisjs+1) - 1
      nvshel = icont2 - icont1 + 1
      call lookup (icont1, nrec1, nbuf1)
      nreca = nrec1
      nbufa = nbuf1
      if (ifora > 0) then
         call movef (jbufiv, nreca, nbufa)
         call movef (jbuffv, nreca, nbufa)
      else if (ifora == 0) then
         call startr (jbufiv, nreca, nbufa)
         call startr (jbuffv, nreca, nbufa)
      else
         call moveb (jbufiv, nreca, nbufa)
         call moveb (jbuffv, nreca, nbufa)
      end if
      if (ALLOCATED(irhsgl)) then
         deallocate(irhsgl, stat=status)
         if (status /= 0) call alloc_error (status, 'rd_ang_data1', 'd')
      end if
      if (ALLOCATED(vsh)) then
         deallocate(vsh, stat=status)
         if (status /= 0) call alloc_error (status, 'rd_ang_data2', 'd')
      end if
      allocate (irhsgl(nvshel), vsh(nvshel), stat=status)
      if (status /= 0) call alloc_error (status, 'rd_ang_data', 'a')
      call ireadb (jbufiv, irhsgl, 1, nvshel)  ! ang indices
      call readb (jbuffv, vsh, 1, nvshel)      ! ang coeffs

                   ifora = 1
                   icount = 1
                   iz = 1
                   do lam = lamst, lamfin, 2
                      lam2 = lam + lam
                      call dracah (lcfg2, lcfgp2, l2, lp2, lam2, &
                           lrgl2, rac)
                      zb(iz) = rac * rme(l, lp, lam) * minus
                      iz = iz + 1
                   end do
                end if

!               call cf_coeffs ! form potential coeffs
! Asymptotic coeffs cf(ijchan,lambda) for channels in sets is,js
      cf = 0.0_wp
      icfs = 0
      if (lamlu > 0 .and. lamsin == 0) then ! limits on lambda
         if (lamst == 0) then
            lams = 2
            izcont = 1
         else
            lams = lamst
            izcont = 0
         end if
         lamf = MIN(lamax, lamfin)
         ii_states: do ist = 1, nstat(is)
            istat = nsetst(is,ist)
            if (ichan1 == jchan1) then
               jstlo = ist
            else
               jstlo = 1
            end if
            jj_states: do jst = jstlo, nstat(js)
               jstat = nsetst(js,jst)
               if (jstat < istat) then ! interchange state #s
                  kjstat = istat
                  kistat = jstat
               else
                  kjstat = jstat
                  kistat = istat
               end if

               iz = izcont
               icfs = icfs + 1
               do lam = lams, lamf, 2
                  iz = iz + 1
                  cf(icfs,lam) = 2 * crl(kistat,kjstat,lam) * zb(iz)
               end do
            end do jj_states
         end do ii_states
      end if

                hsmat = 0.0_wp !terms from pairing of cfgs, sets kis,kjs

                if (lamsin == 0) then ! set upper limits of cfg loops
                   kicsup = ncset(kis)
                   kjcsup = ncset(kjs)
                else                  ! if sets cannot interact
                   kicsup = 1  ! loop once to set up channel positions
                   kjcsup = 1
                end if

                i_ccsfs: do kics = 1, kicsup ! i coupled csfs
                   if (kis == kjs) then      ! set lower limit
                      jcslo = kics
                   else
                      jcslo = 1
                   end if
                   j_ccsfs: do kjcs = jcslo, kjcsup ! cfgs in set kjs
                      if (lamsin == 0) then   ! interaction possible
                         if (irhsgl(icount) < 0) then
                            call cmatii (l, lp, lamst, lamfin, lamcfg, &
                                 zb, icount, ictccd, icclng, iccdif,   &
                                 cmat)
                         else
                            call cmatij (l, lp, lamst, lamfin, lamcfg, &
                                 zb, icount, ictccd, icclng, iccdif,   &
                                 cmat, ind)
                            if (ind == 0) cycle j_ccsfs !no coupling
                         end if
                         if (is > js) then ! sets have been interchanged
                            ics = kjcs
                            jcs = kics
                         else
                            ics = kics
                            jcs = kjcs
                         end if
                      end if

! add cmat, multiplied by aij into relevant channel posns in dmat
                      ihs = 0
                      ichann = ichan1
                      i_states: do ist = 1, nstat(is) ! set is tgts
                         if (lamsin == 0) then
                            istat = nsetst(is,ist)   ! state #
                            ai = aij(istat,ics)    ! mixing coeffs
! when sets is, js are the same a pair of cfgs will contribute twice
! to compensate for the saving made as a result of symmetry
                            if (is == js .and. ics /= jcs) &
                                 ai2 = aij(istat,jcs)
                         end if
                         if (ichan1 == jchan1) then ! modify loop limits
                            jstlo = ist
                            jchann = ichann
                         else
                            jstlo = 1
                            jchann = jchan1
                         end if

                         j_states: do jst = jstlo, nstat(js)!set js tgts
                            if (lamsin == -999) then ! no interaction
                               jchann = jchann + 1
                               cycle j_states
                            end if
                            jstat = nsetst(js,jst)
                            aj = aij(jstat,jcs)
                            aiaj = ai * aj
                            if (is == js .and. ics /= jcs) &
                                 aiaj = aiaj + ai2 * aij(jstat,ics)
                            if (ABS(aiaj) <= eps) then
                               ihs = ihs + nrasqr
                               jchann = jchann + 1
                               cycle j_states
                            end if

                            if (l == lp) then ! form square matrix cmats
                               in = 0
                               n3  =- nra1
                               do n = 1, nra1
                                  n3 = n3 + nra1
                                  n2 = n - nra1
                                  do np = 1, n
                                     in = in + 1
                                     n1 = n3 + np
                                     n2 = n2 + nra1
                                     cin = cmat(in)
                                     cmats(n1) = cin
                                     cmats(n2) = cin
                                  end do
                               end do
                            else if (iconch(jchann,isp) < &
                                 iconch(ichann,isp)) then
                               in = 0
! FUDGE BY HUGO
                               if (nra1 >= nra1) then
!                              if (nra1 >= nra2) then
                                  do n = 1, nra1
                                     do np = 1, nra2
                                        in = in + 1
                                        cin = cmat(in)
                                        jn = (np - 1) * nra1 + n
                                        cmats(jn) = cin
                                     end do
                                  end do
                               else
                                  do n = 1, nra2
                                     do np = 1, nra1
                                        in = in + 1
                                        cin = cmat(in)
                                        jn = (np - 1) * nra2 + n
                                        cmats(jn) = cin
                                     end do
                                  end do
                               end if
                            else
                               cmats(1:icclng) = cmat(1:icclng)
                            end if

                            do n = 1, nrasqr
                               hsmat(n+ihs) = hsmat(n+ihs) + aiaj * &
                                    cmats(n)
                            end do
                            ihs = ihs + nrasqr
                            jchann = jchann + 1
                         end do j_states
                         ichann = ichann + 1
                      end do i_states

                   end do j_ccsfs
                end do i_ccsfs

                if (lamsin == 0) then ! test vsh completely used
                   if (icount /= nvshel + 1) then
                      write (fo,'(a,2i4)') 'sthmat: error using vsh &
                           &file for sets ', kis, kjs
                      stop
                   end if
                end if

! transfer hsmat to a buffer, matrix by matrix
                ichan2 = ichann - 1
                jchan2 = jchann - 1
                ihs = 0
                icfs = 0
                nbuf = 1 ! set buffer # for double buffering
                i_chl: do ich = ichan1, ichan2
                   ichcon = iconch(ich,isp) ! channel # -> RM convention
                   if (ichan1 == jchan1) then
                      jchlo = ich
                   else
                      jchlo = jchan1
                   end if
                   j_chl: do jch = jchlo, jchan2
                      if (jch == ich) then !add 1-elec integral, energy
                         ihs1 = ihs
                         do ir = 1, nrasqr
                            ihs1 = ihs1 + 1
                            hsmat(ihs1) = hsmat(ihs1) + r1(ir)
                         end do
                         ihs1 = ihs + 1
                         ist = istch(ich)
                         e = enat(ist)
                         do ir = 1, nra1
                            hsmat(ihs1) = hsmat(ihs1) + e
                            ihs1 = ihs1 + nra1 + 1
                         end do
                      end if

! calculate the record # so matrices stored in sequence in RM convention
                      jchcon = iconch(jch,isp)
                      if (jchcon < ichcon) then
                         ijrec = nsrec + nch * (jchcon-1) - &
                              (jchcon*(jchcon-1))/2 + ichcon
                      else
                         ijrec = nsrec + nch * (ichcon-1) - &
                              (ichcon*(ichcon-1))/2 + jchcon
                      end if

                      if (nbuf == 1) then  ! fill first buffer
                         icfs = icfs + 1
                         buf1d(:lamax) = cf(icfs,:lamax)
                         do ib = 1+lamax, nrasqr+lamax
                            ihs = ihs + 1
                            buf1d(ib) = hsmat(ihs)
                         end do
                         write (jbufd,rec=ijrec) buf1d(1:lbufd)
                         nbuf = 2
                      else       ! fill second buffer
                         icfs = icfs + 1
                         buf2d(1:lamax) = cf(icfs,1:lamax)
                         do ib = 1+lamax, nrasqr+lamax
                            ihs = ihs + 1
                            buf2d(ib) = hsmat(ihs)
                         end do
                         write(jbufd,rec=ijrec) buf2d(1:lbufd)
                         nbuf = 1
                      end if

                   end do j_chl
                end do i_chl
             end do tgt_j_syms
          end do tgt_i_symms
          deallocate (rkcc, stat=status)
          if (status /= 0) call alloc_error (status, 'sthmat', 'd')
       end do kj
    end do ki
  contains

    subroutine cf_coeffs
! Asymptotic coeffs cf(ijchan,lambda) for channels in sets is,js
      cf = 0.0_wp
      icfs = 0
      if (lamlu > 0 .and. lamsin == 0) then ! limits on lambda
         if (lamst == 0) then
            lams = 2
            izcont = 1
         else
            lams = lamst
            izcont = 0
         end if
         lamf = MIN(lamax, lamfin)
         i_states: do ist = 1, nstat(is)
            istat = nsetst(is,ist)
            if (ichan1 == jchan1) then
               jstlo = ist
            else
               jstlo = 1
            end if
            j_states: do jst = jstlo, nstat(js)
               jstat = nsetst(js,jst)
               if (jstat < istat) then ! interchange state #s
                  kjstat = istat
                  kistat = jstat
               else
                  kjstat = jstat
                  kistat = istat
               end if

               iz = izcont
               icfs = icfs + 1
               do lam = lams, lamf, 2
                  iz = iz + 1
                  cf(icfs,lam) = 2 * crl(kistat,kjstat,lam) * zb(iz)
               end do
            end do j_states
         end do i_states
      end if
    end subroutine cf_coeffs

    subroutine rd_rt
      call lookup (ist3(l+1), nrec1, nbuf1) ! locate 1-elec integral
      nrec = nrec1
      nbuf = nbuf1
      if (ifor1 > 0) then
         call movef (jbuff4, nrec, nbuf)
      else if (ifor1 < 0) then
         call moveb (jbuff4, nrec, nbuf)
      else
         call startr (jbuff4, nrec, nbuf)
      end if
      call readb (jbuff4, rt, 1, nra12)
      ifor1 = 1
    end subroutine rd_rt

    subroutine rd_rk
      if (l == lp) then
         icclng = nra12
      else
         icclng = nrasqr
      end if
      ldif = lp - l + 1
      nrecic = l * llpdim + ldif
      read (jbuff2, rec=nrecic) &
           ictccd(1:lrang1,1:lrang1,1:lrang1+lrang1-1)
      iccdif = iccdst(ldif,l+1)
      nrkd = nccd(ldif,l+1)
      call lookup (iccdif, nrec1, nbuf1)
      nrec = nrec1
      nbuf = nbuf1
      if (nrec < 2) then
         write (fo,'(a,2i3)')  'sthmat: error attempting to read rk&
              &integrals for l, lp = ', l, lp
         stop
      end if
      if (iforr > 0) then
         call movef (jbuff1, nrec, nbuf)
      else if (iforr == 0) then
         call startr (jbuff1, nrec, nbuf)
      else
         call moveb (jbuff1, nrec, nbuf)
      end if
      allocate (rkcc(nrkd), stat=status)
      if (status /= 0) call alloc_error (status, 'rd_rk', 'a')
      call readb (jbuff1, rkcc, 1, nrkd)
      iforr = 1
      ifora = 0
    end subroutine rd_rk

    subroutine rd_ang_data
      kisjs = nset * (kis-1) - (kis*(kis-1))/2 + kjs
      icont1 = ivcont(kisjs)
      icont2 = ivcont(kisjs+1) - 1
      nvshel = icont2 - icont1 + 1
      call lookup (icont1, nrec1, nbuf1)
      nreca = nrec1
      nbufa = nbuf1
      if (ifora > 0) then
         call movef (jbufiv, nreca, nbufa)
         call movef (jbuffv, nreca, nbufa)
      else if (ifora == 0) then
         call startr (jbufiv, nreca, nbufa)
         call startr (jbuffv, nreca, nbufa)
      else
         call moveb (jbufiv, nreca, nbufa)
         call moveb (jbuffv, nreca, nbufa)
      end if
      if (ALLOCATED(irhsgl)) then
         deallocate(irhsgl, stat=status)
         if (status /= 0) call alloc_error (status, 'rd_ang_data1', 'd')
      end if
      if (ALLOCATED(vsh)) then
         deallocate(vsh, stat=status)
         if (status /= 0) call alloc_error (status, 'rd_ang_data2', 'd')
      end if
      allocate (irhsgl(nvshel), vsh(nvshel), stat=status)
      if (status /= 0) call alloc_error (status, 'rd_ang_data', 'a')
      call ireadb (jbufiv, irhsgl, 1, nvshel)  ! ang indices
      call readb (jbuffv, vsh, 1, nvshel)      ! ang coeffs
    end subroutine rd_ang_data

  end subroutine sthmat

  subroutine cmatii (l, lp, lamst, lamfin, lamcfg, zb, icount, ictccd, &
       icclng, iccdif, cmat)
! contribution to Hamiltonian from interaction of a pair of similar cfgs
    use comp_data, only: njcomp, ljcomp
    use rad_data, only: nvary, lrang1
    use rm_data, only: nrang2
    use stghrd_mod, only: lrglup
    use set_data, only: lrang3
    integer, intent(in)       :: l, lp
    integer, intent(in)       :: lamst, lamfin ! lambda = lamst->lamfin
    real(wp), intent(in)      :: zb(:) ! z-coeffs for current l,lp
    integer, intent(inout)    :: icount ! counter to locate vsh for pair
    integer, intent(in)       :: lamcfg ! max lambda for vsh to exist
    real(wp), intent(out)     :: cmat(nrang2*nrang2)   ! Hamiltonian
    integer, intent(in)       :: icclng, iccdif
    integer, intent(in)       :: ictccd(lrang1,lrang1,2*lrang1-1)
    real(wp)                  :: rm(nrang2*nrang2,lrang3+1)
    real(wp)                  :: vsz(0:lrang3)
    integer                   :: nra1, nra2, kz, nsh, ish, nshsgl, lam1
    integer                   :: isig, lsig, nsig, lamlo, lam2, lamup
    integer                   :: klamlo, klamup, lam, kc, klam, i
    integer                   :: nshhun

    cmat(1:icclng) = 0.0_wp  ! accumulate angular terms

    nra1 = nvary(l+1)       ! # n values
    nra2 = nvary(lp+1)      ! # n' values

    kz = 1 - lamst / 2
    nsh = ABS(irhsgl(icount)) / 10000
    nshhun = 100 * nsh
    ish_loop: do ish = 1, nsh
       nshsgl = ABS(irhsgl(icount))
       lam1 = MOD(nshsgl,100)
       if (lam1 == 99) then
          icount = icount + 1
          cycle
       end if
       isig = nshsgl / 100 - nshhun
       lsig = ljcomp(isig)
       nsig = njcomp(isig)
       lamlo = MAX(lam1, lamst)
       lam2 = MIN(lsig+lsig, lamcfg)
       lamup = MIN(lam2, lamfin)
       klamlo = lamlo / 2
       klamup = lamup / 2
       lam = lamlo
       kc = icount - lam1 / 2
       lambda: do klam = klamlo, klamup
          call locccd (ictccd, icclng, iccdif, lsig+1, lsig+1, &
               nsig, nsig, lam+1, rm(:,klam+1))
          vsz(klam) = vsh(kc+klam) * zb(kz+klam)
          lam = lam + 2
       end do lambda
       do klam = klamlo, klamup
          do i = 1, icclng
             cmat(i) = cmat(i) + rm(i,klam+1) * vsz(klam)
          end do
       end do
       icount = kc + lam2 / 2 + 1
    end do ish_loop
  end subroutine cmatii

  subroutine cmatij (l, lp, lamst, lamfin, lamcfg, zb, icount, &
       ictccd, icclng, iccdif, cmat, ind)
! contribution to Hamiltonian from interaction of pair nonidentical cfgs
    use rad_data, only: nvary, lrang1
    use rm_data, only: nrang2
    use comp_data, only: njcomp, ljcomp
    integer, intent(in)       :: l, lp
    integer, intent(in)       :: lamst, lamfin ! lambda = lamst->lamfin
    real(wp), intent(in)      :: zb(:) ! z-coeffs for current l,lp
    integer, intent(inout)    :: icount ! counter to locate vsh for pair
    integer, intent(in)       :: lamcfg ! max lambda for vsh to exist
    real(wp), intent(out)     :: cmat(nrang2*nrang2) ! Hamiltonian
    integer, intent(in)       :: icclng, iccdif
    integer, intent(in)       :: ictccd(lrang1,lrang1,2*lrang1-1)
    integer, intent(out)      :: ind ! 1 if ic,jc coupled, 0 if not
    real(wp)                  :: rm(nrang2*nrang2)
    integer                   :: irsl, lamlo, irs, irho, isig, lrho
    integer                   :: lsig, lamup, nrho, nsig, nra1, nra2
    integer                   :: izcont, i, lam
    real(wp)                  :: vs, vz

    ind = 0
    irsl = irhsgl(icount)
    if (irsl == 9999) then
       icount = icount + 1
       return
    end if

! find the interacting shells and the first lambda from irsl
    lamlo = MOD(irsl, 100)
    irs = irsl / 100
    irho = irs / 100
    isig = MOD(irs, 100)
    lrho = ljcomp(irho)
    lsig = ljcomp(isig)
    lamup = MIN(lrho+lsig, lamcfg)

! test whether limits overlap limits on lambda for Z-coefficients
    if (lamlo > lamfin .or. lamup < lamst) then
       icount = icount + 1 + (lamup - lamlo) / 2
       return
    end if

    nrho = njcomp(irho)
    nsig = njcomp(isig)
    cmat(1:icclng) = 0.0_wp

    nra1 = nvary(l+1)      ! # n values
    nra2 = nvary(lp+1)     ! # n' values
    ind = 1

    lam_loop: do lam = lamlo, lamup, 2
       if (lam < lamst .or. lam > lamfin) then ! move along vshell file
          icount = icount + 1
          cycle
       end if
       vs = vsh(icount)
       icount = icount + 1
       if (ABS(vs) <= eps) cycle lam_loop
       izcont = 1 + (lam - lamst) / 2 ! move zbuff to loc z for lambda
       vz = zb(izcont) * vs
       call locccd (ictccd, icclng, iccdif, lrho+1, lsig+1, nrho,&
            nsig, lam+1, rm)   ! locate the radial integral
! rm(i) contains the nra1 x nra2 matrix of radial integrals.
! when l=lp only the lower half of the matrix is stored
! rm(i) is now multiplied by the factor vz and stored in cmat(n,np)
! when l=lp it is stored in the upper half of cmat

       do i = 1, icclng
          cmat(i) = cmat(i) + rm(i) * vz
       end do
    end do lam_loop
  end subroutine cmatij

  subroutine locccd (ictccd, icclng, iccdif, l1, l1p, n1, n1p, lam,&
       rm)
! locate direct continuum continuum rk integrals
    use rad_data, only: maxnc, maxnhf
    integer, intent(in)      :: icclng, iccdif
    integer, intent(in)      :: l1, l1p
    integer, intent(in)      :: n1, n1p
    integer, intent(in)      :: lam
    integer, intent(in)      :: ictccd(:,:,:)
    real(wp), intent(out)    :: rm(:)    ! c-c rk-integrals
    integer                  :: iccd, nn1p, nij, maxnc1, maxncp
    integer                  :: ni, nj, i

! if l /= lp  rm length = nrang2 * nrang2; if l == lp  lower triangle
! integrals are stored in order:
! l1 <= l1p;  if l1 == l1p  then  n1 >= n1p
    maxnc1 = maxnc(l1)
    maxncp = maxnc(l1p)
    ni = n1 - maxnc1
    nj = n1p - maxncp

    if (l1 < l1p) then
       iccd = ictccd(l1,l1p,lam)
       nn1p = maxnhf(l1p) - maxncp
       nij = (ni - 1) * nn1p + nj
    else if (l1 > l1p) then
       iccd = ictccd(l1p,l1,lam)
       nn1p = maxnhf(l1) - maxnc1
       nij = (nj - 1) * nn1p + ni
    else  ! l1 == l1p
       iccd = ictccd(l1,l1p,lam)
       if (ni >= nj) then
          nij = (ni * (ni - 1)) / 2 + nj
       else
          nij = (nj * (nj - 1)) / 2 + ni
       end if
    end if

    if (iccd == -999) then
       write (fo,'(a)') 'locccd: no pointer found in ictccd for'
       write (fo,'(3(a,i4))') 'l1 = ', l1, ' l1p = ', l1p, ' lam = ',&
            lam
    end if

    nij = (nij - 1) * icclng + iccd - iccdif
    do i = 1, icclng
       nij = nij + 1
       rm(i) = rkcc(nij)
    end do
  end subroutine locccd

end module direct_Hamiltonian
