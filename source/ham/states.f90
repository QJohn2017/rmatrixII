module states
! maintain library of target information
! Time-stamp: "2005-08-09 14:39:22 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use error_prt, only: alloc_error
  implicit none

  integer, allocatable, save :: ll(:), lspn(:), lpty(:) ! tgt q #s
  real(wp), allocatable, save :: enat(:) ! ordered tgt eigenvalues
  real(wp), allocatable, save :: aij(:,:) ! tgt eigenvectors
  integer, allocatable, save  :: kspin(:) ! 1 if tgt spins occurs else 0
  integer, allocatable, save  :: nsetst(:,:) ! tgt # for set, state
  integer, allocatable, save  :: ncset(:)  ! tgt Hamiltonian dimensions
  integer, allocatable, save  :: ncst(:)  ! ordered tgt Hamiltonian dimensions
  integer, save              :: mx_ncst ! max tgt dimension

  private
  public def_state_data
  public wrt_tgt_lib, rd_tgt_lib
  public ll, lspn, lpty, enat, aij
  public kspin, nsetst, ncset, ncst, mx_ncst

contains
  subroutine def_state_data (nset, ll1, lspn1, lpty1)
    use set_data, only: lset, isset, ipset, nast, nstat
    integer, intent(in)   :: nset    ! # tgt symmetries
    integer, pointer      :: ll1(:), lspn1(:), lpty1(:)
    integer               :: istat, mx_nstat, mx_isset, status
    integer               :: lst, lspnst, lptyst, ist, is

    call rd_ncset
    istat = SUM(nstat(1:nset))
    mx_nstat = MAXVAL(nstat(1:nset))
    mx_isset = MAXVAL(isset(1:nset))
    allocate (nsetst(nset,mx_nstat), kspin(mx_isset), ll1(istat), &
         lspn1(istat), lpty1(istat), ncst(istat), stat=status)
    if (status /= 0) call alloc_error (status, 'def_state_data', 'a')
    kspin = 0   ! records occurrence of target spins
    istat = 0
    tgt_syms: do is = 1, nset
       lst = lset(is)
       lspnst = isset(is)
       lptyst = ipset(is)
       tgt_states: do ist = 1, nstat(is)
          istat = istat + 1
          ll1(istat) = lst
          lspn1(istat) = lspnst
          lpty1(istat) = lptyst
          kspin(lspnst) = 1
          nsetst(is,ist) = istat
          ncst(istat) = ncset(is)
       end do tgt_states
    end do tgt_syms
    mx_ncst = MAXVAL(ncst(1:istat))
    allocate (ll(nast), lspn(nast), lpty(nast), enat(nast), &
         aij(nast,mx_ncst), stat=status)
    if (status /= 0) call alloc_error (status, 'def_state_data', 'a')
  end subroutine def_state_data

  subroutine rd_ncset
! read array ncset from index of atgi file
    use io_units, only: jibb
    use filehand, only: open, readix, ireada, ireadb, close
    character(len=7)         :: file, stats
    logical                  :: real_typ
    integer                  :: nsett, status
    integer, allocatable     :: idum(:)

    file = 'atgi'
    stats = 'old'
    real_typ = .false.
    call open (jibb, file, stats, real_typ)

! read pointer arrays for bound-bound angular integrals
    call readix (jibb)
    call ireada (jibb, nsett)
    allocate (ncset(nsett), idum(nsett), stat=status)
    if (status /= 0) call alloc_error (status, 'rd_ncset', 'a')
    call ireadb (jibb, idum, 1, nsett)    ! skip lset
    call ireadb (jibb, idum, 1, nsett)    ! skip pset
    call ireadb (jibb, idum, 1, nsett)    ! skip sset
    call ireadb (jibb, ncset, 1, nsett)
    call close (jibb, stats)
    deallocate (idum, stat=status)
    if (status /= 0) call alloc_error (status, 'rd_ncset', 'd')
  end subroutine rd_ncset

  subroutine wrt_tgt_lib (en, bij)
! dump target eigenvalues and eigenfunctions to disk for restart
    use io_units, only: fl
    real(wp), intent(in)   :: en(:)    ! tgt eigenvalues
    real(wp), intent(in)   :: bij(:,:) ! tgt eigenvectors
    integer       :: i, n, m, ios

    open (unit=fl, file='HTGT', form='unformatted', &
         status='unknown', iostat=ios)
    if (ios /= 0) then
       write (fo,'(a,i4)') 'Error opening file HTGT, iostat = ', ios
       stop
    end if

    n = SIZE(bij, DIM=1)
    m = SIZE(bij, DIM=2)    ! nast
    write (fl) n, m
    write (fl) en
    do i = 1, m
       write (fl) bij(:,i)
    end do
    close (unit=fl)
  end subroutine wrt_tgt_lib

  subroutine rd_tgt_lib (en, bij)
! read target eigenvalues and eigenfunctions from disk for restart
    use io_units, only: fl
    real(wp), pointer  :: en(:)  ! tgt eigenvalues
    real(wp), pointer  :: bij(:,:) ! tgt eigenvectors
    integer            :: ios, i, n, m, status

    open (unit=fl, file='HTGT', form='unformatted', &
         status='old', iostat=ios)
    if (ios /= 0) then
       write (fo,'(a,i4)') 'Error opening file HTGT, iostat = ',&
            ios
       stop
    end if

    read (fl) n, m
    allocate (en(m), bij(n,m), stat=status)
    if (status /= 0) call alloc_error (status, 'rd_tgt_lib', 'a')
    read (fl) en
    do i = 1, m
       read (fl) bij(:,i)
    end do
    close (unit=fl)
  end subroutine rd_tgt_lib

end module states
