module l_data
! Time-stamp: "2003-03-26 11:21:02 cjn"
  use io_units, only: fo
  use debug, only: bug8
  implicit none

  integer, save              :: lrgl
  integer, save              :: npty
  integer, save              :: ltot
  integer, allocatable, save :: lval(:)
  integer, allocatable, save :: nscol(:)
  integer, allocatable, save :: nsetl(:,:)

  integer, save              :: d1, d2

  private
  public def_l_data, cusetl
  public lrgl, npty, ltot, lval, nscol, nsetl

contains

  subroutine def_l_data
    use set_data, only: lrang3, nset
    integer                  :: status

    d1 = 2 * lrang3 - 1
    d2 = nset
    allocate (lval(d1), nscol(d1), nsetl(nset,d1), stat=status)
    if (status /= 0) then
       write (fo,'(a,i6)') 'def_l_data: allocation error = ', status
       stop
    end if
  end subroutine def_l_data

  subroutine cusetl
! Determine which sets couple with each cont a.m. in range allowed
! by lrgl.
    use set_data, only: nset, lset, ipset
    integer     :: ilval(d1), inscol(d1), insetl(d2,d1)
    integer     :: lmin, lmax, lcfg, is, iltot, kl, l, nl, ipi, ikl

    write (fo,'(//,2(a,i3))') 'Total angular momentum = ', lrgl, &
         ', Parity = ', npty
    if (bug8 == 1) then
       write (fo,'(a)') '      kl     l   sets coupled to l'
    end if

! find range of cont a.m. and set up ilval(kl) - possible cont a.m.
    lmin = 999
    lmax = 0
    do is = 1, nset
       lcfg = lset(is)
       lmin = MIN(lmin, ABS(lcfg-lrgl))
       lmax = MAX(lmax, lcfg)
    end do
    lmax = lmax + lrgl
    iltot = lmax - lmin + 1
    do kl = 1, iltot
       ilval(kl) = lmin + kl - 1
    end do

    kl_loop: do kl = 1, iltot  ! over this range of continuum l
       l = ilval(kl)
       nl = 0
       do is = 1, nset
          lcfg = lset(is)
          ipi = ipset(is)
          if (MOD(ipi+l+npty, 2) /= 0) cycle ! parity test
          if (lrgl > lcfg+l .or. lrgl < ABS(lcfg-l)) cycle ! triangle
          nl = nl + 1
          insetl(nl,kl) = is
       end do
       inscol(kl) = nl
    end do kl_loop

! delete cont a.m. which cannot couple with any set
    kl = 0
    do ikl = 1, iltot
       if (inscol(ikl) /= 0) then
          kl = kl + 1
          nscol(kl) = inscol(ikl)
          lval(kl) = ilval(ikl)
          do nl = 1, nscol(kl)
             nsetl(nl,kl) = insetl(nl,ikl)
          end do
       end if
    end do
    ltot = kl
    if (ltot /= 0) then
       lmin = lval(1)
       lmax = lval(ltot)

       if (bug8 == 1) then
          do kl = 1, ltot
             write (fo,'(2x,15i6)') kl, lval(kl), (nsetl(nl,kl), &
                  nl=1,nscol(kl))
          end do
       end if
       write (fo,'(a,i3,a,i3,/)') 'Continuum a.m. takes values ', lmin,&
            ' -> ', lmax
    end if
  end subroutine cusetl
end module l_data
