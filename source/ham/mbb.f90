module mbb_mod
! bound-bound parts of dipole moments
! Time-stamp: "2003-03-26 11:24:01 cjn"
  use precisn, only: wp
  use io_units, only: fo, jbuffm
  use consts, only: eps
  use debug, only: bug2
  implicit none

  integer, allocatable, save  :: irhsgl(:)
  real(wp), allocatable, save :: vsh(:)

  private
  public mbb, def_vsh
  
contains

  subroutine def_vsh (nvshel, jbufiv, jbuffv)
    use error_prt, only: alloc_error
    use filehand, only: ireadb, readb
    integer, intent(in)          :: nvshel
    integer, intent(in)          :: jbufiv
    integer, intent(in)          :: jbuffv
    integer                      :: status

    if (allocated(irhsgl)) then
       deallocate (irhsgl, vsh, stat=status)
       if (status /= 0) call alloc_error (status, 'def_vsh', 'd')
    end if
    allocate (irhsgl(nvshel), vsh(nvshel), stat=status)
    if (status /= 0) call alloc_error (status, 'def_vsh', 'a')
    call ireadb (jbufiv, irhsgl, 1, nvshel)
    call readb (jbuffv, vsh, 1, nvshel)
  end subroutine def_vsh

  subroutine mbb (inobb, inch, icnp1, icnp2, irev, lamup,  &
       lamind, mpol, nvshel, inbi, jnbi, nrai, inb, sign4, icount,&
       lmx)
    use comp_data, only: njcomp, ljcomp
    use rm_data, only: lamax
    use filehand, only: writc
    use crlmat_mod, only: locmbb, rkcc
    use amul, only: amul1, amul2, icpt, mvel
    integer, intent(in)     :: inobb
    integer, intent(in)     :: inch
    integer, intent(in)     :: icnp1, icnp2
    integer, intent(in)     :: irev
    integer, intent(in)     :: lamup
    integer, intent(in)     :: lamind
    integer, intent(in)     :: mpol
    integer, intent(in)     :: nvshel
    integer, intent(in)     :: inbi, jnbi, nrai, inb
    integer, intent(inout)  :: icount
    integer, intent(in)     :: lmx
    real(wp), intent(in)    :: sign4
    integer                 :: istart, ic1, ic2, icsnp, jcsnp, iwpos
    integer                 :: nsh, nshhun, ish, nshsgl, lamst
    integer                 :: klamst, isig, lsig, nsig, lam2, klam2
    integer                 :: kc, klamf, klam, irsl, lam1, j, l
    integer                 :: irho, lrho, nrho, lam, isize, jnb, k, irs
    real(wp)                :: vs, rbbl, rbbv
    real(wp)                :: vsa(lamax)
! hugo
    integer                 :: idim2
    real(wp)                :: signhugo

!   print *,' ENTRY ',amul1(1,1),amul2(1,1)
    if (irev == 1) then
      idim2=icnp2
     else
      idim2=icnp1
    endif
    if (inobb == 0) then
       istart = icpt(inch+1) ! set position for writing
       ic1_L: do ic1 = 1, icnp1 ! init (N+1)-cfgs
          ic2_L: do ic2 = 1, icnp2
             if (irev == 1) then
                icsnp = ic1
                jcsnp = ic2
                signhugo = 1._wp
             else
                icsnp = ic2
                jcsnp = ic1
                signhugo=-1._wp
             end if
             iwpos = istart + icsnp - 1

! cfgs having the same shell structure are treated first
             if (irhsgl(icount) < 0) then
                nsh = ABS(irhsgl(icount)) / 10000
                nshhun = 100 * nsh
                ish_L: do ish = 1, nsh
                   nshsgl = ABS(irhsgl(icount))
                   lam1 = MOD(nshsgl, 100)
                   if (lam1 == 99) then
                      icount = icount + 1
                      cycle ish_L
                   end if
                   lamst = lam1
                   klamst = (lam1 + 1) / 2
                   if (klamst == 0) then
                      lamst = lam1 + 2
                      klamst = 1
                      icount = icount + 1
                   end if
                   isig = nshsgl / 100 - nshhun
                   lsig = ljcomp(isig)
                   nsig = njcomp(isig)
                   lam2 = MIN(lsig+lsig, lamup)
                   klam2 = (lam2 + 1) / 2
                   kc = icount - klamst
                   klamf = MIN(lamind, klam2)
                   lam = lamst
                   klam_L: do klam = klamst, klamf
                      if (lam /= mpol) then
                         lam = lam + 2
                         cycle klam_L
                      end if
                      vsa(klam) = vsh(kc+klam)
                      call locmbb (lsig+1, lsig+1, nsig, nsig, lam, &
                           rkcc, rbbl, rbbv)
                      lam = lam + 2
!                     print *,'HUGO B ',rbbl,rbbv
                      amul1(iwpos,jcsnp) = sign4 * vsa(klam) * rbbl
! add a minus sign not
                      if (mvel == 2) amul2(iwpos,jcsnp) = sign4 * &
                           vsa(klam) * rbbv * signhugo
                   end do klam_L
                   icount = kc + klam2 + 1
                end do ish_L

             else ! cfgs having different shell structure

                irsl = irhsgl(icount)
                if (irsl == 9999) then
                   icount = icount + 1
                   cycle ic2_L
                end if
! find the interacting shells and the first lambda from irsl
                lam1 = MOD(irsl, 100)
                irs = irsl / 100
                irho = irs / 100
                isig = MOD(irs, 100)
                lrho = ljcomp(irho)
                lsig = ljcomp(isig)
                lam2 = MIN(lrho+lsig, lamup)
                nrho = njcomp(irho)
                nsig = njcomp(isig)
                lam1_L: do lam = lam1, lam2, 2
                   if (lam == 0 .or. lam > lmx .or. lam /= mpol) then
                      icount = icount + 1
                      cycle lam1_L
                   end if
                   klam = (lam + 1) / 2
                   vs = vsh(icount)
                   icount = icount + 1
                   if (ABS(vs) <= eps) then
                      amul1(iwpos,jcsnp) = 0.0_wp
                      amul2(iwpos,jcsnp) = 0.0_wp
                      cycle lam1_L
                   end if
! locate the radial integral
                   call locmbb (lrho+1, lsig+1, nrho, nsig, lam, rkcc, &
                        rbbl, rbbv)
!                  print *,' HUGO ',rbbl,rbbv
                   amul1(iwpos,jcsnp) = sign4 * vs * rbbl
! add a minus sign not
                   if (mvel == 2) amul2(iwpos,jcsnp) = sign4 * vs * rbbv * &
                        signhugo
                end do lam1_L
             end if
          end do ic2_L
       end do ic1_L
    end if   ! bound-bound part completed

! test vsh array has been completely used for this set combination:
    if (icount /= nvshel + 1) then 
       write (fo,'(a,2i4)') 'mbb: error using vsh for sets ', inbi, jnbi
       stop
    end if

! write contributions to disk
    isize = SIZE(amul1, DIM=1)
    jnb = SIZE(amul1, DIM=2)
    print *,'CKSIZE ',isize,jnb,idim2
    call writc (jbuffm, amul1, 1, isize, 1, idim2, isize, 0)
    if (mvel == 2) call writc (jbuffm, amul2, 1, isize, 1, idim2, isize,&
         0)
    print *,amul1(1,1),amul2(1,1)

    if (bug2 > 0) then
       write (fo,'(a)') 'continuum-bound length part'
       if (nrai /= 0) then
          do l = 1, jnb
             write (fo,'(2x,8f15.7)') (amul1(j,l), j=icpt(1), &
                  icpt(inch+1)-1)
          end do
          write (fo,'(a/)')
       end if
       write (fo,'(a)') 'bound-bound length part'
       if (inb /= 0) then
          do k = 1, jnb
             write (fo,'(2x,8f15.7)') (amul1(j,k), j=icpt(inch+1), &
                  icpt(inch+1)+inb-1)
          end do
       end if
       write (fo,'(/)')
                
       write (fo,'(a)') 'continuum-bound velocity part'
       if (nrai /= 0) then
          do l = 1, jnb
             write (fo,'(2x,8f15.7)') (amul2(j,l), j=icpt(1), &
                  icpt(inch+1)-1)
          end do
          write (fo,'(a/)')
       end if
       write (fo,'(a)') 'bound-bound velocity part'
       if (inb /= 0) then
          do k = 1, jnb
             write (fo,'(2x,8f15.7)') (amul2(j,k), j=icpt(inch+1), &
                  icpt(inch+1)+inb-1)
          end do
       end if
       write (fo,'(/)')
    end if
  end subroutine mbb

end module mbb_mod
