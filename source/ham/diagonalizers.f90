module diagonalizers
! Time-stamp: "03/06/25 15:23:17 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use error_prt, only: alloc_error
  implicit none

  private
  public select_diagonalizer, diag, diag_kill

  real(wp), parameter                 :: epsi = 1.0e-12_wp
  real(wp), save                      :: sigma
  real(wp), allocatable, target, save :: z(:,:)   ! eigenvectors
  real(wp), allocatable, target, save :: y(:)     ! eigenvectors

  integer, save                       :: method = 4

contains

  subroutine select_diagonalizer (meth)
! define the diagonalization method to be used
    integer, intent(in)       :: meth
    character(len=5)          :: name(4) = (/'dstgr', 'stein', &
         'house', 'dspev'/)
    method = meth
    if (method < 1 .or. meth > 4) then
       write (fo,'(a,i6)') 'select_diagonalizer: invalid method = ', meth
       stop
    end if
    write (fo,'(/,a,/)') name(meth) // ' diagonalization'
  end subroutine select_diagonalizer

  subroutine diag (n, a, eig, x, no)
    integer, intent(in)         :: n         ! degree of matrix
    real(wp), intent(inout)     :: a(:)      ! upper triangle
    real(wp), intent(out)       :: eig(:)    ! eigenvalues
    real(wp), pointer           :: x(:)      ! eigenvector pointer
    integer, intent(in)         :: no        ! eigenvector index

    select case (method)
    case (1)     ! most reliable
       call diag_dstgr (n, a, eig, x, no)
    case (2)     ! quickest
       call diag_stein (n, a, eig, x, no)
    case (3)     ! traditional
       call diag_hsldr (n, a, eig, x, no)
    case (4)     ! standard Lapack
       call diag_dspev (n, a, eig, x, no)
    case default
       write (fo,'(a)') 'invalid diagonalization method selection'
       write (fo,'(a)') ' method = ', method
       stop
    end select
  end subroutine diag

  subroutine diag_dstgr (n, a, eig, x, no)
! diagonalizes a packed upper triangle n*n symmetric matrix
! on first call returns eigenvalues and the first eigenvector.
! on subsequent call one more of the eigenvectors is returned
    integer, intent(in)         :: n         ! degree of matrix
    real(wp), intent(inout)     :: a(:)      ! upper triangle
    real(wp), intent(out)       :: eig(:)    ! eigenvalues
    real(wp), pointer           :: x(:)      ! eigenector pointer
    integer, intent(in)         :: no        ! eigenvector index
    real(wp), allocatable, save :: work(:), p(:), r(:), tau(:)
    integer, allocatable, save  :: iwork(:), isuppz(:)
    integer                     :: info, status, il, iu, nfound, lwork
    integer                     :: liwork, iscale
    real(wp)                    :: vl, vu, abstll, sfmin, t

    first_entry: if (no == 1) then
       if (n == 1) then   ! deal with special case n=1
          allocate (z(n,n), stat=status)
          if (status /= 0) then
             write (fo,'(a,i6)') 'diag: allocation error = ', status
             stop
          end if
          z(1,1) = 1.0_wp
          x => z(:,1)
          eig(1) = a(1)
          return
       end if

       lwork = 18 * n
       liwork = 10 * n
       allocate (p(n), r(n), work(lwork), iwork(liwork), z(n,n), &
            isuppz(2*n), tau(n), stat=status)
       if (status /= 0) then
          write (fo,'(a,i6)') 'diag: allocation error = ', status
          stop
       end if

       select case (n)
       case (3:)     ! tri-diagonalization:
          call scale_matrix (n, a, abstll, iscale)
! reduces real symmetric matrix A stored in packed form to
! symmetric tridiagonal form: Q**T * A * Q = T.
! if UPLO = 'L', AP(i + (j-1)*(2*n-j)/2) = A(i,j) for j<=i<=n.
          call dsptrd ('L', n, a, r, p, tau, info)
          if (info /= 0) then
             write (fo,'(a,i6)') 'dsptrd error, info = ', info
             stop
          end if
       case (2)
          r(1) = a(1)
          r(2) = a(3)
          p(1) = a(2)
       end select

! Eigenvalues, eigenvectors of real symmetric tridiagonal matrix T.
! Eigenvalues are computed by the dqds algorithm, while orthogonal
! eigenvectors computed from various ``good'' L D L^T representations
! (aka Relatively Robust Representations).
! Gram-Schmidt orthogonalization is avoided as far as possible.
       sfmin = TINY(t) * (1.0_wp + EPSILON(t))  ! inverse wont overflow
       call dstegr ('V', 'A', n, r, p, VL, VU, IL, IU, sfmin, nfound, &
            eig, z, n, isuppz, work, lwork, iwork, liwork, info)
       if (info /= 0) then
          write (fo,'(a,i6)') 'dstegr error: info = ', info
          stop
       end if
       if (nfound /= n) then
          write (fo,'(a,i6)') 'dstegr: # eigenvalues found = ', nfound
          stop
       end if
       if (iscale == 1) call dscal (n, 1.0_wp / sigma, eig, 1)
! Apply orthogonal matrix used in reduction to tridiagonal
! form to eigenvectors
       call dopmtr ('L', 'L', 'N', n, n, a, tau, z, n, r, info)
       if (info /= 0) then
          write (fo,'(a,i6)') 'dopmtr error ', info
          stop
       end if
       deallocate (p, r, work, iwork, isuppz, tau, stat=status)
       if (status /= 0) then
          write (fo,'(a,i6)') 'diag: deallocation error = ', status
          stop
       end if
    end if first_entry

    x => z(:,no)
  end subroutine diag_dstgr

  subroutine diag_kill
! deallocated memory used in diagonalization
    integer            :: status

    select case (method)
    case (1:2,4)
       deallocate (z, stat=status)
       if (status /= 0) then
          write (fo,'(a,i6)') 'diag_kill: deallocation error = ', status
          stop
       end if
    case (3)
       deallocate (y, stat=status)
       if (status /= 0) then
          write (fo,'(a,i6)') 'diag_kill: deallocation error = ', status
          stop
       end if
    case default
       write (fo,'(a)') 'diag_kill: method selection error'
       stop
    end select
  end subroutine diag_kill

  subroutine diag_hsldr (n, a, eig, x, no)
! accepts the upper triangle of an n*n symmetric matrix and on the
! first call determines all eigenvalues and first eigenvector.
! on each further call one of remaining eigenvectors is calculated.
    integer, intent(in)         :: n    ! degree of matrix
    real(wp), intent(inout)     :: a(:) ! upper triangle of matrix
    real(wp), intent(out)       :: eig(:) ! eigenvalues
    real(wp), pointer           :: x(:) ! one eigenvector
    integer, intent(in)         :: no   ! specifies eigenvector in x
    integer, allocatable, save  :: lxch(:)
    real(wp), allocatable, save :: p(:), r(:), wrk1(:), wrk2(:)
    real(wp), allocatable, save :: u(:), v(:), w(:)
    real(wp)                    :: app, tmp
    integer                     :: status, i

    if (no == 1) then

       if (n == 1) then  ! deal with special case n=1
          allocate (y(n), stat=status)
          if (status /= 0) then
             write (fo,'(a,i6)') 'hsldr: allocation error = ', status
             stop
          end if
          x => y
          x(1) = 1.0_wp
          eig(1) = a(1)
          return
       end if

       allocate (p(n), r(n), wrk1(n), wrk2(n), u(n), v(n), &
            w(n), lxch(n), y(n), stat=status)
       if (status /= 0) then
          write (fo,'(a,i6)') 'hsldr: allocation error = ', status
          stop
       end if
       x => y

       if (n > 2) then     ! tri-diagonalization:
          call house (n, a, p, r, wrk1)
       else if (n == 2) then
          r(1) = a(1)
          r(2) = a(3)
          p(1) = a(2)
       end if

! elements of tri-diagonal matrix used to determine eigenvalues.
       call eigen (n, eig, p, r, wrk1, wrk2)
! reverse order of eigenvalues so lowest is ordered first
       do i = 1, n/2
          tmp = eig(i)
          eig(i) = eig(n+1-i)
          eig(n+1-i) = tmp
       end do
    end if

! eigenvector of tri-diagonal matrix for a particular eigenvalue:
    call vector (n, eig(no), x, p, r, wrk1, wrk2, u, v, w, lxch)

! get eigenvector of original matrix
    if (n > 2) call eigvec (n, a, x, p)

! normalizing the eigenvector.
    app = 1.0_wp / SQRT(SUM(x*x))
    x = app * x

    if (no == n) then
       deallocate (p, r, wrk1, wrk2, u, v, w, lxch, stat=status)
       if (status /= 0) then
          write (fo,'(a,i6)') 'hsldr: deallocation error = ', status
          stop
       end if
    end if
  end subroutine diag_hsldr

  subroutine house (n, a, p, r, array1)
! Householder method to reduce a tri-diagonal form
! store new main diagonal elements in position in a and also in r,
! and the super-diagonal elements in the array p.
    integer, intent(in)     :: n
    real(wp), intent(inout) :: a(:)
    real(wp), intent(out)   :: p(n)
    real(wp), intent(out)   :: r(n)
    real(wp), intent(out)   :: array1(n)
    integer                 :: n1, n2, k, nj, kc, kd, kdk, lol, m1, i
    integer                 :: lo, mo, nm, l, jm1, j, ke, ilk, n2p2, m
    real(wp)                :: sm, bkr, aim, pin

    n1 = n - 1
    n2 = n - 2

! each time through this loop one more row of the matrix is
! transformed to tri-diagonal form.
    k_loop: do k = 1, n2
       p(k:n) = 0.0_wp

! kb=kc-1 is # of elements in first (k-1) rows of upper triangle
       nj = n - k
       kc = ((k-1)*(nj+n+2))/2 + 1

! square root of sum of squares of remaining off diagonal elements in
! row k is found and stored in sm
       sm = 0.0_wp
       do j = 1, nj
          sm = a(kc+j) * a(kc+j) + sm
       end do
       sm = SQRT(sm)
       if (sm < epsi) then
          p(k) = - epsi
          do i = 1, nj
             a(i+kc) = 0.0_wp
          end do
          cycle k_loop
       end if

! sm is given the same sign as the super diagonal element in row k
       if (a(kc+1) < 0.0_wp) sm = - sm

! first element of vector from which transforming matrix is derived is
! overwritten on old super diagonal element in row k.
! The remaining elements are already in position in row k.
       a(kc+1) = a(kc+1) + sm
       bkr = sm * a(kc+1)

! super diagonal element in row k of new tri-diagonal matrix stored in p
       p(k) = -sm

! kd is # of elements in the first k rows of upper triangle
       kd = (k*(nj+n+1))/2
       kdk = kd - k + 1
! transformation derived from the nj=(n-k) vector elements stored in a
! now applied to the last nj rows of the matrix
! (the last nj spaces of array p are successively overwritten).
       sm = p(1+k)
       do l = 1, nj
          sm = sm + a(kd+l) * a(kc+l)
       end do
       p(1+k) = sm
       lol = kc - k + 1
       m_loop: do m = k+1, n1
          nm = n - m
          mo = ((nm+n+1)*m)/2
          m1 = m - 1
          lo = lol + m1
          sm = p(1+m)
          do l = 1, nm
             sm = sm + a(mo+l) * a(lo+l)
          end do
          jm1 = kdk + m
          do l = k, m1
             array1(l) = a(jm1)
             jm1 = jm1 + n1 - l
          end do
          do l = k, m1
             sm = sm + array1(l) * a(lol+l)
          end do
          p(1+m) = sm
       end do m_loop

       do l = k+1, n
          p(l) = p(l) / bkr
       end do
       sm = 0.0_wp
       do l = 1, nj
          sm = sm + a(kc+l) * p(k+l)
       end do
       sm = sm / bkr
       do l = 1, nj
          p(k+l) = p(k+l) - 0.5_wp * a(kc+l) * sm
       end do
       i_loop: do i = 1, nj
          aim = a(kc+i)
          pin = p(k+i)
          do j = i, nj
             array1(j) = - aim * p(k+j) - pin * a(kc+j)
          end do
          ke = kd + (i-1) * nj - (i*(i-1))/2
          do j = i, nj
             a(ke+j) = a(ke+j) + array1(j)
          end do
       end do i_loop
    end do k_loop

! the last super diagonal element is entered into the array p.
    ilk = ((n+1)*n)/2 - 1
    p(n1) = a(ilk)

! main diagonal elements picked out from the array a are stored in r
    n2p2 = 2 * n + 2
    do i = 1, n
       ilk = ((i-1)*(n2p2-i))/2+1
       r(i) = a(ilk)
    end do
  end subroutine house

  subroutine eigen (n, eig, p, r, poly, beta)
! accepts arrays r, p of main and super diagonal elements respectively.
! using sturm sequence, a bisection method is applied to determine the
! eigenvalues to an accuracy specified by epsi
    integer, intent(in)    :: n
    real(wp), intent(out)  :: eig(n)
    real(wp), intent(in)   :: p(n)
    real(wp), intent(inout):: r(n)
    real(wp), intent(out)  :: poly(n)
    real(wp), intent(out)  :: beta(n)
    real(wp), parameter    :: tiny = 1.0e-5_wp
    integer                :: n1, i, k, lsum, j
    real(wp)               :: asmall, alarg, amid, g, g1, al, bl
    real(wp)               :: cl1, cl, x

! calculate average of greatest and smallest main diagonal elements
    n1 = n - 1
    asmall = r(1)
    alarg = r(1)
    do i = 2, n
       alarg = MAX(alarg, r(i))
       asmall = MIN(asmall, r(i))
    end do
    amid = 0.5_wp * (alarg + asmall)

! reduce each main diagonal element by amid and calcuate, using
! sturm sequence property, eigenvalues of reduced tri-diagonal matrix.
    do i = 1, n
       r(i) = r(i) - amid
    end do
! calculate the maximum infinity norm g of the matrix.
! eigenvalues lie in the range -g to +g.
    g1 = ABS(r(1)) + ABS(p(1))
    g = g1
    do i = 2, n1
       g1 = ABS(r(i)) + ABS(p(i-1)) + ABS(p(i))
       g = MAX(g, g1)
    end do
    g1 = ABS(r(n)) + ABS(p(n1))
    g = MAX(g, g1)

! calculate squares of super diagonal elements and store in beta.
    do i = 1, n1
       beta(i) = p(i) * p(i)
    end do

! determine eigenvalues in order of algebraic size downwards.
    eigenvalues: do k = 1, n
       al = - g
       bl = g
! once through loop is one bisection of the range.
       cl1 = 0.0_wp         ! current estimate of eigenvalue
       iteration: do j = 1, 100
          lsum = 0 ! # of sign agreements in Sturm sequence
          do i = 1, n
             poly(i) = r(i) - cl1
          end do
          x = poly(1)
          if (x > 0.0_wp) lsum = 1

! this loop calculates all the remaining members of the sturm
! sequence. the number of agreements in sign is also determined.
          do i = 2, n
             if (x /= 0.0_wp) then
                x = poly(i) - beta(i-1) / x
                if (x > 0.0_wp) lsum = lsum + 1
             else
! DO NOT UNDERSTAND THIS
                x = poly(i) - ABS(p(i-1)) / (1.0_wp + tiny)
             end if
          end do
          cl = cl1     ! save previous eigenvalue estimate
! new range for eigenvalue (dependent on the value of lsum) determined.
          if (lsum < k) then
             bl = cl1
          else
             al = cl1
          end if
          cl1 = 0.5_wp * (al + bl)
! if eigenvalue determined to accuracy epsi, calculation complete.
          if (ABS(cl1-cl) < epsi) exit iteration
       end do iteration

! eigenvalue stored in eig.
       eig(k) = cl1
    end do eigenvalues

! elements of original tri-diagonal matrix regained, eigenvalues found
    do i = 1, n
       r(i) = r(i) + amid
       eig(i) = eig(i) + amid
    end do
  end subroutine eigen

  subroutine eigvec (n, a, x, p)
! takes eigenvector of tri-diagonalmatrix x and details of matrices used
! in transforming the original matrix to tri-diagonal form, stored in a
!and obtains eigenvector of the original matrix.
    integer, intent(in)     :: n
    real(wp), intent(in)    :: a(:)
    real(wp), intent(inout) :: x(n)
    real(wp), intent(in)    :: p(n)
    integer                 :: n2, n22, k, kp1, nk1, k1, i, j
    real(wp)                :: sop, bkr

    n2 = n - 2
    n22 = (n*(n+1))/2 + 1

! n2=n-2 transformations to obtain each eigenvector.
    k_loop: do k = 1, n2  ! 3
! k1 is # elements in first (k-1) rows of upper triangle stored in a.
       k1 = n22 - ((k+2)*(k+3))/2
       sop = 0.0_wp
       kp1 = k + 1
       nk1 = n - kp1
       do i = 1, kp1
          sop = sop + a(k1+i) * x(nk1+i)
       end do
       if (sop == 0.0_wp) cycle k_loop
! from information stored in a, bkr is determined as in house.
       bkr = - (p(nk1) * a(k1+1))
       sop = sop / bkr
       do j = 1, kp1
          x(nk1+j) = x(nk1+j) - a(k1+j) * sop
       end do
    end do k_loop
! eigenvector of original matrix is normalised.
    call norm (n, x)
  end subroutine eigvec

  subroutine vector (n, eigval, x, p, r, xm, b, u, v, w, lxch)
! arrays r of main diagonal elements and p of super diagonal elements
! of tri-diagonal matrix, and by inverse iterations determines an
! eigenvector of the tri-diagonal matrix.
    integer, intent(in)   :: n
    real(wp), intent(in)  :: eigval
    real(wp), intent(out) :: x(n)
    real(wp), intent(in)  :: p(n)
    real(wp), intent(in)  :: r(n)
    real(wp), intent(out) :: xm(n)      ! row multipliers
    real(wp), intent(out) :: b(n)
    real(wp), intent(out) :: u(n) ! main diagonal elements
    real(wp), intent(out) :: v(n) ! next diagonal elements
    real(wp), intent(out) :: w(n) ! last diagonal elements
    integer, intent(out)  :: lxch(n)
    real(wp)              :: pa, qa, ac
    integer               :: n1, i

! by Gaussian Elimination the new tri-diagonal matrix is transformed into
! upper triangular form. If row i interchanged with row i+1, lxch(i)=1
    pa = r(1) - eigval
    qa = p(1)
    n1 = n - 1
    i_loop: do i = 1, n1
! determine if a row interchange is necessary.
       if (ABS(p(i)) <= MAX(epsi, ABS(pa))) then
! no interchange.
          u(i) = pa
          v(i) = qa
          w(i) = 0.0_wp
          xm(i) = p(i) / u(i)
          pa = r(i+1) - eigval
          qa = p(i+1)
          lxch(i) = 0
       else
! interchange.
          u(i) = p(i)
          v(i) = r(i+1) - eigval
          w(i) = p(i+1)
          xm(i) = pa / u(i)
          pa = qa
          qa = 0.0_wp
          lxch(i) = 1
       end if
! row i is multiplied by xm(i) and subtracted from row i+1.
       pa = pa - xm(i) * v(i)
       qa = qa - xm(i) * w(i)
    end do i_loop

! the single element in the last row is placed in u(n)
    u(n) = pa
    if (ABS(u(n)) < epsi) then
      print *,'ARE SIGNS OK ',u(n)
      u(n) = epsi
    endif

! x is reserved for current estimate of eigenvector
    x(n) = 1.0_wp / u(n)
    x(n1) = (1.0_wp - v(n1) * x(n)) / u(n1)
    if (n > 2) then
       do i = n-2, 1, -1
          x(i) = (1.0_wp - v(i) * x(i+1) - w(i) * x(i+2)) / u(i)
       end do
    end if
    call norm (n, x)   ! normalize eigenvector

! a new column vector related to x by exactly the same row
! interchanges and multiplications by which the upper triangular
! matrix was obtained from the new tri-diagonal matrix, is
! calculated and stored in b.
    do i = 1, n
       b(i) = x(i)
    end do
    n1 = n - 1
    do i = 1, n1
       if (lxch(i) == 1) then
          ac = b(i)
          b(i) = b(i+1)
          b(i+1) = ac
       end if
       b(i+1) = b(i+1) - xm(i) * b(i)
    end do
    call norm (n,b)  ! normalize b

    x(n) = b(n) / u(n)
    x(n1) = (b(n1) - v(n1) * x(n)) / u(n1)
    if (n > 2) then
       do i = n-2, 1, -1
          x(i) = (b(i) - v(i) * x(i+1) - w(i) * x(i+2)) / u(i)
       end do
    end if
    call norm (n,x)  ! normalize new estimate
  end subroutine vector

  subroutine norm (n, x)
! normalize vector x such that largest component is unity.
    integer, intent(in)     :: n
    real(wp), intent(inout) :: x(n)
    real(wp)                :: g
    integer                 :: i

    g = ABS(x(1))
    do i = 2, n
       g = MAX(g, ABS(x(i)))
    end do
    do i = 1, n
       x(i) = x(i) / g
    end do
  end subroutine norm

  subroutine diag_stein (n, a, eig, x, no)
! accepts the upper triangle of an n*n symmetric matrix and on the
! first call determines all eigenvalues and first eigenvector.
! on each further call one of remaining eigenvectors is calculated.
    integer, intent(in)         :: n    ! degree of matrix
    real(wp), intent(inout)     :: a(:) ! upper triangle of matrix
    real(wp), intent(out)       :: eig(:) ! eigenvalues
    real(wp), pointer           :: x(:)   ! one eigenvector
    integer, intent(in)         :: no  ! specifies eigenvector is in x
    integer, allocatable, save  :: iblock(:), isplit(:), iwork(:)
    real(wp), allocatable, save :: p(:), r(:), wrk1(:), work(:)
    integer                     :: status, ifail(1)
    integer                     :: il, iu, info, nfound, nsplit, iscale
    real(wp)                    :: vl, vu, abstll

    if (no == 1) then
       if (n == 1) then ! deal with special case n=1
          allocate (z(n,1), stat=status)
          if (status /= 0) then
             write (fo,'(a,i6)') 'diag_stein: allocation error = ', status
             stop
          end if
          z(1,1) = 1.0_wp
          eig(1) = a(1)
          x => z(:,1)
          return
       end if

       allocate (p(n), r(n), wrk1(n), work(5*n), iblock(n), isplit(n), &
            iwork(3*n), z(n,1), stat=status)
       if (status /= 0) then
          write (fo,'(a,i6)') 'diag_stein: allocation error = ', status
          stop
       end if

       if (n > 2) then     ! tri-diagonalization:
! scale matrix:
          call scale_matrix (n, a, abstll, iscale)

! DSPTRD reduces a real symmetric matrix A stored in packed form to
! symmetric tridiagonal form: Q**T * A * Q = T.
! if UPLO = 'L', AP(i + (j-1)*(2*n-j)/2) = A(i,j) for j<=i<=n.
          call dsptrd ('L', n, a, r, p, wrk1, info)
          if (info /= 0) then
             write (fo,'(a,i6)') 'dsptrd error, info = ', info
             stop
          end if
       else if (n == 2) then
          r(1) = a(1)
          r(2) = a(3)
          p(1) = a(2)
       end if

! DSTEBZ computes the eigenvalues of a symmetric tridiagonal matrix T.
! nfound = # eigenvalues actually found
! nsplit = # diagonal blocks in T; iblock = block # for each eigenvalue
       call dstebz ('A', 'B', n, VL, VU, IL, IU, abstll, r, p, &
            nfound, nsplit, eig, iblock, isplit, work, iwork, info)
       if (info /= 0) then
          write (fo,'(a,i6)') 'dstebz error: info = ', info
          stop
       end if
       if (nfound /= n) then
          write (fo,'(a,i6)') 'dstebz: # eigenvalues found = ', nfound
          stop
       end if
      if (iscale == 1) call dscal (n, 1.0_wp / sigma, eig, 1)
    end if

! DSTEIN computes eigenvectors of real symmetric tridiagonal matrix T
! corresponding to specified eigenvalues, using inverse iteration.
    call dstein (n, r, p, 1, eig(no:no), iblock(no:no), isplit, z, &
         n, work, iwork, ifail, info)
    if (info /= 0) then
       write (fo,'(a,i6)') 'dstein error ', info
       stop
    end if

    if (n > 2) then   ! get eigenvector of original matrix
! Apply orthogonal matrix used in reduction to tridiagonal
! form to eigenvectors returned by DSTEIN.
       call dopmtr ('L', 'L', 'N', n, 1, a, wrk1, z, n, work, info)
       if (info /= 0) then
          write (fo,'(a,i6)') 'dopmtr error ', info
          stop
       end if
    end if

    if (no == n) then
       deallocate (p, r, wrk1, work, iblock, isplit, iwork, stat=status)
       if (status /= 0) then
          write (fo,'(a,i6)') 'hsldr: deallocation error = ', status
          stop
       end if
    end if
    x => z(:,1)
  end subroutine diag_stein

  subroutine scale_matrix (n, ap, abstll, iscale)
! scale matrix to allowable range, if necessary.
    real(wp), intent(inout) :: ap(:)
    integer, intent(out)    :: iscale
    integer, intent(in)     :: n
    real(wp), intent(out)   :: abstll
    real(wp)                :: anrm, work(1)
    real(wp)                :: t = 1.0_wp
    real(wp)                :: safmin, eps, rmin, rmax
    real(wp)                :: smlnum, bignum, dlansp, abstol

! machine constants:
    safmin = TINY(t) * (1.0_wp + EPSILON(t))  ! inverse wont overflow
    eps = 2.0_wp * 10.0_wp**(-PRECISION(t)-1) ! to agree with dlmach
    smlnum = safmin / eps
    bignum = eps / safmin
    rmin = SQRT(smlnum)
    rmax = MIN(SQRT(bignum), 1.0_wp / SQRT(SQRT(safmin)))
    abstol = 2.0_wp * safmin

    iscale = 0
    abstll = abstol
! DLANSP  returns the value of the  element of  largest absolute
! value  of a real symmetric matrix A,  supplied in packed form.
    anrm = dlansp ('m', 'U', n, ap, work)
    if (anrm > 0.0_wp .and. anrm < rmin) then
       iscale = 1
       sigma = rmin / anrm
    else if (anrm > rmax) then
       iscale = 1
       sigma = rmax / anrm
    end if
    if (iscale == 1) then
       call dscal((n*(n+1))/2, sigma, ap, 1)
       if (abstol > 0) abstll = abstol * sigma ! scale accuracy required
    end if
  end subroutine scale_matrix

  subroutine diag_dspev (n, a, eig, x, no)
! diagonalizes a packed upper triangle n*n symmetric matrix
! on first call returns eigenvalues and the first eigenvector.
! on subsequent call one more of the eigenvectors is returned
    integer, intent(in)      :: n       ! degree of matrix
    real(wp), intent(inout)  :: a(:)    ! upper triangle
    real(wp), intent(out)    :: eig(:)  ! eigenvalues
    real(wp), pointer        :: x(:)    ! one eigenector
    integer, intent(in)      :: no      ! specifies eigenvector
    real(wp), allocatable, save :: work(:)
    character(len=1)         :: jobz, uplo
    integer                  :: info, status

    if (no == 1) then
       jobz = 'V'
       uplo = 'L'
       allocate (z(n,n), work(3*n), stat=status)
       if (status /= 0) call alloc_error (status, 'diag_dspev', 'a')
       call dspev (jobz, uplo, n, a, eig, z, n, work, info)
       if (info /= 0) then
          write (fo,'(a,i6)') 'diag: dspev failure, info = ', info
          stop
       end if
! ESSL calling sequence:
!       call dspev (1, a, eig, z, n, n, work)
    end if
    x => z(:,no)
    if (no == n) then
       deallocate (work, stat=status)
       if (status /= 0) call alloc_error (status, 'diag_dspev', 'd')
    end if
  end subroutine diag_dspev
end module diagonalizers
