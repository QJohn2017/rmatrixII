module targets
! set up and diagonalize target Hamiltonians
! Time-stamp: "2005-02-04 10:14:20 dlcjn1"
  use precisn, only: wp
  use io_units, only: fo, jbuff1, jbuff4, jibb, jfbb
  use debug, only: bug6
  use error_prt, only: alloc_error
  implicit none

  private
  public targ
  public ivbbi, ivbbr
  public i1bb, nr1bb, r1bb, irkbb, nrkbb, rkbb

  integer, pointer, save     :: ivbbi(:)=>NULL()
  integer, pointer, save     :: ivbbr(:)=>NULL()

  integer, pointer, save     :: i1bb(:)=>NULL() ! 1e b-b rad int ptrs
  integer, save              :: nr1bb        ! size of r1bb
  real(wp), pointer, save    :: r1bb(:)=>NULL() ! 1e b-b rad integrals
  integer, pointer, save     :: irkbb(:,:,:)=>NULL() !2e BB rad int ptrs
  integer, save              :: nrkbb        ! size of rkbb
  real(wp), pointer, save    :: rkbb(:)=>NULL() ! 2e b-b rad integrals

contains

  subroutine targ (bij, en)
! determine target eigenvalues and eigenvectors
    use rad_data, only: lrang1, lrang2
    use set_data, only: nset, lset, isset, ipset, nstat, llpdim
    use states, only: nsetst
    use filehand, only: readix, ireada, ireadb, lookup, startr, readb, &
         ireadc, open
    use bb_ham, only: hambb, wrtham, import_ptrs
    use diagonalizers, only: diag, diag_kill
    use symmetry_check, only: compare_tgt_symms
    real(wp), intent(out)    :: bij(:,:)  ! eigenvectors
    real(wp), intent(out)    :: en(:)     ! eigenvalues
    character(len=7)         :: file, stats
    integer, allocatable     :: ncset(:)
    real(wp), allocatable    :: hbb(:), values(:)
    integer, allocatable     :: lpseta(:), ippseta(:), ispseta(:)
    integer                  :: is, ny, ny2, nlr, nlam, n
    integer                  :: status, ibuff, nr1bb, ibb, nrec, nrkbb
    integer                  :: nst, no, ist, nsett
    logical                  :: real_typ
    real(wp), pointer        :: x(:)

! open radial files rad1 and rad4.
    stats = 'old'
    file = 'RAD1'
    real_typ = .true.
    call open (jbuff1, file, stats, real_typ)
    file = 'RAD4'
    call open (jbuff4, file, stats, real_typ)

! one electron bound-bound radial integrals:
    call readix (jbuff4)
    allocate (i1bb(lrang1), stat=status)   ! allocate pointers, i1bb
    if (status /= 0) call alloc_error (status, 'targ', 'a')
    call ireadb (jbuff4, i1bb, 1, lrang1)
    call ireada (jbuff4, nr1bb)
    allocate (r1bb(nr1bb), stat=status)   ! allocate integrals, r1bb
    if (status /= 0) call alloc_error (status, 'targ', 'a')
    call lookup (i1bb(1), nrec, ibuff)
    call startr (jbuff4, nrec, ibuff)
    call readb (jbuff4, r1bb, 1, nr1bb)

! two electron bound-bound radial integrals
    call readix (jbuff1)
    nlr = (lrang1 * (lrang1+1)) / 2
    nlam = 2 * lrang1 - 1
    allocate (irkbb(nlam,nlr,nlr), stat=status) ! allocate pointers irkbb
    if (status /= 0) call alloc_error (status, 'targ', 'a')
    do n = 1, nlr
       call ireadc (jbuff1, irkbb(:,:,n), 1, nlam, 1, nlr, nlam, 0)
    end do
    call ireada (jbuff1, ibb)    ! location of radial integrals
    call ireada (jbuff1, nrkbb)  ! # 2-e radial integrals
    allocate (rkbb(nrkbb), stat=status)      ! allocate integrals rkbb
    if (status /= 0) call alloc_error (status, 'targ', 'a')
    call lookup (ibb, nrec, ibuff)
    call startr (jbuff1, nrec, ibuff)
    call readb (jbuff1, rkbb, 1, nrkbb)

! open angular files abbi and abbr
    file = 'atgi'
    real_typ = .false.
    call open (jibb, file, stats, real_typ)
    file = 'atgr'
    real_typ = .true.
    call open (jfbb, file, stats, real_typ)

! read pointer arrays for bound-bound angular integrals
    call readix (jibb)
    call ireada (jibb, nsett)
! Check consistency of target files by comparing HAM and ANG symmetries
    allocate (lpseta(nsett), ippseta(nsett), ispseta(nsett), &
         stat=status)
    if (status /= 0) call alloc_error (status, 'targ', 'a')
    call ireadb (jibb, lpseta, 1, nsett)
    call ireadb (jibb, ippseta, 1, nsett)
    call ireadb (jibb, ispseta, 1, nsett)
    call compare_tgt_symms (nsett, lpseta, ippseta, ispseta)
    deallocate (lpseta, ippseta, ispseta, stat=status)
    if (status /= 0) call alloc_error (status, 'targ', 'd')

    allocate (ivbbi(nset), ivbbr(nset), ncset(nset), stat=status)
    if (status /= 0) call alloc_error (status, 'targ', 'a')
    call ireadb (jibb, ncset, 1, nset)
    call ireadb (jibb, ivbbi, 1, nset)
    call readix (jfbb)
    call ireadb (jfbb, ivbbr, 1, nset)

    call import_ptrs (nr1bb, nrkbb, ivbbi, ivbbr, i1bb, r1bb, &
         irkbb, rkbb)
    if (ABS(bug6) == 5) write (fo,'(/,a,/)') 'Target Hamiltonian data'
    tgt_syms: do is = 1, nset
       ny = ncset(is)             ! # cfgs
       if (ny == 0) cycle
       ny2 = (ny * (ny+1)) / 2
       if (ABS(bug6) == 5) then
          write (fo,'(3(a,i3))') ' L = ', lset(is),&
               '    S = ', isset(is), '    parity = ', ipset(is)
          write (fo,'(a)') '----------------------------------'
       end if
       allocate (hbb(ny2), values(ny), stat=status)
       if (status /= 0) call alloc_error (status, 'targ', 'a')

       call hambb (is, ny2, hbb)

       if (ABS(bug6) == 5) then  ! print Hamiltonian
          write (fo,'(a)') 'Hamiltonian matrix'
          write (fo,'(a)') '------------------'
          write (fo,'(a,i5)') 'size = ', ny
          call wrtham (ny, ny2, hbb)
       end if

! eigenvalues produced in ascending order:
       nst = nstat(is)
       do no = 1, ny
          call diag (ny, hbb, values, x, no) ! diagonalization
          if (no <= nst) then
             ist = nsetst(is,no)
             en(ist) = values(no)
             bij(:ny,ist) = x
          end if
       end do
       deallocate (hbb, values, stat=status)
       if (status /= 0) call alloc_error (status, 'targ', 'd')
       call diag_kill
    end do tgt_syms

    if (ABS(bug6) == 5) write (fo,'(/,a,/)') 'End of debug output &
         &for target Hamiltonian'
    deallocate (ivbbr, ivbbi, i1bb, r1bb, irkbb, rkbb, stat=status)
    if (status /= 0) call alloc_error (status, 'targ', 'd')
  end subroutine targ

end module targets

