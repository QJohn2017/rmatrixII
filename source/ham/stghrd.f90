module stghrd_mod
! Time-stamp: "2005-08-09 16:04:20 cjn"
  use precisn, only: wp
  use io_units, only: fo, fi
  use error_prt, only: alloc_error
  implicit none

  integer, save               :: lrgllo, lrglup, lrglb
  integer, save               :: minim
  integer, save               :: ncore
  integer, save               :: nrang1
  real(wp), save              :: ecore

  private
  public stghrd
  public lrgllo, lrglup, lrglb, minim, ncore, ecore, nrang1

contains
  subroutine stghrd
! read in and write out the input data
    use rad_data, only: rd_maxnhf, rd_rad_hdr, lrang1, lrang2
    use set_data, only: rd_set_data, lset, isset, ipset, nast, nstat,&
         isplo, ispup
    use states, only: def_state_data, ll, lspn, lpty, enat, aij,  &
         nsetst, wrt_tgt_lib, rd_tgt_lib, mx_ncst, ncst
    use accdi, only: rd_accdi, ncset
    use targets, only: targ
    use debug, only: set_debug_switches
    use comp_data, only: def_comp
    use rm_data, only: rd_nmlst, lrgle1, lrgle2, lrgld1, lrgld2,nset, &
         ndiag, lrgp, inorb, nexp, icon, ncfg, maxorb, lrgslo,      &
         lrgsup, ibop, npot, nix, diag, ncfgp
    use dim, only: ldim
    use vpack, only: initialize_vpack
    use diagonalizers, only: select_diagonalizer
    use symmetry_check, only: gen_np1_symms, set_lrange
    real(wp), pointer      :: bij(:,:), en(:)
    integer, allocatable   :: norder(:)
    integer, pointer       :: ll1(:), lspn1(:), lpty1(:)
    integer, allocatable   :: nj(:), lj(:)
    integer, allocatable   :: idummy(:), kdummy(:)
    character(len=19)      :: lspec = 'spdfghspdfghijklmno'
    character(len=19)      :: lspecu = 'SPDFGHSPDFGHIJKLMNO'
    character(len=1)       :: parity(0:1) = (/'e', 'o'/)
    integer                :: status, jj, nparr, k, i, j, n
    integer                :: nxcite, kdum
    integer                :: istat, is, nconf
    integer                :: kst, iorb, lexch, jst, ic, nsnp, ist
    real(wp)               :: ground, enlvl
    integer                :: sc_inorb, ps_inorb, mx_ncorr, idum1, idum2

    call rd_nmlst

    call title
    write (fo,'(//,15x,a)') '==========='
    write (fo,'(15x,a)') 'Program HAM'
    write (fo,'(15x,a,//)') '==========='

! calculate lrgllo and lrglup, the minimum and maximum values of the
! total angular momentum l for the (n+1)-electron system.
    if (ndiag >= 0) then
       if (lrgle1 < 0 .or. lrgle2 < 0) then
          lrgllo = lrgld1
          lrglup = lrgld2
          lrgle1 = -1
          lrglb = -1       ! bound term limit
       else
          lrgllo = MIN(lrgle1, lrgld1)
          lrglup = MAX(lrgle2, lrgld2)
          lrglb = lrgle2
       end if
       write (fo,'(2(a,i3))') 'Hamiltonian matrices for (N+1)-&
            &electron a.m. from ', lrgllo, ' to ', lrglup
       if (lrglb >= 0) then
          write (fo,'(a,i4)') 'Bound terms for l <= ', lrglb
       else
          write (fo,'(a)') 'No bound terms'
       end if
    end if
    call set_lrange (lrgllo, lrglup, lrglb)

    call prt_options
    if (ndiag /= -2) call select_diagonalizer (diag)

! if necessary, calculate lrgslo and lrgsup, the minimum and
! maximum (2*s+1) for the (n+1)-electron system.

    call rd_set_data (nset, lrgslo, lrgsup)
    call gen_np1_symms   ! arrays to check BB and BC files

!! construct the lspi for the n-electron system.
!       is = 1
!       do ipp = 0, 1
!          do isp = isplo, ispup
!             do lp = 0, lpmax
!                lpset(is) = lp
!                ispset(is) = isp
!                ippset(is) = ipp
!                is = is + 1
!             end do
!          end do
!       end do
!    else
!    end if

! determine the range required for the continuum angular momentum
    if (ndiag >= 0) then
       call lcval (lrgle1, lrgle2, lrgld1, lrgld2, minim, lexch)
       write (fo,'(a,i3,a,i3)') 'Continuum angular momentum lc = ', &
            minim-1, ' -> ', lrang2-1
       write (fo,'(a,i3)') 'Exchange included for lc <= ', lexch-1
    end if


! set up the maxorb - inorb nl values using the default sequence
! 1s,2s,2p,3s,...  read in the remaining inorb nl values.
    if (inorb > maxorb) then
       write (fo,'(a,i2)') 'value of inorb = ', inorb
       write (fo,'(a,i2)') 'value of maxorb = ', maxorb
       write (fo,'(a)') 'inorb must be <= maxorb'
       stop
    end if
    allocate (nj(maxorb), lj(maxorb), stat=status)
    if (status /= 0) call alloc_error (status, 'stghrd', 'a')
    iorb = maxorb - inorb
    k = 1
    i = 1
    k_loop: do
       do j = 0, i-1
          nj(k) = i
          lj(k) = j
          k = k + 1
          if (k > iorb) exit k_loop
       end do
       i = i + 1
    end do k_loop

    if (inorb > 0) read (fi,*) (nj(i), lj(i), i = iorb+1, maxorb)
    write (fo,'(a,i3,a)') 'The following', maxorb, ' bound orbitals &
         &are included in the calculation:'
    write (fo,'(12(2x,i2,a1))') (nj(i), lspec(lj(i)+1:lj(i)+1), &
         i = 1, maxorb)
    nrang1 = MAXVAL(nj)
    lrang1 = MAXVAL(lj) + 1
! skip corrector data used in ang:
    read (fi,*) sc_inorb, ps_inorb, mx_ncorr
    if (sc_inorb > 0) then
       read (fi,*) (idum1, idum2, i = 1, sc_inorb)
    end if
    if (ps_inorb > 0) then
       read (fi,*) (idum1, idum2, i = 1, ps_inorb)
    end if

! skip over configuration data
    if (icon == 0) then
       allocate (idummy(maxorb), stat=status)
       if (status /= 0) call alloc_error (status, 'stghrd', 'a')
       read (fi,*) idummy
       read (fi,*) idummy
       do i = 1, ncfg
          read (fi,*) idummy, nxcite
       end do
       deallocate (idummy, stat=status)
       if (status /= 0) call alloc_error (status, 'stghrd', 'd')
    else
       allocate (idummy(ncfg), stat=status)
       if (status /= 0) call alloc_error (status, 'stghrd', 'a')
       read (fi,*) idummy
       kdum = MAXVAL(idummy)
       allocate (kdummy(kdum), stat=status)
       if (status /= 0) call alloc_error (status, 'stghrd', 'a')
       do i = 1, ncfg
          n = idummy(i)
          read (fi,*) kdummy(1:n)
          read (fi,*) kdummy(1:n)
       end do
       deallocate (kdummy, idummy, stat=status)
       if (status /= 0) call alloc_error (status, 'stghrd', 'd')
    end if
    if (ncfgp > 0) call rdcfgp (ncfgp) ! skip N+1 electron cfg data

! read maxnhf array, skip over maxnlg array
    call rd_maxnhf (lrang1, lrang2, ibop, nix, npot)
    call ldim

! open accdi file and read its index
    if (ndiag >= 0) then
       if (lrgp < 0) then   ! both parities required
          nparr = 2
       else                 ! only one total (N+1) parity
          nparr = 1
       end if
       nsnp = nparr * (lrglup-lrgllo+1) * (1 + (lrgsup-lrgslo)/2)
       call rd_accdi (nset)
    end if

    call def_state_data (nset, ll1, lspn1, lpty1)

! read file rad3
    call rd_rad_hdr (lrang1, lrang2, ncore, ecore)

    call initialize_vpack (maxorb, lrang1) ! define ANG packing factors

    call def_comp (maxorb, ncore, nj, lj)
    maxorb = maxorb - ncore
    deallocate (nj, lj, stat=status)
    if (status /= 0) call alloc_error (status, 'stghrd', 'd')

    tgt_diag: if (ABS(ndiag) == 1) then ! diagonalize targets
       allocate (en(nast), bij(mx_ncst,nast), stat=status)
       if (status /= 0) call alloc_error (status, 'stghrd', 'a')
       write (fo,'(a)') 'Diagonalising the target Hamiltonian matrix'
       call targ (bij, en)
       if (ncore > 0) then
          do ist = 1, nast
             en(ist) = en(ist) + ecore
          end do
       end if

! write out state information:
       write (fo,'(/,a,i3,a,/)') 'The following ', nast, &
            ' states are included:'
       do is = 1, nset
          write (fo,'(4x,i1,1x,a1)') isset(is), parity(ipset(is))
          jj = lset(is) + 7
          write (fo,'(5x,a1/4x,a)') lspecu(jj:jj), '---'
          do ist = 1, nstat(is)
             istat = nsetst(is,ist)
             nconf = ncst(istat)
             write (fo,'(4x,a,f14.7,a)') 'energy = ', en(istat), ' a.u.'
             write (fo,'(4x,a/(5f14.7))') 'eigenvector', bij(1:nconf,istat)
             write (fo,'(" ")')
          end do
       end do
! write target information to disk file
       call wrt_tgt_lib (en, bij)
    end if tgt_diag

    if (ndiag == 0 .or. ndiag == -2) then
       call rd_tgt_lib (en, bij)
       write (fo,'(/,a,i3,a,/)') 'The following ', nast, &
            ' states are included:'
       do is = 1, nset
          write (fo,'(4x,i1,1x,a1)') isset(is), parity(ipset(is))
          jj = lset(is) + 7
          write (fo,'(5x,a1/4x,a)') lspecu(jj:jj), '---'
          do ist = 1, nstat(is)
             istat = nsetst(is,ist)
             nconf = ncst(istat)
             write (fo,'(4x,a,f14.7,a)') 'energy = ', en(istat), ' a.u.'
             write (fo,'(4x,a/(5f14.7))') 'eigenvector', bij(1:nconf,istat)
             write (fo,'(" ")')
          end do
       end do
    end if

! if nexp is non-zero, read experimental energies for each state
    if (nexp > 0) then
       write (fo,'(a)') 'Experimental values are used for target state &
            &energies'
!      call calce (ll1, lspn1, lpty1, nast, en, enat)
! Watts version is preferred:
       call calce_w (ll1, lspn1, lpty1, nast, en, enat)
       do ist = 1, nast
          en(ist) = enat(ist)
       end do
    end if

! order the states in ascending energies
    allocate (norder(nast), stat=status)
    if (status /= 0) call alloc_error (status, 'stghrd', 'a')
    call order (en, nast, norder)
    do ist = 1, nast
       jst = norder(ist)
       enat(ist) = en(jst)
       ll(ist) = ll1(jst)
       lspn(ist) = lspn1(jst)
       lpty(ist) = lpty1(jst)
       nconf = ncst(jst)
       do ic = 1, nconf
          aij(ist,ic) = bij(ic,jst)
       end do
    end do
    do is = 1, nset
       do ist = 1, nstat(is)
          jst = nsetst(is,ist)
          do kst = 1, nast
             if (norder(kst) == jst) then
                nsetst(is,ist) = kst
                exit
             end if
          end do
       end do
    end do
    write (fo,'(/,a,/)') 'States re-ordered into ascending energies:'
    write (fo,'(5x,a,4x,a,10x,a,8x,a)') 'state', 'symmetry', &
         'energy (au)', 'above target ground (ryd)'
    write (fo,'(5x,a,4x,a,10x,a,8x,a)') '-----', '--------', &
         '-----------', '-------------------------'
    ground = enat(1)
    do ist = 1, nast
       enlvl = 2.0_wp * (enat(ist) - ground)
       write (fo,'(17x,i1,1x,a1)') lspn(ist), parity(lpty(ist))
       jj = ll(ist) + 7
       write (fo,'(6x,i3,9x,a1,10x,f14.7,10x,f14.7)')  ist, &
            lspecu(jj:jj), enat(ist), enlvl
       enat(ist) = enat(ist) - ecore
    end do
    deallocate (en, bij, ll1, lspn1, lpty1, ncst, norder, &
         stat=status)
    if (status /= 0) call alloc_error (status, 'stghrd', 'd')
  end subroutine stghrd

  subroutine rdcfgp (npefig)
! skip N+1 electron configuration data:
! read the number of occupied shells for ncfgp cfgs,
! then for each configuration read the index for the occupied shells
! and the number of electrons in the shell
    integer, intent(in)   :: npefig
    integer               :: status, i, n, fshp
    integer, allocatable  :: noccshp(:), nocorbp(:), nelcshp(:)

    allocate (noccshp(npefig), stat=status)
    if (status /= 0) call alloc_error (status, 'rdcfgp', 'a')
    read (fi,*) noccshp
    fshp = MAXVAL(noccshp(1:npefig))
    allocate (nocorbp(fshp), nelcshp(fshp), stat=status)
    if (status /= 0) call alloc_error (status, 'rdcfgp', 'a')
    do i = 1, npefig
       n = noccshp(i)
       read (fi,*) nocorbp(1:n)
       read (fi,*) nelcshp(1:n)
    end do
    deallocate (noccshp, nocorbp, nelcshp, stat=status)
    if (status /= 0) call alloc_error (status, 'rdcfgp', 'd')
  end subroutine rdcfgp

  subroutine prt_options
! print major options selected for current run
    use rm_data, only: ndiag, prm, asym_ptlcfs, tgt_mmts, farm

    write (fo,'(/,a,/)') 'Options selected'
    write (fo,'(a,L8)') 'Asyptotic potential coeffs: ', asym_ptlcfs
    write (fo,'(a,L8)') 'Target moments:             ', tgt_mmts
    write (fo,'(a,L8)') 'Opacity output file:        ', farm
    write (fo,'(a,i8)') 'Parallel threshold:         ', prm
    write (fo,'(a,i8,/)') 'Target/scattering selector: ', ndiag
  end subroutine prt_options

  subroutine title
! write a general title indicating the target ion/atom,
! # continuum electrons, highest electron energy
    use rm_data, only: nelc, nz, coment
    character(len=2), parameter    :: atom(57) = (/ &
         ' H', 'He', 'Li', 'Be', ' B', ' C', ' N', ' O', ' F', &
         'Ne', 'Na', 'Mg', 'Al', 'Si', ' P', ' S', 'Cl', 'Ar', &
         ' K', 'Ca', 'Sc', 'Ti', ' V', 'Cr', 'Mn', 'Fe', 'Co', &
         'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr', &
         'Rb', 'Sr', ' Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', &
         'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', ' I', 'Xe', &
         'Cs', 'Ba', 'La'/)
    character(len=1)                :: chrge
    character(len=8)                :: today
    character(len=10)               :: now
    integer                         :: nzeff

    call date_and_time (today, now)
! today: ccyymmdd
    write (fo,'(a,a2,a,a2,a,a4)') 'Job run on ', today(7:8), &
         '/', today(5:6), '/', today(1:4)
! now: hhmmss.sss
    write (fo,'(a,a2,a,a2,a,a5)') 'Time: ', now(1:2), ':', &
         now(3:4), ':', now(5:10)
    if (coment /= ' ') write (fo,'(a80)') coment

    nzeff = nz - nelc
    if (nzeff > 0) then
       chrge = '+'
    else if (nzeff < 0) then
       chrge = '-'
       nzeff = - nzeff
    end if

    if (nzeff > 1) then
       write (fo,'(5x,37("*"))')
       write (fo,'(5x,"*",35x,"*")')
       if (nzeff < 10) then
          write (fo,'(5x,"*",29x,i1,a1,4x,"*")') nzeff, chrge
       else
          write (fo,'(5x,"*",29x,i2,a1,3x,"*")') nzeff, chrge
       end if

       if (nz > 57) then
          write (fo,'(5x,"*",4x,a,i4,6x,"*")') 'Electron scatter&
               &ing by Z = ', nz
       else
          write (fo,'(5x,"*",4x,a,a2,6x,"*")') 'Electron scatter&
               &ing by ', atom(nz)
       end if
       write (fo,'(5x,"*",35x,"*")')
       write (fo,'(5x,37("*"))')
    else
       write (fo,'(/,5x,37("*"))')
       write (fo,'(5x,"*",35x,"*")')
       if (nzeff == 1) write (fo,'(5x,"*",29x,a1,5x,"*")') chrge
       if (nz > 57) then
          write (fo,'(5x,"*",4x,a,i4,6x,"*")') 'Electron scatter&
               &ing by Z = ', nz
       else
          write (fo,'(5x,"*",4x,a,a2,6x,"*")') 'Electron scattering by ',&
               atom(nz)
       end if
       write (fo,'(5x,"*",35x,"*")')
       write (fo,'(5x,37("*"),/)')
    end if
  end subroutine title

  subroutine lcval (lrgle1, lrgle2, lrgld1, lrgld2, minim, lexch)
! determine which sets couple with each value of continuum a.m.
! within the range allowed. Determine range required for continuum a.m.
    use set_data, only: nset, lset, ipset, lrang3
    use rad_data, only: lrang2
    integer, intent(in)       :: lrgle1, lrgle2, lrgld1, lrgld2
    integer, intent(out)      :: minim
    integer, intent(out)      :: lexch
    integer                   :: lval(2*lrang3), ilval(2*lrang3)
    integer                   :: nscol(2*lrang3)
    integer                   :: llo, lup, lrgl1, lrgl2, ipoint, lrgl
    integer                   :: is, kl, lmin, lmax, ipt, npty, ipi
    integer                   :: iltot, l, nl, lcfg, ltot, ikl

    llo = 999
    lup = 0
    if (lrgle1 < 0 .or. lrgle2 < 0) then
       lrgl1 = lrgld1
       lrgl2 = lrgld2
       lexch = 0
       ipoint = 1
    else
       lrgl1 = lrgle1
       lrgl2 = lrgle2
       ipoint = 0
    end if

    ipt_loop: do ipt = 1, 2
       lrgl_loop: do lrgl = lrgl1, lrgl2
          parity_loop: do npty = 0, 1

! find range of continuum angular momentum and
! set up ilval(kl) - the possible values of continuum a.m.
             lmin = 999
             lmax = 0
             do is = 1, nset
                lcfg = lset(is)
                lmin = MIN(lmin, ABS(lcfg-lrgl))
                lmax = MAX(lmax, lcfg)
             end do
             lmax = lmax + lrgl
             iltot = lmax - lmin + 1
             do kl = 1, iltot
                ilval(kl) = lmin + kl - 1
             end do

! loop over this range of continuum l
             do kl = 1, iltot
                l = ilval(kl)
                nl = 0
                do is = 1, nset
                   lcfg = lset(is)
                   ipi = ipset(is)
! test parity
                   if (MOD(ipi+l+npty,2) /= 0) cycle
! triangle rule
                   if (lrgl > lcfg+l .or. lrgl < ABS(lcfg-l)) cycle
                   nl = nl + 1
                   nscol(kl) = 1
                   exit
                end do
             end do

! now delete values of the continuum angular momentum which
! cannot couple with any set
             kl = 0
             do ikl = 1, iltot
                if (nscol(ikl) /= 0) then
                   kl = kl + 1
                   lval(kl) = ilval(ikl)
                end if
             end do
             ltot = kl
             if (ltot /= 0) then
                lup = MAX(lval(ltot), lup)
                llo = MIN(lval(1), llo)
             end if
          end do parity_loop
       end do lrgl_loop

       if (ipoint == 0) then
          lexch = lup + 1
          lrgl1 = MAX(lrgle2, lrgld1)
          lrgl2 = lrgld2
          ipoint = 1
          cycle ipt_loop
       else
          minim = llo + 1
          lrang2 = lup + 1
       end if
    end do ipt_loop
  end subroutine lcval

  subroutine calce (ll1, lspn1, lpty1, nast, enat, est)
! this version allows  selected  levels to be included in the averaging
! also nast-nast1 levels are unidentified and keep their calculated
! values except that they are shifted along with the other states.
! to avoid shifting see comment
    integer, intent(in)      :: nast
    integer, intent(in)      :: ll1(:)
    integer, intent(in)      :: lspn1(:)
    integer, intent(in)      :: lpty1(:)
    real(wp), intent(in)     :: enat(:)
    real(wp), intent(out)    :: est(:)
    integer                  :: linke(nast), linkc(nast)
    real(wp)                 :: expen(nast)
    integer                  :: linkea(nast), linkca(nast)
    integer                  :: nast1, naver, ist, l, isp, ipi, iav
    integer                  :: kst, nwtsum, nweit, i, nj, j
    real(wp)                 :: expval, ajay, emeane, emeanc, sume, sumj
    real(wp)                 :: ajweit, deltem, ground, en, dif
    real(wp), parameter      :: ryd = 109737.40000_wp

    read (fi,*) nast1         ! # input states
    naver = 0
    do ist = 1, nast
       linke(ist) = 0
       linkc(ist) = 0
       linkea(ist) = 0
       linkca(ist) = 0
    end do
    ist_loop: do ist = 1, nast1
       read (fi,*) l, isp, ipi, iav
       do kst = 1, nast
          if (l == ll1(kst) .and. isp == lspn1(kst) .and. &
               ipi == lpty1(kst)) then
             if (linkc(kst) == 0) then
                linkc(kst) = ist
                linke(ist) = kst
                if (iav > 0) then
                   naver = naver + 1
                   linkea(naver) = kst
                   linkca(naver) = ist
                end if
                cycle ist_loop
             end if
          end if
       end do
       write (fo,'(a)') 'calce error:'
       write (fo,'(a,/,(5i6))') 'linkc:', linkc(1:nast)
       write (fo,'(a,/,(5i6))') 'linke:', linke(1:nast)
       stop
    end do ist_loop

    do i = 1, nast1
       read (fi,*) nj
       if (nj == 0) then
          expen(i) = enat(linke(i)) - enat(linke(1))
          cycle
       end if
       sume = 0.0_wp
       sumj = 0.0_wp
       do j = 1, nj
          read (fi,*) expval, ajay
          ajweit = 2.0_wp * ajay + 1.0_wp
          sume = sume + expval * ajweit
          sumj = sumj + ajweit
       end do
       expen(i) = sume / (2.0_wp * sumj * ryd)
       if (i == 1) ground = expen(i)
       expen(i) = expen(i) - ground
    end do

! loop over unidentified states and make the experimental
! energy relative to the ground state equal to the calculated.
! these states will not be included in the averaging procedure.
    do ist = nast1+1, nast
       do kst = 1, nast
          if (linkc(kst) == 0) then
             linkc(kst) = ist
             linke(ist) = kst
             exit
          end if
       end do
       expen(ist) = enat(linke(ist)) - enat(linke(1))
    end do

! calculate statistical weights of target states
! using naver states
    write (fo,'(a,i4)') 'Number of levels averaged to calculate ground &
         &shift is ', naver

! difference between calculated and experimental mean energy
    if (naver > 0) then
       nwtsum = 0
       emeanc = 0.0_wp
       emeane = 0.0_wp
       do i = 1, naver
          nweit = (ll1(linkea(i))*2 +1) * lspn1(linkea(i))
          nwtsum = nwtsum + nweit
          emeanc = emeanc + enat(linkea(i)) * nweit
          emeane = emeane + expen(linkca(i)) * nweit
       end do
       deltem = (emeanc - emeane) / nwtsum
    else
       deltem = enat(linke(1))
    end if

! calculate energies required by stgham
! ****if the unidentified levels are not to be shifted
! the do loop must be split into two loops -
! up to nast1 including shift - after nast1 copy without shift
    do i = 1, nast
       est(linke(i)) = expen(i) + deltem
    end do

! print energy differences:
    write (fo,'(/,a)') 'experimental energies (au)'
    write (fo,'(5f14.7)') est(1:nast)
    write (fo,'(/,a)') 'energy levels relative to ground state&
         &  (ryd)'
     write (fo,'(a)') '    l   s  pi       calc      expt      diff '
     write (fo,'(a)') '    -   -  --       ----      ----      ----'
     do i = 1, nast
        en = 2.0_wp * (enat(linke(i)) - enat(linke(1)))
        dif = en - 2.0_wp * expen(i)
        write (fo,'(1x,3i4,2x,3f10.6)') ll1(linke(i)), &
             lspn1(linke(i)), lpty1(linke(i)), en, 2.0_wp*expen(i), dif
     end do
   end subroutine calce

   subroutine calce_w (ll1, lspn1, lpty1, nast, enat, est)
! Use experimental information to shift calculated target energies
! Watts version
! This version allows  selected  levels to be included in the averaging
    integer, intent(in)      :: nast       ! # atomic states
    integer, intent(in)      :: ll1(:)     ! state orbital a.m. values
    integer, intent(in)      :: lspn1(:)   ! state spin multiplicities
    integer, intent(in)      :: lpty1(:)   ! state parities
    real(wp), intent(in)     :: enat(:)    ! state energies (a.u.)
    real(wp), intent(out)    :: est(:)     ! shifted state energies
    integer                  :: linke(nast), linkc(nast)
    real(wp)                 :: expen(nast)
    integer                  :: linkea(nast), linkca(nast)
    integer                  :: nast1, naver, ist, l, isp, ipi, iav
    integer                  :: kst, nwtsum, nweit, i, nj, j, nref
    real(wp)                 :: expval, ajay, emeane, emeanc, sume, sumj
    real(wp)                 :: ajweit, deltem, ground, en, dif, shift
    real(wp), parameter      :: ryd = 109737.40000_wp
  
    read (fi,*) nast1     ! # observed levels
    if (nast1 /= nast) then
       write (fo,'(a,i4)') 'calce: # calculated energies = ', nast
       write (fo,'(a,i4)') 'calce: does not match observed # levels = ',&
            nast1
       stop
    end if

    naver = 0
    do ist = 1, nast
       linke(ist) = 0
       linkc(ist) = 0
       linkea(ist) = 0
       linkca(ist) = 0
    end do

    observed_levels: do ist = 1, nast1
       read (fi,*) l, isp, ipi, iav     ! L, S, pi
       do kst = 1, nast
          if (l == ll1(kst) .and. isp == lspn1(kst) .and. &
               ipi == lpty1(kst)) then
             if (linkc(kst) == 0) then
                linkc(kst) = ist
                linke(ist) = kst
                if (iav > 0) then
                   naver = naver + 1
                   linkea(naver) = kst
                   linkca(naver) = ist
                end if
                cycle observed_levels
             end if
          end if
       end do
       write (fo,'(a)') 'calce error:'
       write (fo,'(a,/,(5i6))') 'linkc:', linkc(1:nast)
       write (fo,'(a,/,(5i6))') 'linke:', linke(1:nast)
       stop
    end do observed_levels

    levels: do i = 1, nast1
       read (fi,*) nj
       if (nj == 0) then
          read (fi,*) nref
          if (nref >= i) then
             write (fo,*) 'input error'
             stop
          end if
          expen(i) = enat(linke(i)) - enat(linke(nref)) + expen(nref)
          cycle levels
       end if
       sume = 0.0_wp
       sumj = 0.0_wp
       do j = 1, nj
          read (fi,*) expval, ajay
          ajweit = 2.0_wp * ajay + 1.0_wp
          sume = sume + expval * ajweit
          sumj = sumj + ajweit
       end do
       expen(i) = sume / (2.0_wp * sumj * ryd)
       if (i == 1) ground = expen(i)
       expen(i) = expen(i) - ground
    end do levels

! calculate statistical weights of target states using the first 
! naver states
    write (fo,'(a,i4)') '# levels averaged to calculate ground &
         &shift = ', naver
    if (naver > 0) then
! calculate the difference between the calculated mean energy
! and the experimental mean energy
       nwtsum = 0
       emeanc = 0.0_wp   ! calculated mean energy
       emeane = 0.0_wp   ! experimental mean energy
       do i = 1, naver
          nweit = (2 * ll1(linkea(i)) + 1) * lspn1(linkea(i))
          nwtsum = nwtsum + nweit
          emeanc = emeanc + enat(linkea(i)) * nweit
          emeane = emeane + expen(linkca(i)) * nweit
       end do
       write (fo,'(/a,e14.7)') 'emeanc = ', emeanc
       write (fo,'(a,e14.7)') 'emeane = ', emeane
       deltem = (emeanc - emeane) / nwtsum
    else
       deltem = enat(linke(1))
    end if

! calculate energies required by stgham
    do i = 1, nast
       est(linke(i)) = expen(i) + deltem
    end do

! write energy differences
    write (fo,'(/a/)') 'Experimental energies (au)'
    write (fo,'(5f14.7)') est(1:nast)
    write (fo,'(/a/)') 'Energy levels relative to ground state (Ryd)'
    write (fo,'(/a)') '    l   s  pi       calc      expt      diff&
         &     shift '
    write (fo,'(a)')  '    -   -  --       ----      ----      ----&
         &     -----'
    do i = 1, nast
       en = 2.0_wp * (enat(linke(i)) - enat(linke(1)))
       dif = en - 2.0_wp * expen(i)
       shift = 2.0_wp * (est(linke(i)) - enat(linke(i)))
       write (fo,'(1x,3i4,2x,4f10.6)') ll1(linke(i)), lspn1(linke(i)), &
            lpty1(linke(i)), en, 2.0_wp*expen(i), dif, shift
    end do
  end subroutine calce_w

  subroutine order (en, nchan, norder)
! define norder, which points to values of en in ascending order.
    integer, intent(in)      :: nchan
    integer, intent(out)     :: norder(nchan) ! position (decreasing)
    real(wp), intent(in)     :: en(nchan) ! energies (arb order input)
    integer                  :: m, i1, j1, i, j
    real(wp)                 :: x

    norder(1) = 1
    if (nchan == 1) return
    do m = 2, nchan
       j = m
       x = en(j)
       j1 = j - 1
       do i = 1, j1
          if (j >= m) then
             i1 = norder(i)
             if (x >= en(i1)) cycle
          end if
          j = j - 1
          norder(j+1) = norder(j)
       end do
       norder(j) = m
    end do
  end subroutine order

end module stghrd_mod
