module mcb_mod
! Time-stamp: "2005-02-04 10:52:23 dlcjn1"
  use precisn, only: wp
  use io_units, only: fo, jbufiv2d, jbuffv2d
  use error_prt, only: alloc_error
  use rm_data, only: nrang2
  implicit none

  real(wp), allocatable, save   :: rbcl(:), rbcv(:)

  private
  public mcb

contains

  subroutine mcb (mpol, jnb, jnbi, ncnati, isetch, l2pi, lrglj, fac, &
       maxorb, istch, inch, lrang1, lrgli2, idirbc)
    use set_data, only: nset, lset, nstat
    use symmetry_check, only: npset
    use states, only: nsetst
    use rad_data, only: nvary
    use angular_momentum, only: dracah
    use imbc_data, only: imbc, imbci
    integer, intent(in)     :: mpol
    integer, intent(in)     :: lrglj
    integer, intent(in)     :: jnb, jnbi
    integer, intent(in)     :: ncnati(:)
    integer, intent(in)     :: isetch(:)
    integer, intent(in)     :: l2pi(:)
    integer, intent(in)     :: maxorb, lrang1
    integer, intent(in)     :: istch(:)
    integer, intent(in)     :: inch
    integer, intent(inout)  :: idirbc
    real(wp), intent(in)    :: fac
    integer, intent(in)     :: lrgli2
    real(wp)                :: ractab(lrang1)
    integer                 :: jcsnp, ich, is, ncount, lcfgi, lcfgi2
    integer                 :: ichan, isetchn, li, li2, nrai, lslo1
    integer                 :: lslo2, lsiglo, lsighi, lsig, lsig2
    integer                 :: mpol2, status, lrglj2
    real(wp)                :: rac

    if (allocated(rbcl)) then
       deallocate (rbcl, stat=status)
       if (status /= 0) call alloc_error (status, 'mcb', 'd')
    end if
    if (allocated(rbcv)) then
       deallocate (rbcv, stat=status)
       if (status /= 0) call alloc_error (status, 'mcb', 'd')
    end if
    allocate (rbcl(nrang2), rbcv(nrang2), stat=status)
    if (status /= 0) call alloc_error (status, 'mcb', 'a')

!HUGO is this variable uninitialised?
    do is=1,lrang1
      ractab(is)=0._wp
    enddo
    mpol2 = 2 * mpol
    lrglj2 = 2 * lrglj
    jcsnp_loop: do jcsnp = 1, jnb ! init tgt states
       ich = 0
       is_loop2: do is = 1, nset
          if (jnbi > npset) then
             ich = ich + nstat(is) * ncnati(nsetst(is,1))
             cycle
          end if
          if (imbci(is,jnbi) == -999) then
             ich = ich + nstat(is) * ncnati(nsetst(is,1))
             cycle
          end if
          ncount = imbc(jcsnp,is,jnbi)
          if (ncount /= 0) then
             lcfgi = lset(is)
             lcfgi2 = lcfgi + lcfgi

             do ichan = 1,ncnati(nsetst(is,1))
                isetchn = isetch(is) - 1
                li = l2pi(isetchn+ichan)
                li2 = li+li
                nrai = nvary(li+1)

                lslo1 = ABS(li-mpol)
                if (MOD(lslo1+li+mpol,2) == 1) lslo1 = lslo1 + 1
                lslo2 = ABS(lrglj-lcfgi)
                if (MOD(lslo2+lrglj+lcfgi,2) == 1) lslo2 = lslo2 +1

                lsiglo = MAX(lslo1,lslo2)
                lsighi = MIN(lrglj+lcfgi, li+mpol)
                lsighi = MIN(lrang1-1, lsighi)

                do lsig = lsiglo, lsighi, 2
                   lsig2 = lsig + lsig
                   call dracah (lrgli2, lrglj2, li2, lsig2, mpol2,&
                        lcfgi2, rac)
                   ractab(lsig+1) = rac
                   print *,' RACAH ',lsig,lrgli2,lrglj2,li2,lsig2,mpol2,lcfgi2,rac
                end do
                call docb (jcsnp, ncount, maxorb, is, li, ichan,   &
                     istch, nrai, lcfgi, lrglj, ractab, fac, mpol, &
                     jnb, jnbi, idirbc)
             end do
          end if
          ich = ich + nstat(is) * ncnati(nsetst(is,1))
       end do is_loop2
       if (ich /= inch) then ! not all channels treated
          write (fo,'(a)') 'mcb: wrong # of initial channels treated'
          stop
       end if
    end do jcsnp_loop
  end subroutine mcb

  subroutine docb (jcsnp, ncount, maxorb, is, li, ichan, istch, nrai,&
       lcfgi, lrglj, ractab, fac, mpol, jnb, jnbi, idirbc) 
    use rad_data, only: maxnc
    use comp_data, only: njcomp, ljcomp
    use states, only: nsetst, aij
    use angular_momentum, only: rme
    use set_data, only: nast, nstat
    use bcang, only: irs2, abc2, getbcang
    use amul, only: amul1, amul2, mvel, icpt
    integer, intent(in)                  :: jcsnp
    integer, intent(in)                  :: ncount
    integer, intent(in)                  :: maxorb
    integer, intent(in)                  :: is
    integer, intent(in)                  :: li
    integer, intent(in)                  :: ichan
    integer, intent(in)                  :: mpol
    integer, intent(in)                  :: lcfgi, lrglj
    integer, intent(in)                  :: nrai
    real(wp), intent(in)                 :: fac
    real(wp), intent(in)                 :: ractab(:)
    integer, intent(in)                  :: jnb, jnbi
    integer, intent(inout)               :: idirbc
    integer                              :: istch(nast)
    integer                              :: ici, nq, ii1, icsn, jsig
    integer                              :: lsigj, nsigj, maxncj, issc
    integer                              :: nsym, ist, istart, ni
    real(wp)                             :: sign5, bc1

!   print *,'ENTER ',amul1(1,1),amul2(1,1)
    if (ncount == 0) return
    call getbcang (is, jcsnp, jnb, jnbi, idirbc, jbufiv2d, jbuffv2d)
    tgt_cfgs: do ici = 1, ncount   ! non zero contribution to ang part
       ii1 = irs2(ici)
       icsn = ii1 / (maxorb+1)
       jsig = MOD(ii1, maxorb+1)
       lsigj = ljcomp(jsig)
       nsigj = njcomp(jsig)
       maxncj = maxnc(lsigj+1)
       if (MOD(lsigj+li+mpol,2) == 0 .and. ABS(lsigj-li) <= mpol &
            .and. lsigj+li >= mpol) then
          sign5 = 1.0_wp
! POTENTIAL PROBLEM IF |L-L'|=0?
          if (MOD(lcfgi+lsigj+lrglj,2) == 1) sign5 = -1.0_wp
! locate b-c radial integral
          call locmbc (nsigj, lsigj+1, li+1, mpol, maxncj, nrai)
          init_states: do nsym = 1, nstat(is)
             ist = nsetst(is,nsym)
             issc = istch(ist) + ichan - 1   ! position for writing
             istart = icpt(issc)
             bc1 = sign5 * fac * abc2(ici) & 
                  * ractab(lsigj+1) * rme(li,lsigj,mpol) * aij(ist,icsn)
             print *,lsigj,ractab(lsigj+1)
             continuum: do ni = 1, nrai
                nq = ni + istart - 1
                amul1(nq,jcsnp) = amul1(nq,jcsnp) + bc1 * rbcl(ni)
             end do continuum
             if (mvel == 2) then ! velocity radial integrals antisymmetric
                do ni = 1,nrai
                   nq = ni + istart - 1
                   amul2(nq,jcsnp) = amul2(nq,jcsnp) - bc1 * rbcv(ni)
                end do
             end if
          end do init_states
       end if
    end do tgt_cfgs
!   print *, ' MCB ',amul1(1,1),amul2(1,1)
  end subroutine docb

  subroutine locmbc (ni, li, lj, mpol,  maxnci, nraj)
! locate the bound continuum multipole integral
    use radial_integrals, only: ibcpol, rmbc
    integer, intent(in)       :: ni
    integer, intent(in)       :: li
    integer, intent(in)       :: lj
    integer, intent(in)       :: mpol
    integer, intent(in)       :: maxnci
    integer, intent(in)       :: nraj
    integer                   :: istart, ivel, i1

    istart = ibcpol(1,1)
    ivel = 1
    if (mpol == 1) ivel = 2
    i1 = ibcpol(li,lj) - istart + (ni-maxnci-1) * nraj * ivel
    if (i1 < 0) then
       write (fo,'(a)') 'locmbc error: ni, li, lu, istart, i1 = ',&
            ni, li, lj, istart, i1
       stop
    end if
    rbcl(1:nraj) = rmbc(i1+1:i1+nraj)
    if (mpol == 1) then
       i1 = i1 + nraj
       rbcv(1:nraj) = rmbc(i1+1:i1+nraj)
    end if
  end subroutine locmbc
end module mcb_mod
