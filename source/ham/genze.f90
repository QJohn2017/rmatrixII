module z_coeffs
! Table of Z-coefficients
! Time-stamp: "03/04/15 12:44:01 cjn"
  use precisn, only: wp
  use io_units, only: fo
  implicit none

  type zt
     private
     real(wp), pointer        :: ztab(:,:) => NULL()
  end type zt

  type(zt), allocatable, target, save :: ztable(:,:)

  private
  public genze, zc, kilze

contains

  function zc (i, j)
! get pointer to Z-table submatrix
    real(wp), pointer      :: zc(:,:)
    integer, intent(in)    :: i, j
    zc => ztable(i,j)%ztab
  end function zc

  subroutine kilze
! kill Z-table
    integer                :: n, m, i, j, status
    n = SIZE(ztable, DIM=1)
    m = SIZE(ztable, DIM=2)
    do j = 1, m
       do i = 1, n
          deallocate (ztable(i,j)%ztab, stat=status)
          if (status /= 0) then
             write (fo,'(a,i6)') 'kilze: deallocation error = ', status
             stop
          end if
       end do
    end do
    deallocate (ztable, stat=status)
    if (status /= 0) then
       write (fo,'(a,i6)') 'kilze: deallocation error = ', status
       stop
    end if
  end subroutine kilze

  subroutine genze
! calculate table of z  coefficients for current lrgl,npty
! nb  actually calculates z*(2*lam+1)**.5
    use rad_data, only: lrang1, lrang2
    use l_data, only: lrgl, npty, ltot, lval, nscol, nsetl
    use set_data, only: lset, lrang3
    use angular_momentum, only: dracah, rme
    integer              :: status, lrgl2, ikl1, kl, lj, lj2, i
    integer              :: lcfg, lcfg2, ikl, is, lrho, lrho2, lammin
    integer              :: mx, lam, klam, lam2, kapmin, kapmax, mxp
    integer              :: kap, kap2
    real(wp)             :: rm, rac

    mx = SUM(nscol(1:ltot))
    allocate (ztable(lrang1,mx), stat=status)
    if (status /= 0) then
       write (fo,'(a,i6)') 'genze: allocation error = ', status
       stop
    end if
    mxp = 1 + (lrang1 + lrang3)/2
    do ikl = 1, mx
       do kl = 1, lrang1
          allocate (ztable(kl,ikl)%ztab(lrang1+lrang3,mxp), &
               stat=status)
          if (status /= 0) then
             write (fo,'(a,i6)') 'genze: allocation error = ', status
             stop
          end if
          ztable(kl,ikl)%ztab = 0.0_wp
       end do
    end do

    lrgl2 = lrgl + lrgl
    active_shell: do lrho = 0, lrang1-1     ! l_rho
       lrho2 = lrho + lrho
       ikl = 0
       continuum: do kl = 1, ltot
          lj = lval(kl)                   ! l_j
          lj2 = lj + lj
          ikl1 = ikl
          lammin = ABS(lrho -lj)
          lambda: do lam = lammin, lrho+lj, 2   ! lambda
             lam2 = lam + lam
             rm = rme(lam,lj,lrho) / SQRT(REAL(lam2+1,wp))
             ikl = ikl1
             targets: do i = 1, nscol(kl)
                ikl = ikl + 1
                is = nsetl(i,kl)
                lcfg = lset(is)              ! L_j
                lcfg2 = lcfg + lcfg
                kapmax = MIN(lcfg+lrho, lrgl+lam)
                kapmin = MAX(ABS(lcfg-lrho), ABS(lrgl-lam))
                kappa: do kap = kapmin, kapmax    ! kappa
                   kap2 = kap + kap
                   call dracah (kap2, lcfg2, lam2, lj2, lrho2, &
                        lrgl2, rac)
                   klam = (lam - lammin) / 2
                   ztable(lrho+1,ikl)%ztab(kap+1,klam+1) = rac * rm
                end do kappa
             end do targets
          end do lambda
       end do continuum
    end do active_shell
  end subroutine genze
end module z_coeffs
