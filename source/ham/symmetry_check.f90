module symmetry_check
! check locally generated symmetry lists with those written
! on ANG files
  use rm_data, only: lrgslo, lrgsup, lrgp
  use error_prt, only: alloc_error
  use io_units, only: fo
  implicit none

  integer, save                :: npset
  integer, allocatable, save   :: lset1(:), ipset1(:), isset1(:)
  integer, save                :: lrgllo, lrglup, lrglb

  private
  public get_npset, gen_np1_symms, compare_symms, compare_tgt_symms
  public npset, set_lrange

contains

  subroutine set_lrange (l1, l2, l3)
! input allowd range of total L-values
    integer, intent(in)    :: l1
    integer, intent(in)    :: l2
    integer, intent(in)    :: l3
    lrgllo = l1
    lrglup = l2
    lrglb = l3
  end subroutine set_lrange

  function get_npset ()
! find total number of N+1 symmetry sets
    integer                :: get_npset
    integer                :: isnp, ilup, il, lrgplo, lrgpup, ip
    integer                :: is

    isnp = 0
    ilup = MIN(lrglb, lrglup)
    do il = lrgllo, ilup
       if (lrgp < 0) then ! Both parities are required
          lrgplo = 0
          lrgpup = 1
       else              ! only the parity lrgp is required
          lrgplo = lrgp
          lrgpup = lrgplo
       end if
       do ip = lrgplo, lrgpup
          do is = lrgsup, lrgslo, -2
             isnp = isnp + 1
          end do
       end do
    end do
    get_npset = isnp
    print *,'ISNP : ',isnp
  end function get_npset

  subroutine gen_np1_symms
! generate N+1 symmetry quantum numbers
    integer            :: status, isnp, ilup, il, lrgplo, lrgpup, ip, is

    npset = get_npset()
    print *,'NPSET : ',npset
    allocate (lset1(npset), isset1(npset), ipset1(npset), &
         stat=status)
    if (status /= 0) call alloc_error (status, 'ang:sets', 'a')
    isnp = 0
    ilup = MIN(lrglb, lrglup)
    total_L: do il = lrgllo, ilup
       if (lrgp < 0) then  ! both parities required
          lrgplo = 0
          lrgpup = 1
       else  ! lrgp defines the required total parity, even/odd
          lrgplo = lrgp
          lrgpup = lrgplo
       end if
       do ip = lrgplo, lrgpup
          do is = lrgsup, lrgslo, -2
             isnp = isnp + 1
             lset1(isnp) = il
             isset1(isnp) = is
             ipset1(isnp) = ip
          end do
       end do
    end do total_L
  end subroutine gen_np1_symms

  subroutine compare_symms (npseta, lseta, ipseta, isseta)
! check ANG symmetry sets against locally generated values
    integer, intent(in)   :: npseta         ! # symmetry sets
    integer, intent(in)   :: lseta(npseta)  ! L values
    integer, intent(in)   :: ipseta(npseta) ! P values
    integer, intent(in)   :: isseta(npseta) ! 2S+1 values
    integer               :: i

    print *,'NPSET : ',npset
    if (npseta /= npset) then
       write (fo,'(a)') 'compare_symms: Total number of N+1 symmetries &
            &does not match'
       write (fo,'(a,2i6)') 'npset(ANG), npset(HAM) = ', npseta, npset
       stop
    end if
    if (ANY(lseta /= lset1) .or. ANY(ipseta /= ipset1) .or. &
         ANY(isseta /= isset1)) then
       write (fo,'(a)') 'compare_symms: symmetry mismatch'
       write (fo,'(a)') 'ANG:    L    P    S HAM:    L    P    S'
       do i = 1, npset
          write (fo,'(2(4x,3i5))') lseta(i), ipseta(i), isseta(i), &
               lset1(i), ipset1(i), isset1(i)
       end do
       stop
    end if
  end subroutine compare_symms

  subroutine compare_tgt_symms (nseta, lseta, ipseta, isseta)
! check ANG symmetry sets against locally generated values
    use set_data, only: nset, lset, ipset, isset
    integer, intent(in)   :: nseta         ! # symmetry sets
    integer, intent(in)   :: lseta(nseta)  ! L values
    integer, intent(in)   :: ipseta(nseta) ! P values
    integer, intent(in)   :: isseta(nseta) ! 2S+1 values
    integer               :: i

    if (nseta /= nset) then
       write (fo,'(a)') 'compare_tgt_symms: Total number of target &
            &symmetries does not match'
       write (fo,'(a,2i6)') 'nset(ANG), nset(HAM) = ', nseta, nset
       stop
    end if
    if (ANY(lseta /= lset) .or. ANY(ipseta /= ipset) .or. &
         ANY(isseta /= isset)) then
       write (fo,'(a)') 'compare_tgt_symms: symmetry mismatch'
       write (fo,'(a)') 'ANG:    L    P    S HAM:    L    P    S'
       do i = 1, nset
          write (fo,'(2(4x,3i5))') lseta(i), ipseta(i), isseta(i), &
               lset(i), ipset(i), isset(i)
       end do
       stop
    end if
  end subroutine compare_tgt_symms

end module symmetry_check
