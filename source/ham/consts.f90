module consts
! Time-stamp: "02/01/25 07:50:29 cjn"
  use precisn, only: wp
  implicit none
  real(wp), parameter           :: eps = 1.0e-12_wp

  private
  public eps
end module consts
