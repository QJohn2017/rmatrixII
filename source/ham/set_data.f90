module set_data
! Time-stamp: "02/02/01 07:21:23 cjn"
  use io_units, only: fi, fo
  implicit none

!sets:
  integer, save                :: nset
  integer, allocatable, save   :: lset(:)
  integer, allocatable, save   :: isset(:)
  integer, allocatable, save   :: ipset(:)
! state:
  integer, save                :: nast
  integer, allocatable, save   :: nstat(:)
  integer, save                :: isplo
  integer, save                :: ispup
! rad:
  integer, save                :: lrang1
  integer, save                :: llpdim
  integer, save                :: lrang3
  integer, save                :: spn

  integer, save                :: nss

  private
  public   rd_set_data
  public   nset, lset, isset, ipset, nast, nstat, nss
  public   isplo, ispup, lrang1, llpdim, lrang3, spn

contains

  subroutine rd_set_data (nset_tmp, lrgslo, lrgsup)
    use error_prt, only: alloc_error
    integer, intent(in)        :: nset_tmp
    integer, intent(inout)     :: lrgslo, lrgsup
    integer                    :: is, isran1, isran2, i, status

    nset = nset_tmp
    allocate (lset(nset), isset(nset), ipset(nset), nstat(nset), &
         stat=status)
    if (status /= 0) call alloc_error (status, 'rd_set_data', 'a')

    nast = 0
    do is = 1, nset
       read (fi,*) lset(is), isset(is), ipset(is), nstat(is)
       nast = nast + nstat(is)
    end do
    lrang3 = 0
    isran2 = 0
    isran1 = 99
    do i = 1, nset
       lrang3 = MAX(lset(i), lrang3)
       isran1 = MIN(isset(i), isran1)
       isran2 = MAX(isset(i), isran2)
    end do
    lrang3 = lrang3 + 1
    llpdim = lrang3 + lrang3 - 1
    nss = MAXVAL(nstat)
    if (lrgslo < 0 .and. lrgsup == 0) then
       lrgslo = isran1 - 1
       if (lrgslo == 0) lrgslo = 2
       lrgsup = isran2 + 1
    end if
    if (MOD(lrgslo+isran1, 2) == 0 .or. MOD(lrgsup+isran2, 2) == 0) &
         then
       write (fo,'(a,i3,a,i3)') 'rd_set_data error: lrgslo = ', lrgslo,&
            ' lrgsup = ', lrgsup
       stop
    end if
    isplo = isran1
    ispup = isran2
    spn = isran2
  end subroutine rd_set_data
end module set_data
