module sc_ham
! set up and diagonalize scattering Hamiltonians
! Time-stamp: "2005-02-04 10:23:29 dlcjn1"
  use precisn, only: wp
  use io_units, only: fo, jbuff1, jbuff4, jibb, jfbb, jibc, jfbc
  use debug, only: bug6
  use error_prt, only: alloc_error
  implicit none

  private
  public bindx
  public ivbci, ivbcr, i1bc, ibc, irkbc
  public i1bb, nr1bb
  public npcset, npset

  integer, pointer, save     :: ivbbi(:)=>NULL()
  integer, pointer, save     :: ivbbr(:)=>NULL()


  integer, pointer, save     :: i1bb(:)=>NULL() ! 1e b-b rad int ptrs
  integer, save              :: nr1bb        ! size of r1bb
  real(wp), pointer, save    :: r1bb(:)=>NULL() ! 1e b-b rad integrals
  integer, pointer, save     :: irkbb(:,:,:)=>NULL() !2e BB rad int ptrs
  integer, save              :: nrkbb        ! size of rkbb
  real(wp), pointer, save    :: rkbb(:)=>NULL() ! 2e b-b rad integrals

  integer, allocatable, save :: ivbci(:,:)
  integer, allocatable, save :: ivbcr(:,:)
  integer, allocatable, save :: i1bc(:)
  integer, allocatable, save :: ibc(:)
  integer, allocatable, save :: irkbc(:,:,:)

  integer, save              :: npset
  integer, allocatable, save :: npcset(:)

contains

  subroutine bindx (minim)
    use rad_data, only: lrang1, lrang2
    use filehand, only: readix, ireadb, ireadc, ireada, open, startr, &
         readb, lookup
    use set_data, only: llpdim
    use bb_ham, only: import_ptrs
    use symmetry_check, only: compare_symms
    use stghrd_mod, only: lrglb
    integer, intent(in)          :: minim
    character(len=7)             :: file, stats
    integer, allocatable         :: nsch(:)
    integer                      :: l1bc, status, nlr, nlam, lbc, n, ibb
    integer                      :: mx_nsch, i, nrec, ibuff
    integer                      :: nseta
    logical                      :: real_typ
    integer, allocatable         :: lseta(:), ipseta(:), isseta(:)

    write (fo,'(a)') 'subroutine bindx'
    stats = 'old'

! read index on rad4
    call readix (jbuff4)
    if (.NOT.associated(i1bb)) then
       allocate (i1bb(lrang1), stat=status)
       if (status /= 0) call alloc_error (status, 'bindx', 'a')
    end if
    call ireadb (jbuff4, i1bb, 1, lrang1)
    call ireada (jbuff4, nr1bb)
    l1bc = MIN(lrang1, lrang2)
    allocate (i1bc(l1bc+1), stat=status)
    if (status /= 0) call alloc_error (status, 'bindx', 'a')
    call ireadb (jbuff4, i1bc, minim, l1bc+1)

! read index on rad1
    call readix (jbuff1)
    nlr = (lrang1 * (lrang1 + 1)) / 2
    nlam = 2 * lrang1 - 1
    if (.NOT.associated(irkbb)) then
       allocate (irkbb(nlam,nlr,nlr), stat=status) ! allocate ptrs irkbb
       if (status /= 0) call alloc_error (status, 'bindx', 'a')
    end if
    do n = 1, nlr
       call ireadc (jbuff1, irkbb(:,:,n), 1, nlam, 1, nlr, nlam, 0)
    end do
    call ireada (jbuff1, ibb)
    call ireada (jbuff1, nrkbb)
    lbc = MIN(3*lrang1-2, lrang2)
    allocate (irkbc(nlam,nlr,lrang1*lbc), ibc(lbc+1), stat=status)
    if (status /= 0) call alloc_error (status, 'bindx', 'a')
    do n = 1, lrang1*lbc
       call ireadc (jbuff1, irkbc(:,:,n), 1, nlam, 1, nlr, nlam, 0)
    end do
    call ireadb (jbuff1, ibc, minim, lbc+1)

! read one electron BB radial integrals into r1bb
    allocate (r1bb(nr1bb), stat=status) ! allocate integrals, r1bb
    if (status /= 0) call alloc_error (status, 'bindx', 'a')
    call lookup (i1bb(1), nrec, ibuff)
    call startr (jbuff4, nrec, ibuff)
    call readb (jbuff4, r1bb, 1, nr1bb)

! read two electron BB radial integrals into rkbb
    allocate (rkbb(nrkbb), stat=status) ! allocate integrals rkbb
    if (status /= 0) call alloc_error (status, 'bindx', 'a')
    call lookup (ibb, nrec, ibuff)
    call startr (jbuff1, nrec, ibuff)
    call readb (jbuff1, rkbb, 1, nrkbb)

    if (lrglb == -1) return ! no-exchange run
! Read ANG BB-file indices
    real_typ = .false.
    file = 'abbi'
    call open (jibb, file, stats, real_typ)
    call readix (jibb)
    call ireada (jibb, nseta)
    print *,'NSETA ',nseta
! Check consistency of target files by comparing HAM and ANG symmetries
    allocate (lseta(nseta), ipseta(nseta), isseta(nseta), ivbbi(nseta),&
         ivbbr(nseta), npcset(nseta), stat=status)
    if (status /= 0) call alloc_error (status, 'bindx', 'a')
    call ireadb (jibb, lseta, 1, nseta)
    call ireadb (jibb, ipseta, 1, nseta)
    call ireadb (jibb, isseta, 1, nseta)
    call compare_symms (nseta, lseta, ipseta, isseta)
    deallocate (lseta, ipseta, isseta, stat=status)
    if (status /= 0) call alloc_error (status, 'bindx', 'd')
    call ireadb (jibb, npcset, 1, nseta)
    call ireadb (jibb, ivbbi, 1, nseta)
    print *,'NSETA ',nseta
    npset = nseta

    if (SUM(npcset(1:nseta)) == 0) return !there are no bound terms

    real_typ = .true.
    file = 'abbr'
    call open (jfbb, file, stats, real_typ)
    call readix (jfbb)
    call ireadb (jfbb, ivbbr, 1, nseta)

! read ANG BC-file indices
    file = 'abci'
    real_typ = .false.
    call open (jibc, file, stats, real_typ)
    call readix (jibc)
    call ireada (jibc, nseta)
! check BC symmetry list:
    allocate (lseta(nseta), ipseta(nseta), isseta(nseta), nsch(npset),&
         stat=status)
    if (status /= 0) call alloc_error (status, 'bindx', 'a')
    call ireadb (jibc, lseta, 1, nseta)
    call ireadb (jibc, ipseta, 1, nseta)
    call ireadb (jibc, isseta, 1, nseta)
    call compare_symms (nseta, lseta, ipseta, isseta)
    deallocate (lseta, ipseta, isseta, stat=status)
    if (status /= 0) call alloc_error (status, 'bindx', 'd')
    call ireadb (jibc, nsch, 1, nseta)
    mx_nsch = MAXVAL(nsch) + 1  ! allows for extra catalog value at end
    allocate (ivbci(mx_nsch,npset), ivbcr(mx_nsch,npset), stat=status)
    if (status /= 0) call alloc_error (status, 'bindx', 'a')
    do i = 1, npset
       call ireadb (jibc, ivbci(:,i), 1, nsch(i)+1) 
    end do

    file = 'abcr'
    real_typ = .true.
    call open (jfbc, file, stats, real_typ)
    call readix (jfbc)
    do i = 1, npset
       call ireadb (jfbc, ivbcr(:,i), 1, nsch(i)+1)
    end do
    deallocate (nsch, stat=status)
    if (status /= 0) call alloc_error (status, 'bindx', 'd')

    call import_ptrs (nr1bb, nrkbb, ivbbi, ivbbr, i1bb, r1bb, irkbb, &
         rkbb)
  end subroutine bindx

end module sc_ham

