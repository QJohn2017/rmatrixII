module stmmat_mod
! Time-stamp: "2005-02-03 17:58:11 cjn"
  use precisn, only: wp
  use io_units, only: fo, jbuff1, itape1, jbufivd,    &
       jbuffvd, jbufiv2d, jbuffv2d, jbuffm, jbufim
  use debug, only: bug2
  use error_prt, only: alloc_error
  implicit none

  integer, allocatable, save  :: npcset(:)
  integer, allocatable, save  :: lpset(:)
  integer, allocatable, save  :: ipsset(:)
  integer, allocatable, save  :: ippset(:)

  private
  public stmmat, npcset

contains
  subroutine stmmat (mpol, lamax, mvely, lrgllo, lrglup, lrgp,    &
       lrgslo, lrgsup, nbcpol, nccpol)
! calculates multipole matrix elements <i||m||f>
! b-b both bound syms; b-c initial (n+1) sym bound, final (n+1) sym cont
! c-b initial (n+1) sym continuum, final (n+1) symetry bound
! c-c both continuum symetries
    use comp_data, only: njcomp, ljcomp
    use set_data, only: nset, lset, nast, nstat
    use symmetry_check, only: npset
    use states, only: nsetst, ll, lspn, lpty, aij
    use filehand, only: readb, open, readix, ireada, ireadb,&
         ireadc, newfil, catlog, iwrita, iwritb, iwritc, endw, writix, &
         close, writb
    use angular_momentum, only: dracah, rme, cg
    use crlmat_mod, only: locmbb, crl, crlv, rkcc
    use rad_data, only: nvary, maxnc, lrang1, lrang2
    use mcc_mod, only: mcc, def_coefs, wrt_coefs, zero_coefs
    use mcb_mod, only: mcb
    use mbb_mod, only: mbb, def_vsh
    use imbc_data, only: alloc_mbc
    use bcang, only: filestart2, alloc_bcang
    use radial_integrals, only: rd_radial_integrals
    use amul, only: alloc_icpt, alloc_amul, set_jsize, isize, jsize, &
         amul1, amul2
    integer, intent(in)                :: mpol
    integer, intent(in)                :: lamax
    integer, intent(in)                :: lrgllo, lrglup
    integer, intent(in)                :: lrgslo, lrgsup
    integer, intent(in)                :: nbcpol, nccpol
    integer, intent(in)                :: mvely
    integer, intent(in)                :: lrgp ! parity of N+1 state
    real(wp)                           :: sign6(nast)
    integer                            :: ncnati(nast), ncnatj(nast)
    integer                            :: l2pi(nast*lrang2), l2pj(nast*lrang2)
    integer                            :: istch(nast), jstch(nast)
    integer                            :: isetch(nset), jsetch(nset)
    integer, allocatable               :: lmij(:,:)
    integer, allocatable               :: ibcont(:)
    integer, allocatable               :: nsset(:)
    integer                            :: nstset(nast)
    integer                            :: immei(npset)
    integer                            :: immer(npset,npset)
    integer                            :: itrans(npset,npset)
    integer                            :: ischn(npset)
    character(len=7)                   :: filea, fileb, stats
    real(wp), pointer                  :: crlp(:,:)
    integer                            :: i, j, mvel, mpol2, lamind, ist
    integer                            :: jst, llpp
    integer                            :: ns, ns2
    integer                            :: lbeg, lll, lrgplo, lrgpup
    integer                            :: ipa, iss, igg, maxorb, lrglj2
    integer                            :: idirbb, idirbc
    integer                            :: ndummy, intr, inchn, lrgs
    integer                            :: lrgpi, lrgpj, lrgli, inb, inbi
    integer                            :: inch, lrgli2, ljlo
    integer                            :: lrglj, jnb, jnbi, jnch, jch
    integer                            :: inobb, nvshel, icount, i1, i2
    integer                            :: icnp1, icnp2, irev, lamlu
    integer                            :: lamup, isjs, icont1, icont2
    integer                            :: idumbb, mmax, m1, m, nrai
    integer                            :: status, ncs
    real(wp)                           :: faci, sign4, fac
    real(wp), allocatable              :: cgc(:)
    logical                            :: real_typ

    
    if (bug2 > 0) then
       write (fo,'(a)') '       subroutine stmmat'
       write (fo,'(a)') '       -----------------'
    end if
    mvel = mvely + 1
    mpol2 = mpol + mpol
    lamind = (lamax + 1) / 2

! fill in the other half of the moments matrix. 
! there is no need for that in velocity gauge where we have calculated 
! the crl matrix for all target states, due to the antisymmetry of the 
! velocity bound-bound radial integrals.
! N.B. extra factor (-1)**ll(ist)+lpty(ist) has been inserted to make 
! the matrix symmetric; taken care of by multiplying by sign6 below.

    crlp => crl(:,:,mpol)
    do ist = 2, nast
       do jst = 1, ist-1
          crlp(ist,jst) = crlp(jst,ist)
       end do
    end do

! write target reduced dipoles to d file for multiphoton codes
    open (unit=88, file='d', status='new', form='unformatted')
    do ist = 1, nast
       llpp = ll(ist) + lpty(ist)
       sign6(ist) = 1.0_wp
! -2 changed into -1
       if (MOD(llpp,2) == 1) sign6(ist) = - 1.0_wp
    end do
    do jst = 1, nast
       do ist = 1, nast
          crlp(ist,jst) = crlp(ist,jst) * sign6(ist)
       end do
    end do
    write (88) crlp(1:nast,1:nast)     ! length gauge dipoles
    do jst = 1, nast
       do ist = 1, nast
          crlv(ist,jst) = crlv(ist,jst) * sign6(ist)
       end do
    end do
    write (88) crlv(1:nast,1:nast)     ! velocity gauge dipoles
    close (88)

! define correspondance between states and sets (reverse of nsetst)
    do i = 1, nset
       do j = 1, nstat(i)
          ist = nsetst(i,j)     ! target # for sym i and state j
          nstset(ist) = i       ! symmetry for target # ist
       end do
    end do

! read radial integral indexes
! (BB radial integrals stored in rkcc from crlmat)
    call rd_radial_integrals (lrang1, lrang2, nbcpol, nccpol)

! open ANG BB files:
    filea = 'abbmi'
    stats = 'old'
    real_typ = .false.
    call open (jbufivd, filea, stats, real_typ)
    call readix (jbufivd)
    call ireada (jbufivd, ns)
    if (ns /= npset) then
       write (fo,'(a,a5,a)') 'stmmat error: wrong ', filea, ' file'
       write (fo,'(a,2i10)') 'ns, npset = ', ns, npset
       stop
    end if
    ns2 = (ns * (ns + 1)) / 2 + 1
    allocate (lmij(ns,ns), nsset(ns), ibcont(ns2), stat=status)
    if (status /= 0) call alloc_error (status, 'stmmat', 'a')
    call ireadb (jbufivd, nsset, 1, ns)
    call ireadc (jbufivd, lmij, 1, ns, 1, ns, ns,0)
    call ireadb (jbufivd, ibcont, 1, ns2)

    fileb = 'abbmr'
    real_typ = .true.
    call open (jbuffvd, fileb, stats, real_typ)

! open ANG b-c files:
    filea = 'abcmi'
    fileb = 'abcmr'

    real_typ = .false.
    call open (jbufiv2d, filea, stats, real_typ)
    real_typ = .true.
    call open (jbuffv2d, fileb, stats, real_typ)
    call readix (jbufiv2d)
    call ireada (jbufiv2d, ns)
    if (ns /= npset) then
       write (fo,'(a,a5,a)') 'stmmat error, wrong ', filea, ' file'
       stop
    end if
    allocate (npcset(npset), lpset(npset), ipsset(npset), &
         ippset(npset), stat=status)
    if (status /= 0) call alloc_error (status, 'stmmat', 'a')
    call ireadb (jbufiv2d, npcset, 1, npset)
    call ireadb (jbufiv2d, lpset, 1, npset)
    call ireadb (jbufiv2d, ipsset, 1, npset)
    call ireadb (jbufiv2d, ippset, 1, npset)

    ncs = MAXVAL(npcset)
    call alloc_bcang (ncs)

    if (npset == 0) then
       lbeg = lrgllo - 1
    else
       lbeg = lpset(npset)
    end if
    igg = npset
    do lll = lbeg + 1, lrglup
       if (lrgp < 0) then
          lrgplo = 0
          lrgpup = 1
       else
          lrgplo = lrgp
          lrgpup = lrgplo
       end if
       do ipa = lrgplo, lrgpup 
          do iss = lrgsup, lrgslo, -2
             igg = igg + 1
             lpset(igg) = lll
             ipsset(igg) = iss
             ippset(igg) = ipa
          end do
       end do
    end do
    if (igg /= npset) then
       write (fo,'(a,3i6)') 'stmmat! npset, igg, npset = ', &
            npset, igg, npset 
       stop
    end if
    write (fo,'(/,a)') 'stmmat: full set of symmetries:'
    write (fo,'(a)') '      i npcset  lpset ipsset ippset'  
    do i = 1, npset
       write (fo,'(5i7)') i, npcset(i), lpset(i), ipsset(i), ippset(i)
       print *,'Next one'
    end do
    print *,'Got done'

    call ireada (jbufiv2d, maxorb)    ! read maxorb
    call alloc_mbc (nset, jbufiv2d, npcset)
    idirbb = 0    ! ang file position counter
    idirbc = 0    ! ang file position counter

! write multipole matrix elements to disk
    filea = 'mulei'
    fileb = 'muler'
    stats = 'unknown'
    real_typ = .false.
    call open (jbufim, filea, stats, real_typ)
    real_typ = .true.
    call open (jbuffm, fileb, stats, real_typ)
    call newfil (jbufim, ndummy)
    call newfil (jbuffm, ndummy)

!initialise pointer arrays
    do i = 1, npset   ! channel information
       ischn(i) = 0
       immei(i) = -999
       do j = 1, npset    !transition information
          immer(j,i) = -999
          itrans(j,i) = 0
       end do
    end do

    if (mpol == 1) call def_coefs (nast*lrang2)
    intr = 0
    inchn = 0
    S: do lrgs = lrgslo, lrgsup, 2
       Pi: do lrgpi = 0, MOD(mpol+1,2)
          lrgpj = MOD(lrgpi+mpol,2)     ! final parity
          L: do lrgli = lrgllo, lrglup

! inb = # cfgs; inbi is the symmetry set #
             call find_sym (lrgli, lrgs, lrgpi, inb, inbi)
             print *,' SYM I ',lrgli, lrgs, lrgpi, inb,inbi

! inch = # channels; ncnati = chann/state; l2pi = cont a.m.; state;set
             call find_chl (lrgli, lrgs, lrgpi, nast, nset, nstset,&
                  ncnati, l2pi, istch, isetch, inch)

             if (ischn(inbi) == 0) then ! store channel info to disk
                inchn = inchn + 1
                ischn(inbi) = inchn
                call catlog (jbufim, immei(inbi))
                call iwrita (jbufim, inch)
                call iwrita (jbufim, inb)
                call iwritb (jbufim, l2pi, 1, inch)
                do i = 1, inch
                   call iwrita (jbufim, nvary(l2pi(i)+1))
                end do
             end if

             if (inch > 0) then
                lrgli2 = lrgli + lrgli
                faci = SQRT(REAL(lrgli2+1,wp))
                sign4 = 1.0_wp
                if (MOD(lrgli+lrgpi,2) == 1) sign4 = -1.0_wp
                if (bug2 > 0) then
                   write (fo,'(a,6i6)') 'initial state: ', inbi,&
                        lrgli, lrgs, lrgpi, inch, inb
                end if

! define index of channel positions
                call alloc_icpt (inch, l2pi, inb)
             end if

             if (lrgpi /= lrgpj) then
                ljlo = MAX(ABS(mpol-lrgli), lrgllo)
             else
                ljlo = MAX(ABS(mpol-lrgli), lrgli)
             end if

             Lp: do lrglj = ljlo, MIN(mpol+lrgli,lrglup)
                if (lrgli == lrglj .and. lrglj == 0) cycle

! jnb = # cfgs, jnbi = set #
                call find_sym (lrglj, lrgs, lrgpj, jnb, jnbi)
                print *,' SYM J ',lrgli, lrgs, lrgpi, jnb,jnbi


! jnch = # channels; ncnatj = chann/state; l2pj = cont a.m.; state;set
                call find_chl (lrglj, lrgs, lrgpj, nast, nset, nstset,&
                     ncnatj, l2pj, jstch, jsetch, jnch)

                if (ischn(jnbi) == 0) then ! write channel info to disk
                   inchn = inchn + 1
                   ischn(jnbi) = inchn
                   call catlog (jbufim, immei(jnbi))
                   call iwrita (jbufim, jnch)
                   call iwrita (jbufim, jnb)
                   call iwritb (jbufim, l2pj, 1, jnch)
                   do i = 1, jnch
                      call iwrita (jbufim, nvary(l2pj(i)+1))
                   end do
                end if

                if (jnch > 0 .and. inch > 0) then
                   intr = intr + 1
                   itrans(inbi,jnbi) = intr
                   lrglj2 = lrglj + lrglj
                   fac = SQRT(REAL(lrglj2+1,wp)) * faci
                   if (bug2 > 0) then
                      write (fo,'(a,6i6)') 'final state: ', jnbi, &
                           lrglj, lrgs, lrgpj, jnch, jnb
                   end if
                   call set_jsize (jnch, l2pj, jnb)
 
! set position on writing file
                   call catlog (jbuffm, immer(inbi,jnbi))
                   call iwrita (jbuffm, isize)
                   call iwrita (jbuffm, jsize)

                   jch = 0
                   if (mpol == 1) call zero_coefs
                   call alloc_amul (mvel)
                   call mcc (nstset, ncnatj, l2pj, inb, inch,         &
                        ncnati, isetch, l2pi, lrgli, lrglj, istch,    &
                        inbi, fac, jnch, lrang1, mpol, idirbc)
! now form the c-b and b-b elements:
                   if (jnb > 0)  then !  angular b-b integrals file
                      if (jnbi > npset .or. inbi > npset) then
                         inobb = 1
                         icount = 1
                         nvshel = 0
                      else
                         if (inbi <= jnbi) then
                            i1 = inbi
                            i2 = jnbi
                            icnp1 = inb
                            icnp2 = jnb
                            irev = 1
                         else
                            i1 = jnbi
                            i2 = inbi
                            icnp1 = jnb
                            icnp2 = inb
                            irev = -1
                         end if
                         lamlu = lmij(i1,i2)
                         inobb = 0
                         if (lamlu == -999 .or. lamlu == 0) then
                            inobb = 1
                         else
                            lamup = MOD(lamlu, 100)

! transfer b-b angular integrals for lrgli - lrglj combination to vsh:
                            isjs = npset * (i1-1) - (i1*(i1-1))/2 + i2
                            icont1 = ibcont(isjs)
                            icont2 = ibcont(isjs+1) - 1
                            nvshel = icont2 - icont1 + 1
                            idumbb = idirbb
                            call filestart2 (icont1, isjs, idumbb,   &
                                 jbufivd, jbuffvd)
                            call def_vsh (nvshel, jbufivd, jbuffvd)
                            icount = 1
                         end if

                      end if
                      do j = 1, jnb
                         amul1(:,j) = 0.0_wp
                      end do
                      if (mvel == 2) then
                         do j = 1, jnb
                            amul2(:,j) = 0.0_wp
                         end do
                      end if

! c-b contribution:
                      call mcb (mpol, jnb, jnbi, ncnati, isetch, l2pi,&
                           lrglj, fac, maxorb, istch, inch, lrang1,   &
                           lrgli2, idirbc)
! b-b contribution
                      call mbb (inobb, inch, icnp1, icnp2, irev, lamup,&
                           lamind, mpol, nvshel, inbi, jnbi,    &
                           nrai, inb, sign4, icount, lamax)
                      
                      idirbb = npset * (i1-1) - (i1 * (i1-1)) / 2 + i2
                   end if

! clebsch gordan coefficients for photoionization calculation
                   if (mpol == 1) then
                      mmax = MIN(lrgli, lrglj) + 1
                      allocate (cgc(mmax), stat=status)
                      if (status /= 0) call alloc_error (status, &
                           'stmmat', 'a')
                      do m1 = 1,mmax
                         m = m1 - 1
                         cgc(m1) = cg(lrglj, 1, lrgli, m, 0, m) / &
                               SQRT(REAL(lrgli2 + 1,wp))
                      end do

                      call iwrita (jbuffm, mmax)
                      call writb (jbuffm, cgc, 1, mmax)
                      deallocate (cgc, stat=status)
                      if (status /= 0) call alloc_error (status, &
                           'stmmat', 'd')
! angular coefficients:
                      call wrt_coefs (jbuffm, inch, jnch)
                   end if
                end if
             end do Lp
          end do L
       end do Pi
    end do S

! write index file
    call endw (jbufim)
    call endw (jbuffm)
    call writix (jbufim)
    call iwrita (jbufim, mvely)
    call iwrita (jbufim, npset)
    call iwritb (jbufim, lpset, 1, npset)
    call iwritb (jbufim, ipsset, 1, npset)
    call iwritb (jbufim, ippset, 1, npset)
    print *,'Here'
    call iwritb (jbufim, npcset, 1, npset)
    print *,'Back'
    call iwritb (jbufim, immei, 1, npset)
    call iwrita (jbufim, intr)
    call iwritc (jbufim, immer, 1, npset, 1, npset, npset, 0)
    call iwritc (jbufim, itrans,1,npset, 1, npset, npset, 0)
    call endw (jbufim)

! close files
    stats = 'keep'
    call close (jbufivd, stats)
    call close (jbuffvd, stats)
    call close (jbufiv2d, stats)
    call close (jbuffv2d, stats)
    call close (jbufim, stats)
    call close (jbuffm, stats)
    deallocate (lmij, nsset, ibcont, npcset, lpset, ipsset, ippset, &
         stat=status)
    if (status /= 0) call alloc_error (status, 'stmmat', 'd')
    write (fo,'(a)') 'STMMAT complete'
  end subroutine stmmat

  subroutine find_chl (lrgl, lrgs, lrgp, nast, nset, nstset, ncnat, l2p,&
       istchn, isetchn, nch)
! find the channel structure for given lrgl, lrgs, lrgp
    use states, only: ll, lspn, lpty     ! N-state symmetries
    use rad_data, only: lrang2
    integer, intent(in)   :: lrgl          ! total L
    integer, intent(in)   :: lrgs          ! total S
    integer, intent(in)   :: lrgp          ! total parity
    integer, intent(in)   :: nast          ! # target states
    integer, intent(in)   :: nset          ! # tgt symmetries
    integer, intent(in)   :: nstset(:)     ! symm for target # 
    integer, intent(out)  :: ncnat(nast)   ! # channels / state
    integer, intent(out)  :: l2p(:)        ! cont a.m.
    integer, intent(out)  :: istchn(nast)  ! posn 1st channel of state
    integer, intent(out)  :: isetchn(nset) ! posn 1st channel of set
    integer, intent(out)  :: nch           ! # channels
    integer               :: ich, ist, lcfg, ispin, ifirst, ll1, ll2
    integer               :: ipi, il, is

    ncnat = 0               ! channels for each atomic state
    ich = 0                 ! channel counter
    tgts: do ist = 1, nast
       lcfg = ll(ist)       ! tgt l
       ispin = lspn(ist)    ! tgt s
       ipi = lpty(ist)      ! tgt p
       ifirst = 0
       if (ABS(lrgs-ispin) == 1) then   ! spins couple
          ll1 = ABS(lrgl - lcfg)
          if (MOD(ll1+ipi+lrgp,2) == 1) ll1 = ll1 + 1
          ll2 = MIN(lrgl+lcfg, lrang2-1)
          cont_l: do il = ll1, ll2, 2           ! allowed cont l-values
             ich = ich + 1
             if (ifirst == 0) ifirst = ich
             l2p(ich) = il              ! cont l for channel
             ncnat(ist) = ncnat(ist) + 1
          end do cont_l
       end if
       is = nstset(ist)         ! symmetry for target ist
       if (ifirst /= 0) then
          istchn(ist) = ifirst
          isetchn(is) = ifirst
       else 
          istchn(ist) = 0
          isetchn(is) = 0
       end if
    end do tgts
    nch = ich
  end subroutine find_chl
 
  subroutine find_sym (lrgl, lrgs, lrgp, inb, inbi)
! find the (n+1) set and the number of bound configurations
    use symmetry_check, only: npset
    integer, intent(in)         :: lrgl
    integer, intent(in)         :: lrgs
    integer, intent(out)        :: inb    ! # cfgs
    integer, intent(out)        :: inbi   ! symmetry set #
    integer                     :: i, lrgp

    inb = 0
    inbi = 0
    do i = 1, npset
       if (lrgl == lpset(i) .and. lrgp == ippset(i) .and. &
            lrgs == ipsset(i)) then
          inbi = i
          print *,'here',i
          inb = npcset(i)
          return
       end if
    end do
    write (fo,'(a)') 'find_sym: (N+1) symmetry not found'
    stop
  end subroutine find_sym
end module stmmat_mod
