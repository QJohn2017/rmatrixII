module bcang
! Time-stamp: "02/02/05 14:36:56 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use error_prt, only: alloc_error
  implicit none

  integer, save                :: idirbcst
  integer, allocatable, save   :: irs2(:)
  real(wp), allocatable, save  :: abc2(:)
  integer, allocatable, save   :: iduu2(:)
  real(wp), allocatable, save  :: aduu2(:)

  private
  public getbcang, filestart2, alloc_bcang
  public idirbcst, irs2, abc2

contains

  subroutine alloc_bcang (ncs)
    integer, intent(in)      :: ncs
    integer                  :: status
    allocate (irs2(ncs), iduu2(ncs), abc2(ncs), aduu2(ncs), stat=status)
    if (status /= 0) call alloc_error (status, 'alloc_bcang', 'a')
    idirbcst = 0
  end subroutine alloc_bcang

  subroutine getbcang (js, icsnp, inb, inbi, idirbc, jbufiv2, jbuffv2)
    use filehand, only: ireadb, readb
    use imbc_data, only: imbc, imbci
    integer, intent(in)    :: js
    integer, intent(in)    :: icsnp
    integer, intent(in)    :: inb
    integer, intent(in)    :: inbi
    integer, intent(inout) :: idirbc
    integer, intent(in)    :: jbufiv2, jbuffv2  ! ang integral unit #
    integer                :: ifirst, jss, idum, icsnpp, ncount

    ifirst = 1
    js_loop: do jss = 1, js
! initiate reading of angular integral for this js - lrgli
       idum = imbci(jss,inbi)
       if (idum /= -999) then
          if (ifirst == 1) then
             call filestart2 (idum, inbi, idirbc, jbufiv2, jbuffv2)
             ifirst = 0
          end if
! read angular integral files
          icnsp_loop: do icsnpp = 1, icsnp-1
             ncount = imbc(icsnpp,jss,inbi)
             if (ncount /= 0) then
                call ireadb (jbufiv2, iduu2, 1, ncount)
                call readb (jbuffv2, aduu2, 1, ncount)
             end if
          end do icnsp_loop
          icsnpp = icsnp
          ncount = imbc (icsnpp, jss, inbi)
          if (ncount /= 0) then
             call ireadb (jbufiv2, irs2, 1, ncount)
             call readb (jbuffv2, abc2, 1, ncount)
          end if
          do icsnpp = icsnp+1, inb
             ncount = imbc(icsnpp, jss, inbi)
             if (ncount /= 0) then
                call ireadb (jbufiv2, iduu2, 1, ncount)
                call readb (jbuffv2, aduu2, 1, ncount)
             end if
          end do
       end if
    end do js_loop
    idirbc = idirbcst
  end subroutine getbcang

  subroutine filestart2 (icat, ival, ipos, jbuf1, jbuf2)
! use filehand to prepare two files for data read-in at correct place
    use filehand, only: lookup, movef, moveb, startr
    integer, intent(in)       :: icat     ! catalogue number
    integer, intent(in)       :: ival     ! required position label
    integer, intent(inout)    :: ipos     ! current position label
    integer, intent(in)       :: jbuf1    ! file unit number
    integer, intent(in)       :: jbuf2    ! file unit number
    integer                   :: nsrec, nsbuf

    call lookup (icat, nsrec, nsbuf)   ! icat -> record, position
    if (ival > ipos) then
       call movef (jbuf1, nsrec, nsbuf)
       call movef (jbuf2, nsrec, nsbuf)
    else
       call moveb (jbuf1, nsrec, nsbuf)
       call moveb (jbuf2, nsrec, nsbuf)
    end if
    call startr (jbuf1, nsrec, nsbuf)   ! start reading into buffers
    call startr (jbuf2, nsrec, nsbuf)
    ipos = ival
  end subroutine filestart2
end module bcang
