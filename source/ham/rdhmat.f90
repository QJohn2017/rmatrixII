module rdhmat_mod
! Time-stamp: "2005-08-19 11:43:18 cjn"
  use precisn, only: wp
  use io_units, only: fo, jbuff1, jbuff4, jbufd, jibc, jfbc, ihunit
  use debug, only: bug2, bug4, bug6, bug9
  use error_prt, only: alloc_error
  implicit none

  real(wp), allocatable, save   :: hmat(:)

  private
  public rdhmat

contains

  subroutine rdhmat (ksp, isnp, lbufd, more, ecore, mpol, ivec)
! reads the hamiltonian matrix elements stored by sthmat
! arranges them in hmat ready for the householder diagonalisation
! the following debug parameters are used:
!      bug4  =  1     -   print asymptotic coefficients
!            >  1     -   print cc hamiltonian matrix
!      bug6  = 7      -   print full hamiltonian matrix
!      bug9  = 1      -   print eigenvalues
!            = 2      -   print eigenvalues and surface amplitudes
!            > 2      -   print eigenvalues, eigenvectors and
!                               surface amplitudes
    use channels, only: l2pspn, kconat, nschan, nsprec, lenham, chs
    use rad_data, only: nvary, ends, dends, vabs, splcoef, ndim
    use set_data, only: nast
    use l_data, only: lrgl, npty
    use states, only: enat
    use sc_ham, only: npset, npcset
    use rm_data, only: nrang2, neprnt, lamax, prm, asym_ptlcfs, farm
    use bb_ham, only: wrtham, hambb
    use filehand, only: catlog, iwrita, writc
    use diagonalizers, only: diag, diag_kill
    use prmat_interface, only: open_prmat_file, writh1, wrth2
    use farm_interface, only: writcf, write2, write3, write3a, write3b
    integer, intent(in)   :: ksp      ! total/target spin
    integer, intent(in)   :: isnp
    integer, intent(in)   :: lbufd    ! buffer length
    integer, intent(in)   :: more
    real(wp), intent(in)  :: ecore
    integer, intent(in)   :: mpol
    integer, intent(out)  :: ivec(:)
    character(len=4)      :: parity(0:1) = (/'even',' odd'/)
    real(wp)              :: buf1d(lbufd)
    real(wp)              :: h1mat(nrang2, nrang2)
    real(wp), allocatable :: value(:), eigvec(:,:)
    real(wp), allocatable :: vvabs(:,:), v2abs(:,:)
    real(wp), allocatable :: splwave(:,:),spltmp(:,:)
    real(wp), allocatable :: cf(:,:,:)
    real(wp), allocatable, target :: wmat(:,:), rwa(:)
    real(wp), allocatable :: wdmat(:,:)
    real(wp), pointer     :: x(:), w(:)
    integer, allocatable  :: istchs(:)
    integer               :: ifin(nrang2,chs)
    integer               :: isp, nch, nistchs, status, ist, k, ich
    character(len=7)      :: kspchar
    integer               :: jbufsc, ny, ny2, npstar, irec, irec2
    integer               :: noterm, nstrt, nra, nbb, jstart, istart
    integer               :: nrow, nra1, nra2, lp, ncsnp, no, jch, lc
    integer               :: ibuf, irow, ib, nstart, i1, np, lenhbb
    integer               :: l, n
    real(wp)              :: ground, enlevl, sm, smd

    if (ksp < 0) then
       kspchar = ' Target'
    else
       kspchar = '  Total'
    end if

    isp = ABS(ksp)
    nch = nschan(isp)              ! # channels
    if (nch == 0 .and. more /= 0) return

! construct array istchs(ich), state number coupled to channel ich
    nistchs = SUM(kconat(1:nast,isp))

    allocate (istchs(nistchs), stat=status)
    if (status /= 0) call alloc_error (status, 'rdhmat:istchs', 'a')
    ich = 0
    do ist = 1, nast
       do k = 1, kconat(ist,isp)
          ich = ich + 1
          istchs(ich) = ist
       end do
    end do

! record numbers calculated in sthmat in correct order to fill hmat.
! only the upper half of the submatrices on the diagonal of the
! Hamiltonian must be stored, all others remain rectangular.
    write (fo,'(/,a,i3,a,a4,3a,i2)') 'Diagonalising Hamiltonian:  &
         &L = ', lrgl, ', Parity = ', parity(npty), ',', kspchar, &
         ' spin(2S+1) = ', isp

    irec = nsprec(isp)
    irec2 = (nch * (nch+1)) / 2 + irec
    if (nch /= 0) read (jbufd,rec=irec) buf1d(1:lbufd)

    ncsnp = 0
    ny = lenham(isp)
    if (isnp /= 0 .and. isnp <= npset) ncsnp = npcset(isnp)
    ny = ny + ncsnp
    ny2 = (ny * (ny+1)) / 2
    write (fo,'(a,i6)') 'Hamiltonian dimension = ', ny
    write (fo,'(a,i6)') 'Number of channels    = ', nch
    if (ksp > 0) write (fo,'(a,i6)') 'Number of bound cfgs  = ', ncsnp
    if (bug4 > 1) then
       write (fo,'(/,3a,i3,/)') 'Hamiltonian matrix for', kspchar, &
            'spin (2s+1) =', isp
    end if


! write asymptotic file
    if (farm .or. prm /= 0) then
       call write2 (lrgl, ksp, npty, nch, ny, more, kconat(1:nast,isp),&
            l2pspn(1:nch,isp))
    end if
    if (nch == 0) then
       if (asym_ptlcfs) call writcf (cf, nch, lamax)
       return
    end if
    if (asym_ptlcfs) then
       allocate (cf(1:nch,1:nch,1:lamax), stat=status)
       if (status /= 0) call alloc_error (status, 'rdhmat:cf', 'a')
    end if

    if (ny2 > 0) then
       allocate (hmat(1:ny2), stat=status)
       if (status /= 0) call alloc_error (status, 'rdhmat:hmat', 'a')
       hmat(1:ny2) = 0.0_wp
    end if

! fill cf buffer with asymptotic coeffs from start of each Hamiltonian block
! prepare to fill hmat. set initial row length and starting
! position for first row of sub matrices
    ibuf = 0
    irow = ny
    istart= 1
    i_channels: do ich = 1, nch
       l = l2pspn(ich,isp) + 1
       nra1 = nvary(l)
       jstart = istart  ! starting position for first row of submatrices
       j_channels: do jch = ich, nch
          lp = l2pspn(jch,isp) + 1
          nra2 = nvary(lp)
          if (asym_ptlcfs) then
             cf(ich,jch,1:lamax) = buf1d(1:lamax)
             cf(jch,ich,1:lamax) = buf1d(1:lamax)
          end if
! copy sub matrix from buffer to array h1mat, dimension nra1 x nra2
          ib = lamax
          do n = 1, nra1
             do np = 1, nra2
                ib = ib + 1
                h1mat(np,n) = buf1d(ib)
             end do
          end do
          irec = irec + 1
          if (irec < irec2) then ! read next submatrix into buffer
             read (jbufd,rec=irec) buf1d(1:lbufd)
          end if
          if (ich == jch) then ! on the diagonal store upper half only
             nstart = jstart
             nrow = irow
             do n = 1, nra1
                npstar = nstart
                do np = n, nra2
                   hmat(npstar) = h1mat(np,n)
                   npstar = npstar + 1
                end do
                nstart = nstart + nrow
                nrow = nrow - 1
                ifin(n,ich) = nstart - ncsnp - 1
             end do
             jstart = istart + nra2
          else    ! off the diagonal store rectangular array
             nrow = irow - 1
             nstart = jstart
             do n = 1, nra1
                npstar = nstart
                do np = 1, nra2
                   hmat(npstar) = h1mat(np,n)
                   npstar = npstar + 1
                end do
                nstart = nstart + nrow
                nrow = nrow - 1
             end do
             jstart = jstart + nra2
          end if
          if (bug4 > 1) then
             call wrthcc (ich, istchs(ich), l2pspn(ich,isp), jch, &
                  istchs(jch), l2pspn(jch,isp), nra1, nra2, h1mat)
          end if
       end do j_channels
       istart = npstar + ncsnp
       irow = irow - nra1
    end do i_channels

    if (asym_ptlcfs) then ! write cf matrix to asymptotic file
       call writcf (cf, nch, lamax, isp, istchs)
       deallocate (cf, stat=status)
       if (status /= 0) call alloc_error (status, 'rdhmat:cf', 'd')
    end if

! if necessary, construct the bb, bc hamiltonian matrix elements
    if (ncsnp > 0) then
       call hambc (isnp, isp, ncsnp, nch, ifin, l2pspn(:,isp), istchs)
       lenhbb = (ncsnp * (ncsnp+1)) / 2
       lc = l2pspn(nch,isp) + 1
       nra = nvary(lc)
       nbb = ifin(nra,nch) + ncsnp + 1
       if (ABS(bug6) == 5) write (fo,'(a)') 'calling hambb'
       call hambb (isnp, lenhbb, hmat(nbb:))
       if (ABS(bug6) == 5) then
          write (fo,'(/a,/a)') 'bound-bound hamiltonian matrix', &
               '------------------------------'
          call wrtham (ncsnp, lenhbb, hmat(nbb:))
       end if
    end if
    if ((bug6 == 7 .and. ksp > 0) .or. bug6 == -7) call wrtham (ny,&
         ny2, hmat)

    deallocate (istchs, stat=status)
    if (status /= 0) call alloc_error (status, 'rdhmat:istchs', 'd')

    if (prm >= 0 .and. ny >= prm) then ! prepare PRMAT data
       call open_prmat_file (ihunit, isp, lrgl, npty)
       call writh1
       call wrth2 (ksp, more, nch, ny, hmat)
       deallocate (hmat, stat=status)
       if (status /= 0) call alloc_error (status, 'rdhmat:hmat', 'd')
       return ! end of calculation if no diagonalization
    end if

! Diagonalize Hamiltonian; calculate surface amplitudes, write to itape3
    ground = enat(1)
    if (bug9 > 2) then
       write (fo,'(/3a,i3)') 'eigenvalues, eigenvectors and surface&
            & amplitudes for ', kspchar, 'spin (2s+1) =', isp
    else if (bug9 > 1) then
       write (fo,'(/3a,i3)') 'eigenvalues and surface amplitudes for',&
            kspchar, 'spin (2s+1) =', isp
    end if

    if (farm) then
       allocate (wmat(nch,ny), value(ny), stat=status)
       allocate (wdmat(nch,ny), stat=status)
    else
       allocate (rwa(nch), value(ny), stat=status)
    end if
    if (status /= 0) call alloc_error (status, 'rdhmat:wmat,value','a')
    if (mpol == 1) then
       allocate (eigvec(ny,ny), stat=status)
       if (status /= 0) call alloc_error (status, 'rdhmat:&eigvec', 'a')
    end if

    call write_sums (ny, ncsnp)
    eigenvector_loop: do no = 1, ny
       call diag (ny, hmat, value, x, no)
       if (mpol == 1 .and. ksp > 0) eigvec(1:ny,no) = x(1:ny)
       if (bug9 > 1) then
          enlevl = 2.0_wp * (value(no) - ground)
          write (fo,'(/i6,a,f14.7,a,f12.7,a)') no, '   eigenvalue=', &
               value(no)+ecore, ' au - relative to target ground',   &
               enlevl, ' ryd'
          if (bug9 > 2) then
             write (fo,'(a)') 'vector'
             write (fo,'(2x,8f14.7)') x(1:ny)
          end if
       end if
       nch = nschan(isp)    ! # channels
       i1 = 0
       if (farm) then
          w => wmat(:,no)
       else
          if (no == 1) then
             w => rwa
             call write3a (ny, ecore, value)
          end if
       end if
       do ich = 1, nch
          lp = l2pspn(ich,isp) + 1
          noterm = nvary(lp)
          sm = 0.0_wp
          smd = 0.0_wp
          do n = 1, noterm
             i1 = i1 + 1
             sm = sm + x(i1) * ends(n,lp)
             smd = smd + x(i1) * dends(n,lp)
          end do
          wdmat(ich,no) = smd
          w(ich) = sm          ! surface amplitudes
       end do
       if (bug9 > 1) then
          write (fo,'(a)') 'surface amplitudes for each channel'
          write (fo,'(2x,8f14.7)') w(1:nch)
       end if
       if (.NOT.farm) call write3b (nch, no, w)
    end do eigenvector_loop
!
! Determination of the absorbing boundary potential
!
    allocate(vvabs(ny,ny), v2abs(ny,ny), stat=status)
    vvabs=0.0_wp
    if (status /= 0) call alloc_error (status, 'rdhmat:vabs','a')
    nch = nschan(isp)
    i1=1
    do ich = 1, nch
      lp =l2pspn(ich,isp) + 1
      noterm=nvary(lp)
      vvabs(i1:i1+noterm-1,i1:i1+noterm-1)=vabs(1:noterm,1:noterm,lp)
!     print *,'Absorb ',lp,vvabs(i1,i1),vabs(1,1,lp)
      print *,'Absorb ',noterm,vabs(1,3,lp),vabs(3,1,lp)
      i1=i1+noterm
    enddo
!   if ((lrgl.eq.1).and.(npty.eq.1)) then
!     print *,'What is happening with 1P?'
!     sm=0._wp
!     do i1=1,ny
!     do ich=1,ny
!     sm=sm+eigvec(ich,1)*vvabs(ich,i1)*eigvec(i1,1)
!     print *,ich,i1,eigvec(ich,1),vvabs(ich,i1),eigvec(i1,1),sm
!     enddo
!     enddo
!   endif
    v2abs=matmul(vvabs,eigvec)
    vvabs=matmul(transpose(eigvec),v2abs)
    print *,'Absorb ',vvabs(1,1),vvabs(1,2),vvabs(2,1)
    write(38) vvabs
    deallocate(vvabs,v2abs, stat=status)
    if (status /= 0) call alloc_error (status, 'rdhmat:vabs','d')

    allocate(splwave(nch*ndim,ny), spltmp(nch*ndim,ny), stat=status)
    if (status /= 0) call alloc_error (status, 'rdhmat:splwave','a')
    spltmp=0._wp
    i1=1
    do ich=1,nch
      lp =l2pspn(ich,isp) + 1
      noterm=nvary(lp)
      spltmp((ich-1)*ndim+1:ich*ndim,i1:i1+noterm-1)=splcoef(1:ndim,1:noterm,lp)
      i1=i1+noterm
    enddo
    splwave=matmul(spltmp,eigvec)
!   if (nch.ge.2) print *,'SPLINE ',splwave(10,1),splwave(10+ndim,1)
!   if (nch.eq.1) print *,'SPLINE ',splwave(10,1)
!
! Expand the continuum orbitals in spline functions 
!
    do i1=1,ny
!     print *,'SPLINES ',nch*ndim, i1
      write(97) splwave(1:nch*ndim,i1)
    enddo
!
!
!
    deallocate (hmat, stat=status)
    if (status /= 0) call alloc_error (status, 'rdhmat:hmat', 'd')
!
! write out derivative boundary amplitudes
!
     write(39) ((wdmat(ich,no),ich=1, nch),no=1,ny)
     print *,'BADERIV ',ny*nch
! store eigenvectors for dipole matrix elements
    if (mpol == 1 .and. ksp > 0) then
       jbufsc = 47
       if (bug2 > 2) write (fo,'(a,i6,a)') 'eigenvectors symmetry',&
            isnp, ' stored'
       call catlog (jbufsc, ivec(isnp))
       call iwrita (jbufsc, ny)
       call writc (jbufsc, eigvec, 1, ny, 1, ny, ny, 0)
    end if

    if (farm) call write3 (nch, ny, ecore, value, wmat)
    write (fo,'(/,a,f14.7,a)') 'Lowest eigenvalue               =', &
         value(1)+ecore, ' a.u.'
    write (fo,'(a,f14.7,a)') 'Relative to target ground state =', &
         2.0_wp * (value(1)-ground), ' Ryd'
    if (bug9 == 1) then
       write (fo,'(/a,f14.7,a)') 'Eigenvalues relative to target&
            & ground state (', 2.0_wp*(ground + ecore), ' Ryd)'
       if (neprnt <= 0) then
          nstrt = 1
       else
          nstrt = MAX(1, neprnt)
          if (neprnt < ny) write (fo,'(a,i5,a,/)') 'only the first ', &
               neprnt, '  values are printed'
       end if
       write (fo,'(2x,8f14.7)') (2.0_wp*(value(no)-ground), no=1,nstrt)
    end if
    if (farm) then
       deallocate (wmat, wdmat, value, stat=status)
    else
       deallocate (rwa, value, stat=status)
    end if
    if (status /= 0) call alloc_error (status, 'rdhmat:wmat,value,x', &
         'd')
    call diag_kill
    if (mpol == 1) then
       deallocate (eigvec, stat=status)
       if (status /= 0) call alloc_error (status, 'rdhmat:eigvec', 'd')
    end if
  end subroutine rdhmat

  subroutine wrthcc (ich, istchs, ilspn, jch, jstchs, jlspn, nra1,&
       nra2, hcc)
    integer, intent(in)         :: ich, jch
    integer, intent(in)         :: istchs, jstchs
    integer, intent(in)         :: ilspn, jlspn
    integer, intent(in)         :: nra1, nra2
    real(wp), intent(in)        :: hcc(:,:)
    character(len=13)           :: fmt
    integer                     :: nplo, npup, nup, iblank, n, nblocp
    integer                     :: nblock, nbloc, nlo

    write (fo,'(/,6(a,i3),a,/)') 'Contribution from channel', ich, &
         ' (state', istchs, ', l=', ilspn, '), with channel',  &
         jch, ' (state', jstchs, ', l=', jlspn, ')'
    if (ich == jch) then   ! print triangular matrix
       nblock = nra1 / 5 + 1
       nlo = 1
       nup = MIN(5, nra1)
       nbloc_loop: do nbloc = 1, nblock
          iblank = 0
          n_loop: do n = nlo, nup
             nplo = n
             npup = nup
             do nblocp = nbloc, nblock
                if (nblocp == nbloc .and. iblank > 0) then
                   if (iblank <= 3) then
                      write (fmt,'(a2,i2,a2,i1,a6)') '(/', 2+iblank*14, &
                           'x,', 5-iblank, 'f14.7)'
                   else
                      fmt = '(/58x,f14.7)'
                   end if
                   write (fo,fmt) hcc(nplo:npup,n)
                else
                   write (fo,'(2x,5f14.7)') hcc(nplo:npup,n)
                end if
                nplo = npup + 1
                npup = MIN(npup+5, nra2)
             end do
             iblank = iblank + 1
          end do n_loop
          nlo = nup + 1
          nup = MIN(nup+5, nra1)
       end do nbloc_loop
    else               ! print rectangular matrix
       nblock = nra2 / 5 + 1
       do n = 1, nra1
          nplo = 1
          npup = MIN(5, nra2)
          do nblocp = 1, nblock
             write (fo,'(2x,5f14.7)') hcc(nplo:npup,n)
             nplo = npup + 1
             npup = MIN(npup+5, nra2)
          end do
       end do
    end if
  end subroutine wrthcc

  subroutine hambc (isnp, lrgs, ncsnp, nch, ifin, lchan, istchs)
    use accdi, only: ncset
    use channels, only: iconch
    use rad_data, only: lrang1, nvary
    use set_data, only: lset, isset, nstat
    use states, only: nsetst, aij
    use l_data, only: lrgl, npty, ltot, lval, nscol, nsetl
    use comp_data, only: njcomp, ljcomp
    use rm_data, only: nrang2, maxorb
    use dim, only: ldim2
    use stghrd_mod, only: nrang1
    use sc_ham, only: ivbci, ivbcr, i1bc, ibc, irkbc
    use filehand, only: lookup, startr, readb, ireada, ireadb, reada
    use vpack, only: unpck124, unpck3, unpckl2, unpck14
    integer, intent(in)      :: nch           ! # channels
    integer, intent(in)      :: isnp          ! N+1 symm seq #
    integer, intent(in)      :: lrgs          ! spin seq #
    integer, intent(in)      :: ncsnp         ! # bnd N+1 cfgs
    integer, intent(in)      :: ifin(:,:)     ! H col given c-rob, chan
    integer, intent(in)      :: lchan(nch)    ! channel cont orb a.m.
    integer, intent(in)      :: istchs(nch)
    real(wp)                 :: hbc(nrang2)
    integer                  :: irs(2*maxorb)
    real(wp), allocatable    :: r1bc(:)
    real(wp), allocatable    :: rkbc(:)
    real(wp)                 :: r1(nrang2)
    real(wp), allocatable    :: rk(:,:)
    real(wp), allocatable    :: abc(:)
    integer                  :: jch1, klc, lc, ncup, ncsi, nrec, nr1bc
    integer                  :: nrkbc, j, jsn, jspin, lcfgp, nreci
    integer                  :: nrecr, jcsn, icsnp, nconf, ncont1
    integer                  :: ncont2, i, inc, lamdat, ishldt, irho
    integer                  :: jrho, isig, nri, lri, nrj, lrj, nsi, lsi
    integer                  :: lamlo, lamup, lam, nc, nterm, jchann
    integer                  :: jstat, jst, jch, nelem, iblo, ibup
    integer                  :: iblock, lenhcc, ibuff, ibuffi, jchan
    integer                  :: ibuffr, ncont, nblock, ib, ich, status
!   integer                  :: jcsnq
    real(wp)                 :: abc1, cmp

    allocate (rk(nrang2,4*ldim2*maxorb), r1bc(nrang2*nrang1*lrang1), &
         abc(4*ldim2*maxorb), stat=status)
    if (status /= 0) call alloc_error (status, 'hambc', 'a')
    jch1 = 0
    jchan = 0
    if (ABS(bug6) == 6) then
       write (fo,'(a)') 'subroutine hambc'
       write (fo,'(a,3i6)') 'lrgl, lrgs, npty = ', lrgl, lrgs, npty
    end if
    klc_loop: do klc = 1, ltot
       lc = lval(klc)
       ncup = nvary(lc+1)
       ncsi = lc * lrang1 + 1
! read r1bc for cont a.m. lc; no integrals => no pointer
       if (lc >= lrang1) then
          nr1bc = 0
       else
          call lookup (i1bc(lc+1), nrec, ibuff)
          nr1bc = 0
          if (nrec > 0) then
             call startr (jbuff4, nrec, ibuff)
             nr1bc = i1bc(lc+2) - i1bc(lc+1)
             call readb (jbuff4, r1bc, 1, nr1bc)
          end if
       end if

! read rkbc array for continuum angular momentum lc
       if (lc >= 3*lrang1-2) then
          nrkbc = 0
       else
          call lookup (ibc(lc+1), nrec, ibuff)
          nrkbc = 0
          if (nrec > 0) then
             call startr (jbuff1, nrec, ibuff)
             nrkbc = ibc(lc+2) - ibc(lc+1)
             if (allocated(rkbc)) then
                deallocate (rkbc, stat=status)
             end if
             allocate (rkbc(nrkbc), stat=status)
             call readb (jbuff1, rkbc, 1, nrkbc)
          end if
       end if
       j_loop: do j = 1, nscol(klc)
          jsn = nsetl(j,klc)
          lcfgp = lset(jsn)
          jspin = isset(jsn)
          if (ABS(lrgs-jspin) /= 1) then
             jchan = jchan + nstat(jsn)
             cycle j_loop
          end if
          jch1 = jch1 + 1
! check that there are more ANG elements to process:
          if (ivbci(jch1+1,isnp) <= ivbci(jch1,isnp) .or. &
               ivbcr(jch1+1,isnp) <= ivbcr(jch1,isnp)) then
             jchan = jchan + nstat(jsn)
             cycle j_loop
          end if
! move to correct position in angular files
          call lookup (ivbci(jch1,isnp), nreci, ibuffi)
          call lookup (ivbcr(jch1,isnp), nrecr, ibuffr)
          call startr (jibc, nreci, ibuffi)
          call startr (jfbc, nrecr, ibuffr)
          jcsn_loop: do jcsn = 1, ncset(jsn)
             icsnp_loop: do icsnp = 1, ncsnp
                hbc(1:ncup) = 0.0_wp
!                call ireada (jibc, jcsnq)  ! additional iteration count
!                if (jcsnq /= jcsn) then   ! extra header problem
!                   write (fo,'(a,2i8)') 'hambc: iteration count, jcsn, &
!                        &jcsnq = ', jcsn, jcsnq
!                   stop
!                end if
                call ireada (jibc, ncont)
                if (ncont == 0) cycle jcsn_loop
                call unpck124(ncont, ncont1, ncont2, nconf)
                if (ncont2 /= 0) then
                   if (nrkbc == 0) then
                      write (fo,'(a)') 'hambc: ncont2 is non-zero but &
                           &nrkbc is zero'
                      stop
                   end if
                   if (bug6 == -6) write (fo,'(a)')  'two electron &
                        &contributions'

                   call ireadb (jibc, irs, 1, ncont2)
                   i = 0
                   inc_loop: do inc = 1, ncont2
                      call unpck14(irs(inc), ishldt, lamdat)
                      call unpck3(ishldt, isig, jrho, irho)
                      nri = njcomp(irho)
                      lri = ljcomp(irho)
                      nrj = njcomp(jrho)
                      lrj = ljcomp(jrho)
                      nsi = njcomp(isig)
                      lsi = ljcomp(isig)
                      if (MOD(lri+lrj+lsi+lc,2) == 1) then
                         write (fo,'(a)') 'hambc: parity error'
                         write (fo,*) 'lc, lri, lrj, lsi, inc, jcsn, icsnp, jsn ',&
                              lc, lri, lrj, lsi, inc, jcsn, icsnp, jsn
                         stop
                      end if
                      call unpckl2 (lamdat, lamup, lamlo)
                      lamup = lamlo + 2 * lamup - 2
                      lam_loop: do lam = lamlo, lamup, 2
                         i = i + 1
                         call locbc (lam, nri, lri, nrj, lrj, nsi, lsi,&
                              ncup, lc, ncsi, nrkbc, irkbc, rkbc, &
                              rk(:,i))
                         if (bug6 == -6) then
                            do nc = 1, ncup
                               write (fo,'(a,i3,4(2x,2i3),a,e13.5)')  &
                                    'rkbc(', lam, nri, lri, nsi, lsi, &
                                    nrj, lrj, nc, lc, ') = ', rk(nc,i)
                            end do
                         end if
                      end do lam_loop
                   end do inc_loop
                   nterm = i
                   call readb (jfbc, abc, 1, nterm)
                   if (bug6 == -6) write (fo,'(a,/,(8e13.5))')  &
                        ' angular integrals', abc(1:nterm)
                   do i = 1, nterm
                      do nc = 1, ncup
                         hbc(nc) = hbc(nc) + rk(nc,i) * abc(i)
                      end do
                   end do
                end if
                if (ncont1 > 0) then
                   if (nr1bc == 0) then
                      write (fo,'(a)') 'hambc: ncont1 is non-zero but &
                           &nr1bc is zero'
                      stop
                   end if
                   call ireada (jibc, isig)
                   call reada (jfbc, abc1)
                   if (bug6 == -6) then
                      write (fo,'(a)') 'one electron contributions'
                      write (fo,'(a,e13.5)') 'angular integral = ', abc1
                   end if
                   nsi = njcomp(isig)
                   lsi = ljcomp(isig)
                   if (lsi == lc) then
                      call loc1bc (lc, nsi, ncup, nr1bc, i1bc(lc+1),&
                           r1bc, r1(:))
                      if (bug6 == -6) then
                         do nc = 1, ncup
                            write (fo,'(a,2i3,2x,2i3,a,e13.5)') &
                                 'r1bc(', nsi, lsi, nc, lc, ') = ', &
                                 r1(nc)
                         end do
                      end if
                      do nc = 1, ncup
                         hbc(nc) = hbc(nc) + r1(nc) * abc1
                      end do
                   else
                      write (fo,'(a)') 'hambc: angbc gives different &
                           &l-values in data for one electron radial &
                           &integrals'
                      write (fo,'(2(a,i3))') 'lsi = ', lsi, &
                           '   lc = ', lc
                      stop
                   end if
                end if
                jchann = jchan
                do jst = 1, nstat(jsn)
                   jstat = nsetst(jsn,jst)
                   cmp = aij(jstat,jcsn)
                   jchann = jchann + 1
                   jch = iconch(jchann,lrgs)
                   do nc = 1, ncup
                      nelem = ifin(nc,jch) + nconf
                      hmat(nelem) = hmat(nelem) + cmp * hbc(nc)
                   end do
                end do
             end do icsnp_loop
          end do jcsn_loop
          jchan = jchan + nstat(jsn)
       end do j_loop
    end do klc_loop
    if (ABS(bug6) == 6) then
       write (fo,'(/a)') 'bound-continuum hamiltonian matrix'
       write (fo,'(a)') '----------------------------------'
       nblock = ncsnp / 8 + 1
       do ich = 1, nch
          lc = lchan(ich)
          write (fo,'(3(a,i3),a)') 'contribution from channel', ich,&
               ' (state', istchs(ich), ', l=', lc, ')'
          iblo = 1
          ibup = MIN(8, ncsnp)
          do iblock = 1, nblock
             do nc = 1, nvary(lc+1)
                lenhcc = ifin(nc,ich)
                write (fo,'(2x,8f14.7)') (hmat(lenhcc+ib), ib=iblo,ibup)
             end do
             write (fo,*)
             iblo = ibup + 1
             ibup = MIN(ibup+8, ncsnp)
          end do
       end do
    end if
    deallocate (rk, r1bc, abc, stat=status)
    if (status /= 0) call alloc_error (status, 'hambc', 'd')
  end subroutine hambc

  subroutine locbc (lam, nri, lri, nrj, lrj, nsi, lsi, ncup, lc, ncsi,&
       nrkbc, irkbc, rkbc, rk)
    use rad_data, only: maxnhf, maxnc
    integer, intent(in)               :: lam
    integer, intent(in)               :: nri, lri
    integer, intent(in)               :: nrj, lrj
    integer, intent(in)               :: nsi, lsi
    integer, intent(in)               :: ncup
    integer, intent(in)               :: lc, ncsi
    integer, intent(in)               :: nrkbc
    integer, intent(in)               :: irkbc(:,:,:)
    real(wp), intent(in)              :: rkbc(nrkbc)
    real(wp), intent(out)             :: rk(ncup)
    integer                           :: mxcri, mxcrj, mxcsi, nijr
    integer                           :: n3, n4, nrkl, nrs, nc

    mxcri = maxnc(lri+1)
    mxcrj = maxnc(lrj+1)
    mxcsi = maxnc(lsi+1)
    if (lri > lrj) then
       nijr = (lri * (lri+1))/2 + lrj + 1
       n3 = nri - mxcri - 1
       n4 = (nrj - mxcrj - 1) * (maxnhf(lri+1) - mxcri)
    else if (lrj > lri) then
       nijr = (lrj * (lrj+1))/2 + lri + 1
       n3 = nrj - mxcrj - 1
       n4 = (nri - mxcri - 1) * (maxnhf(lrj+1) - mxcrj)
    else
       nijr = (lri * (lri+1))/2 + lrj + 1
       if (nri <= nrj) then
          n3 = nri - mxcri - 1
          n4 = ((nrj - mxcrj - 1) * (nrj - mxcrj)) / 2
       else
          n3 = nrj - mxcrj - 1
          n4 = ((nri - mxcri - 1) * (nri - mxcri)) / 2
       end if
    end if
    nrkl = irkbc(lam+1,nijr,ncsi+lsi)
    if (nrkl == -999) then
       write (fo,'(a)') 'locbc: no pointer found in irkbc for'
       write (fo,'(4(a,i4))') 'lam = ', lam, ' lri = ', lri, &
            ' lrj = ', lrj, ' lsi = ', lsi, ' lc = ', lc
       stop
    end if
    nrs = nrkl + ncup * (nsi - mxcsi - 1 + (maxnhf(lsi+1) - mxcsi) * &
         (n3 + n4))
    do nc = 1, ncup
       rk(nc) = rkbc(nrs+nc)
    end do
  end subroutine locbc

  subroutine loc1bc (lc, nb, ncup, nr1bc, i1bc, r1bc, rval)
    use rad_data, only: maxnc
    integer, intent(in)           :: lc
    integer, intent(in)           :: nb
    integer, intent(in)           :: ncup
    integer, intent(in)           :: nr1bc
    integer, intent(in)           :: i1bc
    real(wp), intent(in)          :: r1bc(nr1bc)
    real(wp), intent(out)         :: rval(ncup)
    integer                       :: nblo, n1bc, nc

    if (i1bc == -999) then
       write (fo,'(a,i4)') 'loc1bc: no pointer found in i1bc for &
            &lc = ', lc
       stop
    end if
    nblo = maxnc(lc+1)+1
    n1bc = ncup * (nb - nblo)
    do nc = 1, ncup
       rval(nc) = r1bc(n1bc+nc)
    end do
  end subroutine loc1bc

  subroutine write_sums (ny, ncsnp)
! print sum values of Hamiltonian matrix sectors
    integer, intent(in)     :: ny   ! Hamiltonian dimension
    integer, intent(in)     :: ncsnp ! # bound configurations
    real(wp)                :: ccod_sm, ccdg_sm, bbdg_sm, bbod_sm, bc_sm
    integer                 :: nc, i, j, ij

    ccod_sm = 0.0_wp
    ccdg_sm = 0.0_wp
    bc_sm = 0.0_wp
    bbod_sm = 0.0_wp
    bbdg_sm = 0.0_wp
    nc = ny - ncsnp
    ij = 0
    do i = 1, ny
       do j = i, ny
          ij = ij + 1
!         print *,i,j,hmat(ij)
          if (i == j) then
             if (i <= nc) then
                ccdg_sm = ccdg_sm + hmat(ij)
             else
                bbdg_sm = bbdg_sm + hmat(ij)
             end if
          else
             if (i <= nc) then
                if (j <= nc) then
                   ccod_sm = ccod_sm + hmat(ij)
                else
                   bc_sm = bc_sm + hmat(ij)
                end if
             else
                bbod_sm = bbod_sm + hmat(ij)
             end if
          end if
       end do
    end do
    write (fo,'(/,a)') 'Hamiltonian Checksums:'
    write (fo,'(a,e30.17)') 'ccdg_sm = ', ccdg_sm
    write (fo,'(a,e30.17)') 'ccod_sm = ', ccod_sm
    if (ncsnp /= 0) then
       write (fo,'(a,e30.17)') 'bbdg_sm = ', bbdg_sm
       write (fo,'(a,e30.17)') 'bbod_sm = ', bbod_sm
       write (fo,'(a,e30.17)') 'bc_sm   = ', bc_sm
    end if
  end subroutine write_sums
end module rdhmat_mod
