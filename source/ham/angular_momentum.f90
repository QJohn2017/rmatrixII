module angular_momentum
! Time-stamp: "02/01/24 17:45:26 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use debug, only: bug1, bug7
  implicit none

  real(wp), allocatable, save :: gammas(:)

  logical, save               :: gamma = .false.
  integer, save               :: ngam = 1000
  real(wp), allocatable, save :: gam(:)    ! logs of factorials

  integer, save               :: lrang1
  integer, save               :: lrang3
  integer, save               :: isran1
  integer, save               :: isran2
  real(wp), allocatable, save :: spinw(:,:)
  real(wp), allocatable, save :: rmetab(:,:,:)

  private
  public cracah, dracah, ractab, rme
  public lrang1, lrang3, isran1, isran2, spinw, rmetab
  public cg
contains

   subroutine cracah (i, j, k, l, m, n, rac)
! calculate racah coefficients
     integer, intent(in) :: i, j, k, l, m, n  ! 2 * angular momenta
     real(wp), intent(out) :: rac
     integer :: j1, j2, j3, j4, j5, j6, j7, icount, numin, numax, kk, ki

     if (.NOT.gamma) then
        call factt
        gamma = .true.
     end if
     j1 = i + j + m
     j2 = k + l + m
     j3 = i + k + n
     j4 = j + l + n
     if (2*MAX(i,j,m) > j1 .or. MOD(j1,2) /= 0 .or.  &
          2*MAX(k,l,m) > j2 .or. MOD(j2,2) /= 0 .or. &
          2*MAX(i,k,n) > j3 .or. MOD(j3,2) /= 0 .or. &
          2*MAX(j,l,n) > j4 .or. MOD(j4,2) /= 0) then
        rac = 0.0_wp
        return
     end if

!  when any of the first four arguments is zero rac=1.
     if (i == 0 .or. j == 0 .or. k == 0 .or. l == 0) then
        rac = 1.0_wp
        return
     end if
     j1 = j1 / 2
     j2 = j2 / 2
     j3 = j3 / 2
     j4 = j4 / 2
     j5 = (i + j + k + l) / 2
     j6 = (i + l + m + n) / 2
     j7 = (j + k + m + n) / 2
     numin = MAX(j1, j2, j3, j4) + 1
     numax = MIN(j5, j6, j7) + 1
     rac = 1.0_wp
     icount = 0
     if (numin /= numax) then
        numin = numin + 1
        do kk = numin, numax
           ki = numax - icount
           rac = 1.0_wp - (rac * REAL(ki * (j5-ki+2) * (j6-ki+2) * &
                (j7-ki+2),wp) / REAL((ki-1-j1) * (ki-1-j2) * &
                (ki-1-j3) * (ki-1-j4),wp))
           icount = icount + 1
        end do
        numin = numin - 1
     end if
     rac = rac * ((-1.0_wp)**(j5 + numin + 1)) *                &
          EXP((gam(numin+1) - gam(numin-j1) - gam(numin-j2) -   &
          gam(numin-j3) - gam(numin-j4) - gam(j5+2-numin) -     &
          gam(j6+2-numin) - gam(j7+2-numin)) + ((gam(j1+1-i) +  &
          gam(j1+1-j) + gam(j1+1-m) - gam(j1+2) + gam(j2+1-k) + &
          gam(j2+1-l) + gam(j2+1-m) - gam(j2+2) + gam(j3+1-i) + &
          gam(j3+1-k) + gam(j3+1-n) - gam(j3+2) + gam(j4+1-j) + &
          gam(j4+1-l) + gam(j4+1-n) - gam(j4+2)) / 2.0_wp))
     rac = rac * ((m+1) * (n+1))**0.5_wp
   end subroutine cracah

   subroutine factt
! calculates the logs of factorials
     integer          :: i
     real(wp)         :: x
     integer          :: status

     allocate (gam(ngam), stat=status)
     if (status /= 0) then
        write (fo,'(a)') 'factt: allocation error'
        stop
     end if
     gam(1:2) = 1.0_wp
     x = 2.0_wp
     do i = 3, 25
        gam(i) = gam(i-1) * x
        x = x + 1.0_wp
     end do
     do i = 1, 25
        gam(i) = LOG(gam(i))
     end do
     x = 25.0_wp
     do i = 26, ngam 
        gam(i) = gam(i-1) + LOG(x)
        x = x + 1.0_wp
     end do
   end subroutine factt

   subroutine dracah (i, j, k, l, m, n, rac)
! calculate racah coefficients
! arguments i,j,k,l,m,n should be twice their actual value
     integer, intent(in)    :: i, j, k, l, m, n
     real(wp), intent(out)  :: rac
     integer :: j1, j2, j3, j4, j5, j6, j7, icount, numin, numax, kk, ki
!
     if (.NOT.gamma) then
        call factt
        gamma = .true.
     end if
     j1 = i + j + m
     j2 = k + l + m
     j3 = i + k + n
     j4 = j + l + n
     if (2*MAX(i,j,m) > j1 .or. MOD(j1,2) /= 0 .or.  &
          2*MAX(k,l,m) > j2 .or. MOD(j2,2) /= 0 .or. &
          2*MAX(i,k,n) > j3 .or. MOD(j3,2) /= 0 .or. &
          2*MAX(j,l,n) > j4 .or. MOD(j4,2) /= 0) then
        rac = 0.0_wp
        return
     end if
     j1 = j1 / 2
     j2 = j2 / 2
     j3 = j3 / 2
     j4 = j4 / 2
     j5 = (i + j + k + l) / 2
     j6 = (i + l + m + n) / 2
     j7 = (j + k + m + n) / 2
     numin = MAX(j1, j2, j3, j4) + 1
     numax = MIN(j5, j6, j7) + 1
     rac = 1.0_wp
     icount = 0
     if (numin /= numax) then
        numin = numin + 1
        do kk = numin, numax
           ki = numax - icount
           rac = 1.0_wp - (rac * REAL(ki * (j5-ki+2) * (j6-ki+2) * &
                (j7-ki+2),wp)) / REAL((ki-1-j1) * (ki-1-j2) *      &
                (ki-1-j3) * (ki-1-j4),wp)
           icount = icount + 1
        end do
        numin = numin - 1
     end if
     rac = rac * ((-1.0_wp)**(j5+numin+1)) *                    &
          EXP((gam(numin+1) - gam(numin-j1) -gam(numin-j2) -    &
          gam(numin-j3) - gam(numin-j4) - gam(j5+2-numin) -     &
          gam(j6+2-numin) - gam(j7+2-numin)) + ((gam(j1+1-i) +  &
          gam(j1+1-j) + gam(j1+1-m) - gam(j1+2) + gam(j2+1-k) + &
          gam(j2+1-l) + gam(j2+1-m) - gam(j2+2) + gam(j3+1-i) + &
          gam(j3+1-k) + gam(j3+1-n) - gam(j3+2) + gam(j4+1-j) + &
          gam(j4+1-l) + gam(j4+1-n) - gam(j4+2)) / 2.0_wp))
   end subroutine dracah

   subroutine ractab
! construct a table of racah coefficients for l, s as required by angcc
     real(wp)        :: asp(4), srac(4)
     integer         :: max1, max2, status, sgn, ispim1, ispip1, ispim2
     integer         :: maxa, maxb, maxc, ia, ib, ic, icup, iclo, ispi
     real(wp)        :: rm, rac

     max1 = MAX(lrang1, lrang3) - 1
     max2 = max1 + max1

! form the table of spin racah coefficients
     allocate (spinw(4,isran2+3), stat=status)
     if (status /= 0) then
        write (fo,'(a)') 'ractab: allocation error'
        stop
     end if
     sgn = 1
     do ispi = 2, isran2+3
        sgn = - sgn
        ispim1 = ispi - 1
        ispip1 = ispi + 1
        ispim2 = ispi - 2
        call dracah (ispim1, ispim2, ispim2, ispim1, 1, 1, srac(1))
        call dracah (ispim1, ispim2, ispi, ispim1, 1, 1, srac(2))
        call dracah (ispim1, ispi, ispi, ispim1, 1, 1, srac(3))
        call dracah (ispim1, ispi, ispi, ispip1, 1, 1, srac(4))
        asp(1) = ispi
        asp(2) = -ispi
        asp(3) = ispi
        asp(4) = ((ispip1 + 1) * ispi)**0.5_wp
        spinw(:,ispi)=asp * srac * sgn
     end do

! special case of zero target spin
     call dracah (0, 1, 1, 0, 1, 1, rac)
     spinw(1:3,1) = rac
     call dracah (0, 1, 1, 2, 1, 1, rac)
     spinw(4,1) = 3**.5_wp * rac

! construct simple table of rme
! for bbcup and bccup maxa=lrang1-1, maxb=3*maxa, maxc=2*maxa
! rm2 is INCONSISTENT here
     maxa = lrang1 - 1 ! ildim8
     maxb = 3 * maxa   ! ildim8
     maxc = 2 * maxa   ! ildim0
     allocate (rmetab(0:maxc,0:maxa,0:maxb), stat = status)
     if (status /= 0) then
        write (fo,'(a)') 'ractab: allocation error'
        stop
     end if
     rmetab = 0.0_wp
     do ia = 0, maxa
        do ib = ia, maxb
           iclo = ABS(ib - ia)
           icup = MIN(ib+ia, maxc)
           do ic = iclo, icup, 2
              rm = rme(ia, ib, ic)
              rmetab(ic,ia,ib) = rm
              if (ib /= ia .and. ib <= maxa) rmetab(ic,ib,ia) = rm
           end do
        end do
     end do
   end subroutine ractab

   function rme (l, lp, k)
! evaluates the reduced matrix element (l//c(k)//lp)  -  see fano
! and racah, irreducible tensorial sets, chap. 14, p. 81
     real(wp)                :: rme
     integer, intent(in)     :: l, lp
     integer, intent(in)     :: k
     integer                 :: i2g, ig, i1, i2, i3, imax
     real(wp)                :: al1, al2, alp1, alp2, ak1, ak2, el1, elp1
     real(wp)                :: qusqrt

     if (k > (l+lp) .or. k < ABS(l-lp)) then
        if (bug7 > 1) write (fo,'(a,i3,a,i3,a,i3,a)') 'l =', l, &
             ' lp =', lp, ' k =', k, ' rme set zero since angle does &
             &not match'
        rme = 0.0_wp
        return
     end if
     i2g = l + lp + k
     ig = i2g / 2
     if (i2g /= 2*ig) then
        rme = 0.0_wp
        return
     end if

     select case (ig)
     case (0)
        rme = 1.0_wp
     case (:-1)
        if (bug7 > 1) write (fo,'(a,i3,a,i3,a,i3,a)') 'l =', l, &
             ' lp =', lp, ' k =', k, ' rme set zero since angle does &
             &not match'
        rme = 0.0_wp
     case default
        i1 = ig - l
        i2 = ig - lp
        i3 = ig - k
        imax = MAX(ig+1, i2g+2, 2*i3+1, 2*i2+1, 2*i1+1)
        if (imax > ngam) then
           write (fo,'(a,i3)') 'program stops in rme imaxf = ', imax
           stop
        end if
        al1 = gam(i1+1)
        al2 = gam(2*i1+1)
        alp1 = gam(i2+1)
        alp2 = gam(2*i2+1)
        ak1 = gam(i3+1)
        ak2 = gam(2*i3+1)
        el1 = 2 * l + 1
        elp1 = 2 * lp + 1
        qusqrt = LOG(el1) + LOG(elp1) + al2 + alp2 + ak2 - gam(i2g+2)
        rme = EXP(0.5_wp * qusqrt + gam(ig+1) - al1 - alp1 - ak1)
     end select
   end function rme

!  function cg (j1, j2, j3, m1, m2, m3)
! calculates a clebsch-gordan coefficient.
! Condon and Shortley page 75 formula (3.14.5))
!    real(wp)                :: cg
!    integer, intent(in)     :: j1, j2, j3
!    integer, intent(in)     :: m1, m2, m3
!    real(wp)                :: xnum, g
!    integer                 :: a, b, c, d, e, numin, numax, j, n

!    if (.NOT.allocated(gammas)) call shriek (ngam)
!    cg = 0.0_wp
!    if (ABS(m1) > j1 .or. ABS(m2) > j2 .or. ABS(m3) > j3) return
!    if (m1+m2 /= m3) return
!    if (2*MAX(j1,j2,j3) > j1+j2+j3) return
!
!    xnum = (gammas(-j1+j2+j3+1) / gammas(j2+m2+1)) * &
!         (gammas(j1-j2+j3+1) / gammas(j1+m1+1)) *    &
!         (gammas(j1+j2-j3+1) / gammas(j1-m1+1)) *    &
!         (gammas(j3-m3+1) / gammas(j2-m2+1)) *       &
!         (REAL(2*j3+1,wp) * gammas(j3+m3+1) / gammas(j1+j2+j3+2))
!    xnum = SQRT(xnum)
!    a = -j1 + m1
!    b = j2 - j1 + m3
!    c = j2 + j3 + m1
!    d = -j1 + j2 + j3
!    e = j3 + m3
!    numin = MAX(0,b)
!    numax = MIN(c,d,e)
!    j = j2 + m2

!    do n = numin, numax
!       g = gammas(n-a+1) * gammas(c-n+1) / (gammas(d-n+1) *         &
!            gammas(e-n+1) * gammas(n-b+1) * gammas(n+1))
!       if (MOD(j+n,2) /= 0) g = -g
!       cg = cg + g
!    end do
!    cg = cg * xnum
!  end function cg

   function cg (j1, j2, j3, m1, m2, m3)
! calculates a clebsch-gordan coefficient.
! Condon and Shortley page 75 formula (3.14.5))
     real(wp)                :: cg
     integer, intent(in)     :: j1, j2, j3
     integer, intent(in)     :: m1, m2, m3
     real(wp)                :: xnum, g
     integer                 :: a, b, c, d, e, numin, numax, j, n

     if (.NOT.gamma) then
        call factt
        gamma=.true.
     end if
     cg = 0.0_wp
     if (ABS(m1) > j1 .or. ABS(m2) > j2 .or. ABS(m3) > j3) return
     if (m1+m2 /= m3) return
     if (2*MAX(j1,j2,j3) > j1+j2+j3) return
!
     xnum = (gam(-j1+j2+j3+1) - gam(j2+m2+1)) + &
          (gam(j1-j2+j3+1) - gam(j1+m1+1)) +    &
          (gam(j1+j2-j3+1) - gam(j1-m1+1)) +    &
          (gam(j3-m3+1) - gam(j2-m2+1)) +       &
          (LOG(REAL(2*j3+1,wp)) + gam(j3+m3+1) - gam(j1+j2+j3+2))
     xnum = xnum / 2.0_wp
     xnum = EXP(xnum)
     a = -j1 + m1
     b = j2 - j1 + m3
     c = j2 + j3 + m1
     d = -j1 + j2 + j3
     e = j3 + m3
     numin = MAX(0,b)
     numax = MIN(c,d,e)
     j = j2 + m2

     do n = numin, numax
        g = gam(n-a+1) + gam(c-n+1) - (gam(d-n+1) +         &
             gam(e-n+1) + gam(n-b+1) + gam(n+1))
        g = EXP(g)
        if (MOD(j+n,2) /= 0) g = -g
        cg = cg + g
     end do
     cg = cg * xnum
   end function cg


   subroutine shriek (nfact)
! evaluates factorials from 1 to nfact-1
! gammas(i+1) = factorial i
     integer, intent(in)      :: nfact
     real(wp)                 :: a
     integer                  :: i, status

     allocate (gammas(nfact), stat=status)
     if (status /= 0) then
        write (fo, '(a,i4)') 'shriek: allocation error = ', status
        stop
     end if
     gammas(1) = 1.0_wp
     if (nfact < 2) return
     do i = 2, nfact
        gammas(i) = REAL(i-1,wp) * gammas(i-1)
        if (gammas(i) > HUGE(a) / 1000.0_wp) then
           write (fo,'(a, i4)') 'shriek: factorial limit = ', i
           exit
        end if
     end do
   end subroutine shriek
 end module angular_momentum
