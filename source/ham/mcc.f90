module mcc_mod
! continuum-continuum part of dipole calculation
! Time-stamp: "2005-02-04 10:44:53 dlcjn1"
  use precisn, only: wp
  use io_units, only: fo, jbuffm, jbuffv2d, jbufiv2d
  use crlmat_mod, only: crl, crlv
  use error_prt, only: alloc_error
  use debug, only: bug2
  implicit none

  integer, save               :: chs
  real(wp), allocatable       :: rccl(:,:), rccv(:,:)
  real(wp), allocatable       :: rbcl(:), rbcv(:)

  real(wp), allocatable, save :: acoef(:,:)
  real(wp), allocatable, save :: blcoef(:,:)
  real(wp), allocatable, save :: bvcoef(:,:)

  private
  public mcc, def_coefs, wrt_coefs, zero_coefs

contains

  subroutine def_coefs (chsp)
    use error_prt, only: alloc_error
    integer, intent(in)       :: chsp
    integer                   :: status

    chs = chsp
    allocate (acoef(chs,chs), blcoef(chs,chs), bvcoef(chs,chs), &
         stat=status)
    if (status /= 0) call alloc_error (status, 'def_coefs', 'a')
    acoef = 0.0_wp
    blcoef = 0.0_wp
    bvcoef = 0.0_wp
  end subroutine def_coefs

  subroutine zero_coefs
    acoef = 0.0_wp
    blcoef = 0.0_wp
    bvcoef = 0.0_wp
  end subroutine zero_coefs

  subroutine wrt_coefs (jbuffm, inch, jnch)
    use filehand, only: writc
    integer, intent(in)       :: jbuffm
    integer, intent(in)       :: inch
    integer, intent(in)       :: jnch

    call writc (jbuffm, acoef, 1, inch, 1, jnch, chs, 0)
    call writc (jbuffm, blcoef, 1, inch, 1, jnch, chs, 0)
    call writc (jbuffm, bvcoef, 1, inch, 1, jnch, chs, 0)
  end subroutine wrt_coefs

  subroutine mcc (nstset, ncnatj, l2pj, inb, inch, ncnati, isetch, &
       l2pi, lrgli, lrglj, istch, inbi, fac, jnch, lrang1, mpol, idirbc)
    use set_data, only: nset, lset, isset, ipset, nast, nstat
    use symmetry_check, only: npset
    use states, only: nsetst, ll, lspn
    use angular_momentum, only: dracah, rme
    use filehand, only: writc
    use rad_data, only: nvary
    use amul, only: isize, amul1, amul2, mvel, icpt
    use imbc_data, only: imbci
    use rm_data, only: nrang2
    integer, intent(in)     :: lrang1, mpol
    integer, intent(in)     :: lrgli, lrglj
    integer, intent(in)     :: nstset(:)
    integer, intent(in)     :: ncnatj(:), ncnati(:)
    integer, intent(in)     :: l2pj(:), l2pi(:)
    integer, intent(in)     :: inb, inch
    integer, intent(in)     :: isetch(:)
    integer, intent(in)     :: istch(:)
    integer, intent(in)     :: inbi
    real(wp), intent(in)    :: fac
    integer, intent(in)     :: jnch
    integer, intent(inout)  :: idirbc
    real(wp)                :: ractab(lrang1)
    integer                 :: jst, js, lcfgj, lcfgj2, jch, lj, lj2
    integer                 :: nraj, ich, ispin, lcfgi, lcfgi2, ipi
    integer                 :: ichan, li, li2, isetchn, nrai, nsym
    integer                 :: ni, nj, nq, nraji, lsiglo, lsighi, lsig
    integer                 :: lsig2, k, j, issc, istart, is, mpol2
    integer                 :: ist, status, jspin, jchan, lrglj2
    integer                 :: l, lrgli2
    real(wp)                :: sign1, sign2, cc1, rac, ccdiag
    real(wp), pointer       :: crlp(:,:)

    crlp => crl(:,:,mpol)
    if (allocated(rccl)) then
       deallocate (rccl, stat=status)
       if (status /= 0) call alloc_error (status, 'mcc', 'd')
    end if
    if (allocated(rccv)) then
       deallocate (rccv, stat=status)
       if (status /= 0) call alloc_error (status, 'mcc', 'd')
    end if
    if (allocated(rbcl)) then
       deallocate (rbcl, stat=status)
       if (status /= 0) call alloc_error (status, 'mcc', 'd')
    end if
    if (allocated(rbcv)) then
       deallocate (rbcv, stat=status)
       if (status /= 0) call alloc_error (status, 'mcc', 'd')
    end if
    allocate (rccl(nrang2,nrang2), rccv(nrang2,nrang2), &
         rbcl(nrang2), rbcv(nrang2), stat=status)
    if (status /= 0) call alloc_error (status, 'mcc', 'a')

    jch = 0
    mpol2 = mpol + mpol
    lrgli2 = lrgli + lrgli
    lrglj2 = lrglj + lrglj
    jstate_L: do jst = 1, nast
       js = nstset(jst)
       lcfgj = ll(jst)
       lcfgj2 = lcfgj + lcfgj
       jspin = lspn(jst)
       jchan_L: do jchan = 1, ncnatj(jst)  ! final channels
          jch = jch + 1
          lj = l2pj(jch)
          lj2 = lj + lj
          nraj = nvary(lj+1)
          amul1 = 0.0_wp
          if (mvel==2) amul2 = 0.0_wp
          ich = 0
          sets: do is = 1, nset ! SLpi sets
             ispin = isset(is)
             if (ispin /= jspin) then   ! spins must be equal
                ich = ich + nstat(is) * ncnati(nsetst(is,1))
                cycle
             end if
             lcfgi = lset(is)
             lcfgi2 = lcfgi + lcfgi
             ipi = ipset(is)
             ichan_L: do ichan = 1, ncnati(nsetst(is,1))
                isetchn = isetch(is) - 1
                li = l2pi(isetchn+ichan)
                li2 = li + li
                nrai = nvary(li+1)
                sign1 = 1.0_wp
                if (MOD(mpol+li+lcfgi+lrglj,2) == 1) sign1 = -1.0_wp
                sign2 = 1.0_wp
                if (MOD(mpol+li+lcfgj+lrgli,2) == 1) sign2 = -1.0_wp
                nsym_loop: do nsym = 1, nstat(is) ! initial states
                   ist = nsetst(is,nsym)
                   issc = istch(ist) + ichan - 1
                   istart = icpt(issc)

! if states identical:
                   if (ist == jst .and. ABS(li-lj) <= mpol &
                        .and. (li+lj) >= mpol .and. &
                        MOD(li+lj+mpol,2) == 0) then

! locate radial integrals:
                      call locmcc (li+1, lj+1, mpol, nrai, nraj)
                      call dracah (lrglj2, lrgli2, lj2, li2, &
                           mpol2, lcfgi2, rac)
                      cc1 = fac * sign1 * rac * rme(lj,li,mpol)

                      if (mpol == 1) acoef(issc,jch) = cc1
                      do nj = 1,nraj
                         do ni = 1,nrai
                            nq = ni + istart - 1
                            amul1(nq,nj) = amul1(nq,nj) + cc1 * &
                                 rccl(nj,ni)
                         end do
                      end do
                      if (mvel == 2) then
                         do nj = 1,nraj
                            do ni = 1,nrai
                               nq = ni + istart - 1
! seems like it needs a phase change... no
                               amul2(nq,nj) = amul2(nq,nj) + &
                                    cc1 * rccv(nj,ni)
                            end do
                         end do
                      end if
                   end if

! same continuum orbitals:
                   if (li == lj .and. ABS(lcfgi-lcfgj) <= mpol &
                        .and. (lcfgi+lcfgj) >= mpol) then
                      call dracah (lrglj2, lrgli2, lcfgj2, &
                           lcfgi2, mpol2,li2,rac)
                      ccdiag = fac * sign2 * rac
!
                      nraji = MIN(nrai, nraj)
                      do ni = 1, nraji
                         nq = ni + istart - 1
                         amul1(nq,ni) = amul1(nq,ni) + &
                              ccdiag * crlp(ist,jst)
                      end do
                      if (mvel == 2) then
                         do ni = 1, nraji
                            nq = ni + istart - 1
! phase unchanged - not sure - not if indices changed...
                            amul2(nq,ni) = amul2(nq,ni) + &
                                 ccdiag * crlv(ist,jst)
                         end do
                      end if

                      if (mpol == 1) then ! angular coeffs
                         blcoef(issc,jch) = amul1(istart,1)
                         bvcoef(issc,jch) = amul2(istart,1)
                      end if
                   end if

                end do nsym_loop
             end do ichan_L
             ich = ich + nstat(is) * ncnati(nsetst(is,1))
          end do sets
          if (ich /= inch) then  ! not all channels treated
             write (fo,'(a)') 'stmmat: wrong number of initial &
                  &channels treated'
             stop
          end if
! continuum-continuum part completed
          print *,'TEST : ',inbi,npset,imbci(js,inbi)
          if (inbi <= npset) then
             if (imbci(js,inbi) /= -999) then
! construct table of racah coefficients
                lsiglo = MAX(ABS(lj-mpol), ABS(lrgli-lcfgj))
                if (MOD(lsiglo+lj+mpol,2) == 1) lsiglo = lsiglo + 1
                lsighi = MIN(lrgli+lcfgj, lj+mpol)
                lsighi = MIN(lrang1-1, lsighi)

                do lsig = lsiglo, lsighi, 2
                   lsig2 = lsig + lsig
                   call dracah (lrglj2, lrgli2, lj2, lsig2, mpol2,&
                        lcfgj2, rac)
                   ractab(lsig+1) = rac
                end do
                istart = icpt(inch+1) ! position for writing

! loop over (n+1)-e configurations
                call dobc (inb, inbi, js, jst, lj, nraj,     &
                     lcfgj, lrglj, istart, ractab, fac, mpol, &
                     idirbc)
             end if
          end if

! write bound_continuum contributions to disk:
          call writc (jbuffm, amul1, 1, isize, 1, nraj, isize, 0)
          if (mvel == 2) call writc (jbuffm, amul2, 1, isize,&
               1, nraj, isize, 0)

! write c-c and b-c contributions for this final channel:
          if (bug2 > 0) then
             write (fo,'(a,i3)') 'final state channel: ', jch

             write (fo,'(a)') 'continuum-continuum length part'
             do k = 1, inch
                do j = 1, nraj
                   write (fo,'(2x,8f15.7)') (amul1(l,j),l=icpt(k),&
                        icpt(k+1)-1)
                end do
                write (fo,'(/)')
             end do
             write (fo,'(a)') 'bound-continuum length part'
             if (inb /= 0) then
                do j = 1, nraj
                   write (fo,'(2x,8f15.7)') (amul1(k,j), &
                        k=icpt(inch+1), icpt(inch+1)+inb-1)
                   write (fo,'(/)')
                end do
             end if

             if (mvel == 2) then
                write (fo,'(a)') 'continuum-continuum velocity part'
                do k = 1, inch
                   do j = 1, nraj
                      write (fo,'(2x,8f15.7)') (amul2(l,j),l=icpt(k),&
                           icpt(k+1)-1)
                   end do
                   write (fo,'(/)')
                end do
                write (fo,'(a)') 'bound-continuum velocity part'
                if (inb /= 0) then
                   do j = 1, nraj
                      write (fo,'(2x,8f15.7)') (amul2(k,j), &
                           k=icpt(inch+1), icpt(inch+1)+inb-1)
                      write (fo,'(/)')
                   end do
                end if
             end if
          end if

       end do jchan_L
    end do jstate_L

! test that all final channels had been treated:
    if (jch /= jnch) then
       write (fo,'(a)') 'mcc: wrong number of final channels treated'
       stop
    end if
  end subroutine mcc

  subroutine dobc (inb, inbi, js, jst, lj, nraj, lcfgj, lrglj,&
       istart, ractab, fac, mpol, idirbc)
    use rad_data, only: maxnc
    use comp_data, only: njcomp, ljcomp
    use states, only: aij
    use angular_momentum, only: rme
    use bcang, only: abc2, irs2, getbcang
    use amul, only: amul1, amul2, mvel
    use imbc_data, only: imbc
    integer, intent(in)              :: inb
    integer, intent(in)              :: inbi
    integer, intent(in)              :: js, jst
    integer, intent(in)              :: istart
    integer, intent(in)              :: lrglj, nraj
    integer, intent(in)              :: lcfgj, lj
    real(wp), intent(in)             :: ractab(:)
    real(wp), intent(in)             :: fac
    integer, intent(in)              :: mpol
    integer, intent(inout)           :: idirbc
    integer                          :: icsnp, ncount, jci, ii1, jcsn
    integer                          :: lsigi, nsigi, maxnci, nj, nq
    integer                          :: isig, maxorb
    real(wp)                         :: sign3, bc1

    maxorb = SIZE(njcomp)    ! or the original maxorb?
    print *,'ENTER DOBC'
    tgt_cfgs: do icsnp = 1, inb
       ncount = imbc(icsnp,js,inbi)
       if (ncount /= 0) then
          call getbcang (js, icsnp, inb, inbi, idirbc, jbufiv2d, &
               jbuffv2d)
       end if
       nq = istart + icsnp - 1
       jci_loop: do jci = 1, ncount
          ii1 = irs2(jci)
          isig = MOD(ii1,maxorb+1)
          jcsn = ii1 / (maxorb + 1)
          lsigi = ljcomp(isig)
          nsigi = njcomp(isig)
          maxnci = maxnc(lsigi+1)
          if (MOD(lsigi+lj+mpol,2) == 0 .and. ABS(lsigi-lj) <= mpol &
               .and. lsigi+lj >= mpol) then
             sign3 = 1.0_wp
             if (MOD(mpol+lcfgj+lsigi+lrglj,2) == 1) sign3 = -1.0_wp
! locate b-c radial integral
!            print *,'GET SOME DATA FROM MBC'
             call locmbc (nsigi, lsigi+1, lj+1, mpol, maxnci, nraj)
             bc1 = sign3 * fac * abc2(jci) * &
                  ractab(lsigi+1) * rme(lj,lsigi,mpol) * aij(jst,jcsn)
             continuum_l: do nj = 1, nraj ! length
                amul1(nq,nj) = amul1(nq,nj) + bc1 * rbcl(nj)
             end do continuum_l
             if (mvel == 2) then         ! velocity
                continuum_v: do nj = 1, nraj
                   amul2(nq,nj) = amul2(nq,nj) + bc1 * rbcv(nj)
                end do continuum_v
             end if
          end if
       end do jci_loop
    end do tgt_cfgs
    print *,'EXIT DOBC'
  end subroutine dobc

  subroutine locmcc (li, lj, mpol, nrai, nraj)
! locate the continuum continuum multipole integral
    use radial_integrals, only: iccpol, rmcc
    integer, intent(in)     :: li
    integer, intent(in)     :: lj
    integer, intent(in)     :: mpol
    integer, intent(in)     :: nrai
    integer, intent(in)     :: nraj
    integer                 :: istart, i1, ni, nj

    istart = iccpol(1,1)
    i1 = iccpol(li,lj) - istart
    if (i1 < 0) then
       write (fo,'(a,4i4)') 'locmcc error: li, lj, istart, i1 = ', &
            li-1, lj-1, istart, i1
       stop
    end if

! length:
    if (li < lj) then
       do ni = 1, nrai
          do nj = 1, nraj
             i1 = i1 + 1
             rccl(nj,ni) = rmcc(i1)
          end do
       end do
    else if (li > lj) then
       do nj = 1, nraj
          do ni = 1, nrai
             i1 = i1 + 1
             rccl(nj,ni) = rmcc(i1)
          end do
       end do
    else
       do ni = 1, nrai
          do nj = 1, ni
             i1 = i1 + 1
             rccl(nj,ni) = rmcc(i1)
             rccl(ni,nj) = rmcc(i1)
          end do
       end do
    end if
!
! velocity
!
    if (mpol == 1) then
       if (li < lj) then
          do ni = 1, nrai
             do nj = 1, nraj
                i1 = i1 + 1
                rccv(nj,ni) = rmcc(i1)
             end do
          end do
       else if (li > lj) then

! c-c radial integrals in velocity when li > lj are computed
!                               ra
! rcc_v(i,j) = [aorb_j * aorb_i]    -   rcc_v(j,i)
!                               0

          do nj = 1, nraj
             do ni = 1, nrai
                i1 = i1 + 1
                rccv(nj,ni) =  - rmcc(i1)
             end do
          end do
       else
          write (fo,'(a)') 'locmcc: v-gauge called for li=lj'
          stop
       end if
    end if
  end subroutine locmcc

  subroutine locmbc (ni, li, lj, mpol,  maxnci, nraj)
! locate the bound continuum multipole integral
    use radial_integrals, only: ibcpol, rmbc
    integer, intent(in)       :: ni
    integer, intent(in)       :: li
    integer, intent(in)       :: lj
    integer, intent(in)       :: mpol
    integer, intent(in)       :: maxnci
    integer, intent(in)       :: nraj
    integer                   :: istart, ivel, i1

    istart = ibcpol(1,1)
    ivel = 1
    if (mpol == 1) ivel = 2
    i1 = ibcpol(li,lj) - istart + (ni-maxnci-1) * nraj * ivel
    if (i1 < 0) then
       write (fo,'(a)') 'locmbc error: ni, li, lu, istart, i1 = ',&
            ni, li, lj, istart, i1
       stop
    end if
    rbcl(1:nraj) = rmbc(i1+1:i1+nraj)
    if (mpol == 1) then
       i1 = i1 + nraj
       rbcv(1:nraj) = rmbc(i1+1:i1+nraj)
    end if
  end subroutine locmbc
end module mcc_mod
