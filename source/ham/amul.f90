module amul
! Time-stamp: "02/01/25 07:49:45 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use error_prt, only: alloc_error
  implicit none

  integer, save               :: mvel = 1
  real(wp), allocatable, save :: amul1(:,:)
  real(wp), allocatable, save :: amul2(:,:)
  integer, allocatable, save  :: icpt(:)
  integer, save               :: isize
  integer, save               :: jsize

  private
  public alloc_icpt, set_jsize, alloc_amul
  public icpt, amul1, amul2, isize, jsize, mvel

contains

  subroutine alloc_icpt (inch, l2pi, inb)
! define index of channel positions
    use rad_data, only: nvary
    integer, intent(in)       :: inch
    integer, intent(in)       :: l2pi(:)
    integer, intent(in)       :: inb
    integer                   :: ich, status, lch

    if (allocated(icpt)) then
       deallocate (icpt, stat=status)
       if (status /= 0) call alloc_error (status, 'alloc_icpt', 'd')
    end if
    allocate (icpt(inch+1), stat=status)
    if (status /= 0) call alloc_error (status, 'alloc_icpt', 'a')
    icpt(1) = 1
    do ich = 2, inch+1
       lch = l2pi(ich-1)
       icpt(ich) = icpt(ich-1) + nvary(lch+1)
    end do
    isize = icpt(inch+1) + inb - 1
  end subroutine alloc_icpt

  subroutine set_jsize (jnch, l2pj, jnb)
    use rad_data, only: nvary
    integer, intent(in)       :: jnch
    integer, intent(in)       :: l2pj(:)
    integer, intent(in)       :: jnb
    integer                   :: jch, lch

    jsize = 0
    do jch = 1, jnch
       lch = l2pj(jch)
       jsize = jsize + nvary(lch+1)
    end do
    jsize = jsize + jnb
  end subroutine set_jsize

  subroutine alloc_amul (mvelt)
    integer, intent(in)       :: mvelt
    integer                   :: status, ijsize
    
    mvel = mvelt
    ijsize = MAX(isize, jsize)
    if (mvel == 1) then
       if (allocated(amul1)) then
          deallocate (amul1, stat=status)
          if (status /= 0) call alloc_error (status, 'alloc_amul', 'd')
       end if
       allocate (amul1(isize,ijsize), stat=status)
    else
       if (allocated(amul1)) then
          deallocate (amul1, amul2, stat=status)
          if (status /= 0) call alloc_error (status, 'alloc_amul', 'd')
       end if
       allocate (amul1(isize,ijsize), amul2(isize,ijsize), stat=status)
    end if
    if (status /= 0) call alloc_error (status, 'alloc_amul', 'a')
  end subroutine alloc_amul
end module amul

