module farm_interface
! Routines for output to FARM interface files
! Also output to PFARM interface files
! Time-stamp: "2005-02-04 10:20:48 dlcjn1"
  use precisn, only:wp
  use io_units, only: fo, itape3
  use rm_data, only: xdr, farm
  implicit none

  integer, save   :: funit     ! unit # for FARM output files

  private
  public open_farm_file, write1, write2, write3, writcf, close_farm_file
  public write3a, write3b

contains

  subroutine open_farm_file
! open H file for FARM or PFARM
    use xdr_files, only: open_xdr
    integer                      :: ios
    character(len=2)             :: fn     
    
    if (xdr) then   ! open XDR output H file
       if (farm) then
          fn = 'H'
       else
          fn = 'HX'
       end if
       funit = open_xdr (file=TRIM(fn), action='write')
    else
       if (farm) then
          fn = 'H'
       else
          fn = 'HN'
       end if
       funit = itape3
       open (unit=funit, file=TRIM(fn), access='sequential',      &
            status='new', form='unformatted', action='write',     &
            iostat=ios)
       if (ios /= 0) then
          write (fo,'(a,i6)') 'open_farm_file: i/o error, iostat = ',&
               ios
          stop
       end if
    end if
    write (fo,'(a,/)') 'Opacity style output file: ' // TRIM(fn)

  end subroutine open_farm_file

  subroutine close_farm_file
    use xdr_files, only: close_xdr
    integer           :: ios

    if (xdr) then
       call close_xdr (funit)
    else
       close (unit=funit, status='keep', iostat=ios)
       if (ios /= 0) then
          write (fo,'(a,i6)') 'close_farm_file: iostat = ', ios
          stop
       end if
    end if
  end subroutine close_farm_file

  subroutine write1 (ecore)
! write part of asymptotic file independent of lrgl, spin and parity
    use debug, only: bug9
    use rad_data, only: ra, lrang1, lrang2, maxnhf, maxnc, bsto, coeff
    use rm_data, only: nrang2, lamax, nz, nelc
    use states, only: ll, lspn, lpty, enat
    use set_data, only: nast
    use xdr_files, only: xdr_io
    real(wp), intent(in)           :: ecore
    integer                        :: ihbuf(5)
    integer                        :: l
    real(wp)                       :: rhbuf(2)

    if (xdr) then
       ihbuf(1:5) = (/nelc, nz, lrang2, lamax, nast/)
       rhbuf(1:2) = (/ra, bsto/)
       call xdr_io (funit, ihbuf, 5)
       call xdr_io (funit, rhbuf, 2)

       call xdr_io (funit, enat, nast)
       rhbuf(1) = ecore
       call xdr_io (funit, rhbuf, 1)
       call xdr_io (funit, ll, nast)
       call xdr_io (funit, lspn, nast)
       if (.NOT.farm) call xdr_io (funit, lpty, nast)
       do l = 1, lrang2
          call xdr_io (funit, coeff(:,l), 3)
       end do
    else
       write (funit) nelc, nz, lrang2, lamax, nast, ra, bsto
! nrang2 should be replaced by noterm
       write (funit) enat(1:nast) + ecore
       write (funit) ll(1:nast)
       write (funit) lspn(1:nast)
       if (.NOT.farm) write (funit) lpty(1:nast)
       write (funit) coeff(1:3,1:lrang2)
    end if

    if (bug9 > 2) then    ! echo numbers written to H
       write (fo,'(/,a,/)') 'subroutine write1:'
       write (fo,'(a)') 'data written on the h file:'

       write (fo,'(5i6,2e16.8)') nelc, nz, lrang2, lamax, nast, ra, bsto
       write (fo,'(3e16.8)') enat(1:nast)
       write (fo,'(10i6)') ll(1:nast)
       write (fo,'(10i6)') lspn(1:nast)
       if (.NOT.farm) write (fo,'(10i6)') lpty(1:nast)
       write (fo,'(3e16.8)') coeff(1:3,1:lrang2)
    end if
  end subroutine write1

  subroutine write2 (lrgl, ksp, npty, nch, ny, more, kco, l2p)
! write asymptotic file
    use xdr_files, only: xdr_io, hbufl
    integer, intent(in)        :: lrgl
    integer, intent(in)        :: ksp
    integer, intent(in)        :: npty
    integer, intent(in)        :: nch
    integer, intent(in)        :: ny
    integer, intent(in)        :: more
    integer, intent(in)        :: kco(:) ! kconat for isp
    integer, intent(in)        :: l2p(:) !l2pspn for isp
    integer                    :: nast
    integer                    :: xbuf(hbufl)

    xbuf(1:6) = (/lrgl, ksp, npty, nch, ny, more/)
    if (xdr) then   ! XDR output files
       call xdr_io (funit, xbuf, 6)
       if (nch == 0) then
          xbuf(1) = 0
          call xdr_io (funit, xbuf, 1)
          call xdr_io (funit, xbuf, 1)
       else
          nast = SIZE(kco)
          xbuf(1:nast) = kco
          call xdr_io (funit, xbuf(1:nast), nast)
          xbuf(1:nch) = l2p
          call xdr_io (funit, xbuf(1:nch), nch)
       end if
    else
       write (funit) xbuf(1:6) ! lrgl, ksp, npty, nch, ny, more
       if (nch == 0) then
          write (funit) 0.0_wp
          write (funit) 0.0_wp
       else
          write (funit) kco
          write (funit) l2p
       end if
    end if
  end subroutine write2

  subroutine writcf (cf, nch, lamax, isp, istchs)
! write asymptotic potential coefficients to disk
    use xdr_files, only: xdr_io, hbufl, close_xdr
    use channels, only: l2pspn
    use debug, only: bug4
    real(wp), intent(in) :: cf(:,:,:) ! asymp potl coefficients
    integer, intent(in)  :: nch     ! # channels
    integer, intent(in)  :: lamax   ! # multipoles
    integer, intent(in), optional :: isp     ! target spin
    integer, intent(in), optional :: istchs(:)! state #s cpld to channel
    real(wp)             :: xbuf(hbufl)
    integer              :: ich, jch, k, ib

    if (xdr) then   ! XDR output file
       if (nch == 0) then
          xbuf(1) = 0.0_wp
          call xdr_io (funit, xbuf, 1)
          return
       end if
       ib = 0
       do k = 1, lamax
          do jch = 1, nch
             do ich = 1, nch
                ib = ib + 1
                xbuf(ib) = cf(ich,jch,k)
                if (ib == hbufl) then
                   call xdr_io (funit, xbuf, hbufl)
                   ib = 0
                end if
             end do
          end do
       end do
       if (ib /= 0) call xdr_io (funit, xbuf, ib)
    else
       if (nch == 0) then
          write (funit) 0.0_wp
       else
          write (funit) cf(1:nch,1:nch,1:lamax)
       end if
    end if

    if (bug4 == 1 .and. nch /= 0) then
       write (fo,'(a,i3)') 'asymptotic coeffs for lambda = 1 to lamax &
            &for target spin (2s+1) =', isp
       do ich = 1, nch
          do jch = ich, nch
             write (fo,'(6(a,i3))') 'channel', ich, ' (state',&
                  istchs(ich), ', l=', l2pspn(ich,isp),  &
                  '), with channel', jch, ' (state', istchs(jch), &
                  ', l=', l2pspn(jch,isp), ')'
             write (fo,'(2x,8f14.7)') cf(ich,jch,1:lamax)
          end do
       end do
    end if
  end subroutine writcf

  subroutine write3 (nch, ny, ecore, value, wmat)
! write R-matrix eigenvectors and reduced-width amplitudes
    use xdr_files, only: xdr_io, hbufl
    integer, intent(in)   :: nch   ! # channels
    integer, intent(in)   :: ny    ! Hamiltonian dimension
    real(wp), intent(in)  :: ecore ! core energy
    real(wp), intent(in)  :: value(:) ! R-matrix eigenvectors
    real(wp), intent(in)  :: wmat(:,:) ! Reduced width amplitudes
    real(wp)              :: xbuf(hbufl)
    integer               :: i, j, ib

    if (xdr) then ! XDR file output
! write value+ecore:
       ib = 0
       do i = 1, ny
          ib = ib + 1
          xbuf(ib) = value(i) + ecore
          if (ib == hbufl) then
             call xdr_io (funit, xbuf, hbufl)
             ib = 0
          end if
       end do
       if (ib /= 0) call xdr_io (funit, xbuf, ib)
! write wmat:
       ib = 0
       do j = 1, ny
          do i = 1, nch
             ib = ib + 1
             xbuf(ib) = wmat(i,j)
             if (ib == hbufl) then
                call xdr_io (funit, xbuf, hbufl)
                ib = 0
             end if
          end do
       end do
       if (ib /= 0) call xdr_io (funit, xbuf, ib)
    else
       write (funit) value(:) + ecore
       write (funit) wmat(1:nch,1:ny)
    end if
  end subroutine write3

  subroutine write3a (ny, ecore, value)
! write R-matrix eigenvectors (PRMAT format)
    use xdr_files, only: xdr_io, hbufl
    integer, intent(in)   :: ny    ! # R-matrix states
    real(wp), intent(in)  :: ecore ! core energy
    real(wp), intent(in)  :: value(:) ! R-matrix eigenvectors
    real(wp)              :: xbuf(hbufl)
    integer               :: i, ib

    if (xdr) then ! XDR file output
       ib = 0
       do i = 1, ny
          ib = ib + 1
          xbuf(ib) = value(i) + ecore
          if (ib == hbufl) then
             call xdr_io (funit, xbuf, hbufl)
             ib = 0
          end if
       end do
       if (ib /= 0) call xdr_io (funit, xbuf, ib)
    else
       write (funit) value(:) + ecore
    end if
  end subroutine write3a

  subroutine write3b (nch, irm, wmat)
! write R-matrix reduced-width amplitudes (PRMAT format)
    use xdr_files, only: xdr_io, hbufl
    integer, intent(in)   :: nch   ! # channels
    integer, intent(in)   :: irm   ! R-matrix state index
    real(wp), intent(in)  :: wmat(:) ! Reduced width amplitudes
    real(wp)              :: xbuf(hbufl)
    integer               :: i, ib

    if (xdr) then ! XDR file output
       i = irm
       call xdr_io (funit, i) ! write state index
       ib = 0
       do i = 1, nch
          ib = ib + 1
          xbuf(ib) = wmat(i)
          if (ib == hbufl) then
             call xdr_io (funit, xbuf, hbufl)
             ib = 0
          end if
       end do
       if (ib /= 0) call xdr_io (funit, xbuf, ib)
    else
       write (funit) irm
       write (funit) wmat(1:nch)
    end if
  end subroutine write3b

end module farm_interface
