module comp_data
! Time-stamp: "02/01/25 07:50:21 cjn"
  use precisn, only: wp
  use io_units, only: fo, fi, jbuff1, jbuff4, itape1
  implicit none

  integer, allocatable, save  :: njcomp(:)
  integer, allocatable, save  :: ljcomp(:)

  private
  public def_comp
  public njcomp, ljcomp

contains

  subroutine def_comp (maxorb, ncore, nj, lj)
    integer, intent(in)       :: maxorb
    integer, intent(in)       :: ncore
    integer, intent(in)       :: nj(:), lj(:)
    integer                   :: status, m, iorb

    m = maxorb - ncore
    allocate (njcomp(m), ljcomp(m), stat=status)
    if (status /= 0) then
       write (fo,'(a,i4)') 'def_comp: allocation error = ', status
       stop
    end if
    do iorb = 1, m
       ljcomp(iorb) = lj(iorb+ncore)
       njcomp(iorb) = nj(iorb+ncore)
    end do
  end subroutine def_comp
end module comp_data
