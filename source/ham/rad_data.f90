module rad_data
! Time-stamp: "03/03/12 09:01:03 cjn"
  use precisn, only: wp
  use io_units, only: fi, fo, itape1
  implicit none

  integer, save                :: lrang1, lrang2
  integer, save                :: llpdim
  real(wp), save               :: ra
  real(wp), save               :: bsto
  integer, allocatable, save   :: maxnhf(:)
  integer, allocatable, save   :: maxnc(:)
  integer, allocatable, save   :: nvary(:)

  real(wp), allocatable, save  :: ends(:,:)
  real(wp), allocatable, save  :: dends(:,:)
  real(wp), allocatable, save  :: coeff(:,:)
  real(wp), allocatable, save  :: vabs(:,:,:)
  real(wp), allocatable, save  :: splcoef(:,:,:)
  integer                      :: nc,k9,ndim,nb,nt

  private
  public rd_maxnhf, rd_rad_hdr
  public maxnhf, maxnc, nvary, lrang1, lrang2
  public dends, ends, coeff, ra, bsto, vabs, splcoef, ndim

contains

  subroutine rd_maxnhf (lrang1, lrang2, ibop, nix, npot)
    integer, intent(in)      :: lrang1
    integer, intent(in)      :: lrang2
    integer, intent(in)      :: ibop
    integer, intent(in)      :: nix
    integer, intent(in)      :: npot
    integer                  :: status, i, n, l, maxn

    maxn = MAX(lrang1, lrang2)
    allocate (maxnhf(maxn), stat=status)
    read (fi,*) maxnhf(1:lrang1)
    read (fi,*)

    if (ibop == 0) then   ! skip slater bound orbital parameters
       do l = 1, lrang1
          do n = l, maxnhf(l)
             read (fi,*) 
             read (fi,*) 
             read (fi,*) 
             read (fi,*)
          end do
       end do
       if (nix >= 0) then ! skip integration mesh data
          read (fi,*)
          read (fi,*)
          read (fi,*)
       end if
    end if

    if (npot == 0) then ! skip data defining static potential
       do i = 1, lrang1
          read (fi,*)
       end do
    else if (npot >= 1) then ! skip parametric potential data
       read (fi,*)
       read (fi,*)
       read (fi,*)
    end if

! extend the maxnhf array
    if (lrang2 > lrang1) then
       do i = lrang1+1, lrang2
          maxnhf(i) = i - 1
       end do
    end if
  end subroutine rd_maxnhf

  subroutine rd_rad_hdr (lrang1, lrang2, ncore, ecore)
! read file rad3
    integer, intent(in)     :: lrang1
    integer, intent(in)     :: lrang2
    integer, intent(out)    :: ncore
    real(wp), intent(out)   :: ecore

    open (unit=itape1, file='RAD3', access='sequential', status='old',&
         form='unformatted')
    call read3 (lrang1, lrang2, ncore, ecore)
    if (ncore > 0) then
       write (fo,'(//,a,f16.8,a)') 'Energy of core configuration is ', &
            ecore, '   au'
    end if
  end subroutine rd_rad_hdr

  subroutine read3 (lrangb, lrangc, ncore, ecore)
! reads basic information from the file rad3
    use debug, only: bug3
    use rm_data, only: buttle
    integer, intent(in)     :: lrangb
    integer, intent(in)     :: lrangc
    integer, intent(out)    :: ncore
    real(wp), intent(out)   :: ecore
    integer                 :: nx, status, l, n, i

    rewind itape1
    read (itape1) ra, bsto, lrang1, lrang2, ncore, ecore
    if (lrang1 /= lrangb) then
       write (fo,'(a,i5)') 'calculated value of lrang1 is ', lrangb
       write (fo,'(a,i5)') 'value of lrang1 read from rad3 is ', lrang1
       stop
    end if
    allocate (maxnc(lrang1), nvary(lrang2), stat=status)
    read (itape1) maxnc
    if (lrang2 /= lrangc) then
       write (fo,'(a,i5)') 'calculated value of lrang2 is ', lrangc
       write (fo,'(a,i5)') 'value of lrang2 read from rad3 is ', lrang2
       stop
    end if
    read (itape1) nvary

    nx = MAXVAL(nvary)
    allocate (ends(nx,lrang2), coeff(3,lrang2), stat=status)
    allocate (dends(nx,lrang2))
    read (itape1) ((ends(n,l),n=1,nvary(l)),l=1,lrang2)
    if (buttle) read(itape1) coeff

    open(unit=34,file='RADdends',form='unformatted')
    read(34)  ((dends(n,l),n=1,nvary(l)),l=1,lrang2)
    close(34)
    print *,'Dends read'
    
    allocate(vabs(nx,nx,lrang2), stat=status)
    open(unit=34,file='AbsBouPot.dat',form='unformatted')
    do l=1,lrang2
     read(34) n, n
     read(34) vabs(1:nvary(l),1:nvary(l),l)
    enddo
    close(34)
    print *,'Absorb Boundary Data read'
    open(unit=34,file='Splinedata',form='unformatted')
    read (34) nc,k9,nb,ndim,nt
    read(34)
    allocate(splcoef(ndim,nx,lrang2), stat=status)
    do l=1,lrang2
     do i=1,nvary(l)
      read(34) splcoef(1:ndim,i,l)
     enddo
    enddo
    close(34)
    open(unit=39,file='BAderiv',form='unformatted')

    if (bug3 > 0) then
       write (fo,'(/,a)') 'read3: data read from file rad3:'
       write (fo,'(2f16.9,3i5,f16.9)') ra, bsto, lrang1, lrang2, &
            ncore, ecore
       write (fo,'(10i5)') maxnc
       write (fo,'(10i5)') nvary
       write (fo,'(6f16.9)') (ends(1:nvary(l),l),l=1,lrang2)
       if (buttle) write (fo,'(6e16.9)') coeff
    end if
  end subroutine read3

end module rad_data
