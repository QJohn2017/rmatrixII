module crlmat_mod
! Time-stamp: "02/02/01 07:45:14 cjn"
  use precisn, only: wp
  use io_units, only: fo, jbuff1, jbufiv, jbuffv, itape1
  use debug, only: bug2
  use consts, only: eps
  use error_prt, only: alloc_error
  implicit none

  integer, allocatable, save          :: ibbpol(:,:,:)
  real(wp), allocatable, target, save :: crl(:,:,:)
  real(wp), allocatable, save         :: crlv(:,:)

  real(wp), allocatable, save :: rkcc(:), vsh(:)
  integer, allocatable, save  :: irhsgl(:)

  private
  public crlmat, locmbb
  public crl, crlv, rkcc

contains
  subroutine crlmat (mpol, mvely, nbbpol, nbcpol, nccpol)
! calculates reduced matrix whose els (multiplied by a z coefficient)
! used to calculate asymptotic  coeffs and CC multipole els
! if mvely=1 calculate crl matrix for lam=1, store it
! in crlv (velocity matrix for all ist-jst is computed)
    use accdi, only: lamij, ivcont, ncset
    use rad_data, only: lrang1
    use set_data, only: nset, lset, nast, nstat
    use states, only: nsetst, aij
    use comp_data, only: njcomp, ljcomp
    use rm_data, only: lamax
    use filehand, only: lookup, startr, readb, movef, moveb, ireadb
    integer, intent(in)            :: mpol
    integer, intent(in)            :: mvely ! = 1, calc crl for lam=1
    integer, intent(out)           :: nbbpol
    integer, intent(out)           :: nbcpol
    integer, intent(out)           :: nccpol
    real(wp)                       :: crladd(lamax)
    real(wp)                       :: rvall(lamax)
    real(wp)                       :: vsa(lamax)
    integer                        :: ifor, is, js, lcfgp, lamcfg, lamlu
    integer                        :: lamlo, lamup, lamlo1, lamup1, lcfg
    integer                        :: isjs, icont1, icont2, nvshel
    integer                        :: icount, jcslo, nterm, nsh, nshhun
    integer                        :: ish, lam1, lamst, klamst
    integer                        :: isig, lsig, nsig, klam2, kc, klamf
    integer                        :: msign, irsl, irs, irho, lrho
    integer                        :: lam, ist, jstlo, jst, jstat, istat
    integer                        :: lamind, iccdif, nrec, status, nbuf
    integer                        :: ics, jcs, nshsgl, lam2, klam, nrho
    real(wp)                       :: vs, crladv, ai, ai2, aj, aiaj, rvv
    real(wp)                       :: rval

    if (bug2 > 1) then
       write (fo,'(a)') 'subroutine crlmat'
       write (fo,'(a)') '-----------------'
       write (fo,'(a)') '     is   js  ics  jcs  lam   crladd        &
            &crladv'
    end if
    lamind = (lamax + 1) / 2
    if (allocated(ibbpol)) then
       deallocate (ibbpol, stat=status)
       if (status /= 0) call alloc_error (status, 'crlmat', 'd')
    end if
    if (allocated(crl)) then
       deallocate (crl, stat=status)
       if (status /= 0) call alloc_error (status, 'crlmat', 'd')
    end if
    if (allocated(crlv)) then
       deallocate (crlv, stat=status)
       if (status /= 0) call alloc_error (status, 'crlmat', 'd')
    end if
    allocate (ibbpol(lrang1,lrang1,lamind), crl(nast,nast,lamax), &
         crlv(nast,nast), stat=status)
    if (status /= 0) call alloc_error (status, 'crlmat', 'a')

! read first radial integral pointer block
    read (itape1) ibbpol, nbbpol
    if (mpol > 0) read (itape1) nbcpol, nccpol
    if (mvely == 1) crlv = 0.0_wp
    crl = 0.0_wp
    print *,ibbpol
    print *,nbbpol

! read bound bound multipole integrals into rkcc array
! starting position was stored in first element of ibbpol
    iccdif = ibbpol(1,1,1)
    call lookup (iccdif, nrec, nbuf)
    call startr (jbuff1, nrec, nbuf)
    if (allocated(rkcc)) then
       deallocate (rkcc, stat=status)
       if (status /= 0) call alloc_error (status, 'crlmat', 'd')
    end if
    allocate (rkcc(nbbpol), stat=status)
    if (status /= 0) call alloc_error (status, 'crlmat', 'a')
    call readb (jbuff1, rkcc, 1, nbbpol)

! loop over sets; set pointer for read of angular integrals
    ifor = 0
    i_sets: do is = 1, nset
       lcfg = lset(is)
       j_sets: do js = is, nset
          lcfgp = lset(js)
          lamcfg = lcfg + lcfgp
          lamlu = lamij(is,js)

! test whether sets is,js can couple
! if they can lamij contains the limits on lambda imposed
! by shell coupling between configurations in the sets
          if (lamlu == -999 .or. lamlu == 0) cycle j_sets
          lamlo = lamlu / 100
          lamup = MOD(lamlu, 100)
          lamlo1 = lamlo
          if (lamlo == 0) lamlo1 = lamlo1 + 2
          lamup1 = MIN(lamup, lamax)

! transfer target angular integrals for this set combination to vsh
          isjs = nset * (is-1) - (is*(is-1))/2 + js
          icont1 = ivcont(isjs)
          icont2 = ivcont(isjs+1) - 1
          nvshel = icont2 - icont1 + 1
!          if (icont2-icont1 >= SIZE(vsh)) then
!             write (fo,'(a)') 'crlmat: dimension of vsh too small'
!             write (fo, '(2(a,i3),3(a,i5))') 'is = ', is, ' js = ', js,&
!                  ' ivdim1 = ', SIZE(vsh), ' icont1 = ', icont1,       &
!                  ' icont2 = ', icont2
!             stop
!          end if
          call lookup (icont1, nrec, nbuf)
          if (ifor > 0) then
             call movef (jbufiv, nrec, nbuf)
             call movef (jbuffv, nrec, nbuf)
          else if (ifor == 0) then
             call startr (jbufiv, nrec, nbuf)
             call startr (jbuffv, nrec, nbuf)
          else
             call moveb (jbufiv, nrec, nbuf)
             call moveb (jbuffv, nrec, nbuf)
          end if
          if (allocated(irhsgl)) then
             deallocate (irhsgl, stat=status)
             if (status /= 0) call alloc_error (status, 'crlmat', 'd')
          end if
          if (allocated(vsh)) then
             deallocate (vsh, stat=status)
             if (status /= 0) call alloc_error (status, 'crlmat', 'd')
          end if
          allocate (irhsgl(nvshel), vsh(nvshel), stat=status)
          if (status /= 0) call alloc_error (status, 'crlmat', 'a')
          call ireadb (jbufiv, irhsgl, 1, nvshel)
          call readb (jbuffv, vsh, 1, nvshel)
          ifor = 1
          icount = 1

          i_configs: do ics = 1, ncset(is) ! cfgs in set is
             if (is == js) then
                jcslo = ics
             else
                jcslo = 1
             end if
             j_configs: do jcs = jcslo, ncset(js) ! cfgs in set js
                crladd(1:lamax) = 0.0_wp
                crladv = 0.0_wp
                nterm = 0
                if (irhsgl(icount) < 0) then !cfgs w same shell structure
                   nsh = ABS(irhsgl(icount)) / 10000
                   nshhun = 100 * nsh
                   ish_loop: do ish = 1, nsh
                      nshsgl = ABS(irhsgl(icount))
                      lam1 = MOD(nshsgl, 100)
                      if (lam1 == 99) then
                         icount = icount + 1
                         cycle ish_loop
                      end if
                      lamst = lam1
                      klamst = (lam1 + 1) / 2
                      if (klamst == 0) then
                         lamst  = lam1 + 2
                         klamst = 1
                         icount = icount + 1
                      end if
                      isig = nshsgl / 100 - nshhun
                      lsig = ljcomp(isig)
                      nsig = njcomp(isig)
                      lam2 = MIN(lsig+lsig, lamup)
                      klam2 = (lam2 + 1) / 2
                      kc = icount - klamst
                      klamf = MIN(lamind, klam2)
                      lam = lamst
                      klam_loop: do klam = klamst, klamf
                         nterm = 1
                         vsa(klam) = vsh(kc+klam)
                         call locmbb (lsig+1, lsig+1, nsig, nsig,&
                              lam, rkcc, rvall(klam), rvv)
                         crladd(klam) = crladd(klam) + vsa(klam) * &
                              rvall(klam)
                         if (bug2 > 1) then
                            write (fo,'(A1,2x,5i5,e14.7)') 'L',is, js, ics, jcs,&
                                 klam, crladd(klam)
                         end if
                         if (mvely == 1 .and. lam == 1) then
                            crladv = crladv + vsa(klam) * rvv
                            msign = 1
                            if (bug2 > 1) then
                               write (fo,'(a1,2x,5i5,2e14.7)') 'V',is, js, ics, &
                                    jcs, klam, crladv
                            end if
                         endif
                         lam = lam + 2
                      end do klam_loop
                      icount = kc + klam2 + 1
                   end do ish_loop
                else ! cfgs having different shell structure
                   irsl = irhsgl(icount)
                   if (irsl == 9999) then
                      icount = icount + 1
                      cycle j_configs
                   end if
! find the interacting shells and the first lambda from irsl
                   lam1 = MOD(irsl, 100)
                   irs = irsl / 100
                   irho = irs / 100
                   isig = MOD(irs, 100)
                   lrho = ljcomp(irho)
                   lsig = ljcomp(isig)
                   lam2 = MIN(lrho+lsig, lamup)
                   nrho = njcomp(irho)
                   nsig = njcomp(isig)
                   lam_loop2: do lam = lam1, lam2, 2
                      if (lam == 0 .or. lam > lamax) then
                         icount = icount+1
                         cycle
                      end if
                      klam = (lam + 1)/2
                      vs = vsh(icount)
                      icount = icount + 1
                      if (ABS(vs) <= eps) then
                         crladd(klam) = 0.0_wp
                         crladv = 0.0_wp
                         cycle
                      end if
! locate the radial integral:
                      call locmbb (lrho+1, lsig+1, nrho, nsig, &
                           lam, rkcc, rval, rvv)
                      print *,nrho,lrho,nsig,lsig,rval,rvv
                      crladd(klam) = vs * rval
                      if (mvely == 1 .and. lam == 1) then
                         crladv = vs * rvv
                         msign = -1
                      end if
                      if (bug2 > 1) then
                         write (fo,'(a1,2x,5i5,e14.7)') 'L',is, js, ics, jcs,&
                              klam, crladd(klam)
                         if (mvely == 1 .and. lam == 1) then
                            write (fo,'(A1,2x,5i5,2e14.7)') 'V',is, js, ics, &
                                 jcs, klam, crladd(klam), crladv
                         else
                            write (fo,'(A1,2x,5i5,e14.7)') 'L',is, js, ics, jcs, &
                                 klam, crladd(klam)
                         end if
                      end if
                      nterm = 1
                   end do lam_loop2
                end if

                if (nterm == 1) then
                   ist_loop2: do ist = 1, nstat(is) ! tgts from set is
! find state number and thus the coefficient aij:
                      istat = nsetst(is,ist)
                      ai = aij(istat,ics)
! if sets is, js are same, a pair of cfgs will contribute twice to 
! compensate for saving made as a result of symmetry.
                      if (is == js .and. ics /= jcs) then
                         ai2 = aij(istat,jcs)
                      end if
                      if (is == js) then ! modify limits
                         jstlo = ist
                      else
                         jstlo = 1
                      end if
                      jst_loop2: do jst = jstlo, nstat(js) !tgts from js
                         jstat = nsetst(js,jst)
                         aj = aij(jstat,jcs)
                         aiaj = ai * aj
                         if (is == js .and. ics /= jcs) then
                            aiaj = aiaj + ai2 * aij(jstat,ics)
                         end if
                         if (ABS(aiaj) <= eps) cycle

! add in contribution to crl matrix for each lambda
                         lam_loop3: do lam = lamlo1, lamup1, 2
                            klam = (lam + 1) / 2
                            if (mvely == 1 .and. klam == 1) then
                               crlv(istat,jstat) = crladv*aiaj + &
                                    crlv(istat,jstat)
!!! PROBLEM --- DEFINITION OF MSIGN. TO KEEP THINGS RUNNING: -1
!                              msign = -1
                               if (abs(crladv).gt.eps) then
                               crlv(jstat,istat) = REAL(msign,wp) * &
                                    crladv * aiaj + crlv(jstat,istat)
                               end if
                            end if
                            if (jstat < istat) then
                               crl(jstat,istat,lam) = crladd(klam) * &
                                    aiaj + crl(jstat,istat,lam)
                            else
                               crl(istat,jstat,lam) = crladd(klam) * &
                                    aiaj + crl(istat,jstat,lam)
                            end if
                         end do lam_loop3
                      end do jst_loop2
                   end do ist_loop2
                end if
             end do j_configs
          end do i_configs

          if (icount /= nvshel + 1) then ! vsh not completely used
             write (fo,'(a,2i4)') 'crlmat: error using vsh file for &
                  &sets ', is, js
             write (fo,'(a,2i8)') 'nvshel,icount', nvshel, icount
             stop
          end if
       end do j_sets
    end do i_sets

    if (bug2 > 1) then
       do lam = 1, lamax
          write (fo,'(a,i2)') 'crl matrix  lambda= ', lam
          do istat = 1, nast
             do jstat = 1, nast 
                if (mvely == 1 .and. lam == 1) then
                   write (fo,'(2x,2i4,2e14.7)') istat, jstat, &
                        crl(istat,jstat,lam), crlv(istat,jstat)
                else
                   write (fo,'(2x,2i4,e14.7)') istat, jstat, &
                        crl(istat,jstat,lam)
                end if
             end do
          end do
       end do
    end if
  end subroutine crlmat

  subroutine locmbb (l1, l1p, n1, n1p, lam, rmbb, rvall, rvalv)
! Locate BB multipole integral (velocity also if lam=1)
! Integrals are stored in order: l1 <= l1p; if l1=l1p  then  n1>=n1p
    use rad_data, only: maxnhf, maxnc
    integer, intent(in)        :: l1, n1
    integer, intent(in)        :: l1p, n1p
    integer, intent(in)        :: lam
    real(wp), intent(in)       :: rmbb(:)
    real(wp), intent(out)      :: rvall
    real(wp), intent(out)      :: rvalv
    integer                    :: lm, maxnc1, maxncp, ni, nj, ibb, maxn
    integer                    :: nij

    rvalv = 0.0_wp
    lm = (lam + 1) / 2
    maxnc1 = maxnc(l1)
    maxncp = maxnc(l1p)
    ni = n1 - maxnc1
    nj = n1p - maxncp
    if (l1 < l1p) then
       ibb = ibbpol(l1,l1p,lm)
       maxn = maxnhf(l1p) - maxncp
       nij = (ni-1) * maxn + nj
    else if (l1 > l1p) then
       ibb = ibbpol(l1p,l1,lm)
       maxn = maxnhf(l1) - maxnc1
       nij = (nj-1) * maxn + ni
    else
       ibb = ibbpol(l1,l1p,lm)
       if (ni >= nj) then
          nij = (ni * (ni-1)) / 2 + nj
       else
          nij = (nj * (nj-1)) / 2 + ni
       end if
    end if

    if (ibb == -999) then
       write (fo,'(a)') 'LOCMBB: no pointer found in ibbpol for:'
       write (fo,'(3(a,i4))') 'l1 = ', l1-1, ' l1p = ', l1p-1, &
            ' lam = ', lam
       stop
    end if

    if (lam /= 1) then
       nij = nij + ibb - ibbpol(1,1,1)
       rvall = rmbb(nij)
    else
       nij = 2 * nij + ibb - ibbpol(1,1,1) - 1
       rvall = rmbb(nij)
       rvalv = rmbb(nij+1)
       if (l1 > l1p) rvalv = - rvalv
    end if
  end subroutine locmbb
!
end module crlmat_mod
