module prmat_interface
! routines for outputting data to parallel code PRMAT
! Time-stamp: "2005-01-07 17:46:55 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use rm_data, only: xdr
  implicit none

  integer, save              :: itmom   ! target moment xdr file unit #
  integer, save              :: iunit
  character(len=8), save     :: hname

  private
  public writh1, wrtmom, wrth2, open_prmat_file

contains
  subroutine writh1
! write part of asymptotic file that is independent of lrgl, spin, parity
    use xdr_files, only: xdr_io
    use debug, only: bug9
    use rm_data, only: nelc, nz, lamax
    use set_data, only: nast
    use rad_data, only: nvary, lrang2, ends, coeff, ra, bsto
    use states, only: ll, lspn, lpty, enat
    use stghrd_mod, only: ecore
    integer                 :: ihbuf(5)
    integer                 :: i, n, l
    real(wp)                :: rhbuf(2)

    write (fo,'(a)') 'Writing H file: ' // TRIM(hname)

    if (xdr) then
       ihbuf(1:5) = (/nelc, nz, lrang2, lamax, nast/)
       rhbuf(1:2) = (/ra, bsto/)
       call xdr_io (iunit, ihbuf, 5)
       call xdr_io (iunit, rhbuf, 2)

       call xdr_io (iunit, enat, nast)
       call xdr_io (iunit, ecore)
       call xdr_io (iunit, ll, nast)
       call xdr_io (iunit, lspn, nast)
       call xdr_io (iunit, lpty, nast)
       call xdr_io (iunit, nvary, lrang2)

       do l = 1, lrang2
          call xdr_io (iunit, ends(:nvary(l),l), nvary(l))
       end do
       do l = 1, lrang2
          call xdr_io (iunit, coeff(:,l), 3)
       end do
    else          ! normal fortran unformatted output
       write (iunit) nelc, nz, lrang2, lamax, nast, ra, bsto
       write (iunit) enat(1:nast), ecore
       write (iunit) ll(1:nast)
       write (iunit) lspn(1:nast)
       write (iunit) lpty(1:nast)
       write (iunit) nvary(1:lrang2)
       write (iunit) ((ends(n,l), n=1,nvary(l)), l=1,lrang2)
       write (iunit) coeff(1:3,1:lrang2)
    end if

    if (bug9 > 2) then
       write (fo,'(a)') 'subroutine writh1'
       write (fo,'(a)') 'Data written on the H file:'
       write (fo,'(5i5,2f12.6)') nelc, nz, lrang2, lamax, nast, ra, bsto
       write (fo,'(5f14.6)') enat
       write (fo,'(5i6)') ll
       write (fo,'(5i6)') lspn
       write (fo,'(5i6)') lpty
       write (fo,'(5i6)') nvary(1:lrang2)
       write (fo,'(5f14.6)') ((ends(n,l), n=1,nvary(l)), l=1,lrang2)
       write (fo,'(5f14.6)') ((coeff(i,l),i=1,3),l=1,lrang2)
    end if
  end subroutine writh1

  subroutine wrtmom
! write file of target moments
    use io_units, only: fo, itm
    use xdr_files, only: xdr_io, open_xdr, close_xdr
    use debug, only: bug2
    use rm_data, only: lamax
    use set_data, only: isplo, ispup, nast
    use states, only: ll, lspn, lpty
    use crlmat_mod, only: crl
    use error_prt, only: alloc_error
    real(wp), allocatable          :: tmom(:)
    integer                         :: ibuf(2)
    integer                         :: lamind, isp, ijl, ist, jst
    integer                         :: lamlo, lamup, lam, lamn, nijl
    integer                         :: ltmom, status, ios

    ltmom = (nast*(nast+1)/2) * ((lamax+1)/2)
    allocate (tmom(ltmom), stat=status)
    if (status /= 0) call alloc_error (status, 'wrtmom', 'a')

    if (xdr) then    ! output file used XDR format
       itmom = open_xdr (file='TARMOM', action='write')
       ibuf = (/nast, lamax/)
       call xdr_io (itmom, ibuf, 2)
       call xdr_io (itmom, ll, nast)
       call xdr_io (itmom, lspn, nast)
       call xdr_io (itmom, lpty, nast)
       ibuf = (/isplo, ispup/)
       call xdr_io (itmom, ibuf, 2) ! upper, lower limits on target multiplicity
    else             ! fortran unformatted output file
       itmom = itm
       open (unit=itmom, file='TARMOM', form='unformatted', &
            status='replace', action='write', iostat=ios)
       if (ios /= 0) then
          write (fo,'(a,i6)') 'wrtmom: error opening TARMOM, &
               &iostat = ', ios
          stop
       end if
       write (itmom) nast, lamax
       write (itmom) ll(1:nast)
       write (itmom) lspn(1:nast)
       write (itmom) lpty(1:nast)
       write (itmom) isplo, ispup
    end if

    lamind = (lamax + 1) / 2
    spins: do isp = isplo, ispup, 2
       ijl = 0
       state_1: do ist = 1, nast
          if (lspn(ist) == isp) then  ! spins match
             state_2: do jst = 1, ist
                if (lspn(jst) == isp) then ! spins match
                   lamlo = ABS(ll(ist) - ll(jst))
                   if (MOD(lpty(ist)+lpty(jst)+lamlo,2) == 1) lamlo = lamlo + 1
                   if (lamlo == 0) lamlo = 2
                   lamup = MIN(ll(ist)+ll(jst), lamax)
                   if ((lamlo+1)/2 <= (lamup+1)/2) then
                      lambda: do lam = lamlo, lamup, 2
                         ijl = ijl + 1
                         lamn = (lam + 1) / 2
                         tmom(ijl) = crl(jst,ist,lam)
                      end do lambda
                   end if
                end if
             end do state_2
          end if
       end do state_1
       nijl = ijl

       if (xdr) then
          ibuf = (/isp, nijl/)
          call xdr_io (itmom, ibuf, 2)
          call xdr_io (itmom, tmom, nijl)
       else
          write (itmom) isp, nijl
          write (itmom) tmom(1:nijl)
       end if

       if (bug2 > 0) then ! echo numbers written to itmom
          write (fo,'(a,i4,a,i4)') 'No. of states =', nast, &
               ' max lambda =', lamax
          write (fo,'(a)') 'Target L:'
          write (fo,'(25i3)') ll(1:nast)
          write (fo,'(a)') 'Target S:'
          write (fo,'(25i3)') lspn(1:nast)
          write (fo,'(a)') 'Target P:'
          write (fo,'(25i3)') lpty(1:nast)
          write (fo,'(a)') 'Multiplicity limits:'
          write( fo,'(25i3)') isplo, ispup

          write (fo,'(a,i4,a,i2,a,i5)') 'target spin = ', isp, &
               'lamn = ', lamn, 'number of elements = ', nijl
          write (fo,'(5f14.6)') tmom(1:nijl)
       end if
    end do spins

    if (xdr) then
       call close_xdr (itmom)
    else
       close (unit=itmom, status='keep')
    end if
    deallocate (tmom, stat=status)
    if (status /= 0) call alloc_error (status, 'wrtmom', 'd')
  end subroutine wrtmom

  subroutine wrth2 (ksp, more, nch, ny, hmat)
! write Hamiltonian matrix to disk using XDR format
    use xdr_files, only: xdr_io, hbufl, close_xdr
    use channels, only: kconat, l2pspn
    use l_data, only: lrgl, npty
    use set_data, only: nast
    integer, intent(in)      :: ksp        ! spin multiplicity
    integer, intent(in)      :: more       ! completion flag
    integer, intent(in)      :: nch        ! # channels
    integer, intent(in)      :: ny         ! Hamiltonian dimension
    real(wp), intent(inout)  :: hmat(:)    ! Hamiltonian matrix
    integer                  :: ibuf(6)
    integer                  :: ny2, isp, i, nm

    ny2 = SIZE(hmat)
    isp = ABS(ksp)
    if (xdr) then ! output to XDR file
       ibuf = (/lrgl, ksp, npty, nch, ny, more/)
       call xdr_io (iunit, ibuf, 6)
       call xdr_io (iunit, kconat(:nast,isp), nast)
       call xdr_io (iunit, l2pspn(:nch,isp), nch)

       ibuf(1) = hbufl
       call xdr_io (iunit, ibuf(1))
       write (fo,'(a,i8)') 'Buffer length used for h = ', ibuf(1)

       buffers: do i = 1, ny2, hbufl
          nm = MIN(hbufl, ny2-i+1)
          call xdr_io (iunit, hmat(i:i+nm-1), nm)
       end do buffers
       call close_xdr (iunit)
    else  ! fortran unformatted file output
       write (iunit) lrgl, ksp, npty, nch, ny, more
       write (iunit) kconat(1:nast,isp)
       write (iunit) l2pspn(1:nch,isp)
!      write (iunit) hmat
       do i = 1, ny2, hbufl
          nm = MIN(hbufl, ny2-i+1)
          write (iunit) hmat(i:i+nm-1)
       end do
       close (unit=iunit, status='keep')
    end if
  end subroutine wrth2

  subroutine open_prmat_file (ihunit, isp, lrgl, npty)
! set up file names:
    use xdr_files, only: open_xdr
    integer, intent(in)          :: ihunit  ! xdr file unit number
    integer, intent(in)          :: isp     ! spin multiplicity
    integer, intent(in)          :: lrgl    ! orb angular momentum
    integer, intent(in)          :: npty    ! parity
    character(len=2)             :: sp, l
    character(len=1)             :: p
    integer                      :: ios

    write (sp,'(i2.2)') isp
    write (l,'(i2.2)')  lrgl
    write (p, '(i1.1)') npty

    if (xdr) then   ! open XDR output file for Hamiltonian
       hname = 'HXD' // sp // l // p
       iunit = open_xdr (file=hname, action='write')
    else
       hname = 'HND' // sp // l // p
       open (unit=ihunit, file=hname, access='sequential',        &
            status='replace', form='unformatted', action='write', &
            iostat=ios)
       if (ios /= 0) then
          write (fo,'(a,i6)') 'open_prmat_file: error opening ' // &
               TRIM(hname) // ' iostat = ', ios
          stop
       end if
       iunit = ihunit
    end if
  end subroutine open_prmat_file

end module prmat_interface
