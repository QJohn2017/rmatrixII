module xdr_files
! f90 dummy interface to XDR routines
! used when library libfxdr.a is not available
! all calls result in noops except open_xdr which terminates
! Time-stamp: "2005-02-04 11:24:17 dlcjn1"
  use io_units, only: fo
  implicit none

  integer, save   :: hbufl = 2048   ! xdr buffer length
  integer, save   :: flag

  private
  public xdr_io, open_xdr, close_xdr, rewind_xdr
  public hbufl

  interface xdr_io
     module procedure xdr_int, xdr_imt, xdr_real
     module procedure xdr_rlmt, xdr_chrs
  end interface

contains
   function open_xdr (file, action)
! open xdr file, fortran open syntax style
    integer                          :: open_xdr
    character(len=*), intent(in)     :: file
    character(len=*), intent(in)     :: action

    open_xdr = 0
    if (open_xdr == 0) then
       write (fo,'(a)') 'open_xdr called: libxdr.a is not available'
       write (fo,'(a)') 'XDR not installed: ' // TRIM(file) // &
            &TRIM(action)
       stop
    end if
  end function open_xdr

  subroutine close_xdr (xdr)
    integer, intent(in)     :: xdr
    flag = xdr
  end subroutine close_xdr

  subroutine rewind_xdr (xdr)
    integer, intent(in)     :: xdr
    flag = xdr
  end subroutine rewind_xdr

  subroutine xdr_int (xdr, ival)
    integer, intent(in)      ::  xdr
    integer, intent(inout)   ::  ival
    flag = xdr
    ival = 0
  end subroutine xdr_int

  subroutine xdr_imt (xdr, ival, nels)
    integer, intent(in)            :: xdr
    integer, intent(in)            :: nels
    integer, intent(inout)         :: ival(:)
    flag = xdr
    ival = nels
  end subroutine xdr_imt

  subroutine xdr_real (xdr, dval)
    use precisn, only: wp
    integer, intent(in)      :: xdr
    real(wp), intent(inout)   :: dval
    flag = xdr
    dval = 0.0_wp
  end subroutine xdr_real

  subroutine xdr_rlmt (xdr, dval, nels)
    use precisn, only: wp
    integer, intent(in)           :: xdr
    integer, intent(in)           :: nels
    real(wp), intent(inout)      :: dval(nels)
    flag = xdr
    dval = REAL(nels,wp)
  end subroutine xdr_rlmt

  subroutine xdr_chrs (xdr, string)
    integer, intent(in)            :: xdr
    character(len=*), intent(inout) :: string
    flag = xdr
    string = ''
  end subroutine xdr_chrs

end module xdr_files
