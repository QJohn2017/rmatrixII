module find_orbitals
! Time-stamp: "2001-12-29 08:40:53 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use debug, only: bug4
  implicit none

  private
  public finder

contains
  subroutine finder (nbt, lc, nodes, etrial)
    use rm_data, only: ra
    use basis_functions, only: basfun, eigen, bvalue
    integer, intent(inout)  :: nbt ! # bound orbitals to orthogonalise to.
    integer, intent(in)     :: lc  ! l value
    integer, intent(in)     :: nodes ! required # nodes in eigenfunction
    real(wp), intent(inout) :: etrial ! initial estimate of eigenenergy
    real(wp), parameter     :: error = 1.0e-16_wp
    real(wp)                :: del, del2, del1, dell, pi, blow, ehigh
    real(wp)                :: ehold, abserr, relerr, b, c, ft, realnn
    real(wp)                :: elow, bhigh, t
    integer                 :: isum, nhold, node, iflag

    if (bug4 > 0) write (fo,'(a)') 'Finder:'
    pi = 4.0_wp * ATAN(1.0_wp)
    del = pi / ra
    del2 = del * del
    dell = del

    isum = 0
    bounds: do
       call basfun (nbt, lc, node, ra, etrial)
       if (bug4 > 0) write (fo,'(2(a,i5),3(a,e14.6))') 'node = ', &
            node, ' nodes = ', nodes, ' bvalue = ', bvalue,      &
            ' eigen = ', eigen, ' etrial = ', etrial

! check that the function has the correct number of nodes.
       if (node /= nodes) then    ! incorrect, modify energy
          if (isum > 0) then
             if ((node-nodes) == nhold) then
                dell = 0.5_wp * dell
                del2 = 0.5_wp * del2
                etrial = ehold
             else
                nhold = nodes - node
             end if
          else
             nhold = nodes - node
          end if

          ehold = etrial
          isum = isum + 1
          realnn = REAL(nodes-node,wp)
          if (etrial < 0.0_wp .or. SQRT(ABS(etrial))+realnn*dell < &
               0.0_wp) then
             etrial = etrial + ABS(realnn) * realnn * del2
          else
             etrial = (REAL(nodes-node,wp) * dell + SQRT(ABS(etrial)))**2
          end if
          if (isum == 10) then
             write (fo,'(a)') 'Finder: cannot find correct # nodes'
             stop
          else
             cycle bounds
          end if
       else            ! correct # nodes
          if (bvalue < 0.0_wp) then ! have upper bound, find lower
             ehigh = etrial
             blow = bvalue
             del1 = del / 3.0_wp
             del2 = del2 / 3.0_wp
             ehold = etrial
             bound1: do
                if (etrial < 0.0_wp .or. SQRT(ABS(etrial)) < del1) then
                   etrial = etrial - del2
                else
                   etrial = (SQRT(ABS(etrial)) - del1)**2
                end if
                call basfun (nbt, lc, node, ra, etrial)
                if (bug4 > 0) write (fo,'(2(a,i5),3(a,e14.6))') 'node = ', &
                     node, ' nodes = ', nodes, ' bvalue = ', bvalue,      &
                     ' eigen = ', eigen, ' etrial = ', etrial
                if (node /= nodes) then ! energy decreased too far
                   del1 = del1 / 3.0_wp
                   del2 = del2 / 3.0_wp
                   etrial = ehold
                else
                   if (bvalue < 0.0_wp) then ! have upper bound, find lower
                      ehigh = etrial
                      blow = bvalue
                      ehold = etrial
                   else        ! now have lower bound
                      elow = etrial
                      bhigh = bvalue
                      exit
                   end if
                end if
             end do bound1
          else                ! have lower bound, try for upper
             elow = etrial
             bhigh=  bvalue
             del1 = del / 3.0_wp
             del2 = del2 / 3.0_wp
             ehold = etrial
             bound2: do
                if (etrial < 0.0_wp) then
                   etrial = etrial + del2
                else
                   etrial = (SQRT(ABS(etrial)) + del1)**2
                end if
                call basfun (nbt, lc, node, ra, etrial)
                if (bug4 > 0) write (fo,'(2(a,i5),3(a,e14.6))') 'node = ', &
                     node, ' nodes = ', nodes, ' bvalue = ', bvalue,      &
                     ' eigen = ', eigen, ' etrial = ', etrial
                if (node /= nodes) then ! energy decreased too far
                   del1 = del1 / 3.0_wp
                   del2 = del2 / 3.0_wp
                   etrial = ehold
                else
                   if (bvalue >= 0.0_wp) then ! better lower bound found, upper
                      elow = etrial
                      bhigh = bvalue
                      ehold = etrial
                   else         ! upper bound to energy found
                      ehigh = etrial
                      blow = bvalue
                      exit
                   end if
                end if
             end do bound2
          end if
       end if
       exit
    end do bounds

! eigenenergy bounded, use root finding routine to get a better estimate
    iflag = 1
    abserr = 0.0_wp
    relerr = 0.1_wp * error
    b = elow
    c = ehigh

    refine: do 
       call root (t, ft, b, c, relerr, abserr, iflag)
       if (iflag < 0) then ! still not close enough
          if (t == elow) then
             ft = bhigh
             cycle
          else if (t == ehigh) then
             ft = blow
             cycle
          else
             etrial = t
             call basfun (nbt, lc, node, ra, etrial)
             if (bug4 > 0) write (fo,'(2(a,i5),3(a,e14.6))') 'node = ', &
                  node, ' nodes = ', nodes, ' bvalue = ', bvalue,      &
                  ' eigen = ', eigen, ' etrial = ', etrial
          end if
          ft = bvalue
          if (ABS(ft) < error) exit ! solution found
       else if (iflag > 1) then     !error in root
          write (fo,'(a,i4)') 'Finder: root error, iflag = ', iflag
          stop
       else                   ! root located to required accuracy
          etrial = b
          call basfun (nbt, lc, node, ra, etrial)
          if (bug4 > 0) write (fo,'(2(a,i5),3(a,e14.6))') 'node = ', &
               node, ' nodes = ', nodes, ' bvalue = ', bvalue,      &
               ' eigen = ', eigen, ' etrial = ', etrial
          exit
       end if
    end do refine

    if (node == nodes) then ! evalue found with correct # nodes
       return
    else   ! incorrect # nodes, give up
       write (fo,'(a)') 'Finder: incorrect # nodes found'
       stop
    end if
  end subroutine finder

  subroutine root (t, ft, b, c, relerr, abserr, iflag)
! computes a root of the nonlinear equation f(x)=0, where f(x)
! is a continuous real function of a single real variable x.
! solution by a combination of bisection and the secant rule.
! Shampine and Gordon,
! 'Computational solution of ordinary differential equations'
    real(wp), intent(inout)   :: t      ! function argument
    real(wp), intent(in)      :: ft     ! function value
    real(wp), intent(inout)   :: b      ! x lower bound, refined
    real(wp), intent(inout)   :: c      ! x upper bound
    real(wp), intent(in)      :: relerr ! relative error
    real(wp), intent(in)      :: abserr ! absolute error
    integer, intent(inout)    :: iflag  ! > 0 to initialize
    real(wp),  parameter      :: u = 1.0e-12_wp
    real(wp), save            :: re, ae, acbs, a, fa, fc, fx
    integer, save             :: ic, kount
    real(wp)                  :: fb, p, q, tol, cmb, acmb

! interval (b,c) such that f(b)*f(c) <= 0.0. each iteration finds
! new values of b and c such that interval (b,c) is shrunk and 
! f(b)*f(c) <= 0.0.
! stopping criterion: ABS(b-c) <= 2.0*(relerr*ABS(b)+abserr)
! if 0.0 is a possible root, one should not choose abserr=0.0.

! when an evaluation of f needed at t, root returns with iflag negative.
! evaluate ft=f(t) and call root again. do not alter iflag.

! iflag=1  if f(b)*f(c).lt.0 and the stopping criterion is met.
!      =2  if a value b is found such that the computed value f(b)
!          is exactly zero. the interval (b,c) may not satisfy
!          the stopping criterion.
!      =3  if abs(f(b)) exceeds the input values abs(f(b)),
!          abs(f(c)).  in this case it is likely that b is close
!          to a pole of f.
!      =4  if no odd order root was found in the intervall.
!          a local mininmum may have been obtained.
!      =5  if too many function evaluations were made.
!          (as programmed, 500 are allowed.)

    if (iflag >= 0) then
       re = MAX(relerr, u)
       ae = MAX(abserr, 0.0_wp)
       ic = 0
       acbs = ABS(b - c)
       a = c
       t = a
       iflag = -1
       return
    end if

    iflag = ABS(iflag)
    select case (iflag)
    case (1)
       fa = ft
       t = b
       iflag = -2
       return
    case (2)
       fb = ft
       fc = fa
       kount = 2
       fx = MAX(ABS(fb), ABS(fc))
    case (3)
       fb = ft
       if (fb == 0.0_wp) then
          iflag = 2
          return
       end if
       kount = kount + 1
       if (SIGN(1.0_wp,fb) == SIGN(1.0_wp,fc)) then
          c = a
          fc = fa
       end if
    end select

    if (ABS(fc) < ABS(fb)) then
! interchange b and c so that abs(f(b)).le.abs(f(c))
       a = b
       fa = fb
       b = c
       fb = fc
       c = a
       fc = fa
    end if
    cmb = 0.5_wp * (c - b)
    acmb = ABS(cmb)
    tol = re * ABS(b) + ae

! test stopping criterion and function count
    if (acmb <= tol) then
       if (SIGN(1.0_wp,fb) == SIGN(1.0_wp,fc)) then
          iflag = 4
          return
       end if
       if (ABS(fb) > fx) then
          iflag = 3
          return
       end if
       iflag = 1
       return
    end if
    if (kount >= 500) then
       iflag = 5
       return
    end if

! calculate new iterate implicitly as b+p/q  where we arrange p >= 0
! the implicit form is used to prevent overflow.
    p = (b - a) * fb
    q = fa - fb
    if (p < 0.0_wp) then
       p = -p
       q = -q
    end if

! update a, check reduction of the size of bracketing interval is
! satisfactory. if not, bisect until it is.
    a = b
    fa = fb
    ic = ic + 1
    if (ic >= 4) then
       if (8.0_wp*acmb >= acbs) then
          b = 0.5_wp * (c + b)
          t = b
          iflag = -3
          return
       end if
       ic = 0
       acbs = acmb
    end if

! test for too small a change
    if (p <= ABS(q)*tol) then
       b = b + SIGN(tol, cmb) ! increment by tolerance
    else
! root ought to be between b and (c+b)/2
       if (p < cmb*q) then
          b = b + p / q     ! secant rule
       else
          b = 0.5_wp * (c + b)  ! bisection
       end if
    end if
    
! have completed computation of new iterate b
    t = b
    iflag = -3
  end subroutine root

end module find_orbitals
