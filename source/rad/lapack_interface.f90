module lapack_interface
! provide an interface to common lapack routines
! Time-stamp: "01/12/28 06:55:38 cjn"
  use precisn, only: wp
  use io_units, only: fo
  implicit none

  private
!!$  public sleqn, ssleqn
  public sleqn

  interface sleqn
     module procedure dsleqn
     module procedure dsleqn1
     module procedure zsleqn
  end interface
!!$  interface ssleqn
!!$     module procedure dssleqn
!!$     module procedure zssleqn
!!$  end interface

contains

  subroutine dsleqn1 (a, n, b, trans)
! solve real linear system  a * x = b   (square matrices)
    integer, intent(in)                :: n
    real(wp), intent(inout)            :: a(n,n)
    real(wp), intent(inout)            :: b(n)
    character(len=1), intent(in), optional :: trans
    integer                            :: pvt(n)
    integer                            :: info
    character(len=1)                   :: tran

    call dgetrf (n, n, a, n, pvt, info)
    if (info /= 0) then
       write (fo,'(a,i6)') 'sleqn: dgetrf error, info = ', info
       stop
    end if
    if (present(trans)) then
       tran = trans
    else
       tran = 'N'
    end if
    call dgetrs (tran, n, 1, a, n, pvt, b, n, info)
    if (info /= 0) then
       write (fo,'(a,i6)') 'dsleqn: dgetrs error, info = ', info
       stop
    end if
  end subroutine dsleqn1

  subroutine dsleqn (a, n, b, m, trans)
! solve real linear system  a * x = b   (square matrices)
    integer, intent(in)                :: n
    real(wp), intent(inout)            :: a(n,n)
    integer, intent(in)                :: m
    real(wp), intent(inout)            :: b(n,m)
    character(len=1), intent(in), optional :: trans
    integer                            :: pvt(n)
    integer                            :: info
    character(len=1)                   :: tran

    call dgetrf (n, n, a, n, pvt, info)
    if (info /= 0) then
       write (fo,'(a,i6)') 'sleqn: dgetrf error, info = ', info
       stop
    end if
    if (present(trans)) then
       tran = trans
    else
       tran = 'N'
    end if
    call dgetrs (tran, n, m, a, n, pvt, b, n, info)
    if (info /= 0) then
       write (fo,'(a,i6)') 'dsleqn: dgetrs error, info = ', info
       stop
    end if
  end subroutine dsleqn

  subroutine zsleqn (a, n, b, m, trans)
! solve real linear system  a * x = b   (square matrices)
    integer, intent(in)                :: n
    complex(wp), intent(inout)         :: a(n,n)
    integer, intent(in)                :: m
    complex(wp), intent(inout)         :: b(n,m)
    character(len=1), intent(in), optional :: trans
    integer                            :: pvt(n)
    integer                            :: info
    character(len=1)                   :: tran

    call zgetrf (n, n, a, n, pvt, info)
    if (info /= 0) then
       write (fo,'(a,i6)') 'zleqn: dgetrf error, info = ', info
       stop
    end if
    if (present(trans)) then
       tran = trans
    else
       tran = 'N'
    end if
    call zgetrs (trans, n, m, a, n, pvt, b, n, info)
    if (info /= 0) then
       write (fo,'(a,i6)') 'zsleqn: dgetrs error, info = ', info
       stop
    end if
  end subroutine zsleqn
  
!!$   subroutine dssleqn (a, n, b, m)
!!$! solve possibly singular linear equations using Lapack SVD routines
!!$     integer, intent(in)        :: n
!!$     real(wp), intent(inout)    :: a(n,n)
!!$     integer, intent(in)        :: m
!!$     real(wp), intent(inout)    :: b(n,m)
!!$     real(wp)                   :: s(n)      ! singular eigenvalues
!!$     real(wp)                   :: u(n,n), vt(n,n), w(n,m)
!!$     integer, save              :: lwork = 0
!!$     real(wp), allocatable      :: work(:)
!!$     integer                    :: info, i
!!$     real(wp)                   :: cn, cnd
!!$     real(wp), parameter        :: singular = 1.0e-12_wp
!!$
!!$! singular value decomposition: a = u * sigma * transpose(v)
!!$!     if (lwork == 0) lwork = MAX(3*n+n, 5*n-4)
!!$     lwork = MAX(3*n+n, 5*n-4)
!!$     allocate (work(lwork))
!!$     call dgesvd ('s', 's', n, n, a, n, s, u, n, vt, n, work, lwork, &
!!$          info)
!!$     if (info /= 0) then
!!$        write (fo,'(a,i4)') 'sleqn: dgesvd error, info = ', info
!!$        stop
!!$     end if
!!$     lwork = INT(work(1)) + 1
!!$     deallocate (work)
!!$
!!$     cn = MAXVAL(ABS(s))
!!$     cnd = MINVAL(ABS(s))
!!$     if (cnd > epsilon(s(1))) then
!!$        cn = cn / cnd
!!$     else
!!$        cn = 0.0_wp
!!$     end if
!!$     write (fo,'(a,e12.4)') 'ssleqn: Condition number = ', cn
!!$     w = matmul(transpose(u), b)
!!$     do i = 1, n
!!$        if (ABS(s(i)) < singular) then
!!$           s(i) = 0.0_wp
!!$           write (fo,'(a,i4)') 'ssleqn: singular, i = ', i ! temporary
!!$        else
!!$           s(i) = 1.0_wp / s(i)
!!$        end if
!!$        w(i,:) = s(i) * w(i,:)
!!$     end do
!!$     b = matmul(transpose(vt), w)
!!$   end subroutine dssleqn
!!$
!!$   subroutine zssleqn (a, n, b, m)
!!$! solve possibly singular linear equations using Lapack SVD routines
!!$     integer, intent(in)        :: n
!!$     complex(wp), intent(inout) :: a(n,n)
!!$     integer, intent(in)        :: m
!!$     complex(wp), intent(inout) :: b(n,m)
!!$     real(wp)                   :: s(n)      ! singular eigenvalues
!!$     complex(wp)                :: u(n,n), vt(n,n), w(n,m)
!!$     integer, save              :: lwork = 0
!!$     complex(wp), allocatable   :: work(:)
!!$     real(wp), allocatable      :: rwork(:)
!!$     integer                    :: info, i
!!$     real(wp)                   :: cn, cnd
!!$     real(wp), parameter        :: singular = 1.0e-12_wp
!!$
!!$! singular value decomposition: a = u * sigma * transpose(v)
!!$!     if (lwork == 0) lwork = MAX(3*n+n, 5*n-4)
!!$     lwork = MAX(3*n+n, 5*n-4)
!!$     allocate (work(lwork))
!!$     allocate (rwork(MAX(3*n,5*n-4))) 
!!$! A = U * SIGMA * conjugate-transpose(V)
!!$     call zgesvd ('s', 's', n, n, a, n, s, u, n, vt, n, work, lwork, &
!!$          rwork, info)
!!$     if (info /= 0) then
!!$        write (fo,'(a,i4)') 'zssleqn: zgesvd error, info = ', info
!!$        stop
!!$     end if
!!$
!!$     lwork = INT(work(1)) + 1
!!$     deallocate (work, rwork)
!!$     cn = MAXVAL(ABS(s))
!!$     cnd = MINVAL(ABS(s))
!!$     if (cnd > epsilon(s(1))) then
!!$        cn = cn / cnd
!!$     else
!!$        cn = 0.0_wp
!!$     end if
!!$     write (fo,'(a,e12.4)') 'zssleqn: Condition number = ', cn
!!$     w = matmul(transpose(u), b)
!!$     do i = 1, n
!!$        if (ABS(s(i)) < singular) then
!!$           s(i) = 0.0_wp
!!$           write (fo,'(a,i4)') 'ssleqn: singular, i = ', i ! temporary
!!$        else
!!$           s(i) = 1.0_wp / s(i)
!!$        endif
!!$        w(i,:) = s(i) * w(i,:)
!!$     enddo
!!$     b = matmul(transpose(vt), w)
!!$   end subroutine zssleqn

end module lapack_interface
