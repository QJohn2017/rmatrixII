module debug
! Time-stamp: "01/12/13 16:47:36 cjn"
  implicit none

  integer, save :: bug1, bug2, bug3, bug4, bug5, bug6, bug7, bug8, bug9

  private
  public set_debug_switches
  public bug1, bug2, bug3, bug4, bug5, bug6, bug7, bug8, bug9

contains
  subroutine set_debug_switches (irbug)
    integer, intent(in) :: irbug(9)

    bug1 = irbug(1)
    bug2 = irbug(2)
    bug3 = irbug(3)
    bug4 = irbug(4)
    bug5 = irbug(5)
    bug6 = irbug(6)
    bug7 = irbug(7)
    bug8 = irbug(8)
    bug9 = irbug(9)
  end subroutine set_debug_switches

end module debug
