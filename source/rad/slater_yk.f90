module slater_yk
! Time-stamp: "03/06/25 15:42:27 cjn"
  use precisn, only: wp
  use io_units, only: fo
  implicit none

  real(wp), parameter :: r3 = 1.0_wp / 3.0_wp
  real(wp), parameter :: r8 = 1.0_wp / 8.0_wp
  real(wp), parameter :: r12 = 1.0_wp / 12.0_wp

  real(wp), save      :: x, y
  real(wp), save      :: frh
  real(wp), save      :: kk1, khat, t2, t4, p2p4
  integer, save       :: ic

  real(wp), save      :: max_threshold
  real(wp), save      :: min_threshold
  real(wp), save      :: max_renormalization
  real(wp), save      :: min_renormalization


  real(wp), allocatable, save :: yk(:)
  real(wp), allocatable, save :: ys(:)

  private
  public yk
  public ykf, alloc_y, dealloc_y

contains
  subroutine alloc_y (i9)
    integer, intent(in)    :: i9
    integer                :: status

    allocate (yk(i9), ys(i9), stat=status)
    if (status /= 0) then
       write (fo,'(a,i4)') 'alloc_y: allocation error = ', status
       stop
    end if

! define renormalization thresholds and factors:
    max_threshold = SQRT(HUGE(yk(1)))
    min_threshold = SQRT(TINY(yk(1)))
    max_renormalization = 1.0_wp / max_threshold
    min_renormalization = 1.0_wp / min_threshold
    write (fo,'(a,e16.8)') 'upper renormalization threshold = ', &
         max_threshold
    write (fo,'(a,e16.8)') 'lower renormalization threshold = ', &
         min_threshold
  end subroutine alloc_y

  subroutine dealloc_y
    integer                :: status

    deallocate (yk, ys, stat=status)
    if (status /= 0) then
       write (fo,'(a,i4)') 'dealloc_y: deallocation error = ', status
       stop
    end if
  end subroutine dealloc_y

  subroutine ykf (n2, l2, n4, l4, k, ra)
! evaluate the function y(p2,p4,k/r)
    use radial_grid, only: hint, ihx, irx, nix
    use bound_basis, only: uj, ipos
    integer, intent(in)     :: n2
    integer, intent(in)     :: l2
    integer, intent(in)     :: n4
    integer, intent(in)     :: l4
    integer, intent(in)     :: k
    real(wp), intent(in)    :: ra
    real(wp)                :: a0(3,2)
    integer                 :: i9, j2, j4, k1, ist, imtch1
    integer                 :: i, j, ifi, ie, is, ke, in, i8, lswt
    integer                 :: lb, ib, lm, id, kd, imatch, isq
    real(wp)                :: ara, h, s, hs, dy, d2y, ra1, ra2
    real(wp)                :: yh, frm, fr, ab, t3, t5, ay, r2l, r3l
    real(wp)                :: rhl, b1, ymatch, t1, h1

    ic = 0
    i9 = irx(nix) + 1       ! # points
    j2 = ipos(n2,l2)        ! location of p2
    j4 = ipos(n4,l4)        ! location of p4
    k1 = k + 1
    kk1 = REAL(k*k1,wp)
    khat = REAL(2*k+1,wp)
    imatch = i9 / 3
    if (MOD(imatch,2) /= 0) imatch = imatch + 1
    if (imatch < 2) imatch = 2
    do i = 1, nix
       if (ABS(imatch - irx(i)) <= 2) then
          imatch = imatch + 4
          exit
       end if
    end do

! evaluate normalization integral at boundary
    x = 0.0_wp
    ara = 0.0_wp
    ist = 2
    intervals: do i = 1, nix
       h = hint * REAL(ihx(i),wp)
       s = 0.0_wp
       if (i /= 1) s = s + uj(ifi,j2) * uj(ifi,j4) * x**k
       ifi = irx(i)
       simpson: do j = ist, ifi
          x = x + h
          if (MOD(j,2) /= 0) then
             s = s + 2.0_wp * uj(j,j2) * uj(j,j4) * x**k
          else
             s = s + 4.0_wp * uj(j,j2) * uj(j,j4) * x**k
          end if
       end do simpson
       ifi = ifi + 1
       x = x + h
       s = s + uj(ifi,j2) * uj(ifi,j4) * x**k
       ist = ifi + 1
       ara = ara + r3 * s * h
    end do intervals

! integrate yk in from r=ra to matching point i=imatch
! initialize quantities at r=ra, r=ra+h/2 for de vogelaere integration
    x = ra
    h = - hint * REAL(ihx(nix),wp)
    hs = h * h
    y = ara / ra**k
    yk(i9) = y
    dy = -0.5_wp * k * y * h / ra
    d2y = kk1 * y / ra**2 - khat * uj(i9,j2) * uj(i9,j4) / ra
    yh = y - dy + r8 * hs * d2y
    ra1 = ra - 0.5_wp * h
    ra2 = ra1 * ra1

! extrapolate the p2 and p4 functions to r=ra+h/2
    t1 = 1.875_wp*uj(i9,j2) - 1.25_wp*uj(i9-1,j2) + 0.375_wp*uj(i9-2,j2)
    t2 = 1.875_wp*uj(i9,j4) - 1.25_wp*uj(i9-1,j4) + 0.375_wp*uj(i9-2,j4)
    p2p4 = t1 * t2
    frm = kk1 * yh / ra2 - khat * p2p4 / ra1
    frm = r3 * frm * hs
    fr = r12 * hs * d2y
    lswt = 1

    inwards: do i = 1, nix
       in = nix + 1 - i
       h = - hint * REAL(ihx(in),wp)
       hs = h * h
!  treat the first point of each subinterval in the integration mesh
       if (i <= 1) then
          i8 = i9
          t2 = r8*(3.0_wp*uj(i9,j2) + 6.0_wp*uj(i9-1,j2) - uj(i9-2,j2))
          t4 = r8*(3.0_wp*uj(i9,j4) + 6.0_wp*uj(i9-1,j4) - uj(i9-2,j4))
       else
          t2 = 0.5625_wp * (uj(i8,j2) + uj(i8-1,j2)) - 0.0625_wp * &
               (t2 + uj(i8-2,j2))
          t4 = 0.5625_wp * (uj(i8,j4) + uj(i8-1,j4)) - 0.0625_wp * &
               (t4 + uj(i8-2,j4))
       end if
       p2p4 = uj(i8-1,j2) * uj(i8-1,j4)
       call vogelaere (dy, fr, frm, h, hs, h1, lswt)
       yk(i8-1) = y
!       if (i8 /= imatch + 1) then
       ie = irx(in)
       if (i < nix) then
          is = irx(in-1) + 3
       else
          is = 3
       end if
       ke = -1

! treat the intermediate integration points of each subinterval
       step_in: do j = is, ie
          ke = ke + 1
          i8 = ie - ke
          p2p4 = uj(i8-1,j2) * uj(i8-1,j4)
! interpolate the p2 and p4 functions to the half-mesh point
          t2 = 0.5625_wp * (uj(i8,j2) + uj(i8-1,j2)) - 0.0625_wp * &
               (uj(i8+1,j2) + uj(i8-2,j2))
          t4 = 0.5625_wp * (uj(i8,j4) + uj(i8-1,j4)) - 0.0625_wp * &
               (uj(i8+1,j4) + uj(i8-2,j4))
          call vogelaere (dy, fr, frm, h, hs, h1, lswt)
          yk(i8-1) = y
!      test to see if the matching point has been reached
          if (i8 <= imatch+3) exit
       end do step_in

! treat last point of each subinterval or the matching point if reached
       if (i8 /= imatch+1) then
          i8 = i8 - 1
          p2p4 = uj(i8-1,j2) * uj(i8-1,j4)

! interpolate the p2 and p4 functions to the half-mesh point
          t2 = 0.5625_wp * (uj(i8,j2) + uj(i8-1,j2)) - 0.0625_wp * &
               (uj(i8+1,j2) + uj(i8-3,j2))
          t4 = 0.5625_wp * (uj(i8,j4) + uj(i8-1,j4)) - 0.0625_wp * &
               (uj(i8+1,j4) + uj(i8-3,j4))
          call vogelaere (dy, fr, frm, h, hs, h1, lswt)
          yk(i8-1) = y
          i8 = i8 - 1
          h1 = h
       end if
       lswt = 2
       if (i8 == imatch+1) exit
    end do inwards
    if (i8 /= imatch+1) then
       write (fo,'(a)') 'ykf: Inwards match failure'
       stop
    end if
    ymatch = y ! inward value of y function at the matching point

! integrate yk outwards from the origin to matching point i=imatch
!  note....the homogeneous outward solution is  y=a*r**k+1
!  initialize at r=h/2, r=h for integration of particular soln
    yk(1) = 0.0_wp
    h = hint * REAL(ihx(1),wp)
    hs = h * h

! obtain coefficients of three term series expansion fit to p2, p4
    if (ABS(uj(2,j2)) < min_threshold) then
       write (fo,'(a)') 'ykf: u2 wave function fit failure'
       stop
    end if
    r2l = 1.0_wp / 2.0_wp**l2
    r3l = 1.0_wp / 3.0_wp**l2
    rhl = 1.0_wp / h**l2
    a0(1,1) = (3.0_wp*uj(2,j2) - 3.0_wp*r2l*uj(3,j2) + r3l * uj(4,j2)) &
         * rhl
    a0(2,1) = (4.0_wp*r2l*uj(3,j2) - 1.5_wp*r3l*uj(4,j2) - &
         2.5_wp*uj(2,j2)) * rhl / h
    a0(3,1) = 0.5_wp * (r3l*uj(4,j2) - 2.0_wp*r2l*uj(3,j2) + uj(2,j2)) &
         * (rhl / hs)
    if (ABS(uj(2,j4)) < min_threshold) then
       write (fo,'(a)') 'ykf: u4 wave function fit failure'
       stop
    end if
    r2l = 1.0_wp / 2.0_wp**l4
    r3l = 1.0_wp / 3.0_wp**l4
    rhl = 1.0_wp / h**l4
    a0(1,2) = (3.0_wp*uj(2,j4) - 3.0_wp*r2l*uj(3,j4) + r3l * uj(4,j4)) &
         * rhl
    a0(2,2) = (4.0_wp*r2l*uj(3,j4) - 1.5_wp*r3l*uj(4,j4) - &
         2.5_wp*uj(2,j4)) * rhl / h
    a0(3,2) = 0.5_wp * (r3l*uj(4,j4) - 2.0_wp*r2l*uj(3,j4) + uj(2,j4)) &
         * (rhl / hs)

! get the second derivative of yk at r=h
    x = h
    lm = l2 + l4 + 1
    b1 = a0(1,1) * a0(1,2)
    y = - khat * b1 * x**lm
    dy = 0.5_wp * lm * y
    y = y / REAL(lm*(lm-1)-k*k1,wp)
    if (ABS(y) < min_threshold) write (fo,'(a)') 'ykf: YK(2) below threshold'
    ys(1) = 0.0_wp
    ys(2) = x**k1
    yk(2) = y
    dy = dy / REAL(lm*(lm-1)-k*k1,wp)
    p2p4 = uj(2,j2) * uj(2,j4)
    ic = ic + 1
    call rssub2
    fr = r12 * hs * frh

! get the second derivative of yk at r=h/2
    x = 0.5_wp * h
    p2p4 = 1.0_wp
    lb = l2 - 1
    do ib = 1, 2
       ab = 0.0_wp
       do id = 1, 3
          kd = lb + id
          ab = ab + a0(id,ib) * x**kd
       end do
       lb = l4 - 1
       p2p4 = p2p4 * ab
    end do
    ic = ic + 1
    call rssub2
    frm = r3 * hs * frh
    x = h
    lswt = 1

    outwards: do i = 1, nix
       h = hint * REAL(ihx(i),wp)
       hs = h * h
       ie = irx(i) - 1
       if (i == 1) then
          is = 2
       else
! treat the first point of each subinterval of the integration mesh
          p2p4 = uj(is+1,j2) * uj(is+1,j4)

! interpolate the p2 and p4 functions to the half-mesh point
          t2 = 0.5625_wp * (uj(is+1,j2) + uj(is,j2)) - 0.0625_wp * &
               (uj(is-2,j2) + uj(is+2,j2))
          t4 = 0.5625_wp * (uj(is+1,j4) + uj(is,j4)) - 0.0625_wp * &
               (uj(is-2,j4) + uj(is+2,j4))
          call vogelaere (dy, fr, frm, h, hs, h1, lswt)
          yk(is+1) = y
          ys(is+1) = x**k1
          is = is + 1
       end if

       if (is /= imatch) then
          isq = ie + 1
          step_out: do j = is, ie
             p2p4 = uj(j+1,j2) * uj(j+1,j4)

! interpolate the p2 and p4 functions to the half-mesh point
             t2 = 0.5625_wp * (uj(j+1,j2) + uj(j,j2)) - 0.0625_wp * &
                  (uj(j-1,j2) + uj(j+2,j2))
             t4 = 0.5625_wp * (uj(j+1,j4) + uj(j,j4)) - 0.0625_wp * &
                  (uj(j-1,j4) + uj(j+2,j4))
             call vogelaere (dy, fr, frm, h, hs, h1, lswt)
             yk(j+1) = y
             ys(j+1) = x**k1
             if (j == imatch-1) then
                isq = j + 1
                exit
             end if
          end do step_out
          j = isq
       else
          j = is
       end if

! treat last point of subinterval or matching point if reached
!       j = j + 1
       if (j /= imatch) then
          h1 = h
! interpolate the p2 and p4 functions to the half-mesh point
          t3 = 0.5625_wp * (uj(j+1,j2) + uj(j+2,j2)) - 0.0625_wp * &
               (uj(j+3,j2) + uj(j-1,j2))
          t5 = 0.5625_wp * (uj(j+1,j4) + uj(j+2,j4)) - 0.0625_wp * &
               (uj(j+3,j4) + uj(j-1,j4))
          t2 = 0.5625_wp * (uj(j+1,j2) + uj(j,j2)) - 0.0625_wp * &
               (uj(j-1,j2) + t3)
          t4 = 0.5625_wp * (uj(j+1,j4) + uj(j,j4)) - 0.0625_wp * &
               (uj(j-1,j4) + t5)
          p2p4 = uj(j+1,j2) * uj(j+1,j4)
       else
          p2p4 = uj(j+1,j2) * uj(j+1,j4)
          t2 = 0.5625_wp * (uj(j+1,j2) + uj(j,j2)) - 0.0625_wp * &
               (uj(j-1,j2) + uj(j+2,j2))
          t4 = 0.5625_wp * (uj(j+1,j4) + uj(j,j4)) - 0.0625_wp * &
               (uj(j-1,j4) + uj(j+2,j4))
       end if
       call vogelaere (dy, fr, frm, h, hs, h1, lswt)
       lswt = 2
       is = ie + 2
       j = j + 1
       yk(j) = y
       ys(j) = x**k1
       if (ABS(y) > max_threshold) yk(1:j) = max_renormalization * &
            yk(1:k)
       if (j == imatch+1) exit
    end do outwards

! matching and renormalization of outward function by adding in the
! appropriate amount of the homogeneous solution
    ay = (ymatch - y) / x**k1
    imtch1 = imatch + 1
    do i = 1, imtch1
       yk(i) = yk(i) + ay * ys(i)
    end do
  end subroutine ykf

  subroutine vogelaere (dy, fr, frm, h, hs, h1, lswt)
! advances the de Vogelaere integration by one step
    real(wp), intent(inout)   :: dy
    real(wp), intent(inout)   :: fr
    real(wp), intent(inout)   :: frm
    real(wp), intent(in)      :: h
    real(wp), intent(in)      :: hs
    real(wp), intent(in)      :: h1
    integer, intent(inout)    :: lswt
    real(wp)                  :: hh, yr, h12, h12s


! advance the integration to the half-mesh point
    hh = 0.5_wp * h
    x = x + hh
    select case (lswt)
    case (1)              ! no change of step length
       dy = dy + fr
       yr = y + dy
       y = yr + fr - 0.125_wp * frm
    case (2)             ! change of integration step length
       h12 = h / h1
       h12s = h12 * h12
       lswt = 1
       dy = h12 * dy + h12s * fr
       yr = y + dy
       y = yr + 0.5_wp * h12s * (fr * (h12 + 1.0_wp) - 0.25_wp * h12 * &
            frm)
    end select

! advance the y and x values through the full intergration step
    call rssub2
    frm = r3 * hs * frh
    dy = dy + frm
    y = yr + dy
    x = x + hh

! update the first and second derivative values
    call rssub2
    fr = r12 * hs * frh
    dy = dy + fr
  end subroutine vogelaere

  subroutine rssub2
! determine second derivative function at the mesh or half-mesh point
    real(wp)        :: v
!
    v = 1.0_wp / x
    if (MOD(ic,2) == 0) then ! ic even so it is a mesh point
       frh = kk1 * y * v - khat * t2 * t4
    else                     ! ic odd so it is a half-mesh point
       frh = kk1 * y * v - khat * p2p4
    end if
    ic = ic + 1
    frh = frh * v
  end subroutine rssub2

end module slater_yk
