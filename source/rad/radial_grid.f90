module radial_grid
! radial mesh generating routines
! Time-stamp: "02/10/31 17:18:04 cjn"
  use precisn, only: wp
  use io_units, only: fi, fo
  use error_prt, only: alloc_error
  implicit none

  integer, save               :: nix
  integer, save               :: nix_in
  integer, allocatable, save  :: irx(:)
  integer, allocatable, save  :: ihx(:)
  real(wp), save              :: hint

  integer, save               :: max_npts = 40000
  integer, save               :: npts,nptsorig
  real(wp), allocatable, save :: xr(:)
  real(wp), allocatable, save :: step(:)
  real(wp), allocatable, save :: wt(:)

  private
  public simpson, npts, nptsorig, xr, step, wt
  public nix_in, nix, irx, ihx, hint, gen_mesh

contains

  subroutine gen_mesh (nix_in, nz, ra, nrang2)
! generate radial mesh
    integer, intent(in)       :: nix_in
    integer, intent(in)       :: nz    ! nuclear charge
    integer, intent(in)       :: nrang2 ! # cont orbs for l = 0
    real(wp), intent(in)      :: ra     ! R-matrix radius
    logical                   :: fine  ! fine grid
    integer                   :: i, status

! if nix is negative, calculate the integration mesh parameters,
    if (nix_in == -1) then
       fine = .false.
       call mesh (nz, ra, nrang2, fine)
    else if (nix_in == -2) then
       fine = .true.
       call mesh (nz, ra, nrang2, fine)
    else ! read them from data file
       nix = nix_in
       allocate (ihx(nix), irx(nix), stat=status)
       if (status /= 0) call alloc_error (status, 'gen_mesh', 'a')
       read (fi,*) hint
       read (fi,*) ihx
       read (fi,*) irx
    end if
    write (fo,'(/,a,/,a,i3)') 'Integration mesh:', 'nix=', nix
    write (fo,'(a,e14.7)') 'hint =', hint
    write (fo,'(a,20i5)') 'ihx=', ihx
    write (fo,'(a,20i5)') 'irx=', irx
  end subroutine gen_mesh

  subroutine mesh (nz, ra, nrang2, fine)
! generate the integration mesh, based on nuclear behaviour of 
! bound orbitals, # continuum orbital loops and current array dimensions
! This version uses the criteria given in CPC 69 (1992) 76
    integer, intent(in)         :: nrang2
    real(wp), intent(in)        :: ra
    integer, intent(in)         :: nz
    logical, intent(in)         :: fine
    integer                     :: ncorse, m, nafac, i, ih, na, ntot
    integer                     :: mpow2, ir, ia, status
    real(wp)                    :: hinner, hmin, hmax

    if (fine) then
       ncorse = 64 * nrang2
       hinner = 0.0125_wp / (4 * nz)
    else
       ncorse = 16 * nrang2
       hinner = 0.025_wp / nz
    end if
    if (ncorse < 96) ncorse = 96

! hmin = hmax/2**m where m+1 is the number of step sizes
    hmax = ra / ncorse    ! coarsest mesh
    m = 0
    hmin = hmax
    do while (hmin > 1.2_wp * hinner)
       m = m + 1
       hmin = hmin / 2
    end do
    hint = hmin ! mesh near origin
    nix = m + 1
    allocate (ihx(nix), irx(nix), stat=status)
    if (status /= 0) call alloc_error (status, 'mesh', 'a')
    
    ih = 1
    do i = 1, nix
       ihx(i) = ih
       ih = ih + ih
    end do

! na is the number of steps at each step size
    if (m >= 4) then
       nafac_L: do nafac = 2, 1, -1
          na = 16 * nafac
          ntot = ncorse + (m-1) * na + na / 8
          if (ntot >= max_npts) then
             if (nafac == 1) then
                write (fo,'(a)') 'mesh error, increase max_npts'
                stop
             end if
             cycle
          end if
          exit
       end do nafac_L

       irx(1) = na
       irx(2) = na + irx(1)
       irx(3) = na + irx(2)
       irx(4) = na + na / 8 + irx(3)
       do i = 5, m
          irx(i) = na + irx(i-1)
       end do
    else   ! m < 4
       mpow2 = 2**m
       nafac1_L: do nafac = 2, 1, -1
          na = 16 * nafac
          ntot = ncorse + m * na - na * (mpow2 - 1) / mpow2
          if (ntot >= max_npts) then
             if (nafac == 1) then
                write (fo,'(a)') 'mesh error, increase max_npts'
                stop
             end if
             cycle
          end if
          exit
       end do nafac1_L
       
       ia = 0
       do i = 1, m
          ia = ia + na
          irx(i) = ia
       end do
    end if
    
! number of steps at each step size must be even
    if (MOD(ntot,2) /= 0) then
       ntot = ntot + 1
       irx(m) = irx(m) + 2
    end if
    irx(nix) = ntot
  end subroutine mesh

  subroutine simpson
! set up arrays for use in simpsons rule integration
    integer                  :: ifi, i, ist, ifx, j, status, ibode
    real(wp)                 :: one3, two3, four3, h, rstart

    npts = irx(nix) + 1
    allocate (xr(npts), wt(npts), step(npts), stat=status)
    if (status /= 0) call alloc_error (status, 'simpson', 'a')
    ibode=0
    if (ibode<1) then
    one3 = 1.0_wp / 3.0_wp
    two3 = 2.0_wp * one3
    four3 = 4.0_wp * one3
    ifi = 1
    xr(1) = 0.0_wp
    step(1) = 0.0_wp
    do i = 1, nix
       h = hint * REAL(ihx(i),wp)
       wt(ifi) = one3 * (h + step(ifi))
       rstart = xr(ifi)
       ist = ifi + 2
       ifi = irx(i) + 1
       ifx = 0
       if (i > 1) ifx = irx(i-1)
       do j = ist, ifi, 2
          xr(j-1) = rstart + (j-2-ifx) * h
          xr(j) = rstart + (j-1-ifx) * h
          step(j-1) = h
          step(j) = h
          wt(j-1) = four3 * h
          wt(j) = two3 * h
       end do
    end do
    wt(ifi) = one3 * h
    else
    one3=1.0_wp/2.0_wp
    two3=1.0_wp/2.0_wp
    four3=1.0_wp/2.0_wp
    ifi=1
    xr(1)=0.0_wp
    step(1)=0.0_wp
    do i=1,nix
      h=hint*real(ihx(i),wp)
      wt(ifi)=one3*(h+step(ifi))
      rstart=xr(ifi)
      ist=ifi+4
      ifi=irx(i)+1
      ifx=0
      if (i>1) ifx=irx(i-1)
      do j = ist,ifi,4
       xr(j-3)= rstart + (j-4-ifx)*h
       xr(j-2)= rstart + (j-3-ifx)*h
       xr(j-1)= rstart + (j-2-ifx)*h
       xr(j) = rstart + (j-1-ifx)*h
       step(j-3)=h
       step(j-2)=h
       step(j-1)=h
       step(j)=h
       wt(j-3) = two3*h
       wt(j-2) = four3*h
       wt(j-1) = two3*h
       wt(j) = 2.0_wp*one3*h
      end do
     enddo
     wt(ifi)=one3*h
     endif 
!    print *,'XR'
!    print *,xr
!    print *,'WT'
!    print *,wt
!    stop
  end subroutine simpson

  subroutine xsimpson
! release dynamic arrays
    integer               :: status
    deallocate (xr, wt, step, stat=status)
    if (status /= 0) call alloc_error (status, 'xsimpson', 'd')
  end subroutine xsimpson

end module radial_grid

