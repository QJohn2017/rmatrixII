module rm_rd
! Time-stamp: "2005-07-28 09:50:36 cjn"
  use precisn, only: wp
  use io_units, only: fi, fo
  use error_prt, only: alloc_error
  use rm_xdata

  private
  public rd_rm_data

  integer, save     :: max_npts = 40000 ! max size of radial mesh

contains

  subroutine rd_rm_data
! read in and write out the input data
    use rm_data, only: nelc, nz, coment, lrgle1, lrgle2, ncfgp,      &
         lrgld1, lrgld2, nset, inorb, maxorb, icon, ncfg, nocore,    &
         npot, ra, bsto, ibop, nix, ek2max, rd_nmlst, nrang2, lfixn
    use symmetries, only: rd_set_data
    use radial_grid, only: gen_mesh, simpson
    use bound_basis, only: slater, potl_defn
    use superstructure, only: ss, nbnd
    character(len=1), parameter :: lspec(0:5) = (/'s','p','d','f',&
         'g','h'/)
    integer, parameter          :: lndef = 20
    integer                     :: lrgllo, lrglup, status, iorb, maxn
    integer                     :: k, i, in, noc, iorb1, ioc, nel
    integer, allocatable        :: nj(:), lj(:)
    integer                     :: sc_inorb, ps_inorb, mx_ncorr
    integer                     :: idum1, idum2

    call rd_nmlst   ! read R-Matrix generic input namelist

    write (fo,'(a)') 'Program: RAD'
    call title (nelc, nz, coment)

! calculate lrgllo and lrglup, the minimum and maximum values of the
! total angular momentum l for the (n+1)-electron system.
    if (lrgle1 < 0 .or. lrgle2 < 0) then
       lrgllo = lrgld1
       lrglup = lrgld2
       lrgle1 = -1
    else
       lrgllo = MIN(lrgle1, lrgld1)
       lrglup = MAX(lrgle2, lrgld2)
    end if
    write (fo,'(a,i3,a,i3)') 'Hamiltonians calculated for (N+1)-&
         &electron total angular momentum ', lrgllo, ' to ', lrglup
    write (fo,'(a)') '(total parity even and odd in each case)'
    lrang4 = lrglup + 1

! for ierm=0,2:
! read the lspi for each n-electron symmetry.
    call rd_set_data (nset)

! determine the range of continuum angular momenta:
    call lcval (lrgle1, lrgle2, lrgld1, lrgld2, minim, lrang2, lexch)
    write (fo,'(a,i3,a,i3)') 'Continuum angular momentum, lc, takes &
         &values between ', minim-1, ' and ', lrang2-1
    write (fo,'(a,i3,/)') 'Exchange will be included up to lc = ', &
         lexch-1

! set up the maxorb - inorb nl values using the default sequence
! 1s,2s,2p,3s,....   Read in the remaining inorb nl values.
    if (inorb > maxorb) then
       write (fo,'(a,i2)') 'value of inorb = ', inorb
       write (fo,'(a,i2)') 'value of maxorb = ', maxorb
       write (fo,'(a)') 'inorb must be <= maxorb'
       stop
    end if
    iorb = maxorb - inorb
    allocate (nj(maxorb), lj(maxorb), stat=status)
    if (status /= 0) call alloc_error (status, 'stgrd', 'a')
    k = 1;  i = 1
    k_loop: do while (k <= iorb)
       do j = 0, i-1
          nj(k) = i
          lj(k) = j
          k = k + 1
          if (k > iorb) exit k_loop
       end do
       i = i + 1
    end do k_loop
    if (inorb > 0) read (fi,*) (nj(i), lj(i), i = iorb+1, maxorb)
    write (fo,'(a,i3,a)') 'The following', maxorb, ' bound orbitals &
         &are included in the calculation:' 
    write (fo,'(12(2x,i2,a1))') (nj(i), lspec(lj(i):lj(i)), &
         i = 1, maxorb)
    nrang1 = MAXVAL(nj)
    lrang1 = MAXVAL(lj) + 1    ! max a.m. of bound orbitals + 1

! skip corrector data used in ang:
    read (fi,*) sc_inorb, ps_inorb, mx_ncorr
    write (fo,'(a,3i6)') 'sc_inorb, ps_inorb, mx_ncorr = ', sc_inorb, &
         ps_inorb, mx_ncorr
    if (sc_inorb > 0) then
       read (fi,*) (idum1, idum2, i = 1, sc_inorb)
    end if
    if (ps_inorb > 0) then
       read (fi,*) (idum1, idum2, i = 1, ps_inorb)
    end if

! read configuration data required to identify the core
    if (icon /= 0) then
       allocate (noccsh(ncfg), stat=status)
       if (status /= 0) call alloc_error (status, 'stgrd', 'a')
       call rdcfg (maxorb, ncfg)
    else
       call rdcfg1 (maxorb, ncfg)
    end if
    if (ncfgp > 0) call rdcfgp (ncfgp)

! construct array of maximum occupation. only 2 electrons
! are allowed in f shells and higher due to cfp table limitations
    allocate (nelmax(maxorb), stat=status)
    if (status /= 0) call alloc_error (status, 'stgrd', 'a')
    do iorb = 1, maxorb
       if (lj(iorb) > 2) then
          nelmax(iorb) = 2
       else
          nelmax(iorb) = 4 * lj(iorb) + 2
       end if
    end do

    if (nocore == 0) then ! find # core orbitals, ncore
       ncore = maxorb
       do in = 1, ncfg
          noc = noccsh(in)
          iorb1 = 0
          do ioc = 1, noc
             iorb = nocorb(ioc,in)
             nel = nelcsh(ioc,in)
             if (nel < nelmax(iorb)) exit
             iorb1 = iorb
          end do
          ncore = MIN(ncore, iorb1)
       end do
    else
       ncore = 0
    end if

! first ncore orbitals form the core.
! determine largest l value of core orbitals, set up maxnc array.
    maxn = MAX(lrang1, lrang2)
    allocate (maxnc(lrang1), maxnhf(maxn), maxnlg(maxn), &
         stat=status)
    if (status /= 0) call alloc_error (status, 'stghrd', 'a')
    do l = 1, lrang1
       maxnc(l) = l - 1
    end do
    maxlc = -1
    do iorb = 1, ncore
       lcore = lj(iorb) + 1
       maxlc = MAX(maxlc, lcore)
       maxnc(lcore) = nj(iorb)
    end do
    write (fo,'(2(a,i4))') 'ncore = ', ncore, ' maxlc = ', maxlc
    write (fo,'(a,10i3)') 'maxnc = ', maxnc(1:maxlc)

! read the maxnhf and maxnlg arrays
    read (fi,*) maxnhf(1:lrang1)
    read (fi,*) maxnlg(1:lrang1)
    write (fo,'(/,a,10i3)') 'L =     ', (l,l=0,lrang1-1)
    write (fo,'(a,10i3)') 'maxnhf: ', maxnhf(:lrang1)
    write (fo,'(a,10i3)') 'maxnlg: ', maxnlg(:lrang1)
    shm = MAXVAL(maxnhf)
! extend maxnhf and maxnlg arrays
    if (lrang2 > lrang1) then
       do i = lrang1+1, lrang2
          maxnhf(i) = i - 1
          maxnlg(i) = i - 1
       end do
    end if
    
    if (ibop == 0) then ! read slater bound orbital parameters.
       if (npot <  0) then
          write (fo,'(/,a)') 'npot must be > 0 when using Slater-type &
               &orbitals'
          stop
       end if
       call slater

! calculate mesh parameters:
       call gen_mesh (nix, nz, ra, nrang2)

    else if (ibop /= 0) then ! read superstructure-type orbital data.
       call ss
       if (nbnd /= maxorb) then
          write (fo,'(/,a,2(a,i5))') 'error in s.s. input: nbound = ',&
               nbnd, ' /=  maxorb = ', maxorb
          stop
       end if
   end if

    write (fo,'(a,20i5)') 'maxnc = ', maxnc(1:lrang1)

! set up arrays for use in simpsons rule integration
    call simpson

! determine nrang2 and lfixn
! nrang2 = standard # continuum terms
! lfixn  = L value at which # of cont terms is allowed to decrease
! Construct nvary(l)
    allocate (nvary(lrang2), stat=status)
    if (status /= 0) call alloc_error (status, 'stgrrd', 'a')
    lfixn = MAX(lfixn, lndef)
    write (fo,'(/,a,i5)') 'nrang2 initially set to ', nrang2
    write (fo,'(a,i5)') 'nrang2 decreases for cont a.m. > ', lfixn-1
    if (lfixn < lrang2) then
       nvary(1:lfixn) = nrang2
       nvary(lfixn+1:lrang2) = 0
    else
       nvary = nrang2
    end if
    nrangx = nvary(1)   ! set nrangx to initial value of nrang2

! define potential used to calculate the continuum orbitals
    call potl_defn (npot)

! check buttle fitting parameters. Seaton procedure assumed.
    write (fo,'(/,a)') 'energy limits for the buttle correction'
    write (fo,'(a,f14.7)') 'maximum energy,  ek2max = ', ek2max
    write (fo,'(a)') 'MJS Buttle fitting procedure'
    if (bsto /= 0) then
       write (fo,'(a)') 'bsto non- zero is not implemented'
       stop
    end if
  end subroutine rd_rm_data

  subroutine rdcfg1 (maxorb, ncfg)
! read configuration data: 
! read the maximum and minimum possible occupations for each orbital,
! followed by ncfg occupation lists and nxcite, the number of 
! excitations required  (currently limited to 1)
    integer, intent(in)   :: maxorb
    integer, intent(inout):: ncfg
    integer               :: maxocc(maxorb), minocc(maxorb)
    integer               :: ishocc(maxorb)
    integer               :: noccsht(ncfg*maxorb)
    integer               :: nocorbt(maxorb,ncfg*maxorb)
    integer               :: nelcsht(maxorb,ncfg*maxorb)
    integer               :: nconf, i, nxcite, iorb, noc, no, jorb
    integer               :: korb, koc, mx, status

    read (fi,*) maxocc
    read (fi,*) minocc
    nconf = 0
    do i = 1, ncfg
       read (fi,*) ishocc, nxcite
       nconf = nconf + 1
       noc = 0
       do iorb = 1, maxorb
          if (ishocc(iorb) /= 0) then
             noc = noc + 1
             nocorbt(noc,nconf) = iorb
             nelcsht(noc,nconf) = ishocc(iorb)
          end if
       end do
       noccsht(nconf) = noc
       if (nxcite == 0) cycle
       if (nxcite > 1) then
          write (fo,'(a)') 'only one electron excitation is &
               &currently allowed'
          stop
       end if
       ncfig = nconf
       no_L: do no = noc, 1, -1  ! dig back into shells
          iorb = nocorbt(no,ncfig) ! current occ
          if (ishocc(iorb) == minocc(iorb)) cycle  ! at min
          ishocc(iorb) = ishocc(iorb) - 1   ! remove electron
          jorb_L: do jorb = iorb+1, maxorb  ! check higher shells
             if (ishocc(jorb) < maxocc(jorb)) then ! max occ?
                ishocc(jorb) = ishocc(jorb) + 1 ! add electron
                nconf = nconf + 1               ! cfg count
                koc = 0                         ! def ncorb, nelcsh
                do korb = 1, maxorb
                   if (ishocc(korb) /= 0) then
                      koc = koc + 1
                      nocorbt(koc,nconf) = korb
                      nelcsht(koc,nconf) = ishocc(korb)
                   end if
                end do
                noccsht(nconf) = koc         ! set # shells for cfg
                ishocc(jorb) = ishocc(jorb) - 1 ! restore ishocc
             end if
          end do jorb_L
          ishocc(iorb) = ishocc(iorb) + 1   ! restore ishocc
       end do no_L
    end do
    ncfg = nconf    ! total # cfgs
    mx = MAXVAL(noccsht(:ncfg))
    allocate (noccsh(ncfg), nocorb(mx,ncfg), nelcsh(mx,ncfg), &
         stat=status)
    if (status /= 0) call alloc_error (status, 'srgrad_rd', 'a')
    noccsh = noccsht(:ncfg)
    nocorb = nocorbt(:mx,:ncfg)
    nelcsh = nelcsht(:mx,:ncfg)
  end subroutine rdcfg1

  subroutine rdcfg (maxorb, ncfg)
! read configuration data: 
! read the number of occupied shells for ncfg cfgs, 
! then for each configuration read the index for the occupied shells 
! and the number of electrons in the shell
    integer, intent(in)   :: maxorb
    integer, intent(in)   :: ncfg
    integer               :: mx, status, i, n

    read (fi,*) noccsh
    mx = MAXVAL(noccsh)
    allocate (nocorb(mx,ncfg), nelcsh(mx,ncfg), stat=status)
    if (status /= 0) call alloc_error (status, 'srgrad_rd', 'a')
    do i = 1, ncfg
       n = noccsh(i)
       read (fi,*) nocorb(1:n,i)
       read (fi,*) nelcsh(1:n,i)
    end do
  end subroutine rdcfg

  subroutine rdcfgp (npefig)
! skip N+1 electron configuration data:
! read the number of occupied shells for ncfgp cfgs,
! then for each configuration read the index for the occupied shells
! and the number of electrons in the shell
    integer, intent(in)   :: npefig
    integer               :: status, i, n, fshp
    integer, allocatable  :: noccshp(:), nocorbp(:), nelcshp(:)

    allocate (noccshp(npefig), stat=status)
    if (status /= 0) call alloc_error (status, 'rdcfgp', 'a')
    read (fi,*) noccshp
    fshp = MAXVAL(noccshp(1:npefig))
    allocate (nocorbp(fshp), nelcshp(fshp), stat=status)
    if (status /= 0) call alloc_error (status, 'rdcfgp', 'a')
    do i = 1, npefig
       n = noccshp(i)
       read (fi,*) nocorbp(1:n)
       read (fi,*) nelcshp(1:n)
    end do
    deallocate (noccshp, nocorbp, nelcshp, stat=status)
    if (status /= 0) call alloc_error (status, 'rdcfgp', 'd')
  end subroutine rdcfgp

  subroutine lcval (lrgle1, lrgle2, lrgld1, lrgld2, minim, lrang2, &
       lexch)
! determine which sets couple with each value of continuum a.m. 
! within the range allowed.
! thus determine the range reqd for continuum angular momentum.
    use symmetries, only: nset, lset, ipset, lrang3
    integer, intent(in)    :: lrgle1, lrgle2
    integer, intent(in)    :: lrgld1, lrgld2
    integer, intent(out)   :: minim
    integer, intent(out)   :: lrang2
    integer, intent(out)   :: lexch
    integer                :: lval(2*lrang3), ilval(2*lrang3)
    integer                :: nscol(2*lrang3)
    integer                :: llo, lup, lrgl1, lrgl2, ipoint, lrgl
    integer                :: is, k, kl, lmin, lmax, ipt, npty
    integer                :: iltot, l, nl, lcfg

    llo = 999
    lup = 0
    if (lrgle1 < 0 .or. lrgle2 < 0) then ! no-exchange case
       lrgl1 = lrgld1
       lrgl2 = lrgld2
       lexch = 0
       ipoint = 1
    else                                 !  exchange included
       lrgl1 = lrgle1
       lrgl2 = lrgle2
       ipoint = 0
    end if

    ipt_L: do ipt = 1, 2
       lrgl_L: do lrgl = lrgl1, lrgl2
          npty_L: do npty = 0, 1
! find range of cont a.m. and set up ilval(kl) - poss vals of cont a.m.
             lmin = 999
             lmax = 0
             do is = 1, nset
                lcfg = lset(is)
                lmin = MIN(lmin, ABS(lcfg-lrgl))
                lmax = MAX(lmax, lcfg)
             end do
             lmax = lmax + lrgl
             iltot = lmax - lmin + 1 ! # posible cont. a.m. values
             do kl = 1, iltot
                ilval(kl) = lmin + kl - 1
             end do

             nscol = 0
             chk_L: do kl = 1, iltot   ! check each of the possible a.m.
                l = ilval(kl)
                nl = 0
                do is = 1, nset
                   lcfg = lset(is)   ! tgt L
                   ipi = ipset(is)   ! tgt parity
                   if (MOD(ipi+l+npty,2) /= 0) cycle ! parity check
                   if (lrgl > lcfg+l .or. lrgl < ABS(lcfg-l)) cycle
                   nl = nl + 1
                   nscol(kl) = 1     ! pointer to L-values which couple
                   exit
                end do
             end do chk_L

! now delete values of the cont a.m. which cannot couple with any set
             kl = 0
             do ikl = 1, iltot
                if (nscol(ikl) /= 0) then
                   kl = kl + 1
                   lval(kl) = ilval(ikl)
                end if
             end do
             ltot = kl
             if (ltot == 0) cycle
             lup = MAX(lval(ltot), lup)
             llo = MIN(lval(1), llo)
          end do npty_L
       end do lrgl_L

       if (ipoint == 0) then      ! exchange included case
          lexch = lup + 1         ! L for which exchange dropped
          lrgl1 = MAX(lrgle2, lrgld1)
          lrgl2 = lrgld2
          ipoint = 1
          cycle
       else
          minim = llo + 1
          lrang2 = lup + 1
          exit
       end if
    end do ipt_L
  end subroutine lcval

  subroutine title (nelc, nz, coment)
! write a general title indicating the target ion/atom, 
! # continuum electrons, highest electron energy
    character(len=80), intent(in)  :: coment
    integer, intent(in)            :: nelc
    integer, intent(in)            :: nz
    character(len=2), parameter    :: atom(57) = (/ &
         ' H', 'He', 'Li', 'Be', ' B', ' C', ' N', ' O', ' F', &
         'Ne', 'Na', 'Mg', 'Al', 'Si', ' P', ' S', 'Cl', 'Ar', &
         ' K', 'Ca', 'Sc', 'Ti', ' V', 'Cr', 'Mn', 'Fe', 'Co', &
         'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr', &
         'Rb', 'Sr', ' Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', &
         'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', ' I', 'Xe', &
         'Cs', 'Ba', 'La'/)
    character(len=1)                :: chrge
    character(len=8)                :: today
    character(len=10)               :: now
    integer                         :: nzeff

    call date_and_time (today, now)
! today: ccyymmdd
    write (fo,'(a,a2,a,a2,a,a4)') 'Job run on ', today(7:8), &
         '/', today(5:6), '/', today(1:4)
! now: hhmmss.sss
    write (fo,'(a,a2,a,a2,a,a5)') 'Time: ', now(1:2), ':', &
         now(3:4), ':', now(5:10)
    if (coment /= ' ') write (fo,'(a80)') coment

    nzeff = nz - nelc
    if (nzeff > 0) then
       chrge = '+'
    else if (nzeff < 0) then
       chrge = '-'
       nzeff = - nzeff
    end if

    if (nzeff > 1) then
       write (fo,'(5x,37("*"))')
       write (fo,'(5x,"*",35x,"*")')
       if (nzeff < 10) then
          write (fo,'(5x,"*",29x,i1,a1,4x,"*")') nzeff, chrge
       else
          write (fo,'(5x,"*",29x,i2,a1,3x,"*")') nzeff, chrge
       end if
       
       if (nz > 57) then
          write (fo,'(5x,"*",4x,a,i4,6x,"*")') 'Electron scatter&
               &ing by Z = ', nz
       else
          write (fo,'(5x,"*",4x,a,a2,6x,"*")') 'Electron scatter&
               &ing by ', atom(nz)
       end if
       write (fo,'(5x,"*",35x,"*")')
       write (fo,'(5x,37("*"))')
    else
       write (fo,'(/,5x,37("*"))')
       write (fo,'(5x,"*",35x,"*")')
       if (nzeff == 1) write (fo,'(5x,"*",29x,a1,5x,"*")') chrge
       if (nz > 57) then
          write (fo,'(5x,"*",4x,a,i4,6x,"*")') 'Electron scatter&
               &ing by Z = ', nz
       else
          write (fo,'(5x,"*",4x,a,a2,6x,"*")') 'Electron scattering by ',&
               atom(nz)
       end if
       write (fo,'(5x,"*",35x,"*")')
       write (fo,'(5x,37("*"),/)')
    end if
  end subroutine title
end module rm_rd
