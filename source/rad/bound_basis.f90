module bound_basis
! Time-stamp: "2003-05-21 08:08:48 cjn"
  use precisn, only: wp
  use io_units, only: fi, fo
  use error_prt, only: alloc_error
  use debug, only: bug5
  implicit none

  real(wp), save   :: eps = 1.0e-4_wp     ! normalization criterion
  real(wp), save   :: eps1 = 0.8e-4_wp    ! normalization criterion

  integer, save               :: npot
  integer, allocatable, save  :: ipot(:)
  real(wp), allocatable, save :: cpot(:)
  real(wp), allocatable, save :: xpot(:)
  integer, allocatable, save  :: istat(:,:)
  
  integer, save               :: nbound
  real(wp), allocatable, save :: uj(:,:)        ! radial orbitals
  real(wp), allocatable, save :: duj(:,:)       ! mod second derivatives
  real(wp), allocatable, save :: d1uj(:,:)      ! first derivatives
  integer, allocatable, save  :: ipos(:,:)      ! orbital pointers

  real(wp), allocatable, save :: p(:,:)

  integer, parameter                  :: slmx = 20   ! max # Slater fns
  real(wp), allocatable, target, save :: c(:,:,:)
  real(wp), allocatable, target, save :: ze(:,:,:)
  integer, allocatable, target, save  :: irad(:,:,:)
  integer, allocatable, save          :: nco(:)

  private
  public evalue, orbital_store, potl_defn
  public slater, xslater_pams, destroy_p
  public nbound, uj, duj, ipos, d1uj, p
  public npot, ipot, cpot, xpot, istat
  public c, ze, irad, nco
  public corect

contains

  subroutine evalue
! evaluate the slater bound orbitals at the mesh points
    use radial_grid, only: irx, nix, xr, step, wt, npts
    use rm_xdata, only: maxnlg, maxnhf, lrang1, minim
    use rm_data, only: nz, ra, ibop, reorth
    integer                    :: status
    integer                    :: l, maxhf, all1, n, nq, m, maxlg
    integer                    :: ntimes, jup, mt, i, j, real_size
    integer                    :: nqh, nqhold, nn, jk, kt, ir, k, ios
    integer                    :: lstart, n1
    real(wp)                   :: h, r, pv, an, y, ovrlap, aterm, r0
    real(wp)                   :: zn, sigma, alpha, c1, zee, dr1, hh4
    real(wp)                   :: dr2, bx, bl

    maxlg = MAXVAL(maxnlg)
    if (maxlg > 0) then
       inquire (iolength=real_size) ra
       open (unit=60, access='direct', status='unknown', file='BND', &
            form='unformatted', recl = real_size*2*irx(nix)*maxlg,  &
            iostat=ios)
       if (ios /= 0) then
          write (fo,'(a,i4)') 'evalue: error opening BND, &
               &iostat = ', ios
          stop
       end if
       allocate (p(maxlg,2*npts-2), stat=status)
       if (status /= 0) call alloc_error (status, 'evalue', 'a')
    end if
    zn = REAL(nz+nz,wp)

    select case (ibop)
    case (0)                 ! Slater functions
       nq = 0
       print *,lrang1
       l_loop: do l = 1, lrang1
          maxhf = maxnhf(l)
          maxlg = maxnlg(l)
          if (minim > l) maxlg = 0
          all1 = REAL(l*(l-1),wp)
          kt = 0
          print *,maxhf
          n_loop: do n = l, maxhf
! first calculate the gaussian correction
             nq = nq + 1
             if (n == l) nqhold = nq
             ntimes = 0
        
             m = nco(nq)
             ipos(n,l) = nq
             call corect (m, ra, c(1:m,n,l), irad(1:m,n,l), ze(1:m,n,l), &
                  r0, sigma, c1)
             if (n <= maxlg) then
                jup = 2      ! orbital for lagrange orthonalization
                kt = kt + 1
                mt = 0
             else
                jup = 1
             end if
             do i = 2, irx(nix)+1
! evaluate the orbital at each mesh point
                h = 0.5_wp * step(i)
                do j = jup, 1, -1
                   r = xr(i) + (1-j) * h
                   pv = 0.0_wp
                   alpha = (r - r0) / sigma
                   if (-alpha < 12.0_wp) pv = -c1 * EXP(-alpha * alpha)
                   do k = 1, m
                      pv = pv + c(k,n,l) * r**irad(k,n,l) * EXP(-ze(k,n,l)*r)
                   end do
                   if (j == 1) then
                      uj(i,nq) = pv
                   end if
                   if (jup == 2) then
                      mt = mt + 1
                      p(kt,mt) = pv
                   end if
                end do
             end do
             uj(1,nq) = 0.0_wp

             if (reorth) then
!================================================             
! overlaps pre orthogonalization:
             do nn = l, n-1
                nqh = ipos(nn,l)
                ovrlap = 0.0_wp
                do i = 2, irx(nix)+1
                   ovrlap = ovrlap + uj(i,nq) * uj(i,nqh) * wt(i)
                end do
                write (*,*) 'Bound overlaps(1): nq, nqh, overlap = ', &
                     nq, nqh, ovrlap
             end do
!================================================
! try to make bound orbitals orthogonal by Schmidt orthogonalization
             do while (ntimes < 2)
                nqh = nqhold
                do nn = l, n-1
                   ovrlap = 0.0_wp
                   do i = 2, irx(nix)+1
                      ovrlap = ovrlap + uj(i,nq) * uj(i,nqh) * wt(i)
                   end do
                   do i = 2, irx(nix)+1
                      uj(i,nq) = uj(i,nq) - ovrlap * uj(i,nqh)
                   end do
                   nqh = nqh + 1
                end do
                ntimes = ntimes + 1
             end do

!======================================
! show overlaps after orthogonalization:
             do nn = l, n-1
                nqh = ipos(nn,l)
                ovrlap = 0.0_wp
                do i = 2, irx(nix)+1
                   ovrlap = ovrlap + uj(i,nq) * uj(i,nqh) * wt(i)
                end do
                write (*,*) 'Bound overlaps(2): nq, nqh, overlap = ', &
                     nq, nqh, ovrlap
             end do
!======================================
             end if             
! renormalise the orbital
             an = 0.0_wp
             do i = 2, irx(nix)+1
                an = an + uj(i,nq) * uj(i,nq) * wt(i)
             end do
             an = SQRT(an)
             do i = 2, irx(nix)+1
                uj(i,nq) = uj(i,nq) / an
             end do
             do i = 1, m
                c(i,n,l) = c(i,n,l) / an
             end do
             if (n <= maxlg) then
                do i = 1, 2*irx(nix)
                   p(kt,i) = p(kt,i) / an
                end do
             end if

! set up the duj array containing the second-order derivative
! and potential terms
             call corect (m, ra, c(1:m,n,l), irad(1:m,n,l), ze(1:m,n,l), &
                  r0, sigma, c1)
             do i = 2, irx(nix)+1
                r = xr(i)
                pv = 0.0_wp
                alpha = (r - r0) / sigma
                if (-alpha < 12.0_wp) then
                   pv = (1.0_wp - 2.0_wp * alpha * alpha) * (2.0_wp / &
                        (sigma * sigma)) * c1 * EXP(-alpha * alpha)
                end if
                do jk = 1, m
                   ir = irad(jk,n,l)
                   zee = -ze(jk,n,l)
                   aterm = REAL(ir*(ir-1),wp) + 2.0_wp * zee * REAL(ir,wp) &
                        * r + zee * zee * r * r
                   pv = pv + c(jk,n,l) * EXP(zee * r) * aterm * r**(ir-2)
                end do
                pv = uj(i,nq) * (all1 / r - zn) / r - pv
                duj(i,nq) = pv
! insert first derivatives in array d1uj
                pv = 0.0_wp
                if (-alpha < 12.0_wp) then
                   pv = 2.0_wp * (alpha / sigma) * c1 * &
                        EXP(-alpha * alpha)
                end if
                do jk = 1, m
                   ir = irad(jk,n,l)
                   zee = -ze(jk,n,l)
                   aterm = REAL(ir,wp) + zee * r
                   pv = pv + c(jk,n,l) * EXP(zee * r) * aterm * r**(ir-1)
                end do
                d1uj(i,nq) = pv
             end do
          end do n_loop
          write (60,rec=l,iostat=ios) p
          if (ios /= 0) then
             write (fo,'(a,i4)') 'evalue: error writing BND, &
                  &iostat = ', ios
             stop
          end if
       end do l_loop
    
       if (nq /= nbound) then
          write (fo,'(/,a,/,a,i5,a,i5)') 'error in evalue: nq /= nbound',&
               'nq = ', nq, '   nbound = ', nbound
          stop
       end if

    case (1)     ! Case of Numerical orbitals
       lstart = MAX(1, minim)
       if (lstart > lrang1) return
       orb_am: do l = lstart, lrang1
          maxlg = maxnlg(l)
          if (maxlg < l) cycle
          all1 = REAL(l*(l-1), wp)
          kt = 0
          do n = l, maxlg                     
             n1 = ipos(n,l)
             kt = kt + 1

! interpolate the bound orbital at the half-mesh points
             y = 0.0_wp
             dr1 = 0.0_wp
             do k = 2, irx(nix)+1
                r = xr(k)
                h = 0.25_wp * (r - xr(k-1))
                hh4 = h * h
                dr2 = ((all1/r - zn)/r) * uj(k,n1) - duj(k,n1)
                p(kt,2*k-3) = 0.5_wp * (uj(k,n1) + y) - (dr2 + dr1) * hh4
                y = uj(k,n1)
                p(kt,2*k-2) = y
                dr1 = dr2
             end do
          end do
! write orbitals to direct access scratch file unit number 60
          write (60,rec=l,iostat=ios) p
          if (ios /= 0) then
             write (fo,'(a,i4)') 'evalue: error writing BND, &
                  &iostat = ', ios
             stop
          end if
       end do orb_am
    end select

    write (fo,'(/,a)') 'Orbital maximum and boundary values'
    do i = 1, nbound
       bx = MAXVAL(ABS(uj(2:irx(nix),i)))
       bl = uj(irx(nix),i)
       write (fo,'(i6,3f10.6)') i, bx, bl, ABS(bl/bx)
    end do

    
! if bug5 >= 1 write the bound orbitals at mesh points in steps of 2
    if (bug5 >= 1) then
       write (fo,'(a)') 'subroutine evalue'
       do i = 1, irx(nix)+1, 2
          write (fo,'(1x,11f10.6)') xr(i), (uj(i,n),n=1,nbound)
       end do
    end if
  end subroutine evalue

  subroutine corect (m, ra, c, irad, ze, r0, sigma, c1)
! calculate parameters of Gaussian correction to be applied to bound 
! orbital specified by quantum numbers (n,lp-1).
! correction to orbital at radius r is -c1*exp(-((r-r0)/sigma)**2).
! the corrected function as well as its first derivative
! will vanish at the r-matrix boundary ra. the second deivative
! of the gaussian is zero at the boundary, i.e., it will not
!     change the second derivative of the function there.
    integer, intent(in)        :: m
    integer, intent(in)        :: irad(m)
    real(wp), intent(in)       :: ra
    real(wp), intent(in)       :: c(m), ze(m)
    real(wp), intent(out)      :: r0, sigma, c1
    integer                    :: j
    real(wp)                   :: pa, dp, term, s, b

! calculate the function and its first derivative at r=ra.
    pa = 0.0_wp
    dp = 0.0_wp
    do j = 1, m
       term = c(j) * ra**REAL(irad(j),wp) * EXP(-ze(j) * ra)
       pa = pa + term
       dp = dp + (REAL(irad(j),wp) / ra - ze(j)) * term
    end do

! original tail correction:
    s = 2.0_wp
    b = ra * dp / pa
    sigma = ra * (s - sqrt(s*s - 0.2_wp*b)) / b
    r0 = s * sigma + 0.9_wp * ra
    c1 = pa / EXP(-((ra-r0)/sigma)**2)

    r0 = ra + pa / dp
    sigma = SQRT(2.0_wp) * ABS(pa / dp)
    c1 = pa * EXP(((ra - r0) / sigma)**2)
    if (bug5 > 1) write (fo,'(a,1p,5e11.3)') 'c1,r0,sigma,&
         &pa,dp = ', c1, r0, sigma, pa, dp
! Change hugo
!    r0=2_wp*ra-r0
!     c1=0
!    c1=-c1	 
  end subroutine corect

  subroutine potl_defn (potl_type)
! define the potential used to calculate the continuum orbitals
    use rm_xdata, only: maxnhf, lrang1
    integer, intent(in)         :: potl_type
    integer                     :: i, status, nhf, j
    character(len=1), parameter :: lspec(0:5) = (/'s','p','d',&
         'f','g','h'/)

    npot = potl_type
    if (npot == 0) then   ! static potl
! target configuration stored in istat
       write (fo,'(a)') 'static potential: shell occupancy of &
            &the ground state'
       nhf = MAXVAL(maxnhf(1:lrang1))
       allocate (istat(lrang1,nhf), stat=status)
       if (status /= 0) call alloc_error (status, 'potl_defn', 'a')
       do i = 1, lrang1
          nhf = maxnhf(i)
          read (fi,*) istat(i,i:nhf)
          write (fo,'(35(i4,a1))') (j, lspec(i-1), j=i,nhf)
          write (fo,'(35i5)') istat(i,i:nhf)
       end do
    else if (npot >= 1) then   ! parametric potential
       allocate (ipot(npot), cpot(npot), xpot(npot), stat=status)
       if (status /= 0) call alloc_error (status, 'potl_defn', 'a')
       read (fi,*) ipot(1:npot)
       read (fi,*) cpot(1:npot)
       read (fi,*) xpot(1:npot)
    end if
  end subroutine potl_defn

  subroutine orbital_store (nc)
! allocate storage for orbital arrays and locator
    use rm_data, only: maxorb, nrang2
    use rm_xdata, only: lrang4, nrang1, lrang1, lrang2
    use radial_grid, only: npts
    use symmetries, only: lrang3
    integer, intent(in), optional :: nc ! # radial points
    integer                  :: dm1, dm2, adm, bdm, cdm, kdm, status

    if (PRESENT(nc)) then  ! allow for superstructure input
       npts = nc
    end if
    dm1 = maxorb + 4 * nrang2
    dm2 = MAX(lrang3 + lrang4 - 1, lrang1, lrang2)
    adm = dm2 - 1 + nrang2
    bdm = nrang1 + nrang2
    cdm = (adm + bdm + 1) / 2
    kdm = adm * (adm/cdm) + bdm * (bdm/cdm) - (adm/bdm) * (bdm/adm) * &
         adm

    allocate (uj(npts,dm1), duj(npts,dm1), d1uj(npts,dm1), ipos(kdm,dm2),&
         stat=status)
    if (status /= 0) call alloc_error (status, 'orbital_store', 'a')
    print *,'IPOS ',kdm,dm2
    nbound = maxorb
    ipos = -1
  end subroutine orbital_store

  subroutine slater
! read in slater orbital parameters
    use rm_data, only: ra
    use rm_xdata, only: maxnhf, lrang1
    use angular_momentum, only: gammas, nfact
    integer                     :: l, n, m1, k, ir, ir2, itest, k2, m
    integer                     :: imin, ira, k1, mxn, status
    real(wp)                    :: x, x1, z1, zr, tinorb, small, zmin
    real(wp)                    :: y, rx, ra_est
    character(len=1), parameter :: lspec(0:5) = (/'s','p','d','f',&
         'g','h'/)

    mxn = MAXVAL(maxnhf(:lrang1))
    allocate (irad(slmx,mxn,lrang1), ze(slmx,mxn,lrang1), &
         c(slmx,mxn,lrang1), nco(mxn*lrang1), stat=status)
    if (status /= 0) call alloc_error (status, 'slater', 'a')

    zmin = 1.0_wp / 1.0e-6_wp
    write (fo,'(//,a,/)') 'Radial Functions:'
    write (fo,'(26x,a,5x,a,5x,a)') 'Slater-type', ' power of r', &
         ' Exponent'

! read in coefficients defining the bound orbitals
    ra_est = 0.0_wp
    m1 = 0
    l_L: do l = 1, lrang1
       n_L: do n = l, maxnhf(l)
          write (fo,'(2x,i2,2a)') n, lspec(l-1), '  orbital'
          m1 = m1 + 1
          read (fi,*) m
          if (m > slmx) then
             write (fo,'(a,i5)') 'Slater error, maximum = ', slmx
             stop
          end if
          read (fi,*) irad(1:m,n,l)
          read (fi,*) ze(1:m,n,l)
          read (fi,*) c(1:m,n,l)
          ir = MAXVAL(irad(:m,n,l))
          ir2 = ir + ir + 1
          if (ir2 > nfact) then
             write (fo,'(a,i5)') 'insufficient factorials &
                  &calculated, number = ', nfact
             stop
          end if
          nco(m1) = m

! check the normalisation of each bound orbital
          x = 0.0_wp
          k_L: do k = 1, m
             if (ze(k,n,l) < zmin) then
                zmin = ze(k,n,l)
                imin = irad(k,n,l)
             end if
             x1 = 0.0_wp
             k2 = irad(k,n,l)+1
             z1 = ze(k,n,l)
             do k1 = 1, m
                x1 = x1 + c(k1,n,l) * gammas(irad(k1,n,l)+k2) / &
                     (ze(k1,n,l) + z1)**(k2 + irad(k1,n,l))
             end do
             x = x + c(k,n,l) * x1
          end do k_L
          if (ABS(x-1.0_wp) > 0.01_wp) then
! the coefficients may be clementi type - renormalise
             do k = 1, m
                ir = irad(k,n,l)
                zr = 2.0_wp * ze(k,n,l)
                c(k,n,l) = c(k,n,l) * SQRT(zr / gammas(ir+ir+1)) * &
                     zr**ir
             end do
! check normalisation
             x = 0.0_wp
             do k = 1, m
                x1 = 0.0_wp
                k2 = irad(k,n,l) + 1
                z1 = ze(k,n,l)
                do k1 = 1, m
                   x1 = x1 + c(k1,n,l) * gammas(irad(k1,n,l)+k2) / &
                        (ze(k1,n,l) + z1)**(k2 + irad(k1,n,l))
                end do
                x = x + c(k,n,l) * x1
             end do
!            if (ABS(x-1.0_wp) > 0.001_wp) then
! error in input data - orbital is not normalised
                write (fo,'(2x,i2,a1,a,f15.6)') n, lspec(l-1),  &
                     ' orbital has normalisation ', x
!               stop
!            end if
          end if

          y = SQRT(x)
          do k = 1, m
             c(k,n,l) = c(k,n,l) / y
             write (fo,'(24x,e14.7,9x,i3,7x,e14.7)') c(k,n,l), &
                  irad(k,n,l), ze(k,n,l)
          end do
          rx = 0.1_wp
          call find_radius (m, n, l, rx)
          write (fo,'(a,f8.4)') 'Orbital normalization radius = ', rx
          ra_est = MAX(ra_est, rx)
       end do n_L
    end do l_L

    write (fo,'(a,f8.4)') 'Estimated R-matrix radius = ', ra_est

! NB the following traditional algorithm for determining
! the R-Matrix radius appears to contain loopholes.
! The alternative code based on analysis of the orbital
! norms is expected to be more reliable.

!!$! if necessary, estimate the boundary radius ra
!!$    if (ra < 0.0) then
!!$       itest = 0
!!$       tinorb = 0.001_wp
!!$       ra = - LOG(tinorb) / zmin
!!$       ra = ra + imin * LOG(ra) / zmin
!!$       ira = INT(ra + 0.5_wp)
!!$       ra_loop: do
!!$          ra = REAL(ira,wp)
!!$          m1 = 0
!!$          small = 0.2_wp
!!$! check that the boundary radius is correct for all orbitals
!!$          lb_L: do l = 1, lrang1
!!$             nb_L: do n = l, maxnhf(l)
!!$                m1 = m1 + 1
!!$                m = nco(m1)
!!$                x_loop: do
!!$                   x = 0.0_wp
!!$                   do k = 1, m
!!$                      x = x + c(k,n,l) * ra**irad(k,n,l) * &
!!$                           EXP(-ze(k,n,l) * ra)
!!$                   end do
!!$                   if (ABS(x) > tinorb) then
!!$                      ra = ra + small
!!$                      itest = 1
!!$                      cycle
!!$                   end if
!!$                   exit
!!$                end do x_loop
!!$             end do nb_L
!!$          end do lb_L
!!$          if (itest == 0) then
!!$             if (ira >= 1) then
!!$                ira = ira - 1
!!$                cycle
!!$             end if
!!$          end if
!!$          exit
!!$       end do ra_loop
!!$    end if

    if (ra < 0.0_wp) then
       ira = NINT(ra_est)
       if (REAL(ira,wp) < ra_est) ira = ira + 1
       ra = REAL(ira,wp)
    end if

    write (fo,'(/,a)') 'R-matrix boundary conditions'
    write (fo,'(a,f10.5)') 'ra = ', ra
    write (fo,'(/,a,/)') 'Amplitude of the functions at ra:'
    m1 = 0
    do l = 1, lrang1
       do n = l, maxnhf(l)
          m1 = m1 + 1
          x = 0.0_wp
          do k = 1, nco(m1)
             x = x + c(k,n,l) * ra**irad(k,n,l) * EXP(-ze(k,n,l) * ra)
          end do
          write (fo,'(i5,a,4x,e14.7)') n, lspec(l-1), x
       end do
    end do
  end subroutine slater
  
  subroutine xslater_pams
    integer               :: status
    deallocate (irad, ze, c, nco, stat=status)
    if (status /= 0) call alloc_error (status, 'slater', 'a')
  end subroutine xslater_pams

  subroutine destroy_p
    integer        :: status
    deallocate (p, stat=status)
    if (status /= 0) call alloc_error (status, 'destroy_p', 'd')
  end subroutine destroy_p

  function snorm (n, z, r)
! incomplete norm of Slater function
    real(wp)                 :: snorm
    integer, intent(in)      :: n   ! principal quantum # sum
    real(wp), intent(in)     :: z   ! zeta sum
    real(wp), intent(in)     :: r   ! radius
    real(wp)                 :: ex, zi, rx
    integer                  :: i

    ex = EXP(-z*r)
    snorm = (1.0_wp - ex) / z
    if (n > 0) then
       zi = 1.0_wp / z
       rx = 1.0_wp
       do i = 1, n
          rx = r * rx
          snorm = zi * (REAL(i,wp) * snorm - ex * rx)
       end do
    end if
  end function snorm

  subroutine fn (m, n, l, x, orb)
! return the normalization integral for range 0 to x
    integer, intent(in)   :: m   ! # slater terms
    integer, intent(in)   :: n   ! orbital principal quantum #
    integer, intent(in)   :: l   ! orbital ang. momentum q. #
    real(wp), intent(in)  :: x   ! range
    real(wp), intent(out) :: orb ! normalization integral
    integer               :: k, kp, nn
    real(wp)              :: z2, sn, sn2, dorb

    orb = 0.0_wp
    do k = 1, m
       do kp = k, m
          nn = irad(k,n,l) + irad(kp,n,l)
          dorb = c(k,n,l) * c(kp,n,l) * &
               snorm(nn, ze(k, n, l)+ze(kp,n,l), x)
          orb = orb + dorb
          if (k /= kp) orb = orb + dorb
       end do
    end do
  end subroutine fn

  subroutine find_radius (m, n, l, r)
 ! determine the R-matrix radius
    integer, intent(in)      :: m  ! # slater terms
    integer, intent(in)      :: n  ! principal quantum #
     integer, intent(in)     :: l  ! orbital a.m. quantum #
    real(wp), intent(inout)  :: r ! radius
    real(wp)                 :: ss, x, rl, ru, s
    integer                  :: i

    x = r
    call fn (m, n, l, x, s)
    ss = ABS(s - 1.0_wp)

    if (ss < eps1) then ! r too large
       do while (ABS(s - 1.0_wp) < eps1)
          x = 0.9_wp * x
          call fn (m, n, l, x, s)
       end do
       if (ABS(x-1.0_wp) < eps) then
          r = x
          return
       end if
       rl = x
       ru = 10.0_wp * x / 9.0_wp
    else if (ss < eps) then ! r OK
       return
    else    ! r too small
       do while (ABS(s - 1.0_wp) > eps)
          x = 1.1_wp * x
          call fn (m, n, l, x, s)
       end do
       if (ABS(s - 1.0_wp) > eps1) then
          r = x
          return
       end if
       ru = x
       rl = 10.0_wp * x / 11.0_wp
    end if
! required radius bracketted by rl, ru
    halving: do i = 1, 50
       x = 0.5_wp * (rl + ru)
       call fn (m, n, l, x, s)
       ss = ABS(s - 1.0_wp)
       if (ss < eps1) then ! still too big
          ru = x
       else if (ss < eps) then ! radius found
          r = x
          return
       else   ! radius still too small
          rl = x
       end if
    end do halving
    write (fo,'(a)') 'find_radius: Radius not found after 50 halvings'
    stop
  end subroutine find_radius
end module bound_basis

