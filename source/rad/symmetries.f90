module symmetries     ! rad version
! define target symmetry (SLpi) values
! Time-stamp: "2001-12-29 09:47:51 cjn"
  use io_units, only: fi, fo
  implicit none

  integer, save                :: nset       ! # N-electron symmetries
  integer, allocatable, save   :: lset(:)    ! L-values
  integer, allocatable, save   :: isset(:)   ! S-values
  integer, allocatable, save   :: ipset(:)   ! pi-values

  integer, save                :: nast       ! # tgt states
  integer, allocatable, save   :: nstat(:)   ! tgts / symmetry
  integer, save                :: isplo      ! min spin, N system
  integer, save                :: ispup      ! max spin, N system

  integer, save                :: lrang3    ! max tgt L + 1
  integer, save                :: llpdim    ! 2 * lrang3 - 1

  private
  public   rd_set_data
  public   nset, lset, isset, ipset, nast, nstat
  public   isplo, ispup, llpdim, lrang3

contains

  subroutine rd_set_data (nset_tmp)
    use error_prt, only: alloc_error
    use rm_data, only: nelc
    integer, intent(in)        :: nset_tmp ! # target symmetries
    integer                    :: is, isran1, isran2, i, status

    nset = nset_tmp
    allocate (lset(nset), isset(nset), ipset(nset), nstat(nset),&
         stat=status)
    if (status /= 0) call alloc_error (status, 'rd_set_data', 'a')

    nast = 0
    do is = 1, nset
       read (fi,*) lset(is), isset(is), ipset(is), nstat(is)
       if (MOD(isset(is)+nelc,2) == 0) then ! input data error
          write (fo,'(a,3i4)') 'rd_set_data: input error,     &
               &spin multiplicity, nelc inconsistent ', is,   &
               isset(is), nelc
          stop
       end if
       nast = nast + nstat(is)
    end do

    lrang3 = MAXVAL(lset) + 1
    llpdim = lrang3 + lrang3 - 1

    isran1 = MINVAL(isset)   ! min tgt spin
    isran2 = MAXVAL(isset)   ! max tgt spin
    isplo = isran1
    ispup = isran2
  end subroutine rd_set_data
end module symmetries
