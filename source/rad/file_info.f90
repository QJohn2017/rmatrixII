module file_info
! file information
! Time-stamp: "2003-03-26 17:15:55 cjn"
  use precisn, only: wp
  use io_units, only: fo
  implicit none

  integer, save      :: nbuff2
  integer, save      :: jbuff1 = 24
  integer, save      :: jbuff2 = 25
  integer, save      :: jbuff4 = 27
  integer, save      :: jbuff5 = 28
  integer, save      :: itape3 = 26

  integer, save      :: u1 = 61
  integer, save      :: u2 = 62
  integer, save      :: u3 = 63

  private
  public nbuff2, jbuff1, jbuff2, jbuff4, jbuff5, itape3
  public open_files

  public open_tmp_files, delete_tmp_files
  public u1, u2, u3

contains

  subroutine open_files
! Open radial files
    use rm_xdata,  only: lrang1, lrang2
    use symmetries, only: lrang3
    use filehand, only:  initda, open
    character(len=7)    :: file1, file2, file3, file4, file5, stats
    integer             :: lbuff2, lbuff5
    integer             :: inout    ! unused dummy argument in filehand
    integer             :: int_size, ios, lr1, lr2, lr3
    logical             :: real_buffer_type

    lr1 = lrang1
    lr2 = lrang2
    lr3 = lrang3

    file1 = 'RAD1'
    file2 = 'RAD2'
    file3 = 'RAD3'
    file4 = 'RAD4'
    file5 = 'RAD5'
    stats = 'UNKNOWN'

! Initiate FILEHAND
    call initda
! Open FILEHAND files:
    real_buffer_type = .true.
    call open (jbuff1, file1, stats, real_buffer_type)
    call open (jbuff4, file4, stats, real_buffer_type)

! files for integer storage:
    lbuff2 = lr1 * lr1 * (2 * lr1 - 1)
    lbuff5 = lr1 * lr1 * ((lr1 + lr2) / 2)
    
    inquire (iolength = int_size) jbuff2   ! integer size in io-units

    open (unit=jbuff2, file=file2, access='DIRECT', iostat=ios, &
         status=stats, form='UNFORMATTED', recl=int_size*lbuff2)
    if (ios /= 0) then
       write (fo,'(a,a,a,i4)') 'Error opening file ', file2, &
            ' iostat = ', ios
       stop
    end if
    open (unit=jbuff5, file=file5, access='DIRECT', iostat=ios, &
         status=stats, form='UNFORMATTED', recl=int_size*lbuff5)
    if (ios /= 0) then
       write (fo,'(a,a,a,i4)') 'Error opening file ', file5, &
            ' iostat = ', ios
       stop
    end if

    open (itape3, file=file3, access='SEQUENTIAL', iostat=ios,   &
         status=stats, form='UNFORMATTED')
    if (ios /= 0) then
       write (fo,'(a,a,a,i4)') 'Error opening file ', file3, &
            ' iostat = ', ios
       stop
    end if
  end subroutine open_files

  subroutine open_tmp_files
! open temporary random access files
    use rm_data, only: nrang2
    use radial_grid, only: npts
    integer                  :: real_size
    integer                  :: ios
    real(wp)                 :: x = 1.0_wp

    inquire (iolength = real_size) x  ! real size in io units
    
    open (unit=u1, file='radial_fns', access='direct',         &
         status='unknown', form='unformatted', iostat=ios,     &
         recl=real_size*nrang2*npts, action='readwrite')
    if (ios /= 0) then
       write (fo,'(a,i4,a,i4)') 'open_tmp_files: unit = ', u1,  &
            ' file = radial_fns, iostat = ', ios
       stop
    end if

    open (unit=u2, file='radial_dv1', access='direct',        &
         status='unknown', form='unformatted', iostat=ios,    &
         recl=real_size*nrang2*npts, action='readwrite')
    if (ios /= 0) then
       write (fo,'(a,i4,a,i4)') 'open_tmp_files: unit = ', u2,  &
            ' file = radial_dv1, iostat = ', ios
       stop
    end if

    open (unit=u3, file='radial_dv2', access='direct',       &
         status='unknown', form='unformatted', iostat=ios,   &
         recl=real_size*nrang2*npts, action='readwrite')
    if (ios /= 0) then
       write (fo,'(a,i4,a,i4)') 'open_tmp_files: unit = ', u3,   &
            ' file = radial_dv2, iostat = ', ios
       stop
    end if
  end subroutine open_tmp_files

  subroutine delete_tmp_files
! delete temporary files:
    integer  :: ios
    
    close (unit=u1, status='delete', iostat=ios)
    if (ios /= 0) write (fo,'(a,i4,a,i4)') 'Error closing radial_fns,&
         & unit = ', u1, ' iostat = ', ios

    close (unit=u2, status='delete', iostat=ios)
    if (ios /= 0) write (fo,'(a,i4,a,i4)') 'Error closing radial_dv1,&
         & unit = ', u2, ' iostat = ', ios

    close (unit=u3, status='delete', iostat=ios)
    if (ios /= 0) write (fo,'(a,i4,a,i4)') 'Error closing radial_dv2,&
         & unit = ', u3, ' iostat = ', ios
  end subroutine delete_tmp_files

end module file_info
