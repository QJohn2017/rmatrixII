module io_units
! Time-stamp: "01/12/05 10:51:03 cjn"
  implicit none
  private
  public fi, fo, dsk, dskp1, dskp2
  public set_dsk, set_dskp1, set_dskp2
! ang values:
  public jbufiv, jbuffv, jbufie, jbuffe
  public jbufiv1, jbuffv1, jbufiv2, jbuffv2
! ham values:
  public jbuff1, jbuff2, jbuff4, jbuff5, itape1, itape3, jbufd
  public jbufim, jbuffm, jbufev

  integer, parameter      :: fi = 5    ! standard input
  integer, parameter      :: fo = 6    ! standard output
  integer, save           :: dsk = 0   ! scratch disk unit
  integer, save           :: dskp1 = 0   ! scratch disk unit
  integer, save           :: dskp2 = 0   ! scratch disk unit

  integer, save           :: jbufiv = 20
  integer, save           :: jbuffv = 21
  integer, save           :: jbuff1 = 24
  integer, save           :: jbuff2 = 25
  integer, save           :: itape1 = 26
  integer, save           :: jbuff4 = 27
  integer, save           :: jbufiv1 = 24    ! -> jbuff1
  integer, save           :: jbuffv1 = 25    ! -> jbuff2
  integer, save           :: jbufiv2 = 26    ! -> itape1
  integer, save           :: jbuffv2 = 27    ! -> jbuff4
  integer, save           :: jbuff5 = 28
  integer, save           :: jbufd = 29
  integer, save           :: itape3 = 10

  integer, save           :: jbufie = 22
  integer, save           :: jbuffe = 23

  integer, save           :: jbufim = 45
  integer, save           :: jbuffm = 46

  integer, save           :: jbufev = 47
contains
  subroutine set_dsk (dsk_unit)
    integer, intent(in)   :: dsk_unit
    dsk = dsk_unit
    return
  end subroutine set_dsk

  subroutine set_dskp1 (dsk_unit)
    integer, intent(in)   :: dsk_unit
    dskp1 = dsk_unit
    return
  end subroutine set_dskp1

  subroutine set_dskp2 (dsk_unit)
    integer, intent(in)   :: dsk_unit
    dskp2 = dsk_unit
    return
  end subroutine set_dskp2

end module io_units
