module buttle_correct
! calculate Buttle correction coefficients
! Time-stamp: "2003-06-05 17:37:23 cjn"
  use precisn, only: wp
  use io_units, only: fo
  implicit none

  real(wp), save                      :: but_ek2min ! energy range
  real(wp), save                      :: but_ek2max ! energy range
  real(wp), allocatable, target, save :: coeff(:,:) ! Buttle coeffs

  private
  public gen_buttle, get_buttle, destroy_buttle

contains
  subroutine gen_buttle
    use rm_data, only: nz, nelc, ra, ek2max
    use rm_xdata, only: nvary, maxnhf, maxnlg, lrang1, lrang2, minim, &
         nrangx
    use bassplnew_orb, only: ends
    use radial_grid, only: nix, irx
    use bound_basis, only: p
    use basis_functions, only: basfun, orb, eigens, bvalue, eigen
    use error_prt, only: alloc_error
    integer, parameter       :: imax = 6
    real(wp), parameter      :: x = 0.2_wp
    logical                  :: ok
    real(wp)                 :: el(imax), rt(imax), rcn(imax), ek(imax)
    real(wp)                 :: res(imax)
    real(wp)                 :: a(6)
    real(wp)                 :: en(nrangx), amp(nrangx)
    integer                  :: ipts, i9, nek, l, i, l1, nbt, itest, n
    integer                  :: npoles, n1, nln, na, nn, indep, node, ii
    integer                  :: nc, nbut, status
    real(wp)                 :: zek, d, e, w, ea, eb, ep, sm, r, deltb
    real(wp)                 :: alpha, beta, dd, ekint, ek2min
    real(wp), parameter      :: tiny = 1.0e-6_wp

! loop over channel a.m. to calculate buttle poles up to lrang2
    ipts = 2 * irx(nix)
    i9 = irx(nix) + 1
    zek = REAL(nz-nelc, wp)
    nek = 1
    if (nelc >= 2)  nek = 2
    if (nelc >= 10) nek = 3
    if (nelc >= 28) nek = 4
    write (fo,'(a)') 'Subroutine Gen_buttle'
    allocate (coeff(3,lrang2) , stat=status)
    if (status /= 0) call alloc_error (status, 'gen_buttle', 'a')
    coeff(1:3,1:lrang2)=0._wp
    if (1==1) return

! set coeff(3,1) to indicate to the asymptotic program mjs fitting
    if (minim > 1) coeff(3,1) = -20000.0_wp

    l_loop: do l = minim, lrang2
       npoles = nvary(l)
       write (fo,'(a,i2,a,i4)') 'l = ', l, ' npoles = ', npoles
       ek2min = -(zek / MAX(nek,l))**2
       d = 0.5_wp * (3.0_wp * eigens(1,l) - eigens(2,l))
       if (ek2min < d) ek2min = d
       write (fo,'(a,i3)') 'Buttle correction coefficients for &
            &l = ',l-1
       write (fo,'(a,f11.6,a,f11.6)') 'ek2min = ', ek2min, &
            ' ek2max = ', ek2max
       but_ek2max = ek2max
       but_ek2min = ek2min

! set up energy points in ek()
       ekint = (ek2max - ek2min) / REAL(imax-1,wp)
       e = ek2min - ekint
       do i = 1, imax
          e = e + ekint
          ek(i)=  e
       end do
       l1 = l - 1
       nbt = 0
       if (l <= lrang1) nbt = maxnlg(l) - l + 1
       if (nbt > 0) read (60,rec=l) p
       itest = 0
       if (l <= lrang1) then
          itest = maxnhf(l) - maxnlg(l)
       end if
       if (itest > 0) then
! if maxnlg(l).lt.maxnhf(l) evaluate buttle poles
          itest = 0
          ok = .true.
          pole_loop: do n = 1, npoles
             nc = n - itest
             if (nc <= 0) then
                w = en(n)
             else
                w = eigens(nc,l)
             end if
             call basfun (nbt, l1, node, ra, w)
             en(n) = eigen
             amp(n) = orb(i9)
             n1 = n - 1
             if (n1 > 0) then
                do nln = 1, n1
                   if (ABS(eigen-en(nln)) < tiny) ok = .false.
                end do
             end if
          end do pole_loop
          if (.NOT.ok) then
             write (fo,'(a)') 'two Buttle poles are equal - &
                  &continue without correction'
             return
          end if
       else
! if maxnhf(l).eq.maxnlg(l) the buttle poles and amplitudes are 
! equal to the continuum eigenvalues and functions evaluated in 
! stg1 and stored in eigens and ends.
          do n = 1, npoles
             amp(n) = ends(n,l)
             en(n) = eigens(n,l)
          end do
       end if

! check channel energies do not coincide with buttle poles
! calculate coefficients for quadratic fit

! set channel energies and test against poles.
! exclude ((1-x)*e(j)+x*e(j-1)) to ((1-x)*e(j)+x*e(j+1))
! where e(j)=en(l,j) and x (=0.2) is set in parameter statement
       el(1:imax) = ek(1:imax)
       na = 1
       do i = 1, imax    ! find first pole above el(i)
          do n = na, npoles
             if (en(n) > el(i)) then
                nn = n
                exit
             end if
          end do
          na = nn
          ea = en(na)
          if (na == 1) then  ! all poles above
             ep = en(2)
             d = x * (ep - ea)
             if ((ea-el(i)) < d) el(i) = ea - d
          else         ! some poles below, find nearest
             eb = en(na-1)
             d = x * (ea - eb)
             if ((el(i)-eb) < d) el(i) = eb + d
             if ((ea-el(i)) < d) el(i) = ea - d
          end if
       end do
       write (fo,'(a,f12.6,a,f12.6)') 'corrections calculated in&
            & the energy range ', el(1), ' to ', el(imax)

! check number of independent values
       indep = 1
       do n = 2, imax
          if (el(n-1) /= el(n)) indep = indep + 1
       end do
       if (indep < 3) then
          write (fo,'(a,i3)') 'Gen_buttle: energy range too small to&
               & avoid buttle pole, l = ', l1
          stop
       end if

! array el() now contains acceptable energies for a.m. l
! evaluate buttle correction at these energies.
       do ii = 1, imax
          if (ABS(el(ii)) < tiny) el(ii) = el(ii) + 2.0_wp * tiny
          call basfun (nbt, l1, node, ra, el(ii))
          rt(ii) = bvalue
          if (ABS(rt(ii)) <= tiny) then
             write (fo,'(a,i3,a,f14.7,a)') 'gen_buttle: cannot evaluate&
                  & Buttle correction for l = ', l1, ' and energy ', &
                  el(ii), ' Ryd.'
             stop
          end if
          sm = 0.0_wp
          do n = 1, npoles
             if (amp(n) /= 0.0_wp) sm = sm + amp(n) * amp(n) / &
                  (en(n) - el(ii))
          end do
          sm = sm / ra
          r = 1.0_wp / rt(ii)
          rcn(ii) = r - sm
          write (fo,'(a,i5,1p,4e14.5)') 'ii, el(ii), r, sm, &
               &rcn(ii) = ',ii, el(ii), r, sm, rcn(ii)
       end do

! Seaton square-well model fit
       call butfit (imax, el, rcn, ra, eigens(npoles,l), alpha, &
            beta, nbut, deltb)
       coeff(1,l) = alpha
       coeff(2,l) = beta
       coeff(3,l) = -10000 * nbut
       write (fo,'(a,f12.6,a,f12.6,a,i4)') 'alpha = ', alpha, &
            ' beta = ', beta, ' nbut = ', nbut
! accuracy of fit
       write (fo,'(a,e15.6)') 'largest error in fit = ', deltb
    end do l_loop
  end subroutine gen_buttle

   subroutine butfit (imax, e, f, ra, emax, alpha, beta, nbut, delta)
! fitting of buttle corrections -- m j seaton, j.phys.b20(1987) l69-72.
     integer, intent(in)    :: imax ! # points for which correction reqd
     real(wp), intent(inout):: e(imax) ! energy points
     real(wp), intent(in)   :: f(imax) ! Buttle correction
     real(wp), intent(in)   :: ra      ! boundary radius
     real(wp), intent(in)   :: emax    ! eigens(nrang2,l)
     real(wp), intent(out)  :: alpha   ! fit parameters
     real(wp), intent(out)  :: beta    ! fit parameters
     integer, intent(out)   :: nbut    ! fit parameter
     real(wp), intent(out)  :: delta   ! accuracy achieved
     logical                :: pole
     integer                :: kk, i, n
     real(wp)               :: d, pi, x11, x12, x22, y1, y2, delta0
     real(wp)               :: u, g, a, b, c, fk, d1, d2, bb, t, tk, dd
     real(wp)               :: det, df

     d = ra * ra
     e = d * e
     pi = 4.0_wp * ATAN(1.0_wp)
     nbut = 0.5_wp + ra * SQRT(emax) / pi
     delta = 1.0e30_wp
     alpha = 1.0_wp
     beta = 0.0_wp

     itn_loop: do kk = 1, 5   ! iterations for fit
        x11 = 0.0_wp
        x12 = 0.0_wp
        x22 = 0.0_wp
        y1 = 0.0_wp
        y2 = 0.0_wp
        delta0 = delta
        delta = 0.0_wp
        pt_loop: do i = 1, imax    ! sum over points
           u = beta + e(i)
! calculate functions b(u) and c(u)
           b = 0.0_wp
           c = 0.0_wp
           if (u > 0.04_wp) then
              fk = SQRT(u)
              pole = .false.
              g = - 0.5 * pi
              do n = 0, nbut
                 g = g + pi
                 if (ABS(fk-g) > 0.3_wp) then
                    a = 1.0_wp / (u - g * g)
                    b = b + a
                    c = c - a * a
                 else
                    pole = .true.
                    d1 = fk - g
                 end if
              end do
              if (pole) then
                 d2 = d1 * d1
                 d = d1 * (1.0_wp + 0.066666667_wp * d2 * (1.0_wp + &
                      0.0952381_wp * d2)) / 3.0_wp
                 a = 1.0_wp /(2.0_wp * fk - d1)
                 bb = (d + a) / fk
                 d = (1.0_wp + d2 * (0.2_wp + 0.031746032_wp * d2)) &
                      / 3.0_wp
                 c = 2.0_wp * c + 0.5_wp * (d - a * a - bb) / u
                 b = 2.0_wp * b + bb
              else
                 t = TAN(fk)
                 tk = t / fk
                 b = 2.0_wp * b + tk
                 c = 2.0_wp * c + 0.5_wp * (1.0_wp + t * t - tk) / u
              end if
           else        ! sum for u <= 0.04
              g = - 0.5_wp * pi
              do n = 0, nbut
                 g = g + pi
                 a = 1.0_wp / (u - g * g)
                 b = b + a
                 c = c - a * a
              end do
! case of u.lt..04 and u.gt.-.04
              if (u > -0.04_wp) then
                 b = (0.4_wp * u + 1.0_wp) * u / 3.0_wp + 2.0_wp * &
                      b + 1.0_wp
                 c = ((0.48571429_wp * u + 0.8_wp) * u + 1.0_wp) / &
                      3.0_wp + 2.0_wp * c
! case of u.lt.-.04
              else
                 fk = SQRT(-u)
                 t = TANH(fk)
                 tk = t / fk
                 b = 2.0_wp * b + tk
                 c = 2.0_wp * c + 0.5_wp * (1.0_wp - t * t - tk) / u
              end if
           end if

! increment matrices x and y
           df = f(i) - alpha * b
           dd = ABS(df)
           if (delta < dd) delta = dd
           x11 = x11 + b * b
           x12 = x12 + b * c
           x22 = x22 + c * c
           y1 = y1 + df * b
           y2 = y2 + df * c
        end do pt_loop

! solve equations and increment alpha and beta
        det = 1.0_wp / (x11 * x22 - x12 * x12)
        beta = beta + det * (-x12 * y1 + x11 * y2) / alpha
        alpha = (x22 * y1 - x12 * y2) * det + alpha

! check convergence
        if (delta < 1.e-4_wp) return
     end do itn_loop

     nbut = -nbut
     if (delta > delta0) nbut = 0     ! no convergence
   end subroutine butfit

  subroutine get_buttle (emin, emax, tcoeff)
    real(wp), intent(out)       :: emin
    real(wp), intent(out)       :: emax
    real(wp), pointer           :: tcoeff(:,:)
 
    emin = but_ek2min
    emax = but_ek2max
    tcoeff => coeff
  end subroutine get_buttle

  subroutine destroy_buttle
    use error_prt, only: alloc_error
    integer                     :: status
    deallocate (coeff, stat=status)
    if (status /= 0) call alloc_error (status, 'destroy_buttle', 'd')
  end subroutine destroy_buttle

end module buttle_correct
