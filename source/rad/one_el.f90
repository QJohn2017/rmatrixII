module one_el
! one-electron matrix elements
! Time-stamp: "2003-03-26 17:30:34 cjn"
  use precisn, only: wp
  use bassplnew_orb, only: dorkin
  implicit none

  real(wp), allocatable, save  :: ykk(:)
  real(wp), allocatable, save  :: rk(:)
  real(wp), allocatable, save  :: rk1(:)
  integer, save                :: jn2, jl2
  integer, save                :: jn4, jl4
  integer, save                :: jkm
  real(wp), allocatable, save  :: sm1(:), sm2(:)
  real(wp), allocatable, save  :: rint(:), ar(:)

  integer, save                :: kmax = 20 ! de Vogelaere threshold

  private
  public ykstor, xykstor, oneele, rs, jkm

contains  
  subroutine oneele (n11, l1, n12, l2, albval)
! one electron matrix elements
    use bound_basis, only: uj, duj, ipos
    use radial_grid, only: wt, npts
    use rm_xdata, only: maxnhf, lrang1
    use bassplnew_orb, only: ends,dends
    integer, intent(in)     :: n11, l1
    integer, intent(in)     :: n12, l2
    real(wp), intent(out)   :: albval
    integer                 :: i1, i2, n1, n2, maxhf

    if (l1 /= l2) then
       albval = 0.0_wp
    else
       maxhf = l1 - 1
       if (l1 <= lrang1) maxhf = maxnhf(l1)
       n1 = n11
       n2 = n12
       if (n2 > maxhf .and. n1 <= maxhf) then
          n1 = n12
          n2 = n11
       end if
       i1 = ipos(n1,l1)
       i2 = ipos(n2,l2)
!     print *,'I-POSITION ',n1,n2,i1,i2,maxhf
       albval = 0.5_wp * SUM(uj(2:npts,i1) * duj(2:npts,i2) * &
            wt(2:npts))
!      print *,albval,npts
       if ((n1>maxhf).and. (n2 > maxhf)) then
         albval=albval+ends(n1-maxhf,l1)*dends(n2-maxhf,l2)*0.5_wp
!        print *,'ENDS',n1,maxhf,ends(n1-maxhf,l1)
!        print *,'D-ENDS',n2,maxhf,dends(n2-maxhf,l2)
       end if
!     print *,albval,npts
    end if
  end subroutine oneele

  subroutine rs (n2, l2, n1, l1, n4, l4, n3, l3, k, rkval)
! Evaluate function Y(P2,P4,K/R) and Slater integral R(P1,P2,P3,P4,K)
! where P1,P2,P3,P4 are numerical orbital functions on a prescribed mesh
    use bound_basis, only: uj, ipos
    use radial_grid, only: xr, step, wt, npts
    use slater_yk, only: ykf, yk
    integer, intent(in)   :: n1, l1     ! orbital quantum numbers
    integer, intent(in)   :: n2, l2
    integer, intent(in)   :: n3, l3
    integer, intent(in)   :: n4, l4
    integer, intent(in)   :: k
    real(wp), intent(out) :: rkval      ! Slater integral
    real(wp), parameter   :: twelth = 1.0_wp / 12.0_wp
    real(wp), parameter   :: third = 1.0_wp / 3.0_wp
    integer               :: j1, j2, j3, j4, k1, dim, j
    real(wp)              :: ara, rin

! p1,p2,p3,p4 stored in uj(j,i), i=1,4. j is the integration point
! j=1 corresponds to r=0.0

    j1 = ipos(n1,l1)  ! p1 position in uj
    j2 = ipos(n2,l2)  ! p2 position
    j3 = ipos(n3,l3)  ! p3 position
    j4 = ipos(n4,l4)  ! p4 position
    k1 = k + 1

! Do not recalculate the YK integral if it already exists
    if (k == jkm) then
       if (n2 == jn2 .and. l2 == jl2 .and. n4 == jn4 .and. &
            l4 == jl4) then
          rkval = SUM(uj(2:npts,j1) * uj(2:npts,j3) * ykk)
          return
       end if
    end if

! if k exceeds kmax then use de Vogelaere calculation:
    if (k > kmax) then     ! large lambda case:
       call ykf (n2, l2, n4, l4, k, xr(npts))
       yk(2:npts) = wt(2:npts) * yk(2:npts) / xr(2:npts)
       rkval = SUM(uj(2:npts,j1) * uj(2:npts,j3) * yk(2:npts))
       return
    end if

    jn2 = n2
    jl2 = l2
    jn4 = n4
    jl4 = l4
    if (k /= jkm) then
       rk = xr(2:npts)**k
       rk1 = 1.0_wp / (rk * xr(2:npts))
       jkm = k
    end if

! initialization
    sm1(2:npts) = uj(2:npts,j2) * uj(2:npts,j4)
    rint(2:npts) = sm1(2:npts) * rk
    ar(2:npts) = sm1(2:npts) * rk1

! simpsons rule integration from 0 to ra
    ara = SUM(wt(2:npts) * ar(2:npts))

! Simpsons rule integrations from 0 to R
    rint(1) = 0.0_wp
    ar(1) = 0.0_wp
    rin = 0.0_wp
    do j = 3, npts, 2
       sm1(j) = step(j) * third * (rint(j-2) + 4.0_wp*rint(j-1) + rint(j))
       sm2(j) = step(j) * third * (ar(j-2) + 4.0_wp*ar(j-1) + ar(j))
    end do
    do j = 3, npts, 2
       sm1(j-1) = step(j-1) * twelth * (5.0_wp*rint(j-2) + &
            8.0_wp*rint(j-1) - rint(j))
       sm2(j-1) = step(j-1) * twelth * (5.0_wp*ar(j-2) + &
            8.0_wp*ar(j-1) - ar(j))
    end do
    do j = 3, npts, 2
       rint(j-1) = rin + sm1(j-1)
       rin = rin + sm1(j)
       rint(j) = rin
       ar(j-1) = ara - sm2(j-1)
       ara = ara - sm2(j)
       ar(j) = ara
    end do

    ykk = (rint(2:npts) * rk1 + ar(2:npts) * rk) * wt(2:npts)
!   print *,'CONT 2',k
!    print *,ar(2:npts)
!    print *,rk
    rkval = SUM(uj(2:npts,j1) * uj(2:npts,j3) * ykk) ! Slater integral
  end subroutine rs

  subroutine ykstor
! generate yk storage
    use radial_grid, only: nix, irx
    use error_prt, only: alloc_error
    use slater_yk, only: alloc_y
    integer           :: npts, npt1, status
    
    npts = irx(nix) + 1
    npt1 = npts - 1
    allocate (ykk(npt1), rk(npt1), rk1(npt1), sm1(npts), sm2(npts), &
         rint(npts), ar(npts), stat = status)
    if (status /= 0) call alloc_error (status, 'ykstor', 'a')

    call alloc_y (npts)    ! initialize de Vogelaere YK
  end subroutine ykstor

  subroutine xykstor
! release yk storage
    use error_prt, only: alloc_error
    integer        :: status

    deallocate (ykk, rk, rk1, sm1, sm2, rint, ar, stat=status)
    if (status /= 0) call alloc_error (status, 'ykstor', 'd')
  end subroutine xykstor

end module one_el
