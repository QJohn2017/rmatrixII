!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! THIS MODULE CONTAINS THE DIAGONALIZATION STUFF AND OTHER THINGS
!
      module math
        use precisn
!        use io_units, only: fo
        implicit none
	real(wp), parameter :: dr5=0.5_wp
	real(wp), parameter :: dr0=0.0_wp
	real(wp), parameter :: dr1=1.0_wp
	real(wp), parameter :: dr2=2.0_wp

        private
	public :: tredi,tqli,interv,bsplvb,lubksd,sort,newmat,gauleg

      CONTAINS
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! HERE COME THE FORTRAN-77 ROUTINES: GAUSS-LEGENDRE POINTS AND WEIGHTS
!
      SUBROUTINE GAULEG(X1,X2,X,W,N)
      IMPLICIT none
      REAL(kind=wp), intent(in) :: X1,X2
      integer, intent(in) :: N
      real(kind=wp), intent(out), dimension(n) :: X,W
      real(kind=wp) :: p1,p2,p3,xm,xl,z,pp,z1,pi
      integer :: M,I,J,K
      integer, parameter :: max_iter=30
      real(kind=wp), parameter :: eps = 1.D-15
      logical :: conv
      pi=dr2*asin(dr1)
      print *,'pi : ',pi
      M=(N+1)/2
      XM=dr5*(X2+X1)
      XL=dr5*(X2-X1)
      DO I=1,M
        Z=COS(pi*(I-.25_wp)/(N+dr5))
        CONV=.false.
         DO K=1,MAX_ITER
          IF (.not.conv) then
            P1=dr1
            P2=dr0
            DO J=1,N
              P3=P2
              P2=P1
              P1=((dr2*J-dr1)*Z*P2-(J-dr1)*P3)/J
            ENDDO
            PP=N*(Z*P1-P2)/(Z*Z-dr1)
            Z1=Z
            Z=Z1-P1/PP
            IF (ABS(Z-Z1).LE.EPS) THEN
             CONV=.true.
            ENDIF
          ENDIF
         ENDDO
        if (.not.conv) print *,'Convergence problem'
        X(I)=XM-XL*Z
        X(N+1-I)=XM+XL*Z
        W(I)=dr2*XL/((dr1-Z*Z)*PP*PP)
        W(N+1-I)=W(I)
      ENDDO
      END subroutine gauleg
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
! THE VALUE OF SPLINES AT X
!
      SUBROUTINE BSPLVB(T,NT,JHIGH,INDEX,X,LEFT,BIATX)
      IMPLICIT NONE
      integer, intent(in) :: nt,jhigh,index,left
      real(kind=wp), intent(in) :: x
      real(kind=wp), intent(in), dimension(nt) :: t
      real(kind=wp), intent(out), dimension(jhigh) :: biatx
      INTEGER :: I,JP1,JI
      integer :: j = 1
      integer, parameter :: jmax=30
      real(kind=wp), dimension(jmax) :: deltal, deltar
      REAL(kind=wp) :: SAVED,TERM
!
      if (index.ne.2) then
       J=1
       BIATX(1)=dr1
      endif
      IF((index.eq.2).or.(J.LT.JHIGH)) THEN
!
      DO JI=J,JHIGH-1
       JP1=JI+1
       DELTAR(JI)=T(LEFT+JI)-X
       DELTAL(JI)=X-T(LEFT+1-JI)
       SAVED=dr0
       DO I=1,JI
        TERM=BIATX(I)/(DELTAR(I)+DELTAL(JP1-I))
        BIATX(I)=SAVED+DELTAR(I)*TERM
        SAVED=DELTAL(JP1-I)*TERM
       ENDDO
       BIATX(JP1)=SAVED
      ENDDO
      ENDIF 
!
   99 RETURN
      END subroutine bsplvb
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! FIND THE INTERVAL WITHIN T CONTAINING X
!
      subroutine interv(xt,lxt,x,left,mflag)
      implicit none
      integer, intent(in) :: lxt
      integer, intent(out) :: left,mflag
      real(kind=wp), intent(in) :: x
      real(kind=wp), intent(in), dimension(lxt) :: xt
      integer :: ihi,ilo,istep,middle,i,n
      ilo=1
      ihi=lxt
      left=0
      mflag=0
      if ((x.lt.xt(ilo)).or.(lxt.le.1)) then
       left = 1
       mflag = -1
      else
       if (x.gt.xt(ihi)) then
        left = lxt
        mflag = 1
       else
        N=int(log(real(lxt-1))/log(dr2)+dr2)
        do i = 1, n
         middle=(ihi+ilo)/2
         if (middle.eq.ilo) left = ilo
         if (x.lt.xt(middle)) ihi = middle
         if (x.ge.xt(middle)) ilo = middle
        enddo
       endif
      endif
      if (left.eq.0) then
        print *,'Possible problem in interval'
        stop
      endif
      end subroutine interv
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! REduce a symmetric matrix to tridiagonal form
!
      subroutine tredi(a,n,np,d,e)
      implicit none
      integer, intent(in) ::  n,np
      real(kind=wp), intent(inout), dimension(np,np) :: a
      real(kind=wp), intent(out), dimension(np) :: d,e
      integer :: i,j,k,l
      real(kind=wp) :: f,g,h,hh,scale
      do i=n,2,-1
       l=i-1
       h=dr0
       scale=dr0
       if (l.gt.1) then
        do k=1,l
          scale=scale+abs(a(i,k))
        enddo
        if (scale.eq.dr0) then
          e(i)=a(i,l)
        else
          a(i,1:l)=a(i,1:l)/scale
          h=h+dot_product(a(i,1:l),a(i,1:l))
          f=a(i,l)
          g=-sign(sqrt(h),f)
          e(i)=scale*g
          h=h-f*g
          a(i,l)=f-g
          f=dr0
          do j=1,l
            a(j,i)=a(i,j)/h
            g=dot_product(a(j,1:j),a(i,1:j))
            g=g+dot_product(a(j+1:l,j),a(i,j+1:l))
            e(j)=g/h
            f=f+e(j)*a(i,j)
          enddo
          hh=f/(dr2*h)
          do j=1,l
            f=a(i,j)
            g=e(j)-hh*f
            e(j)=g
            a(j,1:j)=a(j,1:j)-f*e(1:j)-g*a(i,1:j)
          enddo
        endif
       else
         e(i)=a(i,l)
       endif
       d(i)=h
      enddo
!
      d(1)=dr0
      e(1)=dr0
      do i=1,n
       l=i-1
       if (d(i).ne.dr0) then
         do j=1,l
           g=dot_product(a(i,1:l),a(1:l,j))
           a(1:l,j)=a(1:l,j)-g*a(1:l,i)
         enddo
       endif
       d(i)=a(i,i)
       a(i,i)=dr1
       a(1:l,i)=dr0
       a(i,1:l)=dr0
      enddo
      return
      end subroutine tredi
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Solve a tridiagonal system
!
      subroutine tqli(d,e,n,np,z)
      implicit none
      integer, intent(in) :: n,np
      real(kind=wp), intent(inout), dimension(np) :: d,e
      real(kind=wp), intent(inout), dimension(np,np) :: z
      integer :: i,iter,k,l,m
      real(kind=wp) ::  b,c,dd,f,g,p,r,s,pythag
      do i=2,n
       e(i-1)=e(i)
      enddo
      e(n)=dr0
      do l=1,n
        iter=0
 1      do m=l,n-1
          dd=abs(d(m))+abs(d(m+1))
          if (abs(e(m))+dd.eq.dd) goto 2
        enddo
        m = n
 2      if (m.ne.l) then
          if (iter.eq.30) then
              print *,' NOT CONVERGING IN 30 ITERATIONS'
              stop
          endif
          iter = iter + 1
          g=(d(l+1)-d(l))/(dr2*e(l))
          r=sqrt(g*g+dr1)
          g=d(m)-d(l)+e(l)/(g+sign(r,g))
          s=dr1
          c=dr1
          p=dr0
          do i = m-1,l,-1
            f=s*e(i)
            b=c*e(i)
            r=sqrt(f*f+g*g)
            e(i+1)=r
            if (r.eq.dr0) then
              d(i+1)=d(i+1)-p
              e(m)=dr0
              goto 1
            endif
            s=f/r
            c=g/r
            g=d(i+1)-p
            r=(d(i)-g)*s+dr2*c*b
            p=s*r
            d(i+1)=g+p
            g=c*r-b
            do k = 1,n
              f=z(k,i+1)
              z(k,i+1)=s*z(k,i)+c*f
              z(k,i)=c*z(k,i)-s*f
            enddo
          enddo
          d(l)=d(l)-p
          e(l)=g
          e(m)=dr0
          goto 1
        endif
      enddo
      return
      end subroutine tqli
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
! sort matrices
!
      SUBROUTINE SORT(A,N,B,C)
!      use constants
      IMPLICIT none
      integer, intent(in) :: N
      real(kind=wp), intent(inout), dimension(N) :: a,c
      real(kind=wp), intent(inout), dimension(n,n) :: b
      integer :: i,j
      real(kind=wp) :: temp
      DO I=1,N-1
       DO J=1,N-I
        IF (A(J).GT.A(J+1)) THEN
         TEMP=A(J)
         A(J)=A(J+1)
         A(J+1)=TEMP
         C(:)=B(:,J)
         B(:,J)=B(:,J+1)
         B(:,J+1)=C(:)
        ENDIF
       ENDDO
      ENDDO
      END subroutine sort
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
! backsubstitution algorithm for lower triangular matrix
! 
      SUBROUTINE LUBKSC(A,N,NP,B)
      IMPLICIT none
      integer, intent(in) :: n,np
      real(kind=wp), intent(in), dimension(np,np) :: a
      real(kind=wp), intent(inout), dimension(np) :: b
      integer :: i,j
      real(kind=wp) :: sum
      DO I=1,N
        SUM=B(I)
	IF (I > 1) THEN
         SUM=SUM-DOT_Product(A(i,1:I-1),B(1:I-1))
	ENDIF
        B(I)=SUM/a(i,i)
      ENDDO
      END subroutine lubksc
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
! backsubstitution algorithm for upper triangular matrix
!
      SUBROUTINE LUBKSD(A,N,NP,B)
      IMPLICIT none
      integer, intent(in) :: n,np
      real(kind=wp), intent(in), dimension(np,np) :: a
      real(kind=wp), intent(inout), dimension(np) :: b
      integer :: i,j
      real(kind=wp) :: sumq
      print *,'enter lubksd',n,np
      DO I=N,1,-1
        SUMq=B(I)
	J=i+1
!	print *,i,j,n
!	print *,a(N,i)
!	print *,b(J:N)
        if (i < n) then
        sumq=sumq-dot_product(a(J:N,i),b(J:N))
	endif
        B(I)=SUMq/a(i,i)
      ENDDO
      print *,'leave lubksd'
      END subroutine lubksd
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
! reduce general banded symmetric eigensystem to a symmetric matrix 
!
      SUBROUTINE NEWMAT(A,N,NP,C,F,AT)
      IMPLICIT none
      integer, intent(in) :: n,np
      real(wp), intent(inout), dimension(np,np) :: a,c,f,at
      integer :: i
!      write(fo,'(a)') 'Enter choles'
!      write(fo,'(f16.9)') f(1,1)
      CALL CHOLES(C,N,NP,F)
!      write(fo,'(a)') 'Return from choles'
      at=transpose(a)
      DO 20 I=1,N
   20 CALL LUBKSC(F,N,NP,AT(1,I))
!      write(fo,'(a)') 'Return from lubksc 1'
      a=transpose(at)
      DO 40 I=1,N
   40 CALL LUBKSC(F,N,NP,A(1,I))
!      write(fo,'(a)') 'Return from lubksc 2'
      END subroutine newmat
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
! Cholesky decompose a positive definite matrix
!
      SUBROUTINE CHOLES(A,N,NP,B)
      IMPLICIT none
      integer, intent(in) :: n,np
      real(kind=wp), intent(inout), dimension(np,np) :: a,b
      real(kind=wp) :: sum
      integer :: i,j,k
!      write(fo,'(a)') 'Choles entered'
!      write (fo,'(i3,i4)') n,np
    
!      do i=1,N
!       do j=1,N
!	write (fo,'(i3,i4)') j,i
!        b(j,i)=dr0
!       end do
!      end do 
!      write(fo,'(a)') 'B set to zero'
      B(1,1)=SQRT(A(1,1))
      B(2:N,1)=A(2:N,1)/B(1,1)
      DO I=2,N-1
!       write(fo,'(i3)') i 
       SUM=DOT_PRODUCT(B(I,1:I-1),B(I,1:I-1))
       B(I,I)=SQRT(A(I,I)-SUM)
       DO J=I+1,N
        SUM=dot_product(b(j,1:I-1),b(i,1:i-1))
        B(J,I)=(A(J,I)-SUM)/B(I,I)
       ENDDO
      ENDDO
      SUM = dot_product(b(n,1:n-1),b(n,1:n-1))
      B(N,N)=SQRT(A(N,N)-SUM)
      RETURN
      END subroutine choles

      END module math
