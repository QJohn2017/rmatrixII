module core_energy
! computes the core energy
! Time-stamp: "2005-03-14 10:46:21 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use debug, only: bug7
  implicit none

  private
  public ecore

contains

  subroutine ecore (encore)
! calculates the energy encore of the core of filled shells
    use rm_xdata, only: maxlc, maxnc
    use one_el, only: oneele, jkm, ykstor, xykstor
    use bassplnew_orb, only : dorkin
    use angular_momentum, only: rme
    real(wp), intent(out)         :: encore
    real(wp)                      :: rint, rk0(1), rsum, rklam(1), rmeval, r1
    integer                       :: lr, lr2, nrup, nrho, nrk0, nr
    integer                       :: lam, nslo, nsup, lamlo, lamup
    integer                       :: ls, ns

    jkm = 999
    encore = 0.0_wp
    rint = 0.0_wp
    write (fo,'(/,a)') 'Subroutine Ecore'
    call ykstor

    write (*,*) 'maxnc = ', maxnc(1:maxlc) 
    lr_L: do lr = 1, maxlc ! L-values of core orbitals
       lr2 = 2 * lr
       nrup = maxnc(lr)
       nrho = 2 * (lr2 - 1)
       nrk0 = (lr2 - 1) * (4*lr - 3)
       do nr = lr, nrup
          call oneele (nr, lr, nr, lr, r1)
          print *,'Got here'
          print *,'Entry data : ',nr,lr
          call dorkin (nr, lr, nr, lr, nr, lr, nr, lr, nr, 0, rk0(1))
          print *,'But forget about getting here?'
          rint = rint + nrho * r1 + nrk0 * rk0(1)
          write (*,*) 'core: nrho, r1, brk0, rk0 = ', nrho, r1, nrk0, rk0(1)
       end do
       do lam = 2, lr2-2, 2
          rsum = 0.0_wp
          do nr = lr, nrup
             call dorkin (nr, lr, nr, lr, nr, lr, nr, lr, nr, lam, rklam(1))
             rsum = rsum + rklam(1)
          end do
          rmeval = rme(lr-1,lr-1,lam)
          rint = rint - rsum * rmeval * rmeval
       end do

! loop over other bound orbitals in the core (pair contributions)
       do ls = lr, maxlc
          nsup = maxnc(ls)
          rsum = 0.0_wp
          do nr = lr, nrup
             if (lr /= ls) then
                nslo = ls
             else
                nslo = nr + 1
             end if
             do ns = nslo, nsup
                call dorkin (nr, lr, ns, ls, nr, lr, ns, ls, ns, 0, rk0(1))
                rsum = rsum + rk0(1)
             end do
          end do
          rint = rint + 2*(2*ls-1) * nrho * rsum
          lamlo = ABS(ls - lr)
          lamup = ls + lr - 2
          do lam = lamlo, lamup, 2
             rsum = 0.0_wp
             do nr = lr, nrup
                if (lr /= ls) then
                   nslo = ls
                else
                   nslo = nr + 1
                end if
                do ns = nslo, nsup
                   call dorkin (ns, ls, nr, lr, nr, lr, ns, ls, ns, lam, rklam(1))
                   rsum = rsum + rklam(1)
                end do
             end do
             rmeval = rme(lr-1, ls-1, lam)
             rint = rint - 2.0_wp * rsum * rmeval * rmeval
          end do
       end do
    end do lr_L
    encore = rint
    write (fo,'(a,f16.9,a)') 'energy of core configuration is  ',&
         encore, '   au'
  end subroutine ecore

end module core_energy



