module superstructure
! Time-stamp: "03/06/25 15:42:34 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use debug, only: bug7
  use error_prt, only: alloc_error
  implicit none

  character(len=28), save :: fmt1 = '(2i5,1x,f5.4,f7.2,2x,i3,2i4)'
  character(len=18), save :: fmt2 = '(i5,2(:i4,2e14.7))'
  character(len=34), save :: fmt3 = '(i5,2i5,i3,f4.0,i3,f12.6,i6,f12.6)'

  integer, save                 :: indata = 59 ! unit # for SS input

  real(wp), parameter           :: lne = 69.08 ! ln of 10**(-30)
  real(wp), parameter           :: tiny = 1.0e-6_wp
  real(wp), parameter           :: tinorb = 0.001_wp ! used to set ra
  integer, save                 :: order = 4

  integer, save                 :: nrt     ! # rt elements
  real(wp), allocatable, save   :: rt(:)   ! input radial points
  real(wp), allocatable, save   :: pxt(:)  ! input potentials
  real(wp), allocatable, save   :: r(:)    ! refined radial mesh
  real(wp), allocatable, save   :: px(:)   ! refined superstructure potl

  integer, save                 :: nbnd
  integer, save                 :: nbound
  
  private
  public ss, px, nbnd

contains

  subroutine ss
! reads orbital data from channel indata in superstructure format
    use radial_grid, only: nix, hint, xr, npts
    use rm_xdata, only: maxnhf, lrang1, nrang1, lrang2
    use rm_data, only: nelc, nz, ra, maxorb
    use bound_basis, only: uj, duj, ipos
    real(wp), allocatable :: b(:)
    real(wp)              :: rb, h, f, t, g
    real(wp)              :: p, d, q, e, eps, rc, z, ub, x, r2, rf
    integer               :: key, mo, iz, mxpos, mzlr2, np
    integer               :: ip, nn, i, status, k, j, l, jflg
    integer               :: iflg, ipjs, m, kk, ios, nc, n
    integer               :: no, nsp, nzt, nelct

! Superstructure part disabled by Hugo 8/11/06
    PRINT *,'At the moment I have no intention to adjust this section'
    stop
! Start of old version
    open (unit=indata, file='ss', access='sequential', status='old', &
         form='formatted', pad='yes')
! specifying padding guarantees that ma2 is set to zero on short records
    
! read and print header for s.s. data.
    write (fo,'(/,a,f9.4,a)') 'SS: Reading sstruct indata (ra = ', &
         ra, '):'
    read (indata,fmt1) key, mo, r2, rf, np, nelc, nz
! mo = # orbitals, np = # mesh records, nelc = # electrons, nz = charge
! r2 = second grid point, rf = final grid point
    write (fo,fmt1) key, mo, r2, rf, np, nelc, nz
    if (key /= -9) then
       write (fo,'(a)') 'SS: error, first card read was as above'
       stop
    end if

! read mesh points. store in array r. define rc = boundary radius
    rb = ABS(ra)
    if (ra == 0.0_wp) rb = 1.0e37_wp
    call read_mesh (np, rb)

    z = pxt(1)
    nzt = NINT(z)
    nelct = nz - NINT(pxt(np)) + 1
    if (nz /= nzt .or. nelct /= nelc) then
       write (fo,'(a)') 'SS: input does not match SS file'
       write (fo,'(a,4i6)') 'nz, nelc, nzt, nelct = ', nz, nelc, &
            nzt, nelct
       stop
    end if
    
    nc = np    ! set nc to size of input mesh
    call refine_mesh (nc, nsp, ra, rb)
    rc = r(nc)

! read and print header for each orbital.
    jflg = 0
    iflg = 0
    ipjs = 0
    call read_orbitals (nc, nsp, mo, eps, iflg, jflg, ipjs)

! reset nc to value of ipjs defined by tinorb unless ra specified
    if (ra <= 0.0_wp) then
       write (fo,'(a,2i6)') 'SS: reset nc(old) -> nc(new) = ', nc, ipjs
       nc = ipjs
    end if
    
    call step_changes (nc)
    call def_grid_pams (nc)
    ra = r(npts)    ! redefine ra
    write (fo,'(a,f14.8)') 'SS: R-matrix radius, ra = ', ra

! orthogonalize subsequent orbitals to those with same value l:
    if (iflg /= 0) then
       allocate (b(nc), stat=status)
       if (status /= 0) call alloc_error (status, 'ss', 'a')
       orb_am: do l = 1, lrang1
          m = maxnhf(l)
          if (m <= 0) cycle
          hf_orbs1: do n = 1, m
             j = 0
             nn = ipos(n,l)      ! location of first orbital
             if (nn <= 0) cycle
             hf_orbs2: do k = 1, n
                kk = ipos(k,l)   ! location of second orbital
                if (kk <= 0) cycle
                j = j + 1
                call abnorm (n, l, k, l, t)
! eventually add tail contribution to t from ra to infinity(q)
                if (n /= k) then     ! Schmidt orthogonalize
                   g = 1.0_wp / SQRT(1.0_wp - t*t)
                   h = t
                   do i = 1, npts
                      uj(i,nn) = (uj(i,nn) - uj(i,kk) * h) * g
                      duj(i,nn) = (duj(i,nn) - duj(i,kk) * h) * g
                   end do
                end if
                b(j) = t
             end do hf_orbs2
             write (fo,'(a,i2,a,(t35,6f8.4))') 'overlap integrals(n)&
                  & to l+1=', l, ':',  (b(i),i=1,j)
          end do hf_orbs1
       end do orb_am
       if (jflg /= 0) write (fo,'(a)') 'n.b.: indata must be &
            &normalized but NOT orthogonalized!'
    end if
    
    if (bug7 == 2) then
       write (fo,'(a,i5)') 'orbital', 0
       write (fo,'(1x,6 (i7,1p,e15.6))') (i,r(i),i=1,nc)
       do k = 1, nbound
          write (fo,'(a,i5)') 'orbital', k
          write (fo,'(1x,6 (i7,1p,e15.6))') (i,uj(i,k),i=1,nc)
       end do
    end if
    
    deallocate (b, r, rt, pxt, stat=status)
    if (status /= 0) call alloc_error (status, 'ss', 'd')
    nbnd = nbound
  end subroutine ss

  subroutine first_region (nc)
! adjust radial mesh to ensure there are 7 points in the first region
    integer, intent(inout) :: nc   ! # grid points
    logical                :: exists
    integer                :: i, j, ncs
    real(wp)               :: hint, x
    real(wp), parameter    :: tiny = 1.0e-10_wp

    exists = .false.
    steps: do j = 2, 4
       hint = r(j)
       x = 5.0_wp * hint   ! superstructure grid convention?
       do i = j, nc
          if (ABS(r(i)-x) < tiny) then
             exists = .true.
             exit steps
          end if
       end do
    end do steps
    if (.NOT.exists) then
       write (fo,'(a)') 'first_region: not enough points in interval'
       write (fo,'(a,f12.9,a,f12.6,a,i3,a,f12.6)') ' hint = ', hint, &
            '  x = ', x, ' j = ', j, '  r(j) = ', r(j)
       stop
    end if
! eliminate points which are not multiples of hint:
    ncs = nc
    do i = 2, 7
       do while (ABS(r(i) - (i-1)*hint) > tiny)
          nc = nc - 1
          do j = i, nc
             r(j) = r(j+1)
          end do
       end do
    end do
    if (nc /= ncs) then
       write (fo,'(a)') 'first_region: warning!!'
       write (fo,'(a,2i4)') 'Deletion of mesh points: nc(in), nc(out) &
            &= ', ncs, nc
    end if
  end subroutine first_region

  subroutine read_mesh (np, rb)
! read in initial mesh
    integer, intent(inout)     :: np ! # radial points
    real(wp), intent(in)       :: rb ! radial limit point
    real(wp)                   :: epr1, epr2, eqr1, eqr2
    integer                    :: i, ma1, ma2, status, nc, key
    real(wp)                   :: h
    
    allocate (rt(np), pxt(np), stat=status)
    if (status /= 0) call alloc_error (status, 'read_mesh', 'a')

    nc = 0
    rd_mesh: do i = 1, np, 2
       read (indata,fmt2) key, ma1, epr1, eqr1, ma2, epr2, eqr2
       if (key /= -8) then
          write (fo,fmt2) key, ma1, epr1, eqr1, ma2, epr2, eqr2
          write (fo,'(a)') 'read_mesh: key error, last record above'
          stop
       end if
       if (ma1 /= np .and. ma2 > ma1) then ! read 2nd point on record
          h = 2 * (epr2 - epr1)
          if (epr2 <= rb+h) then
             nc = ma2
             rt(ma2) = epr2
             pxt(ma2) = eqr2
          end if
       end if
       if (epr1 > rb+h) cycle
       nc = MAX(ma1, nc)
       rt(ma1) = epr1
       pxt(ma1) = eqr1
    end do rd_mesh
    nrt = nc
    np = nc
  end subroutine read_mesh
 
  subroutine refine_mesh (nc, nsp, ra, rb)
! add mesh points to ensure step size does not exceed that required
! to calculate integrals of continuum orbitals
    use rm_data, only: maxe
    integer, intent(inout)  :: nc     ! final total mesh size
    integer, intent(out)    :: nsp    ! size of unextrapolated mesh
    real(wp), intent(in)    :: ra, rb ! R-matrix radius values
    real(wp)                :: pi, g, h, h_final, rx, dy
    integer                 :: ir, i, ns, f, m, nf, status
    integer                 :: jlo, ncs, kl, kr
    real(wp)                :: tesr(10), tesb(10), tesc

    if (maxe <= 0) then  ! max energy (Ryd) not specified
       write (fo,'(a,i6)') 'refine_mesh: maxe = ', maxe
       write (fo,'(a)') 'required input with superstructure data'
       stop
    end if
    pi = 4.0_wp * ATAN(1.0_wp)
    g = pi / (23.0_wp * SQRT(REAL(maxe,wp))) ! reqd asymtotic step size

    ir = 0
    do i = 2, nc      ! nc is size of input mesh here
       h = rt(i) - rt(i-1)
       if (h > g) then
          ir = i-1
          exit
       end if
    end do
    if (ir == 0) then ! asymptotic step size criterion satisfied
       if (bug7 == 2) write (fo,'(a)') 'refine_mesh: no refinement'
       ns = nc
       h_final = rt(nc) - rt(nc-1)
       ir = nc
    else if (ir == 1) then ! all steps too large
       if (bug7 == 2) write (fo, '(a)') 'refine_mesh: single step-size'
       ns = nc
       h_final = rt(nc) - rt(nc-1)
    else             ! step refinement required
       h_final = rt(ir) - rt(ir-1)
       ns = ir + INT((rt(nc) - rt(ir)) / h_final)
    end if
    nsp = ns   ! # steps in refined mesh
    f = pi / (23.0_wp * h_final)
    maxe = INT(f * f)        ! energy limit for refined mesh
    if (bug7 == 2) write (fo,'(a,i6)') 'refine_mesh: actual maxe = ',&
         maxe
    
    if (ra /= 0.0_wp) then ! check to see if grid must be extended
       nf = 0
       do i = 2, nrt
          if (rt(i) >= rb) then
             nf = i
             exit
          end if
       end do
       if (nf == 0) then ! extend grid
          ns = ns + INT((rb - rt(nrt)) / h_final)
       else              ! truncate grid
          ns = nf
          write (fo,'(a)') 'refine_mesh: ra truncates input mesh'
          ir = MIN(ir, ns) ! This should not happen
       end if
    end if

    nc = ns     ! reset nc to final mesh size
    allocate (r(nc), px(nc), stat=status)
    if (status /= 0) call alloc_error (status, 'refine_mesh', 'a')
    
    do i = 1, ir         ! insert core points
       r(i) = rt(i)
    end do
    rx = rt(ir)          ! finalize radial values
    do i = ir+1, nc
       rx = rx + h_final
       r(i) = rx
    end do
    ncs = nc
!    call first_region (nc)   ! modify first region if necessary
!    if (nc /= ncs) nsp = nsp + nc - ncs  ! adjust for subtractions
    jlo = 1              ! insert interpolated potential points
    do i = 1, nsp
       call hunt (rt, nrt, r(i), jlo)
       kl = MIN(MAX(jlo-(order-1)/2,1), nrt+1-order) ! L.H. point
       kr = kl + order - 1
       call polint (rt(kl:kr), pxt(kl:kr), order, r(i), px(i), dy)
    end do
    do i = nsp+1, nc
       px(i) = px(i-1)
    end do
    write (fo,'(/,a,i6/)') 'Total number of radial mesh points = ', nc
  end subroutine refine_mesh

  subroutine read_orbitals (nc, nsp, mo, eps, iflg, jflg, ipjs)
! read and if necessary extrapolate orbitals and derivatives
    use bound_basis, only: orbital_store, uj, duj, ipos
    use rm_xdata, only: maxnhf, lrang1, nrang1, lrang2
    use rm_data, only: nelc, nz, ra, maxorb
    integer, intent(in)       :: nc   ! # grid points
    integer, intent(in)       :: nsp  ! # unextrapolated grid points
    integer, intent(in)       :: mo   ! # orbitals
    real(wp), intent(out)     :: eps  ! Whittaker extrapolation switch
    integer, intent(inout)    :: iflg
    integer, intent(inout)    :: jflg
    integer, intent(inout)    :: ipjs   ! point for orbitals negligible
    integer                   :: k, ios, status, n, l, j, no, i
    integer                   :: ma1, ma2, jlo, key, m, s, kl, kr
    real(wp), allocatable     :: ujt(:), dujt(:)
    real(wp)                  :: dy, dy2, h, x, ampl, epr1, epr2
    real(wp)                  :: eqr1, eqr2, rc

    call orbital_store (nc)
    allocate (ujt(nsp), dujt(nsp), stat=status)
    if (status /= 0) call alloc_error (status,'read_orbitals', 'a')
    
    orbs: do k = 1, mo
! n = quantum number, l = quantum number, x = Z, no = # records
! j = # electrons, eps = analytical if negative (Whittaker switch)
! h = ey0 (zero in SS(
       read (indata,fmt3,iostat=ios) key, i, n, l, x, j, eps, no, h
       write (fo,fmt3) key, i, n, l, x, j, eps, no, h
       if (ios /= 0) then
          write (fo,'(a,i6)') 'SS: orbital read error, iostat = ', ios
          stop
       end if
       if (key > -6) exit orbs
       if (key /= -7) then
          write (fo,'(a)') '`SS error: last card read was as above'
          stop
       end if
       nbound = k
       if (k > maxorb) then
          write (fo,'(a,i3)') 'too many orbitals, maxorb < ', nbound
          stop
       end if
       if (l > lrang1-1) then
          write (fo,'(a,i3)') 'read_orbitals: max l exceeded, l = ', l
          stop
       end if
       if (n > nrang1) then
          write (fo,'(a,i4)') 'SS: max n exceeded for n = ', n
          stop
       end if
       ipos(n,l+1) = k
       maxnhf(l+1) = MAX(maxnhf(l+1), n)
       
! read and store orbital functions p and q,
! determine ipjs such that at r(ipjs) all radial functions
! are less than tinorb in relative value
       ampl = 0.0_wp
       ujt = 0.0_wp
       dujt = 0.0_wp
       orbitals: do i = 1, no
          read (indata,fmt2) key, ma1, epr1, eqr1, ma2, epr2, eqr2
          if (key /= -6) then
             write (fo,fmt2) key, ma1, epr1, eqr1, ma2, epr2, eqr2
             write (fo,'(a)') 'SS error: last card read was as above'
             stop
          end if
          if (ma1 > nsp) cycle
          ujt(ma1) = epr1
          dujt(ma1) = eqr1
          if (ma1 == 1) then   ! zero first point ...?
             ujt(ma1) = 0.0_wp
             dujt(ma1) = 0.0_wp
          end if
          ampl = MAX(ampl, ABS(epr1))
          if (h >= tinorb*ampl) ipjs = MAX(ma2, ipjs)
          if (ma2 > nsp .or. ma2 < ma1) cycle
          ujt(ma2) = epr2
          dujt(ma2) = eqr2
          h = ABS(epr2)
          ampl = MAX(h, ampl)    ! orbital maximum
          if (h >= tinorb*ampl) ipjs = MAX(ma2, ipjs)
       end do orbitals
       m = MAX(ma1, ma2)

       jlo = 1              ! insert interpolated orbital points
       s = nsp
       do i = 1, nsp
          if (r(i) > rt(m)) then
             s = i - 1
             exit
          end if
          call hunt (rt, m, r(i), jlo)
          kl = MIN(MAX(jlo-(order-1)/2,1), m+1-order) ! L.H. point
          kr = kl + order - 1
          call polint (rt(kl:kr), ujt(kl:kr), order, r(i), uj(i,k), dy)
          call polint (rt(kl:kr), dujt(kl:kr), order, r(i), duj(i,k), &
               dy2)
       end do
       
! extend orbital array if not given completely up to ra.
! use formula p(r)=p(rb)*(r-ra)/(rb-ra) - or better exploit
! analytic behaviour (if eps < 0 specified)
       if (eps < 0.0_wp) iflg = iflg + 1
       if (m >= nc) cycle orbs
       jflg = jflg + 1
       rc = r(nc)
       call extrapolate_orbital (k, l, eps, rc, x, s, ipjs, nc, ampl)
    end do orbs
    deallocate (ujt, dujt, stat=status)
    if (status /= 0) call alloc_error (status, 'read_orbitals', 'd')
  end subroutine read_orbitals
 
  subroutine extrapolate_orbital (k, l, eps, rc, z, s, ipjs, nc, ampl)
! extend orbital array if not given completely up to ra.
! Whittaker asymptotic series if eps < 0, else
! linear extrapolation assuming orbital zero at ra
    use bound_basis, only: uj, duj
    integer, intent(in)     :: k     ! orbital sequence
    integer, intent(in)     :: l     ! orbital angular momentum
    real(wp), intent(in)    :: eps   ! Whittaker switch
    real(wp), intent(in)    :: rc    ! final radial point
    real(wp), intent(in)    :: z     ! effective charge
    integer, intent(in)     :: s     ! last filled mesh point
    integer, intent(inout)  :: ipjs  ! last point 
    real(wp), intent(inout) :: ampl ! orbital max magnitude
    integer, intent(in)     :: nc
    real(wp)                :: a(30)
    real(wp)                :: f, h, t, rb, ub, e, d, x, p, g
    real(wp)                :: q, uj_test
    integer                 :: j, rp, jj, kk, ma1, ma2, iz, i, m
    logical                 :: whit, err
    
    ma1 = s        ! set first extrapolation point
    m = s
    uj_test = uj(s,k)
    jj = s - 1
    kk = s - 2
    iz = -1
    p = 0.0_wp
    
    if (eps < 0.0_wp) then ! try Whittaker expansion
       whit = .true.
    else                   ! Linear interpolation
       whit = .false.
    end if
    
    rb = r(jj)     ! extrapolation point
    ub = uj(jj,k)  ! P_nl(rb)

! estimate e, d for P=C * r**d * EXP(-er)
    e = (rb*duj(jj,k)/ub - r(kk)*duj(kk,k)/uj(kk,k)) / (rb-r(kk))
    d = (duj(kk,k)/uj(kk,k) - duj(jj,k)/ub) * r(kk) * rb / (rb-r(kk))
    
    if (whit) then
       x = SQRT(-e)
       err = .true.
       do i = 2, m
          if (i < kk-1 .and. ABS(uj(i,k)) <= 1.0e-10_wp) cycle
          iz = i
          if (ABS(duj(i,k)/uj(i,k)-d/r(i)-e) < 0.001_wp) then
             err = .false.
             exit
          end if
       end do
       if (err) write (fo,'(a)') 'SS: input tail too poor for &
            &extrapolating'

! as e too inaccurate at tabulation points m-2,m-1 or p too small.
! 53  z=anint((1.-uj(2,k)/(uj(1,k)*r(2)**(l+1)))*(l+1)/r(2))
! -- but this is found to be too inaccurate if l large.
         
       q = (0.5_wp * d + z) / x
         
! get a semi-convergent asymptotic whittaker series to q,l at rb:

       err = .true.   ! error flag
       f = 1.0_wp     ! sum of asymptotic series
       h = 1.0_wp     ! radial factor in series
       t = 1.0_wp     ! minimum term of series
       a(1) = 1.0_wp
       terms: do i = 1, 29
          a(i+1) = (REAL((l+1)*l,wp) - (i-q) * (i-1-q)) * a(i) / (2*i*x)
          h = rb * h   ! r**i
          if (ABS(a(i+1)) < t*h) t = ABS(a(i+1))/h
          
          if (q+tiny <= i) then
             if (i >= 5 .and. ABS(a(i+1)) > rb*ABS(a(i))) then
                write (fo,'(a)') 'SS: Whittaker series divergent'
                err = .false.
                exit
             end if
! when smallest term in sum degrades by 6 decimal places...
             if (ABS(f*h+a(i+1)) > h*t*1.e+6_wp .or. &
                  ABS(a(i+1)) < ABS(f)*h*1.e-9_wp) then
                err = .false.
                exit
             end if
          end if
          f = a(i+1)/h + f
          j = i + 1        ! # a terms computed
       end do terms
       if (err) write (fo,'(a)') 'SS: warning, 29 whittaker terms &
            &may be not enough'
       g = q * LOG(rb) - x * rb ! exponent of factor multiplying f
       p = uj(m,k)

       radial_pts: do i = m, nc
          h = 1.0_wp / r(i)
          t = a(j)
          do jj = j, 2, -1
             t = t * h + a(jj-1)
          end do
          h = q * LOG(r(i)) - x * r(i) - g ! complete exponent
          
          if (i /= nc) then ! not last radial point
             jj = m
! check if P_nl(r) < 10**(-30):
             if (h < LOG(ABS(f/(t*ub))) - lne) then ! too small
                whit = .false.
                exit
             end if
          end if
            
          h = EXP(h) * ub * t / f  ! P_nl(r)
          
          t = ABS(h)
          if (t > ampl) ampl = t
          if (t >= tinorb*ampl) ipjs = MAX(i, ipjs)
          
          ma2 = i          ! set last extrapolation point
          
          uj(i,k) = h                      ! P_nl(r)
          duj(i,k) = (d / r(i) + e) * h    ! Q_nl(r)
          m = i
       end do radial_pts
    END if
      
    if (.NOT.whit) then
       rb = r(jj)     ! extrapolation point
       ub = uj(jj,k)  ! P_nl(rb)
       do i = m, nc
          h = (r(i) - rc) * ub / (rb - rc)   ! linear interpolation
          uj(i,k) = h                      ! P_nl(r)
          duj(i,k) = (d / r(i) + e) * h    ! Q_nl(r)
          m = i
       end do
    end if
    
    a(1) = -0.5_wp * d
    jj = MIN(ABS(j), 11)

    write (fo,'(a,3(a,i3,a,f7.3))') 'extrapolation range:', ' r(',&
         ma1, ')=', r(ma1), ' r(', ma2, ')=', r(ma2), ' r(', nc,  &
         ')=', rc
    write (fo,'(a,3e15.8)') 'values of pnl(r):   ', uj(ma1,k),    &
         uj(ma2,k), uj(nc,k)
    if (bug7 == 2) then
       write (fo,'(a,e15.5,2i6,f11.5,f7.2)') 'p, j, iz, e = ', p, j, &
            iz, e
       write (fo,'(a,5f12.5,/(3x,5f12.5))') 'a: ', a(1:jj)
       write (fo,'(a,e14.6)') 'Error = ', ABS(uj(s,k) -uj_test)
    end if
  end subroutine extrapolate_orbital

  subroutine step_changes (nc)
! count step changes, allocate grid parameter arrays
    use radial_grid, only: nix, irx, ihx, step, hint
    integer, intent(in)     :: nc   ! # radial points
    integer                 :: c, i, status
    real(wp)                :: ho, h
    
    c = 0
    ho = 0.0_wp
    do i = 2, nc
       h = r(i) - r(i-1)
       if (ABS(h-ho) > 1.0e-10_wp) then ! step change
          c = c + 1
          ho = h
       end if
    end do
    write (fo,'(a,i5)') 'step_changes: count = ', c
    if (c == 0) stop
    nix = c
    allocate (irx(nix), ihx(nix), stat=status)
    if (status /= 0) call alloc_error (status, 'step_changes', 'a')
    hint = r(2) - r(1)
  end subroutine step_changes

  subroutine def_grid_pams (nc)
! define the ihx and irx arrays, nix= number of intervals.
! check that mesh doubles step-length only after an odd point.
    use radial_grid, only: nix, irx, ihx, hint, xr, npts
    integer, intent(in)    :: nc      ! # grid points
    integer                :: i, j
    real(wp)               :: h, ampl, x
    
    h = hint
    j = 1
    nix = 1
    ihx(1) = 1
    ampl = 0.0001_wp

    do i = 1, nc
       x = r(j) + (i-j) * h
       ampl = MAX(0.0001_wp * x, 1.0e-6_wp)
       if (ABS(r(i)-x) < ampl) cycle ! no step change

       if (MOD(i,2) /= 0) then ! step double at odd point
          write (fo,'(a)') 'SS: error, step double at odd point'
          write (fo,'(a,f12.9,a,i3,a,f12.6,a,i3,a,f12.6)') ' hint =',&
               hint, '  nix = ', nix, '  x = ', x, '  j = ', j,      &
               '  r(j) = ', r(j)
          stop
       end if
       x = x + h
       if (ABS(r(i)-x) >= 0.0001_wp) then ! step did not double
          write (fo,'(a)') 'SS: error, incorrect step increase'
          write (fo,'(a,f12.9,a,i3,a,f12.6,a,i3,a,f12.6)') ' hint =', &
               hint, '  nix = ', nix, '  x = ', x, '  j = ', j,       &
               '  r(j) = ', r(j)
          stop
       end if

       nix = nix + 1
       if (nix > SIZE(irx)) then
          write (fo,'(a,i4,a)') 'nix  = ', nix, ' exceeds mxnix'
          stop
       end if
       ihx(nix) = 2 * ihx(nix-1)
       irx(nix-1) = i - 2
       h = ihx(nix) * hint
       j = i
    end do
    npts = MOD(nc,2) + nc - 1
    irx(nix) = npts - 1
  end subroutine def_grid_pams

  subroutine abnorm (n1, l1, n2, l2, braket)
! evaluates the overlap integral between two numerical orbitals
! specified by the quantum numbers (n1,l1-1) and (n2,l2-1).
! integration using simpsons rule.
    use radial_grid, only: hint, nix, irx, ihx
    use bound_basis, only: uj, ipos
    integer, intent(in)         :: n1, l1
    integer, intent(in)         :: n2, l2
    real(wp), intent(out)       :: braket
    integer                     :: k1, k2, ist, mi, i, ifi, j
    real(wp)                    :: b
    
    k1 = ipos(n1,l1)
    k2 = ipos(n2,l2)
    braket = 0.0_wp
    ist = 2
    mi = 4
    do i = 1, nix
       b = 0.0_wp
       ifi = irx(i) + 1
       do j = ist, ifi
          if (j == ifi) mi = 1
          b = mi * uj(j,k1) * uj(j,k2) + b
          if (mi == 4) then
             mi = 2
          else
             mi = 4
          end if
       end do
       braket = (ihx(i) * hint) * b / 3 + braket
       mi = 1
       ist = ifi
    end do
  end subroutine abnorm

  subroutine hunt (xx, n, x, jlo)
! return jlo so x lies between xx(jlo) and xx(jlo+1)
    integer, intent(in)    :: n        ! x-dimension
    integer, intent(inout) :: jlo      ! initial guess
    real(wp), intent(in)   :: x        ! point to locate
    real(wp), intent(in)   :: xx(1:n)  ! monotonic interpolant array
    integer                :: inc, jhi, jm
    logical                :: ascnd

    ascnd = xx(n) > xx(1)
    if (jlo <= 0 .or. jlo > n) then ! initial guess useless
       jlo = 0
       jhi = n + 1
    else
       inc = 1
       if (x >= xx(jlo) .eqv. ascnd) then
          do
             jhi = jlo + inc
             if (jhi > n) then
                jhi = n + 1
                exit
             else if (x >= xx(jhi) .eqv. ascnd) then
                jlo = jhi
                inc = inc + inc
                cycle
             end if
             exit
          end do
       else
          jhi = jlo
          do
             jlo = jhi - inc
             if (jlo < 1) then
                jlo = 0
             else if (x < xx(jlo) .eqv. ascnd) then
                jhi = jlo
                inc = inc + inc
                cycle
             end if
             exit
          end do
       end if
    end if

    do                            ! bisection
       if (jhi-jlo == 1) exit
       jm = (jhi + jlo) / 2
       if (x > xx(jm) .eqv. ascnd) then
          jlo = jm
       else
          jhi = jm
       end if
    end do
  end subroutine hunt
      
  subroutine polint (xa, ya, n, x, y, dy)
! polynomial interpolation on arrays xa(n), ya(n)
    integer, intent(in)    :: n   ! interpolating polynomial order n-1
    real(wp), intent(in)   :: xa(n), ya(n)
    real(wp), intent(in)   :: x
    real(wp), intent(out)  :: y, dy
    integer                :: i, m, ns
    real(wp)               :: den, dif, dift, ho, hp, w
    real(wp)               :: c(n), d(n)

    ns = 1
    dif = ABS(x - xa(1))
    do i = 1, n
       dift = ABS(x - xa(i))
       if (dift < dif) then
          ns = i
          dif = dift
       end if
       c(i) = ya(i)
       d(i) = ya(i)
    end do
    y = ya(ns)
    ns = ns - 1
    do m = 1, n-1
       do i = 1, n-m
          ho = xa(i) - x
          hp = xa(i+m) - x
          w = c(i+1) - d(i)
          den = ho - hp
          if (den == 0.0_wp) then
             write (fo,'(a)') 'polint: error, xs equal'
             stop
          end if
          den = w / den
          d(i) = hp * den
          c(i) = ho * den
       end do
       if (2*ns < n-m) then
          dy = c(ns+1)
       else
          dy = d(ns)
          ns = ns - 1
       end if
       y = y + dy
    end do
  end subroutine polint
end module superstructure
