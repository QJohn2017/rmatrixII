program rad
! calculate radial integrals
! Time-stamp: "03/03/06 06:59:50 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use rm_xdata, only: ncore
  use rm_rd, only: rd_rm_data
  use rm_data, only: ra, buttle, ibop
  use file_info, only: open_files, delete_tmp_files
  use bound_basis, only: evalue, destroy_p, orbital_store
  use radial_integrals, only: genint
  use core_energy, only: ecore
  use basis_functions, only: potf
  use bassplnew_orb, only: splorb
  use buttle_correct, only: gen_buttle, destroy_buttle
  use angular_momentum, only: factt, shriek
  use one_el, only: ykstor, xykstor
  use rad2ham, only: writap
  implicit none
  real(wp)                :: encore, raold
  integer, parameter      :: n_factorial = 50
  integer                 :: c0, c1, cr
  real(wp)                :: t0, t1

  call cpu_time(t0)
  call system_clock (count=c0)

  call shriek (n_factorial)

  call rd_rm_data  ! Read input data
  raold = ra
  call open_files             ! open radial output files
  if (ibop == 0) call orbital_store          ! define orbital storage
  call evalue                 ! Evaluate bound orbitals

  call potf                   ! Evaluate model scattering potential
  call splorb                 ! Evaluate continuum orbitals
  if (buttle) call gen_buttle ! Calculate the Buttle correction

  if (ABS(ra-raold) > 1.0E-8_wp) THEN
     write (fo,'(a,f15.9,a,f15.9)') 'RA reset from ', raold, &
          ' to ', ra
  end if

  if (ncore > 0) then      ! Calculate core energy
     call factt
     call ecore (encore)
  else
     call ykstor
     encore = 0.0_wp
  end if

  call writap (encore)   ! Write serial file for STGHAM
  print *,'Here hugo'
  call destroy_p
  call destroy_buttle

  call genint            ! Generate the radial integrals
  call xykstor
  call delete_tmp_files

  write (fo,'(a)') 'End of RAD'
  call cpu_time (t1)
  write (fo,'(a,f16.4,a)') 'CPU time     = ', t1 - t0, ' secs'
  call system_clock (count=c1, count_rate=cr)
  write (fo,'(a,f16.4,a)') 'Elapsed time = ', REAL(c1-c0,wp) / &
       REAL(cr,wp), ' secs'
  stop
end program rad
