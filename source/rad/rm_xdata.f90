module rm_xdata
! Time-stamp: "2003-06-05 17:32:53 cjn"
  use precisn, only: wp

! configuration specification
  integer, save                 :: ncfg      ! # configurations
  integer, allocatable, save    :: noccsh(:) ! # orbs in cfg
  integer, allocatable, save    :: nocorb(:,:) ! orb indices for cfg
  integer, allocatable, save    :: nelcsh(:,:) ! orb occ for cfg

  integer, save                 :: maxorb      ! # orbitals
  integer, allocatable, save    :: nelmax(:)   ! max occ for orbital

  integer, save                 :: ncore       ! total # core orbitals
  integer, save                 :: lrang1      ! max L bound orbs + 1
  integer, save                 :: maxlc       ! max core l + 1
  integer, allocatable, save    :: maxnc(:) ! max n each core l, else -1
  integer, allocatable, save    :: maxnhf(:) ! max n of bd L orbs
  integer, allocatable, save    :: maxnlg(:)    ! max n for each core l
  integer, allocatable, save    :: nvary(:)   ! # cont bf for each L

  integer, save                 :: minim    ! min (N+1) L-value + 1
  integer, save                 :: lrang2   ! max (N+1) L-value + 1
  integer, save                 :: lexch    ! max L with exchange + 1

  integer, save                 :: lrang4   ! max L-value + 1
  integer, save                 :: shm
  integer, save                 :: nrang1   ! max target n-value
  integer, save                 :: nrangx   ! max nvary value

  private
  public ncfg, noccsh, nocorb, nelcsh, maxorb, nelmax, ncore, lrang1
  public maxlc, maxnc, maxnhf, maxnlg, nvary, minim, lrang2, lexch, shm
  public lrang4, nrang1, nrangx
end module rm_xdata
