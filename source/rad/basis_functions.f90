module basis_functions
! determine radial basis functions using a model potential
! Time-stamp: "03/03/06 09:14:15 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use debug, only: bug1, bug3, bug5
  implicit none

  real(wp), allocatable, save :: frh(:)
  real(wp), allocatable, save :: u(:)
  real(wp), save              :: x

  real(wp), save              :: fm, tlc, wr
  integer, save               :: itst, jr, km, mmm, nbtp1, ng

  real(wp), allocatable, save :: povalu(:)   ! 2 * r * potential
  integer, save               :: imatch      ! matching point

  real(wp), allocatable, save :: orb(:)
  real(wp), allocatable, save :: dorb(:)
  real(wp), allocatable, save :: eigens(:,:)
  real(wp), save              :: eigen
  real(wp), save              :: bvalue

  private
  public basfun, potf
  public orb, dorb, eigen, bvalue, eigens

contains

  subroutine basfun (nbt, lc, nodes, ra, winit)
     use radial_grid, only: hint, nix, ihx, irx, xr, npts
     use bound_basis, only: p
     use lapack_interface, only: sleqn
     integer, intent(inout)  :: nbt ! # fns to which soln is orthogonlized
     integer, intent(in)     :: lc  ! angular momentum value
     real(wp), intent(inout) :: ra  ! R-matrix boundary radius
     real(wp), intent(in)    :: winit ! energy
     integer, intent(out)    :: nodes ! # nodes in orbital
     real(wp)                :: delt(nbt+1,nbt), sdelt(nbt+1,nbt)
     real(wp)                :: adel(nbt+3,nbt+3), bdl(nbt+3)
     real(wp)                :: us(nbt+1,npts), dus(nbt+1,npts)
     real(wp)                :: du(nbt+1)
     real(wp)                :: orth(nbt), yin(nbt+1), dyin(nbt+1)
     real(wp)                :: yout(nbt+1), dyout(nbt+1)
     real(wp)                :: fr(nbt+1), frm(nbt+1)
     real(wp)                :: bndry(2), dbndry(2)
     integer                 :: i, j, m, i8, niz, i1, i2, nstart
     integer                 :: lcm, intx, lp1, lp2, k, ir, nits
     integer                 :: nutty, lswt, nbtp2, nbtp3, i3, imt
     integer                 :: inst, nbtn6, n, jl, nbtm, large
     real(wp)                :: himt, s, h, twoz, zsc, scast, wrsc
     real(wp)                :: ptinf, rlimit, overlp, xtest, h1, hs
     real(wp)                :: orbmax, am, scalen, x1, x2, x3
     real(wp)                :: orb1, orb2, onept5, scaorb, scast1
     real(wp)                :: tiny, frhval, qlarge
     character(len=4), parameter :: uname(4) = (/'u(1)','u(2)','u(3)','u(4)'/)

! check compatibility of hint,ihx,irx and ra
    s = 0.0_wp
    j = 0
    do i = 1, nix
       h = REAL(ihx(i),wp) * hint
       s = s + h * REAL(irx(i)-j,wp)
       j = irx(i)
    end do
    if (ABS(s-ra) > 0.5_wp*hint) then
       write (fo,'(a)') 'basfun: hint, ihx, irx and ra incompatible'
       return
    end if

! check that irx(i) are even integers
    ra = s
    do i = 1, nix
       j = irx(i)
       m = MOD(j, 2)
       if (m /= 0) then
          write (fo,'(a,i1,a)') 'Basfun: iry(', i, &
               ') is an odd number'
          return
       end if
    end do

    onept5 = 1.5_wp
    imatch = irx(nix) - 20
    if (winit >= 0.0_wp .and. nbt == 0) imatch = irx(nix)
    if (MOD(imatch,2) == 1) imatch = imatch + 1

    fm = 0.0_wp
    itst = 0
    nbtp1 = nbt + 1
    ng = nbt
    orb = 0.0_wp        ! final solution
    dorb = 0.0_wp       ! orbital second derivative
    tlc = REAL(lc*(lc+1),wp)
    lcm = lc + 1
    overlp = 1.0_wp / REAL(lcm,wp)
    inst = 1
    himt = hint * REAL(ihx(inst),wp)
    wr = winit
    eigen = wr      ! save energy of orbital

    if (bug1 /= 0) then
       write (fo,'(a)') 'Basfun: debug print-out'
       write (fo,'(a)')
       write (fo,'(a,f15.7,a)') 'energy wt = ', wr, ' Ryds'
       write (fo,'(a)') 'outward integration'
       write (fo,'(8x,a,4(11x,a4))') ' r  ', uname(1:nbtp1)
    end if

! modifications for large l
    twoz = 0.0_wp
    if (povalu(2) > TINY(scast)) then
       niz = INT(povalu(1) / povalu(2) - 0.5_wp)
       if (niz >= 1) twoz = 0.5_wp * povalu(1) * himt
    end if
    zsc = 0.5_wp * twoz

! estimate the point of inflection.  if it is beyond
! the radius at which r**(l+1) overflows calculate scaling factor 
    large = MAXEXPONENT(scast)
    qlarge = 1.0_wp**(large/2)       ! sqrt(large)
    scast = 1.0_wp
    if (wr > 0.0_wp .and. nbt == 0 .and. lcm > 3) then
       if (zsc /= 0.0_wp) then
          wrsc = wr / zsc**2
       else
          wrsc = wr
       end if
       ptinf = (SQRT(1.0_wp + wrsc * tlc) - 1.0_wp) / wrsc / zsc
       rlimit = 10**(large * overlp)
       if (ptinf >= rlimit) then
          scast1 = rlimit / xr(npts)
          if (scast1 < 1.0_wp) then
             scast = scast1**lcm
          end if
       end if
    end if

    xtest = xr(2)     ! find starting point: inst, nstart
    nstart = 1
    if (scast*xtest**lcm == 0.0_wp) then
       i2 = -2
       intx_loop: do intx = 1, nix
          i1 = i2 + 5
          i2 = irx(intx) - 3
          do i = i1, i2
             if (scast*xr(i)**lcm > TINY(scast)) then
                nstart = i - 1
                inst = intx
                exit intx_loop
             end if
          end do
          if (intx == nix) then
             write (fo,'(a,i4)') 'no starting position found &
                  &for l = ', lc
             stop
          end if
       end do intx_loop
    end if

    x = xr(nstart)
    u(1:nbtp1) = 0.0_wp
    du(1:nbtp1) = 0.0_wp
    us(1:nbtp1,1:nstart) = 0.0_wp
    dus(1:nbtp1,1:nstart) = 0.0_wp
    if (bug1 /= 0) write (fo,'(8e15.6)') x, u(1:nbtp1)

! evaluate function and derivative at hint and store the function
    lp1 = lcm
    lp2 = lc + 2
    himt = hint * ihx(inst)
    x = xr(nstart+1)
    h = himt

    do k = 1, nbt
       u(k) = p(k,2*nstart) * x * x / REAL(4*lc+6,wp)
       du(k) = 0.5_wp * REAL(lc+3) * u(k)
       us(k,nstart+1) = u(k)
       dus(k,nstart+1) = 2.0_wp * du(k) / h
    end do
    u(nbtp1) = x**lp1 * (1.0_wp - twoz * x / REAL(2*lp1,wp))
    u(nbtp1) = u(nbtp1) * scast
    du(nbtp1) = x**lc * (REAL(lp1,wp) - twoz * REAL(lp2,wp) * x / &
         REAL(2*lp1,wp))
    du(nbtp1) = du(nbtp1) * scast
    du(nbtp1) = 0.5_wp * du(nbtp1) * h
    us(nbtp1,nstart+1) = u(nbtp1)
    dus(nbtp1,nstart+1) = 2.0_wp * du(nbtp1) / h
    if (bug1 /= 0) write (fo,'(8e15.6)') x, u(1:nbtp1)

! evaluate fr at hint
    km = 2 * nstart - 1
    mmm = 1
    call derfun
    do k = 1, nbtp1
       fr(k) = frh(k) * h * h / 12.0_wp
    end do

! set up frm at hint/2
    x = x - 0.5_wp * himt
    km = 2 * nstart - 2
    do k = 1, nbt
       frm(k) = u(k)
       u(k) = u(k) - du(k) + onept5 * fr(k)
    end do
    frm(nbtp1) = u(nbtp1)
    u(nbtp1) = x**lp1 * (1.0_wp - twoz * x / (2.0_wp * REAL(lp1,wp)))
    u(nbtp1) = u(nbtp1) * scast
    call derfun

    km = 2 * nstart
    x = x + 0.5_wp * himt
    do k = 1, nbtp1
       u(k) = frm(k)
       frm(k) = frh(k) * h * h / 3.0_wp
    end do

! store the contribution to the integral from the first point
    if (nbt > 0) then
       do j = 1, nbt
          do i = 1, nbtp1
             delt(i,j) = 4.0_wp * u(i) * p(j,km) * h
          end do
       end do
    end if

! integrate out to the matching point
    lswt = 1
    i1 = nstart + 1
    intxp_loop: do intx = inst, nix
       h1 = h
       h = hint * REAL(ihx(intx),wp)
       hs = h * h
       i2 = irx(intx)
       if (imatch < i2) i2 = imatch
       if (intx /= inst) then
          lswt = 2
          i1 = irx(intx-1) + 1
       end if

! integrate over a range of equal intervals
       ir_loop: do ir = i1, i2
          jr = ir + 1
          call devgl (nbtp1, du, fr, frm, h, hs, h1, lswt)
          if (bug1 == 1) write (fo,'(8e15.6)') x, u(1:nbtp1)

! calculate the integration factor
          if (nbt /= 0) then
             if (ir == imatch) then
                am = 1.0_wp
             else if (ir /= i2) then
                if (MOD(ir,2) /= 0) then
                   am = 4.0_wp
                else
                   am = 2.0_wp
                end if
             else
                am = REAL(ihx(intx)+ihx(intx+1)) / REAL(ihx(intx),wp)
             end if
! add in the contributation to the integral from the current point
             do j = 1, nbt
                do i = 1, nbtp1
                   delt(i,j) = delt(i,j) + u(i) * p(j,km) * h * am
                end do
             end do
          end if

! store the functions at each integration
          do i = 1, nbtp1
             us(i,jr) = u(i)
             dus(i,jr) = 2.0_wp * du(i) / h
          end do

          if (ir == imatch) exit intxp_loop
       end do ir_loop
    end do intxp_loop

! store functions and derivatives at matching point for outward integration
    do i = 1, nbtp1
       yout(i) = u(i)
       dyout(i) = 2.0_wp * du(i) / h
    end do

    if (imatch == irx(nix)) then    ! no inward integration

      if (nbt /= 0 .or. winit < 0.0_wp) then ! check imatch allowed at ra
         write (fo,'(a)') 'Basfun: match point on the boundary'
         write (fo,'(a,i3)') 'number of bound terms = ', nbt
         write (fo,'(a,e16.5)') 'energy = ', winit
         stop
      end if

       orbmax = 0.0_wp
       do i = 1, npts
          orb(i) = us(1,i)
          orbmax = MAX(orbmax, ABS(orb(i)))
          dorb(i) = dus(1,i)
       end do
       bvalue = dorb(npts) * ra / orb(npts)    ! logarithmic derivative

    else                           ! de Vogelaere inward integration

       mmm = -1
       nbtm = nbt
       itst = 1
       bndry(1:2) = (/1.0_wp, 0.0_wp/)
       dbndry(1:2) = (/0.0_wp, 1.0_wp/)
       nits = 2

! nits=2 for the solution at a given energy for which two independent
!        inward solutions are necessary to obtain continuity.
       nutty_loop: do nutty = 1, nits

! evaluate the function and derivatives at ra
          if (bug1 /= 0) then
             write (fo,'(a,i2)') 'inward integration', nutty
             write (fo,'(8x,a,4(11x,a4))') ' r  ', uname(1:nbtp1)
          end if
          km = 2 * irx(nix) + 1
          h = hint * REAL(ihx(nix),wp)
          hs = h * h
          x = ra
          if (nutty == 2) nbt = 0
          ng = nbt
          nbtp1 = nbt + 1
          do i = 1, nbt
             u(i) = 0.0_wp
             du(i) = 0.0_wp
          end do
          u(nbtp1) = bndry(nutty)
          du(nbtp1) = - 0.5_wp * dbndry(nutty) * h
          if (bug1 /= 0) write (fo,'(8e15.6)') x, u(1:nbtp1)

! evaluate fr at ra
          call derfun
          km = km + 1
          do k = 1, nbtp1
             fr(k) = frh(k) * hs / 12.0_wp
          end do

! evaluate frm at ra+h/2
          x = ra + 0.5_wp * h
          do k = 1, nbtp1
             frh(k) = u(k) - du(k) + onept5 * fr(k)
          end do
          frhval = tlc / x / x - wr - onept5 * povalu(km-1) + &
               0.5_wp * povalu(km-2)
          do k = 1, nbt
             frm(k) = frh(k) * frhval
             frm(k) = frm(k) + onept5 * p(k,km-1) - 0.5_wp * p(k,km-2)
             frm(k) = frm(k) * h * h / 3.0_wp
          end do
          frm(nbtp1) = frh(nbtp1) * frhval
          frm(nbtp1) = frm(nbtp1) * h * h / 3.0_wp
          jr = npts
          x = ra
          km = km - 1

! add contribution to orthogonality integrals from first point
          if (nutty /= 1 .and. nbtm /= 0) then
             do jl = 1, nbtm
                orth(jl) = p(jl,km) * u(1) * h
             end do
          end if
          do j = 1, nbt
             do i = 1, nbtp1
                sdelt(i,j) = u(i) * p(j,km) * h
             end do
          end do

! store the function at ra
          if (nutty == 1) then
             do i = 1, nbtp1
                us(i,jr) = u(i)
                dus(i,jr) = 2.0_wp * du(i) / h
             end do
          else
             orb(jr) = u(1)
             dorb(jr) = 2.0_wp * du(1) / h
          end if

! integrate in to the matching point
          lswt = 1
          intxq_loop: do intx = 1, nix
             h1 = h
             imt = nix - intx + 1
             h = - hint * REAL(ihx(imt),wp)
             hs = h * h
             if (imt /= 1) then
                i1 = irx(imt-1)
             else
                i1 = 0
             end if
             i2 = irx(imt)
             if (imatch > i1) i1 = imatch
             i3 = i1 + 1
             if (intx /= 1) lswt = 2

! integrate over a range of equal integrals
             irp_loop: do ir = i3, i2
                jr = jr - 1
                call devgl (nbtp1, du, fr, frm, h, hs, h1, lswt)
                if (bug1 == 1) write (fo,'(8e15.6)') x, u(1:nbtp1)
! calculate the integration factor
                if (ir /= i2) then
                   if (MOD(ir,2) /= 0) then
                      am = 4.0_wp
                   else
                      am = 2.0_wp
                   end if
                else 
                   if (i1 /= imatch) then
                      am = REAL(ihx(imt)+ihx(imt-1),wp) / &
                           REAL(ihx(imt),wp)
                   else
                      am = 1.0_wp
                   end if
                end if

! add in the contribution to the integral from the current point
                do j = 1, nbt
                   do i = 1, nbtp1
                      sdelt(i,j) = sdelt(i,j) - u(i) * p(j,km) * &
                           h * am
                   end do
                end do
                
! store the functions at each interation
                if (nutty == 1) then
                   do i = 1, nbtp1
                      us(i,jr) = u(i)
                      dus(i,jr) = 2.0_wp * du(i) / h
                   end do
                else
                   orb(jr) = u(1)
                   dorb(jr) = 2.0_wp * du(1) / h
                   if (nbtm /= 0) then
                      do jl = 1, nbtm
                         orth(jl) = orth(jl) - u(1) * p(jl,km) * &
                              h * am
                      end do
                   end if
                end if

                if (jr == imatch+1) exit intxq_loop
             end do irp_loop
          end do intxq_loop

! store functions and derivatives at matching point for inward integration
          if (nutty == 1) then
             do i = 1, nbtp1
                yin(i) = u(i)
                dyin(i) = 2.0_wp * du(i) / h
             end do
          end if
       end do nutty_loop

       nbt = nbtm
       ng = nbt

! set up the matching equations
       nbtp1 = nbt + 1
       nbtp2 = nbt + 2
       bdl(1:nbtp2) = 0.0_wp
       bdl(nbtp1) = 1.0_wp
       if (nbt /= 0) then
          do i = 1, nbt
             do j = 1, nbt
                adel(i,j) = delt(j,i) + sdelt(j,i)
             end do
             adel(i,nbtp1) = delt(nbtp1,i)
             adel(i,nbtp2) = sdelt(nbtp1,i)
          end do
       end if

       nbtp3 = nbtp2 + 1
       bdl(nbtp3) = 0.0_wp
       if (nbt /= 0) then
          do i = 1, nbt
             adel(i,nbtp3) = orth(i)
             adel(nbtp2,i) = yout(i) - yin(i)
             adel(nbtp3,i) = dyout(i) - dyin(i)
          end do
       end if
       adel(nbtp1,1:nbtp3) = 0.0_wp
       adel(nbtp1,nbtp1) = 1.0_wp
       adel(nbtp2,nbtp1) = yout(nbtp1)
       adel(nbtp3,nbtp1) = dyout(nbtp1)
       adel(nbtp2,nbtp2) = - yin(nbtp1)
       adel(nbtp2,nbtp3) = - u(1)
       adel(nbtp3,nbtp3) = - 2.0_wp * du(1) / h
       adel(nbtp3,nbtp2) = - dyin(nbtp1)
       if (bug3 /= 0) then
          write (fo,'(a)') 'arrays for sleqn'
          do m = 1, nbtp3
             write (fo,'(20x,7f15.7)') (adel(m,n),n=1,nbtp3),bdl(m)
          end do
       end if
       call sleqn (adel, nbt+3, bdl)
       nbtp2 = nbtp3
! store energy eigenvalue and form continuous solution in orb
       if (bug3 /= 0) then
          write (fo,'(a)') 'matching solution'
          write (fo,'(8e15.6)') bdl(1:nbtp2)
          write (fo,'(a,i4)') 'matching point = ', imatch
       end if

! evaluate the final unnormalized function at the mesh points
       nbtn6 = nbt + 2
       orbmax = 0.0_wp
       do i = 1, imatch
          orb(i) = 0.0_wp
          dorb(i) = 0.0_wp
          do j = 1, nbtp1
             orb(i) = orb(i) + us(j,i) * bdl(j)
! calculate maximum value of orbital to scale to prevent overflow
             orbmax = MAX(orbmax, ABS(orb(i)))
             dorb(i) = dorb(i) + dus(j,i) * bdl(j)
          end do
       end do
       i1 = imatch + 1
       do i = i1, npts
          orb(i) = orb(i) * bdl(nbtp2) + us(nbtp1,i) * bdl(nbtn6)
          orbmax = MAX(orbmax, ABS(orb(i)))
          dorb(i) = dorb(i) * bdl(nbtp2) + dus(nbtp1,i) * bdl(nbtn6)
          if (nbt /= 0) then
             do j = 1, nbt
                orb(i)=  orb(i) + us(j,i) * bdl(j)
                dorb(i) = dorb(i) + dus(j,i) * bdl(j)
             end do
          end if
       end do
       bvalue = bdl(nbtp2) * ra / orb(npts)

    end if   ! end of inwards integration block

! normalize the solution and the lagrange multipliers
! test for overflow during normalization and scale if necessary
    if (orbmax > qlarge) then
       scalen = qlarge / orbmax
    else
       scalen = 1.0_wp
    end if
    x1 = 0.0_wp
    i1 = 1
    do i = 1, nix
       h = hint * REAL(ihx(i),wp)
       i2 = irx(i) + 1
       if (i /= 1) i1 = irx(i-1) + 1
       do j = i1, i2
          if (MOD(j,2) == 0) then
             am = 4.0_wp
          else
             if (j /= i1 .and. j /= i2) then
                am = 2.0_wp
             else
                am = 1.0_wp
             end if
          end if
          scaorb = scalen * orb(j)
          x1 = x1 + scaorb * scaorb * h * am
       end do
    end do

    if (bug3 /= 0) write (fo,'(a,f15.8)') 'x1 = ', x1
    x2 = scalen * SQRT(3.0_wp / x1)
    nodes = 0
    x3 = x2
    if (orb(2) < 0.0_wp) x3 = - x2

! evaluate nodes, the number of nodes in the final function
    orb1 = orb(1)
    do i = 2, npts
       orb2 = orb(i) * x3
       orb(i) = orb2
       dorb(i) = dorb(i) * x3
       if (orb1*orb2 < 0.0_wp) nodes = nodes + 1
       if (orb1 < 0.0_wp .and. orb2 == 0.0_wp) nodes = nodes + 1
       if (orb1 > 0.0_wp .and. orb2 == 0.0_wp) nodes = nodes + 1
       orb1 = orb2
    end do
  end subroutine basfun

  subroutine derfun
  use bound_basis, only: p
! evaluates the second derivative function for the de vogelaere
! routine devgl
    real(wp)       :: frhval
    integer        :: i

    km = km + mmm
    frhval = tlc / (x * x) - wr - povalu(km)
    if (frhval*fm >= 0.0_wp) then
       fm = frhval
    else
       if (itst /= 1) then
          imatch = jr
          if (MOD(imatch,2) /= 0) imatch = imatch - 1
          itst = 1
       end if
    end if
    frh(nbtp1) = frhval * u(nbtp1)
    if (ng /= 0) then
       do i = 1, ng
          frh(i) = frhval * u(i) + p(i,km)
       end do
    end if
  end subroutine derfun

  subroutine devgl (m, dy, fr, frm, h, hs, h1, lswt)
! de vogelaere integration routine
    integer, intent(in)        :: m
    real(wp), intent(inout)    :: dy(m)
    real(wp), intent(inout)    :: fr(m), frm(m)
    real(wp), intent(in)       :: h
    real(wp), intent(in)       :: hs
    real(wp), intent(in)       :: h1
    integer, intent(inout)     :: lswt
    real(wp)                   :: yr(m)
    real(wp)                   :: h12, h12s, hh
    integer                    :: i

    hh = 0.5_wp * h
    x = x + hh
    if (lswt <= 1) then
       do i = 1, m
          dy(i) = dy(i) + fr(i)
          yr(i) = u(i) + dy(i)
          u(i) = yr(i) + fr(i) - 0.125_wp * frm(i)
       end do
    else
       h12 = h / h1
       h12s = h12 * h12
       lswt = 1
       do i = 1, m
          dy(i) = h12 * dy(i) + h12s * fr(i)
          yr(i) = u(i) + dy(i)
          u(i) = yr(i) + 0.5_wp * h12s * (fr(i) * (h12 + 1.0_wp) - &
               0.25_wp * h12 * frm(i))
       end do
    end if
    call derfun
    do i = 1, m
       frm(i) = hs * frh(i) / 3.0_wp
       dy(i) = dy(i) + frm(i)
       u(i) = yr(i) + dy(i)
    end do
    x = x + hh
    call derfun
    do i = 1, m
       fr(i) = hs * frh(i) / 12.0_wp
       dy(i) = dy(i) + fr(i)
    end do
  end subroutine devgl

  subroutine potf
! calculate potential function used to generate continuum orbitals.
! npot = 0: static potential using target configuration in istat
! npot > 0: potential evaluated using parameters ipot, cpot and xpot
    use rm_data, only: nelc, nz
    use rm_xdata, only: lrang1, maxnhf
    use bound_basis, only: uj, ipos, cpot, xpot, ipot, npot, istat, p
    use radial_grid, only: irx, nix, xr, step, wt, npts
    use error_prt, only: alloc_error
    use superstructure, only: px
    integer             :: i9, i, j, k, nq, numorb, l, ix, k1, k2
    integer             :: status, nbtx
    real(wp)            :: znm, sum1, sum2, a, b, x, y, zn, anum
    real(wp)            :: h, r, va

    write (fo,'(a)') 'Subroutine Potf'
    i9 = 2 * irx(nix)
    zn = REAL(nz,wp)
    allocate (povalu(2*npts-2), stat=status)
    if (status /= 0) call alloc_error (status, 'potf', 'a')
    povalu = 0.0_wp

    select case (npot)
    case (0)               ! static potential
       write (fo,'(a)') !static potential'
       i_loop: do i = 1, lrang1
          j_loop: do j = i, maxnhf(i)
             nq = ipos(j,i)
             numorb = istat(i,j)
             if (numorb == 0) cycle
             anum = REAL(numorb,wp)
             k = irx(nix) + 1
             sum1 = 0.0_wp
             sum2 = 0.0_wp
             povalu(i9) = povalu(i9) + anum
             do l = i9-2, 2, -2
                k = k - 1
                sum1 = sum1 + 0.5_wp * step(k+1) * (uj(k,nq)**2 + &
                     uj(k+1,nq)**2)
                sum2 = sum2 + 0.5_wp * step(k+1) * (uj(k,nq)**2 / &
                     xr(k) + uj(k+1,nq)**2 / xr(k+1))
                povalu(l) = povalu(l) + (1.0_wp - sum1 + xr(k) * &
                     sum2) * anum
             end do
          end do j_loop
       end do i_loop
       do i = 2, i9, 2
          povalu(i) = 2.0_wp * (zn - povalu(i))
       end do
       va = povalu(i9)

! interpolate missing points using 4-point interpolation
       povalu(1) = 0.75_wp * (zn + povalu(2)) - 0.125_wp * povalu(4)
       ix = 5
       a = 9.0_wp / 16.0_wp
       b = 1.0_wp /16.0_wp
       povalu(3) = a * (povalu(4) + povalu(2)) - b * (povalu(6) + &
            2.0_wp * zn)
       do i = 1, nix
          do j = ix, 2*irx(i)-3, 2
             povalu(j) = a * (povalu(j+1) + povalu(j-1)) - b * &
                  (povalu(j+3) + povalu(j-3))
          end do
          if (i /= nix) then ! change of interval (step-size doubling)
             k1 = 2 * irx(i) + 1
             k2 = k1 - 2
             povalu(k1) = a * (povalu(k1+1) + povalu(k1-1)) - b * &
                  (povalu(k1+3) + povalu(k1-5))
             povalu(k2) = a * (povalu(k2+1) + povalu(k2-1)) - b * &
                  (povalu(k2+2) + povalu(k2-3))
             ix = k1 + 2
          else               ! final point to be interpolated
             k1 = 2 * irx(nix) - 1
             povalu(k1) = a * (povalu(k1+1) + povalu(k1-1)) - b * &
                  (povalu(k1-3) + REAL(2*(nz-nelc),wp))
          end if
       end do
       k = 1
       do i = 2, i9, 2
          k = k + 1
          povalu(i) = povalu(i) / xr(k)
          povalu(i-1) = povalu(i-1) / (xr(k) - 0.5_wp * step(k))
       end do

    case (1:)       ! parametric potential
       write (fo,'(a)') 'parametric potential'
       write (fo,'(a,10i12)') 'ipot ', ipot(1:npot)
       write (fo,'(a,10f12.7)') 'cpot ', cpot(1:npot)
       write (fo,'(a,10f12.7)') 'xpot ', xpot(1:npot)
       do i = 1, i9
          j = (i + 2) / 2
          x = xr(j)
          if (MOD(i,2) /= 0) x = 0.5_wp * (xr(j+1) + x)
          y = 0.0_wp
          do k = 1, npot
             y = cpot(k) * x**ipot(k) * EXP(-xpot(k)*x) + y
          end do
          povalu(i) = y
       end do
       va = povalu(i9) * xr(npts)

    case (-1)
! Using S.S orbitals, and a parametric potential involving
! the statistical model electric charge
       write (fo,'(a)') 'parametric potential using statistical model &
            &electric charge'
       do i = 1, i9
          j = (i + 2) / 2
          x = xr(j)
          if (MOD(i,2) /= 0) x = 0.5_wp * (xr(j+1) + x)
          povalu(i)= (((px(j) - zn) * nelc) / (nelc - 1) + zn) * 2.0_wp / x
       end do
    case default
       write (fo,'(a)') 'potf: invalid parameter npot = ', npot
       stop
    end select

    if (bug5 > 1) write (fo,'(5e16.8)') (povalu(i),i=1,i9,4)

    call create_orbout
    nbtx = MAX(SIZE(p,DIM=1), 1)
    allocate (frh(nbtx+1), u(nbtx+1), stat=status)
    if (status /= 0) call alloc_error (status, 'potf', 'a')
  end subroutine potf

  subroutine create_orbout
! create orbital output arrays
    use radial_grid, only: npts
    use rm_data, only: nrang2
    use rm_xdata, only: lrang2
    use error_prt, only: alloc_error
    integer           :: status

    allocate (orb(npts), dorb(npts), eigens(nrang2,lrang2), stat=status)
    if (status /= 0) call alloc_error (status, 'create_orbout', 'a')
  end subroutine create_orbout

end module basis_functions
