module cfps
! Time-stamp: "2003-11-25 07:32:04 cjn"
  use precisn, only: wp
  use io_units, only: fo
  implicit none

  integer, save, target, allocatable :: p2_table(:,:), p3_table(:,:)
  integer, save, target, allocatable :: p4_table(:,:), p5_table(:,:)
  integer, save, target, allocatable :: p6_table(:,:)
  integer, save, target, allocatable :: d2_table(:,:), d3_table(:,:)
  integer, save, target, allocatable :: d4_table(:,:), d5_table(:,:)
  logical, save         :: tables_exist = .false.
  integer, save, target :: j1(1), j2(3), j3(3), j4(3), j5(1), j6(1)
  integer, save, target :: k1(1), k2(5), k3(8), k4(16), k5(16)
  integer, pointer      :: pt(:,:), p(:), pp(:)

!$OMP THREADPRIVATE(p2_table,p3_table,p4_table,p5_table,p6_table, &
!$OMP& d2_table,d3_table,d4_table, d5_table, &
!$OMP& tables_exist, j1, j2, j3, j4, j5, j6, k1, k2, k3, k4, k5, pt, &
!$OMP& p, pp)

  private
  public cfp, erase_tables

contains

  subroutine cfp (lij, n, v, l, s, vp, lp, sp, coefp)
! choose appropriate fractional parentage routine
    integer, intent(in)        :: lij
    integer, intent(in)        :: n
    integer, intent(in)        :: v, l, s
    integer, intent(in)        :: vp, lp, sp
    real(wp), intent(out)      :: coefp

    select case (lij+1)
    case (1)                ! s-shells
       coefp = 1.0_wp
    case (2)                ! p-shells
       coefp = cfpp (n, s, l, sp, lp)
    case (3)                ! d-shells
       coefp = cfpd (n, s, l, v, sp, lp, vp)
    case default            ! f, g, ... up to 2 electrons
       coefp = cfpf (n, s, l, v, sp, lp, vp)
    end select
  end subroutine cfp

  function cfpp (n, s, l, sp, lp)
    real(wp)                      :: cfpp
    integer, intent(in)           :: n
    integer, intent(in)           :: s, l
    integer, intent(in)           :: sp, lp
    integer                       :: indi, indj
    real(wp)                      :: num, den

    if (.not.tables_exist) call define_tables
    select case (n)
    case (1)
       if (sp == 1 .and. lp == 0 .and. s == 2 .and. l == 1) then
          cfpp = 1.0_wp
          return
       else
          write (fo,'(a,3i6)') 'cfpp n=1 failure: ', sp, lp
          stop
       end if
    case (2)
       pt => p2_table
       p => j2
       pp => j1
    case (3)
       pt => p3_table
       p => j3
       pp => j2
    case (4)
       pt => p4_table
       p => j4
       pp => j3
    case (5)
       pt => p5_table
       p => j5
       pp => j4
    case (6)
       pt => p6_table
       p => j6
       pp => j5
    case default
       write (fo,'(a)') 'cfpp select failure: '
       stop
    end select
    indi = pmatch(s,l)   ! state index
    p => pp
    indj = pmatch(sp,lp)  ! parent index
    num = pt(indi, indj)
    den = pt(indi, SIZE(pt,dim=2))
    cfpp = SIGN(1.0_wp,num) * SQRT(ABS(num/den))
  end function cfpp

  function pmatch (s, l)
    integer                       :: pmatch
    integer, intent(in)           :: s, l
    integer                       :: i, j

    pmatch = 0
    if (l <= 2 .and. s <= 4) then
       j = 4 * s + l + 4
       do i = 1, SIZE(p)
          if (j == p(i)) then
             pmatch = i
             exit
          end if
       end do
    end if
    if (pmatch == 0) then
       write (fo,'(a,3i6)') 'pmatch failure: ', s, l
       stop
    end if
  end function pmatch

  function cfpd (n, s, l, v, sp, lp, vp)
    real(wp)                      :: cfpd
    integer, intent(in)           :: n
    integer, intent(in)           :: s, l, v
    integer, intent(in)           :: sp, lp, vp
    integer                       :: indi, indj, np
    real(wp)                      :: num, den, num1, den1, sgn

    if (.not.tables_exist) call define_tables
    if (n > 5) then
       np = 10 - n
       sgn = 1.0_wp
       print *,'IS THIS IT',s,sp,l,lp,vp
       if (MOD((s + sp - 7) / 2 + l + lp, 2) == 1) sgn = -1.0_wp
       if (MOD((vp-1) / 2, 2) == 1 .and. n == 6) sgn = - sgn
       num1 = sgn * (np + 1) * sp * (2 * lp + 1)
       den1 = n * s * (2 * l + 1)
    end if
    select case (n)
    case (1)
       if (v == 1 .and. s == 2 .and. l == 2 .and. sp == 1 .and. &
            lp == 0 .and. vp == 0) then
          cfpd = 1.0_wp
          return
       else
          write (fo,'(a,3i6)') 'cfpd n=1 failure: ', sp, lp, vp
          stop
       end if
    case (2)
       pt => d2_table
       p => k2
       pp => k1
    case (3)
       pt => d3_table
       p => k3
       pp => k2
    case (4)
       pt => d4_table
       p => k4
       pp => k3
    case (5)
       pt => d5_table
       p => k5
       pp => k4
    case (6)
       pt => d5_table
       p => k5
       pp => k4
    case (7)
       pt => d4_table
       p => k4
       pp => k3
    case (8)
       pt => d3_table
       p => k3
       pp => k2
    case (9)
       pt => d2_table
       p => k2
       pp => k1
    case (10)
       if (v == 0 .and. s == 1 .and. l == 0 .and.    &
            vp == 1 .and. sp == 2 .and. lp == 2) then
          cfpd = 1.0_wp
          return
       else
          write (fo,'(a,3i6)') 'cfpd n=10 failure: ', sp, lp, vp
          stop
       end if
    case default
       write (fo,'(a)') 'cfpd select failure: '
       stop
    end select
    if (n <= 5) then
       indi = match(s,l,v)   ! state index
       p => pp
       indj = match(sp,lp,vp)  ! parent index
       num = pt(indi, indj)
       den = pt(indi, SIZE(pt,dim=2))
       cfpd = SIGN(1.0_wp,num) * SQRT(ABS(num/den))
    else
       indi = match(sp,lp,vp)   ! state index
       p => pp
       indj = match(s,l,v)  ! parent index
       num = pt(indi, indj)
       den = pt(indi, SIZE(pt,dim=2))
       cfpd = SIGN(1.0_wp,num*num1) * SQRT(ABS(num*num1/(den*den1)))
    end if
  end function cfpd

  function match (s, l, v)
    integer                       :: match
    integer, intent(in)           :: s, l, v
    integer                       :: i, j

    match = 0
    if (l <= 6 .and. s <= 6 .and. v <= 5) then
       j = 64 * s + 8 * l + v + 8
       do i = 1, SIZE(p)
          if (j == p(i)) then
             match = i
             exit
          end if
       end do
    end if
    if (match == 0) then
       write (fo,'(a,3i6)') 'match failure: ', s, l, v
       stop
    end if
  end function match

  function cfpf (n, s, l, v, sp, lp, vp)
! dummy routine to calculate cfp of f-electrons (or g-...).
! up to 2 electrons in shell
    real(wp)               :: cfpf
    integer, intent(in)    :: n
    integer, intent(in)    :: v, l, s
    integer, intent(in)    :: vp, lp, sp
    cfpf = 1.0_wp
  end function cfpf

  subroutine define_tables
    integer            :: status

    integer, parameter :: l2p(3) = (/1,2,0/)
    integer, parameter :: s2p(3) = (/3,1,1/)
    integer, parameter :: l3p(3) = (/0,2,1/)
    integer, parameter :: s3p(3) = (/4,2,2/)
    integer, parameter :: l4p(3) = (/1,2,0/)
    integer, parameter :: s4p(3) = (/3,1,1/)
    integer, parameter :: l5p(1) = (/1/)
    integer, parameter :: s5p(1) = (/2/)

    integer, parameter :: l2(5) = (/3,1,4,2,0/)
    integer, parameter :: s2(5) = (/3,3,1,1,1/)
    integer, parameter :: v2(5) = (/2,2,2,2,0/)

    integer, parameter :: l3(8) = (/3,1,5,4,3,2,2,1/)
    integer, parameter :: s3(8) = (/4,4,2,2,2,2,2,2/)
    integer, parameter :: v3(8) = (/3,3,3,3,3,1,3,3/)

    integer, parameter :: l4(16) = (/2,5,4,3,3,2,1,1,6,4,4,3,2,2,0,0/)
    integer, parameter :: s4(16) = (/5,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1/)
    integer, parameter :: v4(16) = (/4,4,4,2,4,4,2,4,4,2,4,4,2,4,0,4/)

    integer, parameter :: l5(16) = (/0,4,3,2,1,6,5,4,4,3,3,2,2,2,1,0/)
    integer, parameter :: s5(16) = (/6,4,4,4,4,2,2,2,2,2,2,2,2,2,2,2/)
    integer, parameter :: v5(16) = (/5,5,3,5,3,5,3,3,5,3,5,1,3,5,3,5/)

    if (tables_exist) call erase_tables
    j1 = (/13/)
    j2 = 4 * s2p + l2p + 4
    j3 = 4 * s3p + l3p + 4
    j4 = 4 * s4p + l4p + 4
    j5 = 4 * s5p + l5p + 4
    j6 = (/8/)
    k1 = (/153/)
    k2 = 64 * s2 + 8 * l2 + v2 + 8
    k3 = 64 * s3 + 8 * l3 + v3 + 8
    k4 = 64 * s4 + 8 * l4 + v4 + 8
    k5 = 64 * s5 + 8 * l5 + v5 + 8

    allocate (d2_table(5,2), d3_table(8,6), d4_table(16,9), &
         d5_table(16,17), p2_table(3,2), p3_table(3,4),     &
         p4_table(3,4), p5_table(1,4), p6_table(1,2), stat = status)
    if (status /= 0) then
       write (fo,'(a,i6)') 'define_tables: allocation status = ', status
       stop
    end if

    p2_table(:,1) = (/1,    1,    1/)
    p2_table(:,2) = (/1,    1,    1/)

    p3_table(:,:3) = reshape( shape = (/3,3/), source =     &
         (/   1,   1,  -9, 0, - 1,  -5,  0,  0,  4/))
    p3_table(:,4) = (/1, 2, 18/)

    p4_table(:,:3) = reshape( shape = (/3,3/), source =     &
         (/   -4,   0,  0, 5, -3,  0,  -3,  -1,  1/))
    p4_table(:,4) = (/12, 4, 1/)

    p5_table(1,:3) = (/ 9,   5,  1/)
    p5_table(1,4) = 15
    p6_table(1,1) = 1
    p6_table(1,2) = 1

    p3_table(:,4) = (/1, 2, 18/)
    d2_table(:,1) = (/1,    1,    1,    1,    1/)
    d2_table(:,2) = (/ 1, 1, 1, 1, 1/)

    d3_table(:,:5) = reshape( shape = (/8,5/), source =      &
            (/ 4,   -7,   -1,   21,    7,  -21,   21,   -8, &
              -1,   -8,    0,    0,   28,   -9,  -49,    7, &
               0,    0,    1,   11,  -25,   -9,  -25,    0, &
               0,    0,    0,  -10,  -10,   -5,   45,   15, &
               0,    0,    0,    0,    0,   16,    0,    0/))
    d3_table(:,6) = (/5,   15,    2,   42,   70,   60,  140,   30/)

    d4_table(:,:8) = reshape( shape = (/16,8/), source =   &
             (/7,   20, -560,  224, -112,  -21,  -56,   16,&
               0,    0,    0,    0,    0,    0,    0,    0,&
               3,    0,    0,  -56, -448,   49,  -64,  -14,&
               0,    0,    0,    0,    0,    0,    0,    0,&
               0,   26,  308,  110,  220,    0,    0,    0,&
               7, -154,  -28, -132,    0,    0,    0,    0,&
               0,   -9,  297,   90, -405,   45,    0,    0,&
               3,   66, -507,   -3,  -60,   15,    0,    0,&
               0,    5,  315,  -14, -175,  -21,  -56,  -25,&
               0,   70,  385, -105,   28,   63,    0,    0,&
               0,    0,    0,  315,    0,    0,  135,    0,&
               0,  189,    0,    0,  105,    0,    1,    0,&
               0,    0,  200,   15,  120,   60,  -35,   10,&
               0,  -25,   88,  200,   45,   20,    0,    1,&
               0,    0,    0,   16, -200,  -14,  -14,   25,&
               0,    0,    0,  120,  -42,   42,    0,    0/))
    d4_table(:,9) = (/10,   60, 1680,  840, 1680,  210,  360, &
         90, 10,  504, 1008,  560,  280,  140,    1,    1/)


    d5_table(:,:8) = reshape( shape = (/16,8/), source = &
         (/    1, -105, -175, -175,  -75,    0,    0,    0, &
               0,    0,    0,    0,    0,    0,    0,    0, &
               0,  154, -110,    0,    0,  231,  286,  924, &
            -308,  220, -396,    0,    0,    0,    0,    0, &
               0,  -66,  -90,  180,    0,   99,  -99,  891, &
           -5577, -405,   -9,    0,   45,   45,    0,    0, &
               0,    0,  224,    0,  -56,    0, -220, 1680, &
               0,  112,    0,  -21,   21,    0,  -16,    0, &
               0,  -70,   14,  -84,   56,    0,   55,  945, &
            4235, -175, -315,    0,  -21,  189,  -25,    0, &
               0,   25,  -15, -135,   35,    0,    0,  600, &
             968,  120,  600,    0,   60,   60,   10,    3, &
               0,    0,  -56,    0,  -64,    0,    0,    0, &
               0,  448,    0,   -9,  -49,    0,   14,    0, &
               0,    0,  -16,  126,   14,    0,    0,    0, &
               0, -200,  360,    0,  -14,  126,   25,    0/))
    d5_table(:,9:16) =  reshape( shape=(/16,8/), source = &
         (/    0,    0,    0,    0,    0, -175,  182, -728,&
           -2184,    0,    0,    0,    0,    0,    0,    0,&
               0,    0,    0,    0,    0,    0,  220,  880,&
               0, -400,    0,   -9,  -25,    0,    0,    0,&
               0,    0,    0,    0,    0,  -45,   -5,  845,&
           -1215,  275,  495,    0,  -11,   99,    0,    0,&
               0,    0,    0,    0,    0,    0,   33,   -7,&
           -2541,  105, -525,    0,   35,   35,  -15,    0,&
               0,    0,    0,    0,    0,    0,    0, -800,&
               0, -160,    0,   -5,   45,    0,   30,    0,&
               0,    0,    0,    0,    0,    0,    0, -100,&
            1452,  180, -100,    0,  -10,   90,   15,   -2,&
               0,    0,    0,    0,    0,    0,    0,    0,&
               0,    0,    0,    6,    0,    0,    0,    0,&
               0,    0,    0,    0,    0,    0,    0,    0,&
               0,    0,    0,    0,  -14,  -56,    0,    0/))
    d5_table(:,17) =  (/ 1,  420,  700,  700,  300, 550, 1100, &
         8400, 18480, 2800, 2800,   50,  350, 700,  150,    5/)

    tables_exist = .true.
  end subroutine define_tables

  subroutine erase_tables
    integer           :: status
    deallocate (d2_table, d3_table, d4_table, d5_table, stat = status)
    if (status /= 0) then
       write (fo,'(a,i6)') 'erase_tables: deallocation status = ', status
       stop
    end if
    tables_exist = .false.
  end subroutine erase_tables
end module cfps
