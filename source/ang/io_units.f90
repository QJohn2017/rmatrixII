module io_units
! Time-stamp: "03/02/18 08:30:41 cjn"
  implicit none
  private
  public fi, fo, dsk, dskp1, dskp2
  public set_dsk, set_dskp1, set_dskp2
  public jbufiv, jbuffv, jbufie, jbuffe
  public jbufiv1, jbuffv1, jbufiv2, jbuffv2
  public jbufivt, jbuffvt
  integer, parameter      :: fi = 5    ! standard input
  integer, parameter      :: fo = 6    ! standard output
  integer, save           :: dsk = 0   ! scratch disk unit
  integer, save           :: dskp1 = 0   ! scratch disk unit
  integer, save           :: dskp2 = 0   ! scratch disk unit

! filehand:
  integer, save           :: jbufiv = 20
  integer, save           :: jbuffv = 21
  integer, save           :: jbufiv1 = 24
  integer, save           :: jbuffv1 = 25
  integer, save           :: jbufiv2 = 26
  integer, save           :: jbuffv2 = 27
  integer, save           :: jbufivt = 28
  integer, save           :: jbuffvt = 29

  integer, save           :: jbufie = 22
  integer, save           :: jbuffe = 23
contains
  subroutine set_dsk (dsk_unit)
    integer, intent(in)   :: dsk_unit
    dsk = dsk_unit
    return
  end subroutine set_dsk

  subroutine set_dskp1 (dsk_unit)
    integer, intent(in)   :: dsk_unit
    dskp1 = dsk_unit
    return
  end subroutine set_dskp1

  subroutine set_dskp2 (dsk_unit)
    integer, intent(in)   :: dsk_unit
    dskp2 = dsk_unit
    return
  end subroutine set_dskp2

end module io_units
