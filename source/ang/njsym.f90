module recoupling
! general recoupling coefficient between two coupling schemes
! Time-stamp: "2005-03-11 15:06:20 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use debug, only: bug3
  implicit none

  integer, parameter :: kfl2 = 19
  integer, parameter :: kfl3 = 20
  integer, parameter :: kfl4 = 100
  integer, parameter :: kfl5 = 200
  integer, parameter :: kfl6 = 12
  integer, parameter :: kfl7 = 100

! couple:
  integer, save :: m          ! total # momenta in both states
  integer, save :: n          ! # basic momenta
  integer, save :: j1(kfl7)   ! angular momenta (2j+1)
  integer, save :: j2(3,kfl2) ! initial state cplg indexes into j1
  integer, save :: j3(3,kfl2) ! final state cplg indexes into j1

! depths
  integer, save :: j4(kfl7)
  integer, save :: j5(kfl7)
  integer, save :: i3, i4, i5, i6, i7, i8, i9, i17, i18, i19, i20

  private
  public njsym, gensum
  public m, n, j1, j2, j3

contains
  subroutine njsym (j6c, j7c, j8c, jwc, k6, k7, k8, kw, recup)
! general recoupling programme
! evaluates the recoupling coefficient between two coupling schemes

! k6,k7,k8 and kw evaluated by njsym. the entry in each case corresponds
! to a position in the j1 array where the 2j+1 value is found if less
! than or equal to m, or to a summation variable if greater than m

! summation over the variables in k6,k7,k8 and kw and the evaluation
! of recup is carried out in gensum

! gensum can be re-entered directly to evaluate different recoupling
! coefficients with the same structure by just altering the numbers
! in the j1 array
! the value of k6 gives position in j1 array where j value is found
    integer, intent(out)   :: j6c  ! # of elements in the k6 array
    integer, intent(out)   :: j7c  ! # of elements in the k7 array
    integer, intent(out)   :: j8c  ! # of elements in the k8 array
    integer, intent(out)   :: jwc  ! # of elements in the kw array
    integer, intent(inout) :: k6(kfl4) ! factor sqrt(2j+1) in recup.
    integer, intent(inout) :: k7(kfl5) ! factor (-1)**j in recup
    integer, intent(inout) :: k8(kfl4) ! factor (-1)**(-j) in recup
    integer, intent(inout) :: kw(6,kfl3) ! 1 racah coeff in recup / col
    real(wp), intent(out)  :: recup      ! recoupling coefficient
    logical                :: fnd, fnd1, fnd2, skp
    integer                :: ip, i, mp, i1, i2, jj2, jj3, j, jp, iq
    integer                :: i10, i11, i12, i13, i14, i15, itest
    integer                :: i16, itest1

! check n, m against array sizes
    if (kfl2 < n-1) then
       write (fo,'(a)') 'njsym: kfl2 dimension failure'
       stop
    end if
    if (kfl7 < m) then
       write (fo,'(a)') 'njsym: kfl7 dimension failure'
       stop
    end if

! ip = # inequivalent triads to be recoupled. Initially set to total #
! then decreased in section 1 as recoupling proceeds, eventually zero
    ip = n - 1

    if (bug3 == 1) then
       write (fo,'(a)') ' j2                  j3'
       do i = 1, ip
          write (fo,'(3i5,i10,2i5)') j2(:,i), j3(:,i)
       end do
    end if

! set counts zero. mp is count on the j values which are summed over.
    j6c = 0
    j7c = 0
    j8c = 0
    jwc = 0
    mp = m     ! count on j values summed over

! s e c t i o n  1
! searches j2, j3 arrays for equivalent triads. If so, puts them at end
! of j2, j3; sets ip = # inequivalent triads. If ip=0 recouping complete
! Exit to gensum to carry out summations
    L117: do
       i1 = 1
       L16: do
          fnd = .false.
          do i2 = 1, ip
             if (j2(1,i2) == j3(1,i1)) then
                if (j2(2,i2) == j3(2,i1)) fnd = .true.
                exit
             end if
             if (j2(2,i2) == j3(1,i1)) then
                if (j2(1,i2) == j3(2,i1)) fnd = .true.
                exit
             end if
          end do
          if (.NOT.fnd) then
             i1 = i1 + 1
             if (i1 > ip) exit L16
             cycle L16
          end if
          if (i2 < ip) then
! rearrange so equivalent triads occur at end of j2, j3
             i3 = j2(1,i2)
             i4 = j2(2,i2)
             i5 = j2(3,i2)
             i6 = i2 + 1
             do i7 = i6, ip
                j2(:,i7-1) = j2(:,i7)
             end do
             j2(:,ip) = (/i3, i4, i5/)
          end if

          if (i1 < ip) then
             i3 = j3(1,i1)
             i4 = j3(2,i1)
             i5 = j3(3,i1)
             i6 = i1 + 1
             do i7 = i6, ip
                j3(:,i7-1) = j3(:,i7)
             end do
             j3(:,ip) = (/i3, i4, i5/)
          end if

! is 3rd element in j2 summed over? if so replace by 3rd element in j3
          skp = .false.
          if (j2(3,ip) <= m) then
             jj2 = j2(3,ip)
             jj3 = j3(3,ip)
             if (jj2 == jj3) then
                skp = .true.
             else
                if (j1(jj2) /= j1(jj3)) then
! recoupling coefficient set zero when triad in initial and final
! states do not match. gensum is not called and k6,k7,k8,kw  are
! not set ready for further direct entries to gensum.
                   if (bug3 == 1) write (fo,'(a)') 'njsym: recoup&
                        &ling coefficient set to zero, triangle does&
                        & not match'
                   recup = 0.0_wp
                   return
                end if
             end if
          end if
          if (.NOT.skp) then
             L44: do
                j = j3(3,ip)
                jp = j2(3,ip)
                j2(3,ip) = j
                if (ip >= 2)then
! replace all other els in j2,kw,k7,k8, k6 which are summed over at
!same time by the same quantity j
                   iq = ip-1
                   do i3 = 1, iq
                      do i4 = 1, 3
                         if (j2(i4,i3) == jp) j2(i4,i3) = j
                      end do
                   end do
                end if
                if (jwc > 0) then
                   do i = 1, 6
                      do i3 = 1, jwc
                         if (kw(i,i3) == jp) kw(i,i3) = j
                      end do
                   end do
                end if
                if (j7c > 0) then
                   do i3 = 1,  j7c
                      if (k7(i3) == jp) k7(i3) = j
                   end do
                end if
                if (j8c > 0) then
                   do i3 = 1, j8c
                      if (k8(i3) == jp) k8(i3) = j
                   end do
                end if
                if (j6c > 0) then
                   do i3 = 1, j6c
                      if (k6(i3) == jp) k6(i3) = j
                   end do
                end if
! reset i1 = 1 to start search for equivalent triads.
                i1 = 1
! test whether triangle matches
                jj2 = j2(3,ip)
                jj3 = j3(3,ip)
                if (jj2 == jj3) exit L44
                if (j1(jj2) /= j1(jj3)) then
! recoupling coefficient = 0. when triad in initial and final states
! do not match. gensum is not called; k6,k7,k8,kw are not set up
! ready for further direct entries to gensum.
                   if (bug3 == 1) write (fo,'(a)') 'recoupling coeff&
                        &icient set zero as triangle does not match'
                   recup = 0.0_wp
                   return
                end if
             end do L44
          end if

! if j2 a.m. are in opposite order to j3 a.m. interchange and store
! sign changes in k7 and k8.
          if (j2(1,ip) /= j3(1,ip)) then
             j = j2(1,ip)
             j2(1,ip) = j2(2,ip)
             j2(2,ip) = j
             k7(j7c+1:j7c+2) = j2(1:2,ip)
             j7c = j7c + 2
             k8(j8c+1) = j2(3,ip)
             j8c = j8c + 1
             if (kfl5 < j7c) then
                write (fo,'(a)') 'njsym: kfl5 dimension failure'
                stop
             end if
             if (kfl4 < j8c) then
                write (fo,'(a)') 'njsym: kfl4 dimension failure'
                stop
             end if
          end if
! decrease ip and return to look for further equivalent triads
          ip = ip - 1
          if (i1 > ip) exit L16
       end do L16

! if ip = 0 this means that all triads have been transformed to be
! equivalent. now exit to sum over racah coefficients
       if (ip <= 0) then
          if (bug3 /= 1) return
          write (fo,'(a,20i5)') 'j1 = ', j1(1:m)
          write (fo,'(a)') 'kw'
          if (jwc > 0) then
             do j = 1, jwc
                write (fo,'(6i5)') kw(1:6,j)
             end do
          else
             write (fo,'(a)') 'no kw arrays set'
          end if
          if (j6c > 0) then
             write (fo,'(a,38i3)') 'k6 = ', k6(1:j6c)
          else
             write (fo,'(a)') 'no k6 arrays set'
          end if
          if (j7c > 0) then
             write (fo,'(a,38i3)') 'k7 = ', k7(1:j7c)
          else
             write (fo,'(a)') 'no k7 arrays set'
          end if
          if (j8c > 0) then
             write (fo,'(a,38i3)') 'k8 = ', k8(1:j8c)
          else
             write (fo,'(a)') 'no k8 arrays set'
          end if
          return
       end if

! s e c t i o n  2
! itest = 0 determines the mimimum recoupling of j2 to obtain an
! equivalent triad to one in j3. Store row of j3 in itest1.
! itest = 1 determine recoupling of j2 to obtain an equivalent
! triad of itest1 row of j3.
       i10 = 9999
       itest = 0
       i1 = 1

! genj45 determines the level of each j in the coupling tree of j2, j3
! and stores the result in j4, j5 respectively
       L96: do

          call genj45 (ip)
!      look for j in j2 array which is same as first element in j3 array
          L95: do
             fnd1 = .false.
             L52: do i2  =  1, ip
                if (j2(1,i2) == j3(1,i1)) then
                   i3 = 1
                   i5 = i2
                   fnd1 = .true.
                   exit
                end if
                if (j2(2,i2) == j3(1,i1)) then
                   i3 = 2
                   i5 = i2
                   fnd1 = .true.
                   exit
                end if
             end do L52
! now look for j in j2 array which is same as other element in j3 array
             if (fnd1) then
                fnd2 = .false.
                L56: do i2 = 1, ip
                   if (j2(1,i2) == j3(2,i1)) then
                      i4 = 1
                      i6 = i2
                      fnd2 = .true.
                      exit
                   end if
                   if (j2(2,i2) == j3(2,i1)) then
                      i4 = 2
                      i6 = i2
                      fnd2 = .true.
                      exit
                   end if
                end do L56
! i7, i8 denote the position in j1 of the two common j values in j2, j3
                if (fnd2) then
                   i7 = j2(i3,i5)
                   i8 = j2(i4,i6)
! geni9 determines # recouplings of two elements of j2 necessary to
! obtain identical triads in j2, j3. This # + 2 is stored in i9

                   call geni9 (ip)

                   if (i9 < i10) then
! a smaller recoupling pair found. store lowest as j2(i13,i14) and
! highest as j2(i11,i12). i15, i16 contain level of these below
! common triads. Finally itest1 denotes triad in j3 for next entry
! to section 2 and is required if more than one recoupling
                      i10 = i9
                      i11 = i17
                      i12 = i19
                      i13 = i18
                      i14 = i20
                      i15 = i7
                      i16 = i8
                      itest1 = i1
                   end if
                end if
             end if
             if (itest /= 0) exit L95
!  i1 is only increased if searching for smallest recoupling pair
             i1 = i1 + 1
             if (i1 > ip) exit
          end do L95
          if (i10 >= 9999) then
! fail because no pair in j2 and j3 found which could be recoupled
             write (fo,'(a)') 'i10 failure in njsym'
             stop
          end if

! s e c t i o n  3
! the pair of j values that require the smallest # of recouplings of
! j2 to bring into the same order as j3 has now been found.
          if (i15 < i16) then
! i1, i2 denotes the level above the given levels of triad of els to
! be recoupled
             i1 = i15 - 1
             i2 = i16 - 2
          else
             i1 = i16 - 1
             i2 = i15 - 2
          end if
          i3 = i11
          i4 = i13
          i5 = i12
          i6 = i14
          if (i1 > 0) then ! find first element to be recoupled
             L72: do i = 1, i1
                do i7 = 1, ip
                   if (j2(1,i7) == j2(3,i3)) then
                      i5 = 1
                      i3 = i7
                      cycle L72
                   end if
                   if (j2(2,i7)== j2(3,i3)) then
                      i5 = 2
                      i3 = i7
                      cycle L72
                   end if
                end do
                i5 = 1
                i3 = i7
             end do L72
          end if

! 1st el to be recoupled is j2(i3,i5), now find 2nd el to be recoupled
          if (i2 > 0) then
             L80: do i = 1, i2
                do i7 = 1, ip
                   if (j2(1,i7) == j2(3,i4)) then
                      i6 = 1
                      i4 = i7
                      cycle L80
                   end if
                   if (j2(2,i7) == j2(3,i4)) then
                      i6 = 2
                      i4 = i7
                      cycle L80
                   end if
                end do
                i6 = 1
                i4 = i7
             end do L80
          end if

! second element to be recoupled is j2(i6,i4)
          if (i6 <= 1) then
! interchange els of i4 row of j2 if necessary; include signs in k7, k8
             k7(j7c+1) = j2(1,i4)
             k7(j7c+2) = j2(2,i4)
             j7c = j7c + 2
             k8(j8c+1) = j2(3,i4)
             j8c = j8c + 1
             i = j2(1,i4)
             j2(1,i4) = j2(2,i4)
             j2(2,i4) = i
          end if
          if (i5 <= 1) then
! interchange els of i3 row of j2 if necessary and store signs in k7, k8
             k7(j7c+1) = j2(1,i3)
             k7(j7c+2) = j2(2,i3)
             j7c = j7c + 2
             k8(j8c+1) = j2(3,i3)
             j8c = j8c + 1
             i = j2(1,i3)
             j2(1,i3) = j2(2,i3)
             j2(2,i3) = i
          end if

! now recouple the two elements of j2 and store square roots in k6
! and racah coeffs in kw arrays. mp = a j which will be summed over
          k6(j6c+1) = j2(3,i4)
          mp = mp + 1
          k6(j6c+2) = mp
          j6c = j6c + 2
          jwc = jwc + 1
          kw(1,jwc) = j2(1,i4)
          kw(2,jwc) = j2(2,i4)
          kw(3,jwc) = j2(3,i3)
          kw(4,jwc) = j2(2,i3)
          kw(5,jwc) = j2(1,i3)
          kw(6,jwc) = mp
          j2(1,i3) = j2(1,i4)
          j2(1,i4) = j2(2,i4)
          j2(2,i4) = j2(2,i3)
          j2(3,i4) = mp
          j2(2,i3) = mp

!      test dimensions and exit if failure
          if (kfl5 < j7c) then
             write (fo,'(a)') 'njsym: kfl5 dimension failure'
             stop
          end if
          if (kfl4 < j8c .or. kfl7 < mp .or. kfl4 < j6c) then
             write (fo,'(a)') 'njsym: kfl4, kfl7 dimension failure'
             stop
          end if
          if (kfl3 < jwc) then
             write (fo,'(a)') 'njsym: kfl3 dimension failure'
             stop
          end if
          if (i1+i2 <= 0) cycle L117
! more than one recoupling required. return to section 2 to decide
! which elements of j2 to recouple in next step. if all recouplings
! of a particular pair have been carried out then identical pairs
! are now present in j2 and j3 arrays. return to section 1 to see
! if any more recoupling required
          itest = 1
          i1 = itest1
          i10 = 9999
       end do L96
    end do L117
  end subroutine njsym

  subroutine gensum (j6c, j7c, j8c, jwc, j6, j7, j8, jw, j1, recup)
! carries out the summation over coefficients defined by the arrays
! j6,j7,j8 and jw to give recup
! entry is either made from njsym or directly assuming that j6,j7,j8,
! jw have already been determined by a previous entry to njsym and that
! the summation is required for another set of j values defined by j1
    use angular_momentum, only: cracah, dracah
    integer, intent(inout) :: j6c  ! # of elements in the k6 array
    integer, intent(inout) :: j7c  ! # of elements in the k7 array
    integer, intent(inout) :: j8c  ! # of elements in the k8 array
    integer, intent(inout) :: jwc  ! # of elements in the kw array
    integer, intent(inout) :: j6(kfl4) ! factor sqrt(2j+1) in recup.
    integer, intent(inout) :: j7(kfl5) ! factor (-1)**j in recup
    integer, intent(inout) :: j8(kfl4) ! factor (-1)**(-j) in recup
    integer, intent(inout) :: jw(6,kfl3) ! 1 racah / column
    integer, intent(inout) :: j1(kfl7)
    real(wp), intent(out)  :: recup ! recoupling coefficient
    integer  :: ist(6), jword(6,kfl3), j6p(kfl4), j7p(kfl5), j8p(kfl4)
    integer  :: jsum1(kfl6), jsum2(kfl6), jsum4(kfl6,kfl3), ipair(2,2)
    integer  :: jsum5(kfl6,kfl3), jsum3(kfl6), jsum6(kfl6)
    integer  :: jsum7(kfl6), jsum8(kfl6), jsum(2,kfl3), jwtest(kfl3)
    integer  :: i, j, maxjwe, jwrd, jj, i1, ix2, j6cp, j7cp, iastor
    integer  :: j8cp, nsum, maxsum, nsum1, i2, nct, nct1
    logical  :: skp, occured, cyc
    real(wp) :: x1, stor, stor1, wstor(kfl3)

! s e c t i o n  1
! evaluates all terms in j6,j7,j8 and jw which do not involve a
! summation and form modified arrays j6p,j7p,j8p and jword which do
! the result of the evaluation is stored in recup and aistor

! delete terms in j6 because cracah is used in this mod
    j6c = 0
    recup = 1.0_wp
    maxjwe = m
    jwrd = 0
    if (jwc > 0) then
! multiply recup by all racah coeffs which do not involve a summation
       L1: do i = 1, jwc
          L2: do j = 1, 6
             if (jw(j,i) > m) then
! jwrd = # racah coefficients which involve a summation
! jword(i,j),i=1,6,j=1,jwrd contains the # which give the location of
! the j values for the racah coefficients either in the j1 list or
! in the jsum1 list
                jwrd = jwrd + 1
                do jj = 1, 6
                   jword(jj,jwrd) = jw(jj,i)
! maxjwe contains the max j in the list of variables to be summed over
                   if (maxjwe < jw(jj,i))  maxjwe = jw(jj,i)
                end do
                cycle L1
             end if
          end do L2
          do j = 1, 6
             i1 = jw(j,i)
             ist(j) = j1(i1) - 1
          end do
          call cracah (ist(1), ist(2), ist(3), ist(4), ist(5), &
               ist(6), x1)
          recup = recup * x1
       end do L1
    end if

    j6cp = 0
    if (j6c > 0) then
! j6p(i),i=1,j6cp contains all j6 which involve  a summation
! multiply recup by all those which do not
       do i = 1, j6c
          if (j6(i) <= m) then
             i1 = j6(i)
             recup = recup * SQRT(REAL(j1(i1),wp))
          else
             j6cp = j6cp + 1
             j6p(j6cp) = j6(i)
          end if
       end do
    end if

    iastor = 0
    j7cp = 0
    if (j7c > 0) then
! j7p(i),i=1,j7cp contains all j7 which involve  a summation.
! multiply recup by all those which do not
       do i = 1, j7c
          if (j7(i) <= m) then
             i1 = j7(i)
             iastor = iastor + j1(i1) - 1
          else
             j7cp = j7cp + 1
             j7p(j7cp) = j7(i)
          end if
       end do
    end if

    j8cp = 0
    if (j8c > 0) then
! j8cp(i),i=1,j8cp contains all j8 which involve a summation
! multiply recup by all those which do not
       do i = 1, j8c
          if (j8(i) <= m) then
             i1 = j8(i)
             iastor = iastor - j1(i1) + 1
          else
             j8cp = j8cp + 1
             j8p(j8cp) = j8(i)
          end if
       end do
    end if

! no racah coefficients remaining and thus no summations to be
! carried out if jwrd=0. include (-1) factors in recup and exit
    if (jwrd <= 0) then
       if (j6cp+j7cp+j8cp > 0) then
          write (fo,'(a)') 'gensum: failure, no racah coeffs left'
          stop
       end if
       ix2 = iastor / 2
       if (MOD(ix2,2) == 1) recup = - recup
       return
    end if

! s e c t i o n  2
! search through the jword list to find all the summation variables
! nsum = # summation variables
! jsum1(i),i=1,nsum contains a list of all summation variables in
! the same notation as in jw list
    nsum = 0
    maxsum = maxjwe - m
    do i = 1, maxsum
       jsum6(i) = 0
       jsum7(i) = 0
    end do

! find summation variables
    L14: do i = 1, jwrd
       L15: do j = 1, 6
          if (jword(j,i) <= m) cycle L15
          nsum = nsum + 1
          if (nsum <= 1) then
             jsum1(nsum) = jword(j,i)
             i1 = nsum
          else
! if summation variable has not occured before, include in jsum1 list
             occured = .false.
             nsum1 = nsum - 1
             do i1 = 1, nsum1
                if (jword(j,i) == jsum1(i1)) then
                   nsum = nsum1
                   occured = .true.
                   exit
                end if
             end do
             if (.NOT.occured) then
                jsum1(nsum) = jword(j,i)
                i1 = nsum
             end if
          end if
! jsum6(i),i=1,nsum = # times each summation variable occurs in jword
          jsum6(i1) = jsum6(i1) + 1
          i2 = jsum6(i1)
! jsum4(i,j),jsum5(i,j),i=1,nsum,j=1,jsum6(i) is the position in
! the jword list where the jsum1 element occurs
          jsum4(i1,i2) = j
          jsum5(i1,i2) = i
! (jword-m) gives location in jsum1 list if a summation variable
          jword(j,i) = m + i1
       end do L15
    end do L14
    if (kfl6 < nsum) then
       write (fo,'(a)') 'gensum: kfl6 dimension failure'
       stop
    end if

    if (j6cp > 0) then
! check that no extra summation variables occur in j6p. set j6p
! equal to the location in jsum1 list of summation variable
       L28: do i = 1, j6cp
          do j = 1, nsum
             if (j6p(i) == jsum1(j)) then
                j6p(i) = j
                cycle L28
             end if
          end do
          write (fo,'(a)') 'gensum: L28 failure'
          stop
       end do L28
    end if

    if (j7cp > 0) then
! check that no extra summation variables occur in j7p, set j7p
! equal to the location in jsum1 list of summation variable
       L32: do i = 1, j7cp
          do j = 1, nsum
             if (j7p(i) == jsum1(j)) then
                j7p(i) = j
                cycle L32
             end if
          end do
          write (fo,'(a)') 'gensum: L32 failure'
          stop
       end do L32
    end if

    if (j8cp > 0) then
! check that no extra summation variables occur in j8p. set j8p
! equal to the location in jsum1 list of summation variable
       L39: do i = 1, j8cp
          do j = 1, nsum
             if (j8p(i) == jsum1(j)) then
                j8p(i) = j
                cycle L39
             end if
          end do
          write (fo,'(a)') 'gensum: L39 failure'
          stop
       end do L39
    end if

! s e c t i o n  3
! orders the summation variables so that the range of each
! summation has been previously defined
    nct = 0
    nct1 = 0
    L64: do
       L43: do i = 1, jwrd
          L44: do j = 1, 6
             i1 = jword(j,i) - m
             if (i1 <= 0) cycle L44
! jsum7(i),i=1,nsum is the order of the summations over the j
! variables. initially this array is zero
             if (jsum7(i1) > 0) cycle L44
! rows of ipair give limits of summation imposed by triangular condition
             select case (j)
             case (1)
                ipair(1,1) = jword(2,i)
                ipair(1,2) = jword(5,i)
                ipair(2,1) = jword(3,i)
                ipair(2,2) = jword(6,i)
             case (2)
                ipair(1,1) = jword(1,i)
                ipair(1,2) = jword(5,i)
                ipair(2,1) = jword(4,i)
                ipair(2,2) = jword(6,i)
             case (3)
                ipair(1,1) = jword(1,i)
                ipair(1,2) = jword(6,i)
                ipair(2,1) = jword(4,i)
                ipair(2,2) = jword(5,i)
             case (4)
                ipair(1,1) = jword(2,i)
                ipair(1,2) = jword(6,i)
                ipair(2,1) = jword(3,i)
                ipair(2,2) = jword(5,i)
             case (5)
                ipair(1,1) = jword(1,i)
                ipair(1,2) = jword(2,i)
                ipair(2,1) = jword(3,i)
                ipair(2,2) = jword(4,i)
             case (6)
                ipair(1,1) = jword(1,i)
                ipair(1,2) = jword(3,i)
                ipair(2,1) = jword(2,i)
                ipair(2,2) = jword(4,i)
             end select

! test whether range of summation has been defined. we choose the
! first pair of j values that define the range and store in jsum
             cyc = .true.
             L54: do i2 = 1, 2
                do i3 = 1, 2
                   if (ipair(i2,i3) <= m) cycle
                   i4 = ipair(i2,i3) - m
! jsum7 greater than zero means that limit is defined previously
                   if (jsum7(i4) <= 0) cycle L54
                end do
                cyc = .false.
                exit L54
             end do L54
             if (cyc) cycle L44

! nct is count on order of summation
             nct = nct + 1
             jsum7(i1) = nct
! jsum(i,j),i=1,2,j=1,nsum contains the position of the j values
! that define the range of each variable. the first row corresponds
! to the first j and the second row to the second j defining range.
! if value in range 1 to m then corresponds to an element in j1.
! if value greater than m then corresponds to a summation variable
! in jsum1 list. note that jsum does not necessarily contain the
! most restrictive ranges since only one of two possible pairs from
! the racah coefficient is taken
             do i3 = 1, 2
                jsum(i3,i1) = ipair(i2,i3)
             end do
          end do L44
       end do L43

! check whether the range of all summations set. fail if not
! possible to set all ranges
       if (nct < nsum) then
          if (nct <= nct1) then
             write (fo,'(a)') 'gensum: nct <= nct1 failure'
             stop
          end if
          nct1 = nct
          cycle L64
       else
! jsum8(i),i=1,nsum is the position in the jsum7 list where the ith
! summation is found
          L65: do j = 1, nsum
             do i1 = 1, nsum
                if (jsum7(i1) == j) then
                   jsum8(j) = i1
                   cycle L65
                end if
             end do
             i1 = nsum
             jsum8(j) = i1
          end do L65
          exit L64
       end if
    end do L64

! s e c t i o n  4
! carry out the summations.
! i6 denotes the first j that requires to be set to the lowest
! value in the range
! i7 = 0 the first time the js are set but but is set equal to 1
! on subsequent times
    i6 = 1
    i7 = 0
    L100: do
       if (i6 <= nsum) then
! jsum2(i),i=1,nsum contains current value of (2j+1) in the same
! order as jsum1 list. set jsum2 equal to lowest value in each range
          L68: do j = i6, nsum
             i1 = jsum8(j)
             if (jsum(1,i1) <= m) then
! first j defining range fixed
                i2 = jsum(1,i1)
                i3 = j1(i2)
             else
! first j defining range variable
                i2 = jsum(1,i1) - m
                i3 = jsum2(i2)
             end if
             if (jsum(2,i1) <= m) then
! second j defining range fixed
                i2 = jsum(2,i1)
                i4 = j1(i2)
             else
! second j defining range variable
                i2 = jsum(2,i1) - m
                i4 = jsum2(i2)
             end if
! set lower limit of range in jsum2
             jsum2(i1) = ABS(i3 - i4) + 1
          end do L68

! jsum3(i),i=1,nsum is 1 if j has altered from its previous value
! and is 0 if it is still the same
          do i = i6, nsum
             jsum3(i) = 1
          end do
          if (i7 <= 0) then
             i7 = 1
! jwtest(i),i=1,jwrd is 1 if required to evaluate racah coefficient
! and is 0 if value the same as before.jwtest is set zero the first
! time through and later set 1 if necessary
             jwtest(1:jwrd) = 0
! stor1 = product of racah coefficients times (2j+1) factors
! stor will contain sums of the stor1
             stor1 = 1.0_wp
             stor = 0.0_wp
          end if
       end if

! check the triangular relation for all j values in jword list.
! If a summation variable then value taken from jsum2 list
       skp = .true.
       L79: do j = 1, jwrd
          do i = 1, 6
             if (jword(i,j) <= m) then
                i1 = jword(i,j)
                ist(i) = j1(i1) - 1
             else
                i1 = jword(i,j) - m
                ist(i) = jsum2(i1) - 1
             end if
          end do
          if (ist(1)+ist(2) < ist(5) .or. ABS(ist(1)-ist(2)) > &
               ist(5)) then
             skp = .false.
             exit
          end if
          if (ist(3)+ist(4) < ist(5) .or. ABS(ist(3)-ist(4)) > &
               ist(5)) then
             skp = .false.
             exit
          end if
          if (ist(1)+ist(3) < ist(6) .or. ABS(ist(1)-ist(3)) > &
               ist(6)) then
             skp = .false.
             exit
          end if
          if (ist(2)+ist(4) < ist(6) .or. ABS(ist(2)-ist(4)) > &
               ist(6)) then
             skp = .false.
             exit
          end if
       end do L79

! fail one of the triangular relations. increase the j values
       L83: do
          if (.not.skp) then
             i2 = nsum
             L203: do
                i1 = jsum8(i2)
! increase a summation j value which is in jsum2 and set jsum3 to
! show value changed
                jsum2(i1) = jsum2(i1) + 2
                jsum3(i1) = 1
! now store j value  defining range of this j in i3 and i4.
                if (jsum(1,i1) <= m) then
                   i20 = jsum(1,i1)
                   i3 = j1(i20)
                else
                   i20 = jsum(1,i1) - m
                   i3 = jsum2(i20)
                end if
                if (jsum(2,i1) <= m) then
                   i20 = jsum(2,i1)
                   i4 = j1(i20)
                else
                   i20 = jsum(2,i1) - m
                   i4 = jsum2(i20)
                end if
                i5 = i3 + i4 - 1
                i6 = i2 + 1
! now test j values against maximum in range. if satisfied return
! to set remaining j values which depend on this j to their
! lowest values. if not return to increase preceding j value
                if (jsum2(i1) <= i5) then
                   skp = .false.
                   cycle L100
                end if
                i2 = i2 - 1
                if (i2 <= 0) then
! no more j values to sum over. the summation is therefore complete
! multiply by common factor and exit
                   recup = recup * stor
                   if (bug3 == 1) write (fo,'(3(a,f12.8))') 'recup = ',&
                        recup, ' stor = ', stor, ' stor1 = ', stor1
                   return
                end if
             end do L203
          end if
          skp = .false.

! triangular relations are satisfied. now evaluate racah coefficients
! first determine which racah coeffs need re-evaluating, set jwtest
          do j = 1, nsum
             if (jsum3(j) <= 0) cycle
             i2 = jsum6(j)
             do i1 = 1, i2
                i3 = jsum5(j,i1)
                jwtest(i3) = 1
             end do
          end do
! now evaluate all jwrd racah coefficients not already evaluated
          L109: do i = 1, jwrd
             if (jwtest(i) <= 0) cycle
             do i1 = 1, 6
                if (jword(i1,i) <= m) then
                   i2 = jword(i1,i)
                   ist(i1) = j1(i2) - 1
                else
                   i2 = jword(i1,i) - m
                   ist(i1) = jsum2(i2) - 1
                end if
             end do
             if (bug3 == 1) write (fo,'(a,6i6)') 'gensum (dracah) =', &
                  ist(1:6)
             call dracah (ist(1), ist(2), ist(3), ist(4), ist(5), &
                  ist(6), x1)
             wstor(i) = x1
          end do L109

!  wstor(i),i=1,jwrd contains the evaluated racah coefficients
          if (bug3 == 1) write (fo,'(a,10f10.6)') 'wstor = ', &
               wstor(1:jwrd)

! set jsum3 and jwtest to zero to indicate that racah coefficients
! need not be evaluated unless j value changes
          jsum3(1:nsum) = 0
          jwtest(1:jwrd) = 0
! form product of racah coeffs, (2j+1)- and (-1)- factors in stor1
          do i = 1, jwrd
             stor1 = stor1 * wstor(i)
          end do
!iastor contains the power of (-1)which is common to all terms
          ix2 = iastor
          if (j6cp > 0) then
             do i = 1, j6cp
                i1 = j6p(i)
                stor1 = stor1 * SQRT(REAL(jsum2(i1),wp))
             end do
          end if
          if (j7cp > 0) then
             do i = 1, j7cp
                i1 = j7p(i)
                ix2 = ix2 + jsum2(i1) - 1
             end do
          end if
          if (j8cp > 0) then
             do i = 1, j8cp
                i1 = j8p(i)
                ix2 = ix2 - jsum2(i1) + 1
             end do
          end if
          ix2 = ix2 / 2
! add term into stor and reset stor1 to 1 ready for next term
          if (MOD(ix2,2) == 1) stor1 = - stor1
          stor = stor + stor1
          stor1 = 1.0_wp
       end do L83
       exit L100
    end do L100
  end subroutine gensum

  subroutine genj45 (ip)
! find level of each j in coupling trees of j2, j3
! store in the j4, j5 respectively. if an element of j1 does not occur
! in j2 the j4 entry = -1; if an el does not occur in j3, j5 entry = -1
    integer, intent(in)   :: ip
    logical               :: skp1, skp
    integer               :: i, i2, i3, i4
    integer               :: ed16

    L1: do i = 1, m
       skp1 = .true.
       L2: do i2 = 1, ip ! store level of each j in j2 array in j4
          if (j2(1,i2) == i .or. j2(2,i2) == i) then
             skp1 = .false.
             exit
          end if
       end do L2
       if (.NOT.skp1) then
          i3 = 1
          L9: do
             do i4 = 1, ip
                if (j2(1,i4) == j2(3,i2) .or. j2(2,i4) == j2(3,i2)) then
                   i3 = i3 + 1
                   i2 = i4
                   cycle L9
                end if
             end do
             j4(i) = i3
             exit L9
          end do L9
       else
          j4(i) = -1
          do i2 = 1, ip
             if (j2(3,i2) == i) then
                j4(i) = 0
                exit
             end if
          end do
       end if

! store level of each j in j3 array in j5
       skp = .false.
       do i2 = 1, ip
          if (j3(1,i2) == i .or. j3(2,i2) == i) then
             skp = .true.
             exit
          end if
       end do
       if (.NOT.skp) then
          do i2 = 1, ip
             if (j3(3,i2) == i) then
                j5(i) = 0
                cycle L1
             end if
          end do
          j5(i) = -1
          cycle L1
       end if

       i3 = 1
       ed16 = 0
       L16: do
          ed16 = ed16 + 1
          if (ed16 > 1000) then
             write (fo,'(a)') 'ed16 (genj45) exceeds 1000'
             stop
          end if
          do i4 = 1, ip
             if (j3(1,i4) == j3(3,i2) .or. j3(2,i4) == j3(3,i2)) then
                i3 = i3 + 1
                i2 = i4
                cycle L16
             end if
          end do
          j5(i) = i3
          cycle L1
       end do L16
    end do L1
  end subroutine genj45

  subroutine geni9 (ip)
! determines # recouplings necessary to bring j2(i3,i5), j2(i4,i6) into
! same triad.
! Will give a triad identical with one in j3.
! on exit i9 contains # recouplings + 2, i7 contains level of i5 triad
! below the common triad and i8 contains the level of the i6 triad below
! the common triadsa
    integer, intent(in)    :: ip
    logical                :: skp
    integer                :: i, j, i1, i2

    i1 = j4(i7)
    i2 = j4(i8)
! determines which j of j2(i3,i5) and j2(i4,i6) lies lowest, store
! lowest as j2(i20,i18) and highest as j2(i19,i17)
    if (i1 <= i2) then
       i17 = i5
       i18 = i6
       i19 = i3
       i20 = i4
       i3 = i2 - i1
       i7 = 0
       i8 = i3
       i4 = i1
       if (i3 > 0) then
! i6 = lowest triad, scan triads for new triad i6 at same level as i5
          L4: do i = 1, i3
             do j = 1, ip
                if (j2(1,j) == j2(3,i6) .or. j2(2,j) == j2(3,i6)) then
                   i6 = j
                   cycle L4
                end if
             end do
             j = ip
             i6 = j
          end do L4
       end if
    else
       i17 = i6
       i18 = i5
       i19 = i4
       i20 = i3
       i3 = i1 - i2
       i7 = i3
       i8 = 0
! i5 = lowest triads. scan triads for new triad i6 at same level i5
       L9: do i = 1, i3
          do j = 1, ip
             if (j2(1,j) == j2(3,i5) .or. j2(2,j) == j2(3,i5)) then
                i5 = j
                cycle L9
             end if
          end do
          j = ip
          i5 = j
       end do L9
       i4 = i2
    end if

! i5, i6 now denotes triads at same level. i4 contains the common level
    L13: do i = 1, i4
       i1 = i
       if (i5 == i6) then
          i9 = i3 + 2 * i1
          i8 = i8 + i1
          i7 = i7 + i1
          return
       end if
! i5, i6 denote different triads scan to find triads at next
! level which replace i5 and i6
       skp = .false.
       do j = 1, ip
          if (j2(1,j) == j2(3,i5) .or. j2(2,j) == j2(3,i5)) then
             skp = .true.
             exit
          end if
       end do
       if (.NOT.skp) j = ip
       i5 = j
       do j = 1, ip
          if (j2(1,j) == j2(3,i6) .or. j2(2,j) == j2(3,i6)) then
             i6 = j
             cycle L13
          end if
       end do
       j = ip
       i6 = j
    end do L13

! i5 and i6 now both denote the common triad
    i9 = i3 + 2 * i1
    i8 = i8 + i1
    i7 = i7 + i1
  end subroutine geni9

end module recoupling
