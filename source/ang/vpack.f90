module vpack
! dynamic assignmenr of packing factors
! Time-stamp: "02/05/15 07:19:37 cjn"
  use precisn, only: wp
  use io_units, only: fo
  implicit none

  integer, save         :: mult(5)     ! packing factors
  integer, save         :: mult1       ! lambda packing factor
  logical, save         :: wrt2 = .false. ! 2-integer packing
  integer, save         :: mx1, mx2, mx3

  private
  public initialize_vpack
  public pck, pck2, pckl2, pck3, pck14, pck124, pckl
  public unpck, unpck2, unpckl2, unpck14, unpck3, unpck124, unpckl
  public max_pck, max_pck2, mx1, mx2, mx3, wrt2

contains

  subroutine initialize_vpack (norb, lrang1)
! find efficient packing factors
    integer, intent(in)   :: norb   ! # cortex orbitals
    integer, intent(in)   :: lrang1 ! # bound l-values
    integer               :: n, i, base
    integer               :: min_lambda, n_lambda, n5
    real(wp)              :: b

    n = huge(i)        ! maximum integer size
    n_lambda = 2 * lrang1 - 1
    min_lambda = MAX(2, 2 * lrang1 - 1)
    n5 = n_lambda + (n_lambda+1) * min_lambda
    mult1 = n_lambda + 1
    mult(5) = n / (n5 + 1)  ! # lamda's, lamlo factor
    b = SQRT(SQRT(REAL(mult(5),wp)))
    base = FLOOR(b)
    if (norb >= base) then
       b = SQRT(SQRT(REAL(n,wp)))
       base = FLOOR(b)
       wrt2 = .true.
       if (norb > base) then
          write (fo,'(a)') 'btest: packing failure'
          write (fo,'(a,i6)') 'two-integer orbital maximum = ', base
          write (fo,'(a,i6)') 'number of cortex orbitals   = ', norb
          stop
       end if
    end if
    mult(1) = 1
    do i = 2, 4
       mult(i) = mult(i-1) * base
    end do
    mx1 = mult(2) - 1
    mx2 = mult(2) * mult(2) - 1
    mx3 = n / mult(4) - 1
    write (fo,'(//,a)') 'Index packing:'
    write (fo,'(a, i10)') 'base = ', base
    write (fo,'(a,5i10)') 'mult = ', mult
    write (fo,'(a,i10)') 'max number of lambda values = ', n_lambda
    write (fo,'(a,3i10)') 'mx1, mx2, mx3 = ', mx1, mx2, mx3
    if (wrt2) write (fo,'(a,/)') 'Two-integer labels'
  end subroutine initialize_vpack
    
  pure function pck (i1, i2, i3, i4)
    integer             :: pck
    integer, intent(in) :: i1
    integer, intent(in) :: i2
    integer, intent(in) :: i3
    integer, intent(in) :: i4

    pck = mult(1) * i1 + mult(2) * i2 + mult(3) * i3 + mult(4) * i4
  end function pck

  pure function pck2 (i1, i2)
    integer             :: pck2
    integer, intent(in) :: i1
    integer, intent(in) :: i2
    pck2 = mult(1) * i1 + mult(2) * i2
  end function pck2

  pure function pckl2 (i1, i2)
    integer             :: pckl2
    integer, intent(in) :: i1
    integer, intent(in) :: i2
    pckl2 = i1 + mult1 * i2
  end function pckl2

  pure function pckl (i1, i2)
    integer             :: pckl
    integer, intent(in) :: i1
    integer, intent(in) :: i2
    if (wrt2) then
       pckl = i2
    else
       pckl = i1 + mult(5) * i2
    end if
  end function pckl

  pure function pck14 (i1, i4)
    integer             :: pck14
    integer, intent(in) :: i1
    integer, intent(in) :: i4
    pck14 = mult(1) * i1 + mult(4) * i4
  end function pck14

  pure function pck3 (i1, i2, i3)
    integer             :: pck3
    integer, intent(in) :: i1
    integer, intent(in) :: i2
    integer, intent(in) :: i3

    pck3 = mult(1) * i1 + mult(2) * i2 + mult(3) * i3
  end function pck3

  function pck124 (i1, i2, i4)
    integer             :: pck124
    integer, intent(in) :: i1
    integer, intent(in) :: i2
    integer, intent(in) :: i4

    if (i1 > mult(2)) then
       write (fo,'(a)') 'pck124: pack error, i1 exceeds base'
       write (fo,'(a,i10)') 'i1 = ', i1
       stop
    end if
    pck124 = mult(1) * i1 + mult(2) * i2 + mult(4) * i4
  end function pck124

  function max_pck ()
    integer          :: max_pck
    max_pck = mult(2) - 1
  end function max_pck

  function max_pck2 ()
    integer          :: max_pck2
    max_pck2 = mult1 - 1
  end function max_pck2

  pure subroutine unpck (label, i1, i2, i3, i4)
    integer, intent(in)  :: label     ! packed integer
    integer, intent(out) :: i1
    integer, intent(out) :: i2
    integer, intent(out) :: i3
    integer, intent(out) :: i4
    integer              :: p

    p = label
    i4 = p / mult(4)
    p = MOD(p, mult(4))
    i3 = p / mult(3)
    p = MOD(p, mult(3))
    i2 = p / mult(2)
    i1 = MOD(p, mult(2))
  end subroutine unpck

  pure subroutine unpck2 (label, i1, i2)
    integer, intent(in)  :: label     ! packed integer
    integer, intent(out) :: i1
    integer, intent(out) :: i2
    integer              :: p

    p = label
    i2 = p / mult(2)
    i1 = MOD(p, mult(2))
  end subroutine unpck2

  pure subroutine unpckl (label, i1, i5)
    integer, intent(in)  :: label     ! packed integer
    integer, intent(out) :: i1
    integer, intent(out) :: i5
    integer              :: p

    p = label
    i5 = p / mult(5)
    i1 = MOD(p, mult(5))
  end subroutine unpckl

  pure subroutine unpckl2 (label, i1, i2)
    integer, intent(in)  :: label     ! packed integer
    integer, intent(out) :: i1
    integer, intent(out) :: i2
    integer              :: p

    p = label
    i2 = p / mult1
    i1 = MOD(p, mult1)
  end subroutine unpckl2

  pure subroutine unpck3 (label, i1, i2, i3)
    integer, intent(in)  :: label     ! packed integer
    integer, intent(out) :: i1
    integer, intent(out) :: i2
    integer, intent(out) :: i3
    integer              :: p

    p = label
    i3 = p / mult(3)
    p = MOD(p, mult(3))
    i2 = p / mult(2)
    i1 = MOD(p, mult(2))
  end subroutine unpck3

  pure subroutine unpck14 (label, i1, i4)
    integer, intent(in)  :: label     ! packed integer
    integer, intent(out) :: i1
    integer, intent(out) :: i4
    integer              :: p

    p = label
    i4 = p / mult(4)
    i1 = MOD(p, mult(4))
  end subroutine unpck14

  pure subroutine unpck124 (label, i1, i2, i4)
    integer, intent(in)  :: label     ! packed integer
    integer, intent(out) :: i1
    integer, intent(out) :: i2
    integer, intent(out) :: i4
    integer              :: p

    p = label
    i4 = p / mult(4)
    p = MOD(p, mult(4))
    i2 = p / mult(2)
    i1 = MOD(p, mult(2))
  end subroutine unpck124
end module vpack
