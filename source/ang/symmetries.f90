module symmetries     ! ang version
! define target symmetry (SLpi) values
! Time-stamp: "2005-08-19 16:54:25 cjn"
  use io_units, only: fi, fo
  use error_prt, only: alloc_error
  implicit none

  integer, save                :: nset       ! # N-electron symmetries
  integer, allocatable, save   :: lset(:)    ! L-values
  integer, allocatable, save   :: isset(:)   ! S-values
  integer, allocatable, save   :: ipset(:)   ! pi-values

  integer, save                :: nast       ! # tgt states
  integer, allocatable, save   :: nstat(:)   ! tgts / symmetry
  integer, save                :: isplo      ! min spin, N system
  integer, save                :: ispup      ! max spin, N system
  integer, save                :: isran1
  integer, save                :: isran2

  integer, save                :: lrang3    ! max tgt L + 1
  integer, save                :: llpdim    ! 2 * lrang3 - 1

  integer, save                :: lrgs1
  integer, save                :: lrgs2

  private
  public   rd_set_data, prt_set_data
  public   nset, lset, isset, ipset, nast, nstat
  public   isran1, isran2
  public   isplo, ispup, llpdim, lrang3, lrgs1, lrgs2

contains

  subroutine rd_set_data (nset_tmp)
    use rm_data, only: nelc
    integer, intent(in)        :: nset_tmp ! # target symmetries
    integer                    :: is, i, status

    nset = nset_tmp
    allocate (lset(nset), isset(nset), ipset(nset), nstat(nset),&
         stat=status)
    if (status /= 0) call alloc_error (status, 'rd_set_data', 'a')

    nast = 0
    do is = 1, nset
       read (fi,*) lset(is), isset(is), ipset(is), nstat(is)
       if (MOD(isset(is)+nelc,2) == 0) then ! input data error
          write (fo,'(a,3i4)') 'rd_set_data: input error,     &
               &spin multiplicity, nelc inconsistent ', is,   &
               isset(is), nelc
          stop
       end if
       nast = nast + nstat(is)
    end do

    lrang3 = MAXVAL(lset) + 1
    llpdim = lrang3 + lrang3 - 1

    isran1 = MINVAL(isset)   ! min tgt spin
    isran2 = MAXVAL(isset)   ! max tgt spin
    isplo = isran1
    ispup = isran2
  end subroutine rd_set_data

  subroutine prt_set_data
    use rm_data, only: lrgslo, lrgsup
    character(len=1), parameter  :: parity(0:1) = (/'e', 'o'/)
    character(len=19), parameter :: lcspec = 'SPDFGHSPDFGHIJKLMNO'
    integer                      :: is, iq

    if (lrgslo < 0 .and. lrgsup == 0) then
       lrgs1 = isran1 - 1
       if (lrgs1 == 0) lrgs1 = 2
       lrgs2 = isran2 + 1
    else
       lrgs1 = lrgslo
       lrgs2 = lrgsup
    end if
    if (MOD(lrgs1+isran1,2) == 0 .or. MOD(lrgs2+isran2,2) == 0) then
       write (fo,'(2(a,i3))') 'stgard: input error, lrgslo = ',  &
            lrgs1, ' lrgsup = ', lrgs2
       stop
    end if

    write (fo,'(a,/)') 'N-electron symmetries:'
    write (fo,'(a,i3)') 'number of symmetries = ', nset
    if (lrang3 <= 13) then
       do  is = 1, nset
          write (fo,'(/22x,i2,1x,a1)') isset(is), parity(ipset(is))
          iq = 7 + lset(is)
          write (fo,'(3x,a,i3,9x,a1)') ' symmetry', is, lcspec(iq:iq)
       end do
    else
       write (fo,'(/,24x,a,/)') 'l    2s+1   parity'
       do is = 1, nset
          write (fo,'(3x,a,i3,8x,i2,5x,i2,7x,i1)') ' symmetry', &
               is, lset(is), isset(is), ipset(is)
       end do
    end if
  end subroutine prt_set_data

end module symmetries
