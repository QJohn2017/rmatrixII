program ang
! R-matrix II Angular coefficient program
! Time-stamp: "2005-08-09 13:30:46 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use error_prt, only: alloc_error
  use rm_data, only: lrgp, nz, ncfg, mpol, restart, ncfgp, maxorb
  use startup, only: stgard, multco, maxdep, nocorb, noccsh, nelcsh, &
       lrgllo, lrglup, ncore, maxnc, lrang1, nelc1, fsh, ncf,      &
       rdcfgp, noccshp, nocorbp, nelcshp, lrglb
  use symmetries, only: nset, lset, isset, ipset, isran1, &
       isran2, lrang3, lrgs1, lrgs2
  use dim, only: ldim
  use debug, only: bug2, bug9
  use filehand, only: initda
  use surface, only: prsurf, surfce
  use shell_coupling, only: equiv, bequiv, eq_estim
  use bb_angular_integrals, only: angbb, wr_abbi
  use bb_aux, only: set_files
  use cc_angular_integrals, only: angcc
  use bc_angular_integrals, only: angbc
  use bb_multipoles, only: angmbb
  use bc_multipoles, only: angmbc
  use plus_minus, only: minus, plus, est_plus, est_minus
  use sym_sets, only: putset, get_ncset
  use angular_momentum, only: ractab, factt
  use ls_states, only: allocate_lsvals, del_lsvals, reallocate_lsvals
  use coupling_limits, only: set_limits, climits
  use surfacing_tables, only: dealloc_surf_table, check_table
  use statistics, only: report_stat, reset_stat
  use cfg_pack, only: pck, npack, def_pck, del_pck, cpy_pck, npackp
  implicit none
  integer, allocatable :: lset1(:), isset1(:), ipset1(:)
  integer, allocatable :: neqsen(:,:), nshqn(:,:)
  integer, allocatable :: neqsen1(:,:), nshqn1(:,:)
  integer, pointer     :: nmefig(:,:), nmefig1(:,:)
  integer, pointer     :: nnq(:)
  integer, pointer     :: noccsh1(:), nocorb1(:,:), nelcsh1(:,:)
  integer, pointer     :: nnq1(:)
  integer, allocatable :: noccsh2(:), nocorb2(:,:), nelcsh2(:,:)
  integer, allocatable :: nnq2(:)
  integer, pointer     :: iparty(:), ncoumx(:), nqn1(:)
  integer, pointer     :: iparty1(:), ncoumx1(:), nqn11(:)
  integer, allocatable :: iparty2(:), ncoumx2(:), nqn12(:)
  type(pck), pointer   :: npack1(:), npack2(:), npackt(:)
  type(pck), pointer   :: nqpack(:), nqpack1(:), nqpack2(:)
  integer, pointer     :: ncset1(:)
  integer :: maxlnp, minlnp, maxsnp, minsnp, status
  integer :: maxln, maxsn, i1, maxlm, maxsm, maxlmm, maxsmm, minsmm
  integer :: ndiag, minsn, minsm, nqfig, lnmax, minlm, minlmm, minln
  integer :: mefig, mqfig, m2qfig, lmmmax, lmmax, m2efig, ncc3, nccx
  integer :: nelcp1, mxnbnd, lnpmax, isnp, ilup, lrgplo, lrgpup, ip
  integer :: npqfig, il, is, nefig2, nqfig2, mefig2, mqfig2, ncc1, ncc2
  integer :: lsset, lsset1, lsset2, npset, chs, nq2, np1, nm1, nm2
  integer :: c0, c1, cr, nefig, nelc, lrgslo, lrgsup, c2, c3, npefig
  real(wp) :: t0, t1, t2, t3

  call cpu_time(t0)
  call system_clock (count=c0)
! read data and set up n electron arrays
  call stgard (ndiag)
  call factt
  nefig = ncfg
  npefig = ncfgp
  nelc = nelc1
  lrgslo = lrgs1
  lrgsup = lrgs2
  call ldim

! set max. values of L,S for top cplg for N+1, N, N-1, N-2 electron cfgs
! n+1 cfgs only required if bound terms will occur.
! n-2 cfgs only required if ndiag = 1
  if (lrgllo <= lrglb) then
     maxlnp = MIN(lrglup, lrglb)
     minlnp = lrgllo
     maxsnp = lrgsup
     minsnp = lrgslo
     maxln = MAX(maxlnp+lrang1, lrang3) - 1
     minln = 0
     maxsn = MAX(maxsnp+1, isran2)
     if (minsnp > 1) then
        minsn = minsnp - 1
     else
        minsn = minsnp + 1
     end if
     minsn = MIN(minsn, isran1)
     maxdep = maxdep + 1
  else
     maxln = lrang3 - 1
     minln = 0
     maxsn = isran2
     minsn = isran1
  end if
  call set_limits (1, maxln, minln, maxsn, minsn)
  call set_limits (4, maxlnp, minlnp, maxsnp, minsnp)

  if (fsh < maxdep) then ! should not occur
     write (fo,'(a)') 'ANG: overflow in dimension fsh'
     write (fo,'(a,i4)') 'fsh must be > ', maxdep
     stop
  end if

! form factors for storing the quantum numbers of the occupied shells
  multco(1) = 1
  do i1 = 2, maxdep
     multco(i1) = multco(i1-1) * 17
  end do
  if (multco(maxdep)*17 < 0) then
     write (fo,'(a)') 'ANG: overflow in packing qn'
     stop
  end if

  maxlm = maxln + lrang1 - 1
  minlm = 0
  maxsm = maxsn + 1
  if (minsn > 1) then
     minsm = minsn - 1
  else
     minsm = minsn + 1
  end if
  if (ABS(ndiag) == 1) then
     maxlmm = lrang3 + lrang1 + lrang1 - 3
     minlmm = 0
     maxsmm = isran2 + 2
     minsmm = isran1 - 2
     if (minsmm <= 0) then
        minsmm = minsn
     end if
  end if
  call set_limits (2, maxlm, minlm, maxsm, minsm)
  call set_limits (3, maxlmm, minlmm, maxsmm, minsmm)

  call initda ! initiate filehand
  call prsurf ! prepare for surfacing; gensum arrays to maxdep

! Surfacing coeffs are calculated only for unique trees
! i.e. cfgs with same pattern of L,S that differ only in principal
! q.#s of the shells are considered as equivalent.
! nnq(in) = inq relates each N electron cfg, in, with its equivalent inq
! nqn1(inq)=in relates each equivalent cfg inq with in which is
! discovered first - in represents the set of cfgs equivalent to inq.

! ================= form equivalent N-electron configurations ==========
! If N-elec Hamiltonians are to be diagonalised call bequiv. not equiv
! (stores information about couplings for calculating B-B matrix els)
  if (bug2 == 1) write (fo,'(/,a,/)') 'Removing equivalent config&
       &urations:'
  lsset = 1
  call climits(1)
 ! determine the number of equivalent cfgs nq2, max coupling / cfg ncc1
  call def_pck (nqpack, nefig*maxdep)
  call eq_estim (0, nefig, noccsh, nocorb, nelcsh, 0, nq2, nqpack,&
       ncc1)   ! nq2 equiv cfgs, ncc1 = max # cplgs/cfg
  call allocate_lsvals (ncc1, nq2, lsset)
  call del_pck (nqpack)
  call def_pck (nqpack, nq2)
  allocate (iparty(nq2), ncoumx(nq2), nnq(nefig), nqn1(nq2), &
       stat=status)
  if (status /= 0) call alloc_error (status, 'ang:Ne', 'a')


  lnmax = 0
  if (ABS(ndiag) == 1) then
     allocate (neqsen(ncc1,nq2), nshqn(ncc1,nq2), stat=status)
     if (status /= 0) call alloc_error (status, 'ang', 'a')

     call bequiv (0, nefig, noccsh, nocorb, nelcsh, 0, nqfig, nqpack,&
          nnq, nqn1, iparty, ncoumx, neqsen, nshqn, lsset, lnmax)
  else
     call equiv (0, nefig, noccsh, nocorb, nelcsh, 0, nqfig, nqpack, &
          nnq, nqn1, iparty, ncoumx, lsset, lnmax)
  end if

! ================= form N-1 electron configurations ===================
! subtract electron from each shell in turn. Form linking array nmefig
  if (bug2 == 1) write (fo,'(/,a,/)') '(N-1)-electron configuration &
       &data:'
  call def_pck (npack1, nefig*maxdep)
  call est_minus (0, nefig, noccsh, nocorb, nelcsh, npack, 0, mefig, &
       npack1) ! there are mefig N-1 electron cfgs
  call del_pck (npack1)
  call def_pck (npack1, mefig)
  allocate (noccsh1(mefig), nocorb1(fsh,mefig), nelcsh1(fsh,mefig), &
       nnq1(mefig), nmefig(fsh,nefig), stat=status)
  if (status /= 0) call alloc_error (status, 'ang:N-1', 'a')
  noccsh1 = 0
  nocorb1 = 0
  nelcsh1 = 0
  nnq1 = 0
  nmefig = -777
  call minus (0, nefig, noccsh, nocorb, nelcsh, npack, 0, mefig, &
       noccsh1, nocorb1, nelcsh1, npack1, nmefig)

!......... form equivalent N-1 configurations
  if (bug2 == 1) write (fo,'(/,a,/)') 'Removing equivalent config&
       &urations:'
  call climits(2)
  lsset = 2
  call def_pck (nqpack1, mefig)
  call eq_estim (0, mefig, noccsh1, nocorb1, nelcsh1, 0, nm1, nqpack1,&
       ncc2)  ! nm1 = # equiv N-1 cfgs, ncc2 = max # cplgs/cfg
  call allocate_lsvals (ncc2, nm1, lsset)
  call del_pck (nqpack1)
  call def_pck (nqpack1, nm1)
  allocate (iparty1(nm1), ncoumx1(nm1), nqn11(nm1), stat=status)
  if (status /= 0) call alloc_error (status, 'ang:N-1e', 'a')
  call equiv (0, mefig, noccsh1, nocorb1, nelcsh1, 0, mqfig, nqpack1, &
       nnq1, nqn11, iparty1, ncoumx1, lsset, lmmax)

! form surfacing coeffs bring 1 electron from each shell of each N
! electron cfg leaving an N-1 electron cfg (omit for H-like targets).
  if (nelc > 1) then
     call climits (1,2)
     call cpu_time(t2)
     call system_clock (count=c2)
     call surfce (1, 0, nqfig, noccsh, nocorb, nelcsh, nqn1, nmefig, &
          nnq1, ncoumx, ncoumx1)
     call cpu_time (t3)
     write (fo,'(a,f16.4,a)') 'ST1: CPU time     = ', t3 - t2, ' secs'
     call system_clock (count=c3, count_rate=cr)
     write (fo,'(a,f16.4,a)') 'ST1: Elapsed time = ', REAL(c3-c2,wp) / &
          REAL(cr,wp), ' secs'
     call check_table (1)
  end if

! construct tables of Racah coeffs for l and for s; construct rme table.
  call ractab

! put N electron couplings into l,s,pi sets
  lsset = 1
  call putset (nefig, nqfig, nnq, nqn1, ncoumx, lsset, iparty, nset, &
       lset, isset, ipset)

  targets: if (ABS(ndiag) == 1) then ! calculate target data

!========================== form N-2 electron configurations ===========
! surfacing coeffs for N-1 leaving N-2 (omit for H-like targets)
     if (nelc > 1) then
        write (fo,'(a)') 'Target Hamiltonian is to be diagonalised'
        if (bug2 == 1) write (fo,'(/,a,/)') '(N-2)-electron config&
             &uration data:'
        call def_pck (npack2, mefig*maxdep)
        call est_minus (0, mefig, noccsh1, nocorb1, nelcsh1, npack1, 0,&
             m2efig, npack2)
        call del_pck (npack2)
        call def_pck (npack2, m2efig)
        allocate (noccsh2(m2efig), nocorb2(fsh,m2efig),              &
             nelcsh2(fsh,m2efig),  nnq2(m2efig), nmefig1(fsh,mefig), &
             stat=status)
        if (status /= 0) call alloc_error (status, 'ang:N-2', 'a')
        nmefig1 = -777

        call minus (0, mefig, noccsh1, nocorb1, nelcsh1, npack1, 0, &
             m2efig, noccsh2, nocorb2, nelcsh2, npack2, nmefig1)

!......... form equivalent N-2 configurations
        if (bug2 == 1) write (fo,'(/,a,/)') 'Removing equivalent &
             &configurations:'
        call climits(3)
        lsset = 3
        call def_pck (nqpack2, m2efig)
        call eq_estim (0, m2efig, noccsh2, nocorb2, nelcsh2, 0, nm2, &
             nqpack2, ncc3)
        call allocate_lsvals (ncc3, nm2, lsset)
        call del_pck (nqpack2)
        call def_pck (nqpack2, nm2)
        allocate (iparty2(nm2), ncoumx2(nm2), nqn12(nm2), stat=status)
        if (status /= 0) call alloc_error (status, 'ang:N-2e', 'a')

        call equiv (0, m2efig, noccsh2, nocorb2, nelcsh2, 0, m2qfig, &
             nqpack2, nnq2, nqn12, iparty2, ncoumx2, lsset, lmmmax)
     end if
     if (nelc > 2) then
        call climits (2,3)
        call cpu_time(t2)
        call system_clock (count=c2)
        call surfce (2, 0, mqfig, noccsh1, nocorb1, nelcsh1, nqn11, &
             nmefig1, nnq2, ncoumx1, ncoumx2)
        call cpu_time (t3)
        write (fo,'(a,f16.4,a)') 'ST2: CPU time     = ', t3 - t2, ' secs'
        call system_clock (count=c3, count_rate=cr)
        write (fo,'(a,f16.4,a)') 'ST2: Elapsed time = ', REAL(c3-c2,wp) / &
             REAL(cr,wp), ' secs'
        call check_table (2)
     end if

! write out the n-electron coupling data
     write (fo,'(/,a,/)') 'N-electron configuration couplings:'
     call wrtcup (nefig, nqfig, nnq, nqn1, ncoumx, 1, iparty, nset,&
          lset, isset, ipset)

! Angular coeffs to form target Hamiltonians for all l,s,pi
     lsset1 = 2
     lsset2 = 3
     call set_files (.true.)
     call angbb (nelc, nset, lset, isset, ipset, neqsen, nshqn,        &
          noccsh, nocorb, nelcsh, nmefig, nnq, lsset1, ncoumx1,        &
          noccsh1, nocorb1, nelcsh1, nmefig1, nnq1, lsset2, ncoumx2,   &
          nnq2, 1, 2, 1)
     call report_stat
     call reset_stat
     call dealloc_surf_table (2)
     if (nelc > 1) then
        call del_lsvals (lsset2)
        call del_pck (nqpack2)
        call del_pck (npack2)
        deallocate (noccsh2, nocorb2, nelcsh2, iparty2, ncoumx2, nnq2, &
            nqn12, nmefig1, stat=status)
        if (status /= 0) call alloc_error (status, 'ang:bnd', 'd')
     end if
  end if targets

!============= continuum-continuum angular integrals ===================
  scattering: if (ndiag >= 0) then  ! calculate scattering data
     if (.NOT.restart) then  ! skip if this is a restart run
        lsset = 2
        call angcc (nelc, nset, lset, isset, ipset, noccsh, nocorb, nelcsh, &
             nmefig, nnq, lsset, ncoumx1, nnq1, 1)
     end if

! skip if no bound terms i.e. no-exchange run
     if (lrgllo <= lrglb) then ! N+1 cfgs, add 1 elec to outermost shell

!================== form N+1 electron configurations ===================
        write (fo,'(/,a,/)') '(N+1)-electron configuration data:'
        if (npefig <= 0) then ! use plus to form N+1 electron cfgs
           call def_pck (npack2, nefig*maxorb)
           call est_plus (nefig, noccsh, nocorb, nelcsh, npack,    &
                npefig, npack2, nnq, iparty)
           call del_pck (npack2)
           call def_pck (npack2, npefig)
           allocate (noccsh2(npefig), nocorb2(fsh,npefig),         &
                nelcsh2(fsh,npefig), nnq2(npefig), stat=status)
           if (status /= 0) call alloc_error (status, 'ang:N+1', 'a')
           call plus (nefig, noccsh, nocorb, nelcsh, npack, npefig, &
                noccsh2, nocorb2, nelcsh2, npack2, nnq, iparty)
        else  ! read required N+1 electron configurations
           call rdcfgp (npefig)
           allocate (noccsh2(npefig), nocorb2(fsh,npefig),        &
                nelcsh2(fsh,npefig), nnq2(npefig), stat=status)
           if (status /= 0) call alloc_error (status, 'ang:N+1', 'a')
           call def_pck(npack2, npefig)
           npack2 = npackp
           noccsh2 = noccshp
           nocorb2 = nocorbp
           nelcsh2 = nelcshp
        end if

!......... form equivalent N+1 configurations
        if (bug2 == 1) write (fo,'(/,a,/)') 'Removing equivalent config&
             &urations:'
        lnpmax = -1
        call climits(4)
        lsset = 3
        call def_pck (nqpack2, npefig)
        call eq_estim (0, npefig, noccsh2, nocorb2, nelcsh2, 0, np1, &
             nqpack2, ncc2)
        call del_pck (nqpack2)
        call def_pck (nqpack2, np1)
        call allocate_lsvals (ncc2, np1, lsset)
        allocate (nqn12(np1), iparty2(np1), ncoumx2(np1),        &
             neqsen1(ncc2,np1), nshqn1(ncc2,np1), stat=status)
        if (status /= 0) call alloc_error (status, 'ang:N+1e', 'a')

        call bequiv (0, npefig, noccsh2, nocorb2, nelcsh2, 0, npqfig, &
             nqpack2, nnq2, nqn12, iparty2, ncoumx2, neqsen1,       &
             nshqn1, lsset, lnpmax)

! construct lspi for N+1 electron case
        npset = get_npset(lrgllo, lrglb, lrgp, lrgsup, lrgslo)
        allocate (lset1(npset), isset1(npset), ipset1(npset), &
                stat=status)
        if (status /= 0) call alloc_error (status, 'ang:sets', 'a')
!        lrglb = lnpmax   ! limit now disallowed to permit HAM checks
        isnp = 0
        ilup = MIN(lrglb, lrglup)
        total_L: do il = lrgllo, ilup
           if (lrgp < 0) then  ! both parities required
              lrgplo = 0
              lrgpup = 1
           else  ! lrgp defines the required total parity, even/odd
              lrgplo = lrgp
              lrgpup = lrgplo
           end if
           do ip = lrgplo, lrgpup
              do is = maxsnp, minsnp, -2
                 isnp = isnp + 1
                 lset1(isnp) = il
                 isset1(isnp) = is
                 ipset1(isnp) = ip
              end do
           end do
        end do total_L
        if (lrgllo <= lnpmax) then! skip if there are no bound terms
!========= form N-elecron configurations from N+1 configurations =======
! subtract 1 elec from each shell of N+1 cfgs
! discover if the input configuration set is consistent!
! in general, input csf data will be extended
           if (bug2 == 1) write (fo,'(/,a,/)') 'N-electron config&
                &uration data:'
           npackt => npack   ! save existing npack data
           call def_pck (npack, npefig*maxdep)
           allocate (nmefig1(fsh,npefig), stat=status)
           if (status /= 0) call alloc_error (status, 'ang:s', 'a')
           nmefig1 = -777
           call cpy_pck (npackt, npack)
           call est_minus (0, npefig, noccsh2, nocorb2, nelcsh2, &
                npack2, nefig, nm1, npack)
           call del_pck (npack)
           call def_pck (npack, nm1)
           call cpy_pck (npackt, npack)
           call del_pck (npackt)
           
! extend size of N-electron configuration data:
           call extend_cfgs (nm1, noccsh, nocorb, nelcsh, nnq)

           call minus (0, npefig, noccsh2, nocorb2, nelcsh2, npack2,   &
                nefig, nefig2, noccsh, nocorb, nelcsh, npack, nmefig1)

!......... form equivalent N-electron configurations
           if (bug2 == 1) write (fo,'(/,a,/)') 'Removing equivalent &
                &configurations:'
           call climits(1)
           lsset = 1
           npackt => nqpack
           call def_pck (nqpack, nefig2)
           call eq_estim (nefig, nefig2, noccsh, nocorb, nelcsh, nqfig,&
                nm2, nqpack, nccx)
           call del_pck (nqpack)
           if (nccx > ncc1 .or. nm2 > nqfig) &
                call reallocate_lsvals (MAX(nccx,ncc1), MAX(nm2,nqfig), lsset)
           call def_pck (nqpack, nm2)
           call cpy_pck (npackt, nqpack)
           call del_pck (npackt)
           call extend_ecfgs (nm2, nqn1, iparty, ncoumx)

           call equiv (nefig, nefig2, noccsh, nocorb, nelcsh, nqfig,  &
                nqfig2, nqpack, nnq, nqn1, iparty, ncoumx, lsset, lnmax)

           call climits (4,1)
           call cpu_time(t2)
           call system_clock (count=c2)
           call surfce (3, 0, npqfig, noccsh2, nocorb2, nelcsh2, &
                nqn12, nmefig1, nnq, ncoumx2, ncoumx)
           call cpu_time (t3)
           write (fo,'(a,f16.4,a)') 'ST3: CPU time     = ', t3 - t2, &
                ' secs'
           call system_clock (count=c3, count_rate=cr)
           write (fo,'(a,f16.4,a)') 'ST3: Elapsed time = ', &
                REAL(c3-c2,wp) / REAL(cr,wp), ' secs'
           call check_table (3)

! put the n+1 couplings into sets according to total l,s,pi
           lsset = 3
           call putset (npefig, npqfig, nnq2, nqn12, ncoumx2, lsset, &
                iparty2, npset, lset1, isset1, ipset1)
           ncset1 => get_ncset(2)

!================= form extra N-1 electron configurations ==============
! Form any extra N-1 elec cfgs -> new total # cfgs, mefig2
! Then form surfacing coefficients for the extra N electron cfgs
           if (bug2 == 1) write (fo,'(/,a,/)') '(N-1)-electron config&
                &uration data:'
           npackt => npack1
           call def_pck (npack1, nefig2*maxdep)
           call cpy_pck (npackt, npack1)
           call est_minus (nefig, nefig2, noccsh, nocorb, nelcsh, &
                npack, mefig, nm1, npack1)
           call del_pck (npack1)
           call def_pck (npack1, nm1)
           call cpy_pck (npackt, npack1)
           call del_pck (npackt)
           call extend_cfgs (nm1, noccsh1, nocorb1, nelcsh1, nnq1)
           call extend_nmefig (nefig2, nmefig)

           call minus (nefig, nefig2, noccsh, nocorb, nelcsh, npack, &
                mefig, mefig2, noccsh1, nocorb1, nelcsh1, npack1,    &
                nmefig)

           if (bug2 == 1) write (fo,'(/,a,/)') 'Removing equivalent &
                &configurations:'
           call climits(2)
           lsset = 2
           npackt => nqpack1
           call def_pck (nqpack1, mefig2)
           call cpy_pck (npackt, nqpack1)
           call eq_estim (mefig, mefig2, noccsh1, nocorb1, nelcsh1,  &
                mqfig, nm2, nqpack1, nccx)
           call del_pck (nqpack1)
           call def_pck (nqpack1, nm2)
           call cpy_pck (npackt, nqpack1)
           call del_pck (npackt)
           if (nccx > ncc2 .or. nm2 > mqfig) &
                call reallocate_lsvals (MAX(nccx,ncc2), MAX(mqfig,nm2), lsset)
           call extend_ecfgs (nm2, nqn11, iparty1, ncoumx1)

           call equiv (mefig, mefig2, noccsh1, nocorb1, nelcsh1, mqfig,  &
                mqfig2, nqpack1, nnq1, nqn11, iparty1, ncoumx1,  &
                lsset, lmmax)

           if (nelc > 1) then
              call climits (1,2)
              call cpu_time(t2)
              call system_clock (count=c2)
              call surfce (4, nqfig, nqfig2, noccsh, nocorb, nelcsh, &
                   nqn1, nmefig, nnq1, ncoumx, ncoumx1)
              call cpu_time (t3)
              write (fo,'(a,f16.4,a)') 'ST4: CPU time     = ', t3 - t2,&
                   ' secs'
              call system_clock (count=c3, count_rate=cr)
              write (fo,'(a,f16.4,a)') 'ST4: Elapsed time = ', &
                   REAL(c3-c2,wp) / REAL(cr,wp), ' secs'
              call check_table (4)
           end if

! write out the (n+1)-electron coupling data
           write (fo,'(/,a,/)') '(N+1)-electron configuration couplings:'
           lsset = 3
           call wrtcup (npefig, npqfig, nnq2, nqn12, ncoumx2, lsset,   &
                iparty2, npset, lset1, isset1, ipset1)
           mxnbnd = MAXVAL(ncset1(:npset))
           write (fo,'(/,a,i6,/)') 'Largest # of (N+1) bound cfgs is ',&
                mxnbnd

! calculate angular integrals for B-B part of N+1 electron Hamiltonian
           nelcp1 = nelc + 1
           lsset1 = 1
           lsset2 = 2
           call set_files (.false.)
           call cpu_time(t2)
           call system_clock (count=c2)
           call angbb (nelcp1, npset, lset1, isset1, ipset1, neqsen1,     &
                nshqn1, noccsh2, nocorb2, nelcsh2, nmefig1, nnq2,         &
                lsset1, ncoumx, noccsh, nocorb, nelcsh, nmefig, nnq,      &
                lsset2, ncoumx1, nnq1, 3, 1, 2)
           call cpu_time (t3)
           write (fo,'(a,f16.4,a)') 'ANGBB: CPU time     = ', t3 - t2, &
                ' secs'
           call system_clock (count=c3, count_rate=cr)
           write (fo,'(a,f16.4,a)') 'ANGBB: Elapsed time = ', &
                REAL(c3-c2,wp) / REAL(cr,wp), ' secs'
           call report_stat

! angular integrals for bound-continuum part of N+1 electron Hamiltonian
           lsset1 = 1
           lsset2 = 2
           call set_chs (chs, npset, lset1, ipset1, isset1) ! define chs
           call climits (2,3)
           call cpu_time(t2)
           call system_clock (count=c2)
           call angbc (nelcp1, npset, lset1, isset1, ipset1,              &
                noccsh2, nocorb2, nelcsh2, nmefig1, nnq2, lsset1, ncoumx, &
                nset, lset, isset, ipset, noccsh, nocorb, nelcsh, nmefig, &
                nnq, lsset2, ncoumx1, nnq1, chs, 3, 1)
           call cpu_time (t3)
           write (fo,'(a,f16.4,a)') 'ANGBC: CPU time     = ', t3 - t2, &
                ' secs'
           call system_clock (count=c3, count_rate=cr)
           write (fo,'(a,f16.4,a)') 'ANGBC: Elapsed time = ', &
                REAL(c3-c2,wp) / REAL(cr,wp), ' secs'
        else  ! no bound terms found
! write file abbi for ham since call to angbb has been skipped
           call set_files (.false.)
           call wr_abbi(npset, lset1, isset1, ipset1)
        end if 
     end if ! no-exchange case

  end if scattering

  if (mpol > 0) then  ! calculate ang ints for bound-bound multipoles
     lsset = 1
     call angmbb (nelcp1, npset, lset1, isset1, ipset1, noccsh2,      &
          nocorb2, nelcsh2, nmefig1, nnq2, lsset, ncoumx, nnq, 3)

     call angmbc (npset, lset1, isset1, ipset1, noccsh2, nocorb2,     &
          nelcsh2, nmefig1, nnq2, ncoumx, nset, lset, isset, ipset,   &
          nnq, 3)
  end if

  write (fo,'(a)') 'ANGular integral calculation complete'
  call cpu_time (t1)
  write (fo,'(a,f16.4,a)') 'CPU time     = ', t1 - t0, ' secs'
  call system_clock (count=c1, count_rate=cr)
  write (fo,'(a,f16.4,a)') 'Elapsed time = ', REAL(c1-c0,wp) / &
       REAL(cr,wp), ' secs'
  stop

contains

  subroutine extend_cfgs (n, noccsh, nocorb, nelcsh, nnq)
! extend size of configuration data
    integer, intent(in)    :: n   ! new size
    integer, pointer       :: noccsh(:)
    integer, pointer       :: nocorb(:,:)
    integer, pointer       :: nelcsh(:,:)
    integer, pointer       :: nnq(:)
    integer, pointer       :: t1a(:), t1b(:)
    integer, pointer       :: t2a(:,:), t2b(:,:)
    integer                :: m, p, status

    m = SIZE(noccsh)    ! old size
    p = SIZE(nocorb, DIM=1) ! first dimension
    t1a => noccsh
    t1b => nnq
    t2a => nocorb
    t2b => nelcsh
    allocate (noccsh(n), nocorb(p,n), nelcsh(p,n), nnq(n),  &
         stat=status)
    if (status /= 0) call alloc_error (status, 'extend_cfgs', 'a')
! copy values into new arrays:
    noccsh(1:m) = t1a(1:m)
    nnq(1:m) = t1b(1:m)
    nocorb(:,1:m) = t2a(1:p,1:m)
    nelcsh(:,1:m) = t2b(1:p,1:m)
    deallocate (t1a, t1b, t2a, t2b, stat=status)
    if (status /= 0) call alloc_error (status, 'extend_cfgs', 'd')
  end subroutine extend_cfgs

  subroutine extend_nmefig (n, nmefig)
! extend size of nmefig array
    integer, intent(in)    :: n   ! new size
    integer, pointer       :: nmefig(:,:)
    integer, pointer       :: t2a(:,:)
    integer                :: m, p, status

    m = SIZE(nmefig, DIM=2)   ! old size
    p = SIZE(nmefig, DIM=1)   ! first dimension
    t2a => nmefig
    allocate (nmefig(p,n), stat=status)
    if (status /= 0) call alloc_error (status, 'extend_nmefig', 'a')
! copy values into new array:
    nmefig(:,1:m) = t2a
    nmefig(:,m+1:n) = -777
    deallocate (t2a, stat=status)
    if (status /= 0) call alloc_error (status, 'extend_nmefig', 'd')
  end subroutine extend_nmefig

  subroutine extend_ecfgs (n, nqn1, iparty, ncoumx)
! extend size of equivalent configuration data
    integer, intent(in)    :: n   ! new size
    integer, pointer       :: nqn1(:)
    integer, pointer       :: iparty(:)
    integer, pointer       :: ncoumx(:)
    integer, pointer       :: t1a(:), t1b(:), t1c(:)
    integer                :: status, m

    m = SIZE(iparty)
    t1a => nqn1
    t1b => iparty
    t1c => ncoumx

    allocate (nqn1(n), iparty(n), ncoumx(n), stat=status)
    if (status /= 0) call alloc_error (status, 'extend_ecfgs', 'a')
! copy values into new arrays:
    nqn1(1:m) = t1a(1:m)
    iparty(1:m) = t1b(1:m)
    ncoumx(1:m) = t1c(1:m)
    deallocate (t1a, t1b, t1c, stat=status)
    if (status /= 0) call alloc_error (status, 'extend_cfgs', 'd')
  end subroutine extend_ecfgs

  subroutine wrtcup (nefig, nqfig, nnq, nqn1, ncoumx, lsset, &
       iparty, nset, lset, isset, ipset)
! write out coupling data
    use ls_states, only: lvals, svals
    integer, intent(in)         :: nefig
    integer, intent(in)         :: nqfig
    integer, intent(in)         :: nqn1(nqfig)
    integer, intent(in)         :: iparty(nqfig)
    integer, intent(in)         :: nnq(nefig)
    integer, intent(in)         :: ncoumx(nqfig)
    integer, intent(in)         :: nset
    integer, intent(in)         :: lset(nset)
    integer, intent(in)         :: isset(nset)
    integer, intent(in)         :: ipset(nset)
    integer, intent(in)         :: lsset   ! q.# set index
    character(len=4)            :: parity
    integer, allocatable        :: item(:,:), lcups(:)
    integer                     :: neq(nqfig)
    integer                     :: imax, inq, i, in, status, inc, ipi
    integer                     :: is, ll, lpi, ls, ncup, nt, incup
    integer                     :: icup, jmax, ncupx
    integer, pointer            :: lval(:,:), ispval(:,:)

    lval => lvals(lsset)
    ispval => svals(lsset)
    jmax = SIZE(lval, DIM=1)

! find dimensions of item:
    imax = 0
    do inq = 1, nqfig      ! ecfgs
       i = 1               ! counter - real cfgs eqiv to inq
       do in = nqn1(inq)+1, nefig   ! real cfgs
          if (nnq(in) == inq) then
             i = i + 1
          end if
       end do
       imax = MAX(imax,i)   ! max # cfgs equiv to any ecfg
    end do

!find dimension of lcups:
    ncupx = 0
    do is = 1, nset   ! symmetry sets
       ll = lset(is)           ! L
       ls = isset(is)          ! S
       lpi = ipset(is)         ! p
       ncup = 0
       do inq = 1, nqfig                ! ecfgs
          if (iparty(inq) /= lpi) cycle   ! parity does not match
          do incup = 1, ncoumx(inq)     ! couplings
             if (lval(incup,inq) == ll .and. ispval(incup,inq) == ls) &
                  ncup = ncup + imax
          end do
       end do
       ncupx = MAX(ncupx, ncup)
    end do
    ncupx = MAX(ncupx, imax*jmax)

    allocate (item(imax,nqfig), lcups(ncupx), stat=status)
    if (status /= 0) call alloc_error (status, 'wrtcup', 'a')

    do inq = 1, nqfig       ! ecfgs
       inc = nqn1(inq)      ! first corresponding cfg
       ipi = iparty(inq)    ! parity of ecfg
       i = 1
       item(i,inq) = inc
       do in = inc+1, nefig
          if (nnq(in) == inq) then
             i = i + 1
             item(i,inq) = in ! real cfgs for each ecfg
          end if
       end do
       neq(inq) = i           ! # real cfgs for each ecfg
    end do

    is_loop: do is = 1, nset   ! symmetry sets
       ll = lset(is)           ! L
       ls = isset(is)          ! S
       lpi = ipset(is)         ! p
       if (lpi == 0) then
          parity = 'even'
       else
          parity = 'odd'
       end if
       write (fo,'(2(a,i3),a,a4)') '    l = ',ll,'    spin = ',ls,&
            '    parity  ', parity
       ncup = 0
       do inq = 1, nqfig                ! ecfgs
          if (iparty(inq) /= lpi) cycle   ! parity does not match
          nt = neq(inq)                 ! # corresponding cfgs
          do incup = 1, ncoumx(inq)     ! couplings
             if (lval(incup,inq) == ll .and. ispval(incup,inq) == ls) then
! accumulate couplings for this set:
                do icup = 1, nt
                   lcups(ncup+icup) = item(icup,inq)
                end do
                ncup = ncup + nt
             end if
          end do
       end do
       if (ncup /= 0) write (fo,'("configuration index:",/,(4x,8i5))') &
            lcups(1:ncup)
       write (fo,'(a,i6)') 'number of configuration couplings &
            &stored for this symmetry is   ', ncup
    end do is_loop
    deallocate (item, lcups, stat = status)
    if (status /= 0) call alloc_error (status, 'wrtcup', 'd')
  end subroutine wrtcup

  function get_npset (lrgllo, lrglup, lrgp, lrgsup, lrgslo)
    integer                :: get_npset
    integer, intent(in)    :: lrgllo, lrglup
    integer, intent(in)    :: lrgsup, lrgslo
    integer, intent(in)    :: lrgp  ! total parity
    integer                :: isnp, ilup, il, lrgplo, lrgpup, ip
    integer                :: is

    isnp = 0
    ilup = MIN(lrglb, lrglup)
    do il = lrgllo, ilup
       if (lrgp < 0) then ! Both parities are required
          lrgplo = 0
          lrgpup = 1
       else              ! only the parity lrgp is required
          lrgplo = lrgp
          lrgpup = lrgplo
       end if
       do ip = lrgplo, lrgpup
          do is = lrgsup, lrgslo, -2
             isnp = isnp + 1
          end do
       end do
    end do
    get_npset = isnp
  end function get_npset

  subroutine set_chs (chs, npset, lpset, ippset, ispset)
! generate chs, the max # channels
    use symmetries, only: nset, lset, isset, ipset
    integer, intent(out) :: chs
    integer, intent(in)  :: npset
    integer, intent(in)  :: lpset(:)
    integer, intent(in)  :: ippset(:)
    integer, intent(in)  :: ispset(:)
    integer              :: isnp, lrgl, lrgpi, lrgs, lmin, lmax
    integer              :: lcfgp, jch1, lj, jspin, jpi, jsn
    chs = 0
    do isnp = 1, npset
       lrgl = lpset(isnp)    ! L
       lrgpi = ippset(isnp)  ! P
       lrgs = ispset(isnp)   ! S

       lmin = 999
       lmax = 0
       do jsn = 1, nset
          lcfgp = lset(jsn)
          lmin = MIN(lmin, ABS(lcfgp-lrgl))
          lmax = MAX(lmax, lcfgp+lrgl)
       end do

       jch1 = 0
       do lj = lmin, lmax
          do jsn = 1, nset
             lcfgp = lset(jsn)
             jspin = isset(jsn)
             jpi = ipset(jsn)
             if (MOD(lj+jpi+lrgpi, 2) == 1) cycle
             if (lrgl >lcfgp+lj .or. lrgl < ABS(lcfgp-lj)) cycle
             if (ABS(lrgs-jspin) /= 1) cycle
             jch1 = jch1+1
          end do
       end do
       chs = MAX(chs, jch1)
    end do
    write (fo,'(/,a,i4,/)') 'Max # channels, chs = ', chs
  end subroutine set_chs

end program ang
