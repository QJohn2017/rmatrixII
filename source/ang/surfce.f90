module surface
! Time-stamp: "2005-07-29 14:46:20 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use debug, only: bug3, bug8
  use rm_data, only: maxorb
  use startup, only: maxdep, ljcomp, njcomp
  implicit none

! assume that the maximum # of filled shells is given by maxdep
  integer, save      :: dim6 ! max # filled shells

! conshc:
  integer, parameter :: kfl3 = 20
  integer, parameter :: kfl4 = 100
  integer, parameter :: kfl5 = 200
  integer, allocatable, save :: j6ca(:), j7ca(:), j8ca(:)
  integer, allocatable, save :: jwca(:), k6a(:,:), k7a(:,:)
  integer, allocatable, save :: k8a(:,:), kwa(:,:,:)

  integer                    :: k6(kfl4), k7(kfl5), k8(kfl4), kw(6,kfl3)
  integer, allocatable, save :: j1qn1(:,:), j1qn(:,:)
  integer, allocatable, save :: mact(:), mnt(:)
!$OMP THREADPRIVATE (mact, mnt, j1qn, j1qn1)

  private
  public dealloc_conshc, prsurf, surfce

contains
  subroutine alloc_conshc
! allocate arrays of conshc group
    integer          :: status
    dim6 = maxdep
    allocate (j6ca(dim6), j7ca(dim6), j8ca(dim6), jwca(dim6),&
         k6a(kfl4,dim6), k7a(kfl5,dim6), k8a(kfl4,dim6),     &
         kwa(6,kfl3,dim6), stat=status)
    if (status /= 0) then
       write (fo,'(a)') 'surface: allocation error, stat = ', status
       stop
    end if
  end subroutine alloc_conshc

  subroutine dealloc_conshc
! deallocate arrays of conshc group
    integer          :: status
    deallocate (j6ca, j7ca, j8ca, jwca, k6a, k7a, k8a, kwa, &
         stat=status)
    if (status /= 0) then
       write (fo,'(a)') 'surface: deallocation error, stat = ', status
       stop
    end if
  end subroutine dealloc_conshc

  subroutine prsurf
! the algebra for calculating the surfacing coefficient will depend
! on the depth of the shell below the surface
! to save time the algebra is calculated by njsym for all depths

! set up j2 and j3 arrays for njsym for all depths
! idep is counted such that the top shell has zero depth
  use recoupling, only: njsym, m, n, j1, j2, j3
    real(wp)     :: dummy
    integer      :: idep, idep1, idep2, idep22, idep32, m21, m22, m23
    integer      :: i, m31, m32, m33, iw
    integer      :: j6c, j7c, j8c, jwc

    call alloc_conshc
    L5: do idep = 0, maxdep-1
       idep1 = idep + 1
       idep2 = idep + 2
       idep22 = idep2 + idep2
       idep32 = idep22 + idep2

       j2(:,1) = (/1, idep32, 2/)
       j3(2,1) = 1

       m21 = idep2
       m22 = 1
       m23 = idep2 + 1
       do i = 2, idep2
          m21 = m21 + 1
          m22 =  m22 + 1
          m23 = m23 + 1
          j2(:,i) = (/m21, m22, m23/)
       end do
       j3(1,1) = idep2 + 1
       j3(3,1) = idep22 + 1
       j3(:,idep2) = (/idep32 - 1, idep32, idep22/)

       m31 = idep22
       m32 = 2
       m33 = idep22 + 1
       do i = 2, idep1
          m31 = m31 + 1
          m32 = m32 + 1
          m33 = m33 + 1
          j3(:,i) = (/m31, m32, m33/)
       end do
       m = idep32
       n = idep + 3

       call njsym (j6c, j7c, j8c, jwc, k6, k7, k8, kw, dummy)
       if (bug3 == 1) then
          write (fo,'(a)') 'njsym'
          write (fo,'(a,i5,20i4)') 'k6 ', j6c, k6(1:j6c)
          write (fo,'(a,i5,20i4)') 'k7 ', j7c, k7(1:j7c)
          write (fo,'(a,i5,20i4)') 'k8 ', j8c, k8(1:j8c)
          write (fo,'(a)') 'kw'
          do iw = 1, jwc
             write (fo,'(5x,6i4)') kw(1:6,iw)
          end do
       end if

! store the arrays
       j6ca(idep1) = j6c
       k6a(1:j6c,idep1) = k6(1:j6c)
       j7ca(idep1) = j7c
       k7a(1:j7c,idep1) = k7(1:j7c)
       j8ca(idep1) = j8c
       k8a(1:j8c,idep1) = k8(1:j8c)
       jwca(idep1) = jwc
       kwa(1:6,1:jwc,idep1) = kw(1:6,1:jwc)
    end do L5
  end subroutine prsurf

  subroutine surfce (id, nq1, nq2, noccsh, nocorb, nelcsh, nqn1,&
       nmefig, mmq, ncoumx, mcoumx)
! given a set of electron configurations the subroutine
! calculates the coefficient which brings an electron
! from its shell to form the top branch of the coupling
! tree.  this is repeated for all shells of all configurations.
! for each configuration it is repeated for all possible
! shell quantum numbers.  these loops are in subroutine conqn
! which in turn calls subroutine consh to loop over the
! allowed shell couplings.    the calculation of the coefficients
! is in the heart of the looping structure in consh.

! each configuration contains say n electrons.  bringing one
! to the surface leaves an n-1 configuration plus a continuum
! like top branch.
! extended to cope with 2 electrons in l>2 shells.
    use shell_coupling, only: il, j, n
    use surfacing_tables, only: alloc_surf_table, pack_indx, &
         alloc_surcof, new_indx, zero_ctrs
    use cfps, only: cfp
    use recoupling, only: j1
    integer, intent(in)    :: id  ! Surfacing table id (1->4)
    integer, intent(in)    :: nq1 ! initial cfg index - 1
    integer, intent(in)    :: nq2 ! final cfg index
    integer, intent(in)    :: noccsh(:)   ! # shells in cfg
    integer, intent(in)    :: nocorb(:,:) ! orb index for shells in cfg
    integer, intent(in)    :: nelcsh(:,:) ! occ of each shell in cfg
    integer, intent(in)    :: nqn1(:)     ! equiv cfg -> 1st real cfg
    integer, intent(in)    :: mmq(:)      ! true -> equiv cfg
    integer, intent(in)    :: nmefig(:,:) ! link n-1 cfg -> n cfg
    integer, intent(in)    :: ncoumx(:)   ! # n-electron couplings
    integer, intent(in)    :: mcoumx(:)   ! # (n-1)-electron couplings
    logical                :: ispec
    integer                :: j1l(SIZE(j1)), j1s(SIZE(j1))
    integer                :: ifsh(12), mxs(12), mcs(12), ni(12)
    integer                :: j1kqn(3)
    real(wp)               :: coeffp
    integer, save :: ls(11) = (/1,3,6,11,12,13,14,15,16,17,18/)
    integer                :: inq, ine, ncomx, jact, i, neltop
    integer                :: kshell, ish, idep, ime, imq, mcomx, inco
    integer                :: idep32, korb, lshell, mc, nc, jx, jact1
    integer                :: ll, ll1, lk1, m, ki, mcoupl, ncoupl
    integer                :: j1q1, j1q2, j1q3, mi, nn, k, nel
    integer                :: mcoup, ncoup, ivi, ili, ivj, ilj, smx
    integer                :: shell_mx, cfg_mx, status

    cfg_mx = nq2 - nq1
    if (nq1 == nq2) then
       shell_mx = 5
    else
!    shell_mx = MAXVAL(noccsh(nq1+1:nq2))
       shell_mx = MAXVAL(noccsh(nqn1(nq1+1):nqn1(nq2)))
       if (shell_mx > 50) then
          write (fo,*) 'initialization error in surfce ', shell_mx
          stop
       end if
    end if
    if (id /= 4) then
       call alloc_surf_table (id, shell_mx, cfg_mx)
    else
       call alloc_surf_table (id, shell_mx, nq2, nq1+1)
    end if

!$OMP PARALLEL &
!$OMP& DEFAULT (none) &
!$OMP& PRIVATE(inq, ine, ncomx, jact, i, neltop, ish, ispec,&
!$OMP&         kshell, idep, ime, imq, mcomx, korb, lshell, idep32, m, &
!$OMP&         j1l, j1s, mc, nc, jact1, jx, nn, ll, ll1, k, ifsh, mxs, &
!$OMP&         mcs, nel, mcoup, ncoup, mcoupl, ncoupl, ni, j1q1, ivj,  &
!$OMP&         j1q2, j1q3, mi,  ivi, ili, ilj, coeffp, lk1, ki,  &
!$OMP&         j1kqn, status)      &
!$OMP& SHARED(nq1, nq2, ncoumx, noccsh, nocorb, nelcsh, nmefig, bug8,  &
!$OMP&        mmq, mcoumx, ljcomp, ls, il, j, n, id, nqn1, maxdep)
    allocate (mact(maxdep), mnt(maxdep), j1qn(3,maxdep), &
         j1qn1(3,2*maxdep), stat = status)
    call zero_ctrs
!$OMP DO                          &
!$OMP& SCHEDULE(DYNAMIC,1)
    cfgs: do inq = nq1+1, nq2   ! configuration loop
       ine = nqn1(inq)          ! first real cfg of type
       ncomx = ncoumx(inq)      ! # couplings for cfg
       jact = noccsh(ine)       ! # shells in cfg
       do i = 1, jact
          mact(i) = nocorb(i,ine)   ! orbital ptrs for cfg
          mnt(i) = nelcsh(i,ine)    ! occ of cfg orbitals
       end do
       neltop = mnt(jact)       ! occ of top shell

       shells: do ish = jact, 1, -1  ! loop down through cfg orbs
          ispec = .false.       ! flag kshell=singly occ top shell
          kshell = ish          ! active shell
          idep = jact - kshell  ! depth of active shell
          if (idep == 0 .and. neltop == 1) ispec = .true.

          ime = nmefig(kshell,ine) ! corresponding (n-1)-cfg
          if (ime == 0) then
             if (bug8 /= 0) write (fo,'(a,3i6)') 'surfce: ine, kshell,&
                  & ime', ine, kshell, ime
             cycle shells
          else if (ime < 0) then
             write (fo,'(a,3i10)') 'surfce: ine, kshell, ime', ine,&
                  kshell, ime
             write (fo,'(a)') 'surfce: catastrophic error'
!            stop
          end if
          imq = mmq(ime)           ! equivalent (n-1)-cfg
          mcomx = mcoumx(imq)      ! # couplings for (n-1)-cfg

          call alloc_surcof (ncomx) ! form linked list of surfacing cfs

          korb = mact(kshell)      ! orbital seq of active shell
          lshell = ljcomp(korb)    ! L-value of active shell

! Set GENSUM input: angular momentum, spin of surfacing electron
          idep32 = 3 * (idep + 2)
          j1l(idep32) = lshell + lshell + 1  ! 2 * L + 1
          j1s(idep32) = 2                    ! spin multiplicity
          mc = idep32                        ! total # a.m.
          nc = idep + 3                      ! # core a.m.

! determine the quantum numbers for each shell
! insert extra element corresponding to shell k with 1 electron removed
          jact1 = jact + 1
          q_nos: do jx = 1, jact1
             i = jx
             if (jx > kshell) i = jx - 1
             nn = mact(i)                    ! orbital sequence
             ll = ljcomp(nn)                 ! orbital L
             ll1 = ll + ll + 1
             lk1 = ll + 1
             m = mnt(i)                      ! orb occupancy
! insert the loop for kshell with m-1 electrons
             if (jx == kshell+1) m = m - 1
             k = m
             if (m > ll1) k = 2 * ll1 - m    ! k = true occupancy
             if (k == 0) then
                ifsh(i) = 2      ! zero or fully occupied shells
             else 
                if (ll >= 3 .and. k == 2) then
                   mxs(jx) = ll1
                   mcs(jx) = -1
                   cycle
                end if
                ifsh(i) = ls(lk1) + k - 1  ! loc of L**k data
             end if
             ki = ifsh(i)
             mxs(jx) = il(ki)          ! # terms for L**k
             mcs(jx) = j(ki)         ! loc of q.#s for terms
          end do q_nos

          nel = mnt(kshell)        ! # electrons in active shell

! loop over the allowed quantum numbers for each shell
! set the count for the couplings for the n and n-1 configs
          mcoup = 0
          ncoup = 0
          mcoupl = 0
          ncoupl = 0
          i = 0
          couplings: do
             i = i + 1
             jx = i
             if (i > kshell) jx = i - 1
             ni(i) = 0         ! set term counter for shell
             terms: do
                ni(i) = ni(i) + 1 ! reset term # for shell i

                if (mcs(i) == -1) then  ! f or higher shells
                   j1q1 = 2
                   if (ni(i) == 1) j1q1 = 0
                   j1q2 = 2 * ni(i) - 1
                   j1q3 = 1
                   if (MOD(ni(i),2) == 0) j1q3 = 3
                else
                   mi = mcs(i) + (ni(i) - 1) * 3
                   j1q1 = n(mi+1)  ! seniority
                   j1q2 = n(mi+2)  ! 2 * L + 1
                   j1q3 = n(mi+3)  ! spin multiplicity
                end if
                if (i /= kshell+1) then ! not the depleted shell
                   j1qn(:,jx) = (/j1q1, j1q2, j1q3/)
                   if (i == kshell) then     ! active shell
                      j1l(2) = j1q2  ! insert gensum L-array value
                      j1s(2) = j1q3  ! insert gensum S-array value
                      if (lshell > 1) ivi = j1q1
                      if (lshell > 0) ili = (j1l(2) - 1) / 2
                      if (kshell == 1) then
                         j1l(idep+3) = 1
                         j1s(idep+3) = 1
                         j1l(idep+4) = j1l(2)
                         j1s(idep+4) = j1s(2)
                      else if (kshell == 2) then
                         j1l(idep+3) = j1qn(2,1)
                         j1s(idep+3) = j1qn(3,1)
                      end if
                   else if (i > kshell+1) then
                      j1l(i-kshell+1) = j1q2
                      j1s(i-kshell+1) = j1q3
                   end if
                else                ! the depleted shell
                   j1kqn(:) = (/j1q1, j1q2, j1q3/)
                   j1l(1) = j1q2      ! insert gensum L-value
                   j1s(1) = j1q3      ! insert gensum S-value
                   if (lshell > 1) ivj = j1q1 ! seniority
                   if (lshell > 0) ilj = (j1l(1) - 1) / 2

! coefficient of fractional parentage:
                   coeffp = 1.0_wp
                   if (nel > 0 .and. lshell > 0) then
                      call cfp (lshell, nel, ivi, ili, j1s(2), ivj,&
                           ilj, j1s(1), coeffp)
                   end if

                   if (kshell == 1) then
                      j1l(idep+idep+5) = j1l(1)
                      j1s(idep+idep+5) = j1s(1)
                   end if
                end if

                if (i < jact1) cycle couplings ! incomplete cplg tree
                if (i > jact1) cycle shells    ! new selection of terms

                call sconsh (jact, kshell, lshell, ispec, j1l, j1s,&
                     j1kqn, coeffp, ncoupl, mcoupl)

                new_terms: do
                   if (ni(i) < mxs(i)) then  ! more terms for this shell
                      if (i == kshell) then
                         mcoupl = mcoup
                         ncoup = ncoupl
                      else if (i == kshell+1) then
                         ncoupl = ncoup
                      else if (i < kshell) then
                         ncoup = ncoupl
                         mcoup = mcoupl
                      end if
                      cycle terms
                   end if
                   i = i - 1
                   jx = i
                   if (i > kshell) jx = i - 1
                   if (i <= 0) exit couplings
                end do new_terms
             end do terms
          end do couplings

          call pack_indx (id, kshell, inq)
       end do shells
! all shells for this configuration have been active
    end do cfgs
!$OMP END DO
    deallocate (mact, mnt, j1qn, j1qn1, stat=status)
!$OMP END PARALLEL
  end subroutine surfce

  subroutine sconsh (jact, kshell, lshell, ispec, j1l, j1s, j1kqn,&
       coeffp, ncoupl, mcoupl)
! determine the coupling between the shells
    use recoupling, only: j1, gensum
    use coupling_limits, only: maxln, maxsn, minsn, maxlm, maxsm, &
         minsm, minln, minlm
    use surfacing_tables, only: new_indx
    integer, intent(in)    :: jact          ! # shells
    integer, intent(in)    :: kshell        ! interacting shell
    integer, intent(in)    :: lshell        ! L-value of kshell
    logical, intent(in)    :: ispec ! true if kshell top, singly occ
    integer, intent(inout) :: j1l(SIZE(j1)) ! gensum L-values
    integer, intent(inout) :: j1s(SIZE(j1)) ! gensum S-values
    integer, intent(in)    :: j1kqn(3)      ! q.#s of depleted shell
    real(wp), intent(in)   :: coeffp        ! coeff fractional parentage
    integer, intent(inout) :: ncoupl        ! n coupling counts
    integer, intent(inout) :: mcoupl        ! n-1 coupling counts
    logical                :: okn, lpoint
    integer                :: li(12), lll(12), llh(12), lsp(12)
    integer                :: lsl(12), lsh(12)
    real(wp)               :: recupl, recups
    integer                :: idep, idp1, idp24, idp35, mcou, ncou
    integer                :: mcoup, ncoup, jactm1, i, j, ll0, ls0, lli
    integer                :: ifill, lcfgn, ispinn, ls0k, ll0k, lcfgm
    integer                :: jactpk, kshlm2, lsi, ispinm

    idep = jact - kshell    ! depth of shell to be surfaced
    idp1 = idep + 1
    idp24 = idep + idep + 4
    idp35 = idep + idep + idep + 5

! initialise count of the couplings of the n and n-1 configs
    mcou = mcoupl
    ncou = ncoupl
    mcoup = mcoupl
    ncoup = ncoupl

    lpoint = .true.
    jactm1 = jact - 1       ! # couplings
    if (jactm1 < 0) return  ! no couplings, j1l, j1s defined by surfce
    if (jactm1 == 0) then
       call single_shell
       return
    end if

! add in extra loops corresponding to the couplings in the n-1
! configuration from kshell upwards
    jactpk = jact + jact - kshell
    if (kshell == 1) jactpk = jactpk - 1 ! special case, kshell bottom

    j1qn1(1,1:jactpk) = 0
    ls0 = j1qn(3,1) - 1                     ! 2 * s0
    ll0 = (j1qn(2,1) - 1)/2                 ! l0
    lll(1) = ABS(ll0 - (j1qn(2,2) - 1)/2)   ! lp1-min
    llh(1) = ll0 + (j1qn(2,2) - 1)/2 + 1    ! lp1-max + 1
    lsl(1) = ABS(ls0 - j1qn(3,2) + 1) - 1   ! 2 * s1-min
    lsh(1) = ls0 + j1qn(3,2)                ! 2 * s1-max + 1

! loop over all possible couplings between the shells then repeat for
! coupling from the deprived shell to the top of the n-1 configuration
    kshlm2 = kshell - 2
    i = 0
    shells_loop: do
       i = i + 1
       if (i <= jactm1) then
          j = i
       else
          j = i - jactpk + jactm1
       end if

       li(i) = lll(i)   ! set min L for shell
       orb_am_loop: do
          li(i) = li(i) + 1   ! increment L for coupling
          lli = li(i) - 1     ! actual L of coupling
          j1qn1(2,i) = 2*lli + 1
          lpoint = .true.    ! updated l set lpoint to true
          if (j < jactm1) then
             lll(i+1) = ABS(lli - (j1qn(2,j+2) - 1)/2)
             llh(i+1) = lli + (j1qn(2,j+2) - 1)/2 + 1
          end if

          lsp(i) = lsl(i)   ! set min S for shell
          spin_loop: do
             lsp(i) = lsp(i) + 2
             lsi = lsp(i) - 1
             j1qn1(3,i) = lsi + 1

! updated s so update j1 arrays for gensum
             if (j >= kshlm2) then
                if (i == j) then
                   ifill = idep + 3 + i - kshlm2
                else
                   ifill = idep + idep + 4 + j - kshlm2
                end if
                j1s(ifill) = j1qn1(3,i)
                if (lpoint) j1l(ifill) = j1qn1(2,i)
             end if
             if (j > jactm1) exit shells_loop

             if (j  /= jactm1) then
                lsl(i+1) = ABS(lsi - j1qn(3,j+2) + 1) - 1
                lsh(i+1) = lsi + j1qn(3,j+2)
                cycle shells_loop

             else   ! at treetop, determine couplings:
                if (i == j) then ! n-electron couplings...
                   lcfgn = (j1l(idp24) - 1)/2
                   ispinn = j1s(idp24)
                   if (lcfgn <= maxln  .and. lcfgn >= minln .and. &
                        ispinn <= maxsn .and.  ispinn >= minsn) then
                      okn = .true.
                      ncou = ncou + 1
                   else
                      okn = .false.
                   end if
! loop over couplings with electron removed from kshell; skip if 1 shell
                   mcou = mcoup
                   ls0k = j1kqn(3) - 1
                   ll0k = (j1kqn(2) - 1)/2
                   if (kshlm2 <= 0) then
                      if (kshlm2 ==  - 1) then
                         lll(i+1) = ABS(ll0k - (j1qn(2,2) - 1)/2)
                         llh(i+1) = ll0k + (j1qn(2,2) - 1)/2 + 1
                         lsl(i+1) = ABS(ls0k - j1qn(3,2) + 1) - 1
                         lsh(i+1) = ls0k + j1qn(3,2)
                      else
                         lll(i+1) = ABS(ll0 - ll0k)
                         llh(i+1) = ll0 + ll0k   + 1
                         lsl(i+1) = ABS(ls0 - ls0k)  - 1
                         lsh(i+1) = ls0k + ls0
                      end if
                   else
                      lll(i+1) = ABS(li(kshlm2) - 1 - ll0k)
                      llh(i+1) = li(kshlm2) - 1 + ll0k + 1
                      lsl(i+1) = ABS(lsp(kshlm2) - 1 - ls0k) - 1
                      lsh(i+1) = lsp(kshlm2) + ls0k
                   end if
                   cycle shells_loop

                else ! (n-1)-electron couplings...
                   lcfgm = (j1l(idp35) - 1)/2
                   ispinm = j1s(idp35)
                   if (lcfgm <= maxlm .and. lcfgm >= minlm .and.   &
                        ispinm <= maxsm .and. ispinm >= minsm) then ! OK
                      mcou = mcou + 1
                      if (okn) then
                         if (itrtst(lcfgn,lcfgm,lshell) == 0 .and. &
                              itrtst(ispinn,ispinm,2) == 0) then
                            if (.NOT.ispec) then ! regular shell

                               if (lpoint) then   ! L recoupling coeff
                                  call gensum (j6ca(idp1), j7ca(idp1),&
                                       j8ca(idp1), jwca(idp1),        &
                                       k6a(:,idp1), k7a(:,idp1),      &
                                       k8a(:,idp1), kwa(:,:,idp1),    &
                                       j1l, recupl)
                                  lpoint = .false.
                               end if

! spin recoupling coefficient:
                               call gensum (j6ca(idp1), j7ca(idp1),   &
                                    j8ca(idp1), jwca(idp1),           &
                                    k6a(:,idp1), k7a(:,idp1),         &
                                    k8a(:,idp1), kwa(:,:,idp1),       &
                                    j1s, recups)

                               call new_indx (mcou, ncou, recupl * &
                                    recups * coeffp)
                            else ! singly occupied top shell
                               call new_indx (mcou, ncou, coeffp)
                            end if
                         end if
                      end if
                   end if
                end if
             end if

! update mcoup if i<=kshell-2
             find_loop: do     ! find next shell value to increment
                if(lsp(i) < lsh(i)) then
                   if (i <= kshlm2) mcoup = mcou
                   cycle spin_loop
                end if
                if (li(i) < llh(i)) then
                   if (i <= kshlm2) mcoup = mcou
                   cycle orb_am_loop
                end if
                i = i - 1      ! recur back to previous shell
                if (i <= jactm1) then   ! an n-electron config
                   j = i
                else
                   j = i - jactpk + jactm1 ! an (n-1)-electron config
                end if
                if (i <= 0) exit shells_loop  ! all couplings processes
             end do find_loop
          end do spin_loop
       end do orb_am_loop
    end do shells_loop
    ncoupl = ncou    ! reset coupling counts
    mcoupl = mcou
  contains
    subroutine single_shell
! if there is only one occupied shell there are no couplings to find

        i = 1
        j = 1
! n electron case: test if coupling is within allowed limits
        lcfgn = (j1l(idp24) - 1) / 2
        ispinn = j1s(idp24)
        if (lcfgn <= maxln .and. lcfgn >= minln .and. ispinn <= maxsn &
             .and. ispinn >= minsn) then
           okn = .true.
           ncou = ncou + 1
        else
           okn = .false.
        end if
        j = 2
! n-1 case: test if coupling is within the allowed limits
        lcfgm = (j1l(idp35) - 1) / 2
        ispinm = j1s(idp35)
        if (lcfgm <= maxlm .and. lcfgm >= minlm .and. ispinm <= maxsm &
             .and. ispinm >= minsm) then
           mcou = mcou + 1
           if (okn) then
              if (itrtst(lcfgn,lcfgm,lshell) == 0 .and. &
                   itrtst(ispinn,ispinm,2) == 0) then
                 if (.NOT.ispec) then
                    if (lpoint) then
                       call gensum (j6ca(idp1), j7ca(idp1), j8ca(idp1),&
                            jwca(idp1), k6a(1,idp1), k7a(1,idp1),      &
                            k8a(1,idp1), kwa(1,1,idp1), j1l, recupl)
                       lpoint=.false.
                    end if
                    call gensum (j6ca(idp1), j7ca(idp1), j8ca(idp1), &
                         jwca(idp1), k6a(1,idp1), k7a(1,idp1),       &
                         k8a(1,idp1), kwa(1,1,idp1), j1s, recups)
                    call new_indx (mcou, ncou, recupl * recups * coeffp)
                 else
                    call new_indx (mcou, ncou, coeffp)
                 end if
              end if
           end if
        end if
        ncoupl = ncou
        mcoupl = mcou
      end subroutine single_shell
  end subroutine sconsh

  function itrtst (l, m, n)
! test triangular relation
    integer     :: itrtst ! = , not satisfied; =0 satisfied
    integer, intent(in) :: l, m, n

    if (n >= ABS(l-m) .and. n <= l+m) then
       itrtst = 0
    else
       itrtst = 1
    end if
  end function itrtst

end module surface
