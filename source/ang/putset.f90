module sym_sets
! Time-stamp: "2005-03-11 15:03:53 cjn"
  use io_units, only: fo
  implicit none

  integer, save                       :: ncsy = 0
  integer, allocatable, target, save  :: ncset1(:)   ! # cfgs / symmetry
  integer, allocatable, target, save  :: ncfga1(:,:)
  integer, allocatable, target, save  :: ncfgb1(:,:)
  integer, allocatable, target, save  :: ncset2(:)   ! # cfgs / symmetry
  integer, allocatable, target, save  :: ncfga2(:,:)
  integer, allocatable, target, save  :: ncfgb2(:,:)

  private
  public putset, get_ncset, get_ncfg, ncsy

contains
  function get_ncset (i)
    integer, pointer     :: get_ncset(:)
    integer, intent(in)  :: i      ! case
    select case (i)
    case (1)
       get_ncset => ncset1
    case default
       get_ncset => ncset2
    end select
  end function get_ncset

  subroutine get_ncfg (i, ncfga, ncfgb)
    integer, intent(in)  :: i      ! case
    integer, pointer     :: ncfga(:,:)
    integer, pointer     :: ncfgb(:,:)

    select case (i)
    case (1)
       ncfga => ncfga1
       ncfgb => ncfgb1
    case default
       ncfga => ncfga2
       ncfgb => ncfgb2
    end select
  end subroutine get_ncfg

  subroutine putset (nefig, nqfig, nnq, nqn1, ncoumx, lsset, &
       iparty, nset, lset, isset, ipset)
! Define ccfgs sorted according to symmetry so that equivalent
! ccfgs follow each other in sequence
    use ls_states, only: lvals, svals
    use error_prt, only: alloc_error
    integer, intent(in)   :: nefig      ! # cfgs
    integer, intent(in)   :: nqfig      ! # equiv cfgs
    integer, intent(in)   :: nset       ! # symmetry (SLp) sets
    integer, intent(in)   :: nqn1(:)    ! equiv cfg -> first real cfg
    integer, intent(in)   :: lsset      ! q.# set
    integer, intent(in)   :: ncoumx(:)  ! # couplings for cfg
    integer, intent(in)   :: iparty(:)  ! parity of cfg
    integer, intent(in)   :: nnq(:)     ! true cfg -> equiv cfg
    integer, intent(in)   :: lset(:)    ! symmetry L
    integer, intent(in)   :: isset(:)   ! symmetry S
    integer, intent(in)   :: ipset(:)   ! symmetry P
    integer               :: item(nefig)
    integer               :: i, nt, lcfg, in, incup, inc, ncs, inq, ipi
    integer               :: ispin, ncsx, status
    integer, pointer      :: lval(:,:), ispval(:,:)
    integer, pointer      :: ncfga(:,:), ncfgb(:,:), ncset(:)

    call estim_ncs (nefig, nqfig, nnq, nqn1, ncoumx, lsset, iparty, &
         nset, lset, isset, ipset, ncsx)
    ncsy = MAX(ncsy, ncsx)
    select case (lsset)
    case (1)
       allocate (ncfga1(ncsx,nset), ncfgb1(ncsx,nset), ncset1(nset), &
            stat=status)
       if (status /= 0) call alloc_error (status, 'putset', 'a')
       ncfga => ncfga1
       ncfgb => ncfgb1
       ncset => ncset1
    case default
       allocate (ncfga2(ncsx,nset), ncfgb2(ncsx,nset), ncset2(nset), &
            stat=status)
       if (status /= 0) call alloc_error (status, 'putset', 'a')
       ncfga => ncfga2
       ncfgb => ncfgb2
       ncset => ncset2
    end select

    lval => lvals(lsset)
    ispval => svals(lsset)
    ncset(1:nset) = 0
    eq_cfgs: do inq = 1, nqfig   ! equiv cfgs
       inc = nqn1(inq)            ! first corresp real cfg
       ipi = iparty(inq)          ! parity
       i = 1
       item(i) = inc              ! list of cfg seqs equiv to inq
       do in = inc+1, nefig       ! remaining real cfgs
          if (nnq(in) == inq) then ! real cfg equiv to inq
             i = i + 1
             item(i) = in
          end if
       end do
       nt = i                      ! total # cfgs equiv to inq
       cplgs: do incup = 1, ncoumx(inq) ! couplings for ecfg
          lcfg = lval(incup,inq)    ! L-value
          ispin = ispval(incup,inq) ! S-value
          symmetries: do in = 1, nset  ! check symmetry sets
             if (lcfg == lset(in) .and. ispin == isset(in) .and.&
                  ipi == ipset(in)) then ! cplg is set = in
                do i = 1, nt
                   inc = item(i)
                   ncs = ncset(in)
                   ncs = ncs + 1
                   if (ncs > ncsx) then ! array overflow should not occur
                      write (fo,'(a)') 'putset: ncs error'
                      stop
                   end if
                   ncset(in) = ncs
                   ncfga(ncs,in) = inc
                   ncfgb(ncs,in) = incup
                end do
                cycle cplgs
             end if
          end do symmetries
       end do cplgs
    end do eq_cfgs
  end subroutine putset

  subroutine estim_ncs (nefig, nqfig, nnq, nqn1, ncoumx, lsset, iparty,&
       nset, lset, isset, ipset, ncs_max)
! Estimate the max #  couplings in a symmetry, ncs
    use ls_states, only: lvals, svals
    integer, intent(in)    :: nefig
    integer, intent(in)    :: nqfig
    integer, intent(in)    :: nnq(nefig)
    integer, intent(in)    :: nqn1(nqfig)
    integer, intent(in)    :: ncoumx(nqfig)
    integer, intent(in)    :: lsset      ! q.# set
    integer, intent(in)    :: iparty(nqfig)
    integer, intent(in)    :: nset
    integer, intent(in)    :: lset(nset), isset(nset), ipset(nset)
    integer, intent(out)   :: ncs_max
    integer                :: ncset(nset)
    integer                :: inq, inc, ipi, i, in, nt, incup
    integer                :: lcfg, ispin
    integer, pointer       :: lval(:,:), ispval(:,:)

    lval => lvals(lsset)
    ispval => svals(lsset)
    
    ncset = 0
    inq_L: do inq = 1, nqfig
       inc = nqn1(inq)
       ipi = iparty(inq)
       i = 1
       do in = inc+1, nefig
          if (nnq(in) == inq) i = i + 1
       end do
       nt = i
       incup_L: do incup = 1, ncoumx(inq)
          lcfg = lval(incup,inq)
          ispin = ispval(incup,inq)
          do in = 1, nset
             if (lcfg == lset(in) .and. ispin == isset(in) .and. &
                  ipi == ipset(in)) then
                ncset(in) = ncset(in) + nt
                exit
             end if
          end do
       end do incup_L
    end do inq_L
    ncs_max = MAXVAL(ncset)
    write (fo,'(a,i6)') 'Max # cfg couplings per symmetry = ', ncs_max
  end subroutine estim_ncs

end module sym_sets
