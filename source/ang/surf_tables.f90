module surfacing_tables
! definition and manipulation of surfacing tables
! packed storage to take advantage of sparsity
! Time-stamp: "2005-03-11 14:46:36 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use error_prt, only: alloc_error
  use rm_data, only: sbk
  implicit none

! table of surfacing coefficients, notionally is
!                 surf(mcou, ncou, kshell, nefig)
! nefig = index of the N-electron configuration
! kshell = index of the shell which is being deprived of an electron
! ncou = index for the coupling of the N electron configuration
! mcou = index for the coupling of the N-1 electron configuration
!        when an electron from kshell is removed

  type sur_cof
     private
     integer, pointer  :: min_ind(:) => NULL()
     integer, pointer  :: max_ind(:) => NULL()
     integer, pointer  :: inds(:) => NULL()
     real(wp), pointer :: cofs(:) => NULL()
  end type sur_cof

  type(sur_cof), allocatable, target, save :: surf1(:,:) ! bnd, N,N-1
  type(sur_cof), allocatable, target, save :: surf2(:,:) ! bnd, N-1,N-2
  type(sur_cof), allocatable, target, save :: surf3(:,:) ! cont, N+1,N
  type(sur_cof), allocatable, target, save :: surf4(:,:) ! Table 1 Xtsn

  type scf
     private
     type(scf), pointer    :: next_scf(:) => NULL() ! link pointer
     integer, pointer      :: i(:)     ! i index
     integer, pointer      :: j(:)     ! j index
     real(wp), pointer     :: sc(:)    ! surfacing coefficient
  end type scf

  integer, save             :: nsz       ! surcof column dimension
  integer, save             :: nscf      ! total # scf list elements
  integer, save             :: nblk      ! # elements in current scf block
  type(scf), pointer, save  :: surcof(:) ! surfacing coeff linked list
  type(scf), pointer, save  :: scof(:)   ! surcof pointer
!$OMP THREADPRIVATE(surcof, nsz, nscf, nblk, scof)

  logical, save :: alloc_surf1 = .false.
  logical, save :: alloc_surf2 = .false.
  logical, save :: alloc_surf3 = .false.
  logical, save :: alloc_surf4 = .false.

  integer, save        :: t4mn = 0    ! min cfg in surf4

  private
  public alloc_surf_table, dealloc_surf_table
  public pack_indx, surf_tab, surf_vec
  public check_table
  public alloc_surcof, new_indx, zero_ctrs

contains

  subroutine check_table (id)
! Print statistics of Table of surfacing coefficients
    integer, intent(in)    :: id              ! table id
    type(sur_cof), pointer :: tab(:,:)
    real(wp)               :: tsum, atsum
    integer                :: i, j, n, blks

    if (id == 1) write (fo,'(/,a,i6)') 'surcof block-size: ', sbk
    n = 0
    blks = 0
    tsum = 0.0_wp
    atsum = 0.0_wp
    j = -1    ! id is an absolute table id
    tab => select_table (id, j)
    do j = LBOUND(tab, DIM=2), UBOUND(tab, DIM=2)
       do i = 1, SIZE(tab, DIM=1)
          if (.NOT.ASSOCIATED(tab(i,j)%cofs)) cycle
          blks = blks + 1
          n = n + SIZE(tab(i,j)%cofs)
          tsum = tsum + SUM(tab(i,j)%cofs)
          atsum = atsum + SUM(ABS(tab(i,j)%cofs))
       end do
    end do
    write (fo,'(/,a,i4)') 'Surfacing Table ', id
    write (fo,'(a,i10)') 'No. coefficient blocks = ', blks
    write (fo,'(a,i10)') 'No. elements           = ', n
    write (fo,'(a,f30.17)') 'Sum          = ', tsum
    write (fo,'(a,f30.17)') 'Absolute sum = ', atsum
    write (fo,'(/)')
  end subroutine check_table

  subroutine alloc_surf_table (id, shell_max, cfg_max, cfg_min)
! define a surfacing table
    integer, intent(in)   :: id          ! table id
    integer, intent(in)   :: shell_max   ! max # shells
    integer, intent(in)   :: cfg_max     ! max # configurations
    integer, intent(in), optional :: cfg_min ! min cfg #
    integer               :: status

    select case (id)
    case (1)
       allocate (surf1(shell_max,cfg_max), stat=status)
       alloc_surf1 = .true.
    case (2)
       allocate (surf2(shell_max,cfg_max), stat=status)
       alloc_surf2 = .true.
    case (3)
       allocate (surf3(shell_max,cfg_max), stat=status)
       alloc_surf3 = .true.
    case (4)
       if (present(cfg_min)) then
          t4mn = cfg_min
          if (t4mn /= UBOUND(surf1,DIM=2)+1) then
             write (fo,'(a)') 'alloc_surf_table: extension error'
             stop
          end if
       else
          t4mn = UBOUND(surf1,DIM=2) + 1
       end if
       allocate (surf4(shell_max,t4mn:cfg_max), stat=status)
       alloc_surf4 = .true.
    end select
    if (status /= 0) then
       write (fo,'(a,i6)') 'alloc_surf: allocation error = ', status
       stop
    end if
  end subroutine alloc_surf_table

  function select_table (id, cfg)
! select surfacing table
    type(sur_cof), pointer  :: select_table(:,:)
    integer, intent(in)     :: id    ! Table id
    integer, intent(in)     :: cfg   ! cfg #

    select_table => NULL()
    select case (id)
    case (1)
       if (t4mn == 0 .or. cfg < t4mn) then
          if (alloc_surf1) select_table => surf1
       else
          if (alloc_surf4) select_table => surf4   ! extension of table 1
       end if
    case(2)
       if (alloc_surf2) select_table => surf2
    case (3)
       if (alloc_surf3) select_table => surf3
    case(4)
       if (alloc_surf4) select_table => surf4
    end select
  end function select_table

  subroutine alloc_surcof (n)
! allocate head of surfacing coefficient linked ist
    integer, intent(in) :: n   ! notional surcof dimensions
    integer             :: status

    nsz = n   ! save surcof column dimension
    allocate (surcof(1), stat=status)
    if (status /= 0) call alloc_error (status, 'alloc_surcof', 'a')
    allocate (surcof(1)%i(sbk), surcof(1)%j(sbk), surcof(1)%sc(sbk),&
         stat=status)
    if (status /= 0) call alloc_error (status, 'alloc_surcof', 'a')
    scof => surcof
  end subroutine alloc_surcof

  subroutine new_indx (i, j, coef)
    integer, intent(in)     :: i   ! i index
    integer, intent(in)     :: j   ! j index
    real(wp), intent(in)    :: coef
    type(scf), pointer      :: scft(:)   ! temporary block pointer
    integer                 :: status

    if (coef == 0.0_wp) return    ! only non-zero values
    nblk = nblk + 1               ! block counter
    nscf = nscf + 1               ! coefficient counter
    if (nblk > sbk) then          ! block is full
       scft => scof
       allocate (scof(1), stat=status)
       if (status /= 0) call alloc_error (status, 'new_indx', 'a')
       allocate (scof(1)%i(sbk), scof(1)%j(sbk), scof(1)%sc(sbk), &
            stat=status)
       if (status /= 0) call alloc_error (status, 'new_indx(2)', 'a')
       scft(1)%next_scf => scof   ! set link index on previous block
       nblk = 1                   ! reset block pointer
       scof(1)%i = 0
       scof(1)%j = 0
       scof(1)%sc = 0.0_wp
    end if
    scof(1)%i(nblk) = i
    scof(1)%j(nblk) = j
    scof(1)%sc(nblk) = coef
  end subroutine new_indx

  subroutine pack_indx (id, shl, cfg)
! transform coefficients into packed form
    integer, intent(in)      :: id          ! table id
    integer, intent(in)      :: shl         ! shell index
    integer, intent(in)      :: cfg         ! cfg index
    integer                  :: n, nz, status, iz, i, j, nlnk, in
    integer, allocatable     :: ni(:)
    type(sur_cof), pointer   :: surft(:,:), surf
    type(scf), pointer       :: scoft(:)

    nz = nscf               ! non-zero element count for block
    if (nz == 0) return     ! there are no elements in list
    n = nsz                 ! set column dimension
    surft => select_table(id, cfg)

    allocate (surft(shl,cfg)%min_ind(n), surft(shl,cfg)%max_ind(n),   &
         surft(shl,cfg)%inds(nz), surft(shl,cfg)%cofs(nz), ni(n),     &
         stat=status)
    if (status /= 0) then
       write (fo,'(a,i6)') 'pack_indx: allocation error = ', status
       stop
    end if
    surf => surft(shl,cfg)
    surf%min_ind = 0
    surf%max_ind = 0
    surf%inds = 0

! determine coefficient counts for each column:
    ni = 0     ! zero column counts
    nblk = 0
    scoft => surcof
    nlnk = 1   ! link counter
    do iz = 1, nz
       nblk = nblk + 1
       if (nblk > sbk) then   ! get next link
          nlnk = nlnk + 1
          scoft => scoft(1)%next_scf
          nblk = 1
       end if
       j = scoft(1)%j(nblk)
       ni(j) = ni(j) + 1
    end do

    i = 0        ! set up column limit indicies
    do j = 1, n
       if (ni(j) == 0) cycle 
       surf%min_ind(j) = i + 1
       i = i + ni(j)
       surf%max_ind(j) = i
    end do

! second pass of surcof: read back coefficients
    ni = 0       ! ni will now hold column counts
    scoft => surcof
    nblk = 0
    do iz = 1, nz
       nblk = nblk + 1
       if (nblk > sbk) then
          scoft => scoft(1)%next_scf
          nblk = 1
       end if
       i = scoft(1)%i(nblk)
       j = scoft(1)%j(nblk)
       in = surf%min_ind(j) + ni(j)    ! output index
       surf%cofs(in) = scoft(1)%sc(nblk)
       if (surf%inds(in) /= 0) then
          write (fo,'(a)') 'pack_indx: surf table index error'
          stop
       end if
       surf%inds(in) = i
       ni(j) = ni(j) + 1
    end do

! now delete link list:
    do j = 1, nlnk
       scoft => surcof(1)%next_scf
       deallocate (surcof(1)%i, surcof(1)%j, surcof(1)%sc, &
            stat=status)
       if (status /= 0) call alloc_error (status, 'pack_indx', 'd')
       deallocate (surcof, stat=status)
       if (status /= 0) call alloc_error (status, 'pack_indx', 'd')
       if (.not.associated(scoft)) exit
       surcof => scoft
    end do
    deallocate (ni, stat=status)
    if (status /= 0) call alloc_error (status, 'pack_indx', 'd')
    nblk = 0
    nscf = 0
  end subroutine pack_indx

  subroutine zero_ctrs
    nblk = 0
    nscf = 0
  end subroutine zero_ctrs

  subroutine surf_tab (id, act, cfg, mini, maxi, ind, cof)
! retrieve a packed matrix of surfacing coefficients
    integer, intent(in)     :: id       ! table id
    integer, intent(in)     :: act      ! active shell index
    integer, intent(in)     :: cfg      ! configuration index
    integer, pointer        :: mini(:)  ! min index array
    integer, pointer        :: maxi(:)  ! max index array
    integer, pointer        :: ind(:)   ! coefficient indicies
    real(wp), pointer       :: cof(:)   ! surfacing coefficients
    type(sur_cof), pointer  :: surf(:,:), surft

    surf => select_table(id, cfg)
    surft => surf(act,cfg)
!   if (.NOT.associated(surft)) then
!      write (fo,'(a)') 'surf_tab: unassocaited table requested'
!      stop
!   end if
    cof => surft%cofs
    ind => surft%inds
    mini => surft%min_ind
    maxi => surft%max_ind
  end subroutine surf_tab

  subroutine surf_vec (id, col, act, cfg, ind, cof)
! retrieve a packed column of surfacing coefficients and indicies
    integer, intent(in)     :: id      ! table id
    integer, intent(in)     :: col     ! column index
    integer, intent(in)     :: act     ! active shell index
    integer, intent(in)     :: cfg     ! configuration index
    integer, pointer        :: ind(:)  ! cofficient indicies
    real(wp), pointer       :: cof(:)  ! surfacing coefficients
    type(sur_cof), pointer  :: surf(:,:), surft
    integer                 :: i1, i2

    surf => select_table(id, cfg)
    surft => surf(act,cfg)
    if (.NOT.associated(surft)) then
       write (fo,'(a)') 'surf_vec: unassocaited table requested'
       stop
    end if
    i1 = surft%min_ind(col)
    i2 = surft%max_ind(col)
    if (i1 == 0) then
       ind => NULL()
       cof => NULL()
    else
       ind => surft%inds(i1:i2)
       cof => surft%cofs(i1:i2)
    end if
  end subroutine surf_vec

  subroutine dealloc_surf_table (id)
! delete a surfacing table
    integer, intent(in)     :: id
    integer                 :: status, i, j, jmin
    type(sur_cof), pointer  :: surf(:,:)

    select case (id)
    case (1)
       if (.NOT.ALLOCATED(surf1)) return
    case (2)
       if (.NOT.ALLOCATED(surf2)) return
    case (3)
       if (.NOT.ALLOCATED(surf3)) return
    case (4)
       if (.NOT.ALLOCATED(surf4)) return
    end select
    j = -1      ! id is absolute table id
    surf => select_table(id, j)
    if (.NOT.ASSOCIATED(surf)) return
    jmin = LBOUND(surf,DIM=2)
    do j = jmin, SIZE(surf,DIM=2)
       do i = 1, SIZE(surf,DIM=1)
          if (.NOT.associated(surf(i,j)%cofs)) cycle
          deallocate (surf(i,j)%min_ind, surf(i,j)%cofs, &
               surf(i,j)%max_ind, surf(i,j)%inds, stat=status)
          if (status /= 0) then
             write (fo,'(a,i6)') 'dealloc_surf: deallocation &
                  &error = ', status
             stop
          end if
       end do
    end do
    select case (id)
    case(1)
       deallocate (surf1, stat=status)
    case(2)
       deallocate (surf2, stat=status)
    case(3)
       deallocate (surf3, stat=status)
    case(4)
       deallocate (surf4, stat=status)
    end select
    if (status /= 0) then
       write (fo,'(a,i6)') 'dealloc_surf: deallocation error = ', status
       stop
    end if
  end subroutine dealloc_surf_table
end module surfacing_tables
