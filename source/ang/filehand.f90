module filehand
! FILEHAND: double buffered file handling system written by
!           V.M. Burke
! TRANSFER and inquire used for portability
! Time-stamp: "02/01/25 09:32:29 cjn"
  use precisn, only: wp
  use io_units, only: fo
  implicit none
  type buffer
     private
     real(wp), pointer  :: b1(:)
     real(wp), pointer  :: b2(:)
     real(wp), pointer  :: b3(:)
     integer, pointer   :: i1(:)
     integer, pointer   :: i2(:)
     integer, pointer   :: i3(:)
  end type buffer
  integer, parameter     :: lbuff  =  256
  integer, parameter     :: list  =  20
  integer, parameter     :: ibnk  =  12
  type(buffer), save     :: buf(list)    ! filehand buffers
  logical, save          :: rt(list)    ! real/integer file type
  integer, save          :: macdim
  integer, save          :: nipw       ! # integers per wp word
  integer, parameter     :: lbufm1  =  lbuff  -  1
  integer, save          :: ib(list)     ! double buffering switch (+-1)
  integer, save          :: iover(list)
  integer, save          :: junit(99)
  integer, save          :: iunit(99)    ! list of peripheral unit #s
  integer, save          :: nlist
  integer, save          :: ibuf(list)   ! last used posn in buffer
  integer, save          :: irec(list)
  integer, save          :: nfinis(list)
  integer, save          :: nbank(list)
  integer, save          :: ibank(ibnk,list)

  private
  public catlog, close, delfil, endovr, endw, initda, ireada, ireadb,&
       ireadc, iwrita, iwritb, iwritc, lookup, moveb, movef, &
       newbuf, newfil, open, overw, reada, readb, readc, readix, &
       reopen, startr, tclose, writa, writb, writc, writix

contains
  subroutine catlog (nunit, icatl)
! called to find the next available position on unit nunit.
! used to construct a catalogue of the contents of the file during
! writing. The logic follows that of subroutine writa.
    integer, intent(in)     :: nunit
    integer, intent(out)    :: icatl
    integer                 :: lunit, ibufi, ibi, nrec, iov, nbanki, n
    integer                 :: nr1, nr2, nr3
    logical                 :: ft

    lunit = junit(nunit)      ! logical unit
    ft = rt(lunit)            ! buffer type
    ibufi = ibuf(lunit) + 1   ! buffer #

! find nrec from the buffer which is not currently in use
    ibi = ib(lunit)
    if (ft) then
       nr1 = TRANSFER(buf(lunit)%b1(lbuff), n)
       nr2 = TRANSFER(buf(lunit)%b2(lbuff), n)
       nr3 = TRANSFER(buf(lunit)%b3(lbuff), n)
    else
       nr1 = buf(lunit)%i1(lbuff)
       nr2 = buf(lunit)%i2(lbuff)
       nr3 = buf(lunit)%i3(lbuff)
    end if
    if (ibi  >  0) then
       nrec = nr2
    else
       nrec = nr1
    end if

! treat the case of the start of a new buffer.  the record to
! which the buffer is to be written has not been reserved unless
! it is the first record in the file.
    if (ibufi == 1) then  ! test for start of file
       if (ibi > 0 .and. nr1 == 0 .and. nr2 /= 0) then
          icatl = nrec*lbufm1 + ibufi
          return
       end if
! find next available record: first try the overwrite facility
       iov = 0
       if (iover(lunit)  >  0) then
          iov = 1
          nrec = nr3
! if record # = 0, file currently being overwritten is finished.
! try the overwite bank for another file.
          if (nrec == 0) iov = 0
       end if
       if (iov == 0) then      ! try for another file
          nbanki = nbank(lunit)
          if (nbanki  >  0) then
             nrec = ibank(nbanki,lunit)
             if (nrec == 0) then
                write (fo,'(a,i6)') 'CATLOG: coding error  record # &
                     &obtained from bank is zero on unit ', nbanki, &
                     ' nbank = ', nunit
                stop
             end if
          else ! since the overwrite bank is empty use new record
             nrec = nfinis(lunit) + 1
          end if
       end if
    end if
    icatl = nrec * lbufm1 + ibufi
  end subroutine catlog

  function l2u(s)
! change lower case strings to upper case
    character(len=*), intent(in) :: s           ! character string
    character(len=len(s))        :: l2u
    integer, parameter           :: co = ichar('a')-ichar('A') ! offset
    integer                      :: i
  
    l2u = s
    do i = 1, LEN(s)
       select case (s(i:i))
       case ("a":"z")
          l2u(i:i) = CHAR(ICHAR(s(i:i))-co)
       end select
    end do
  end function l2u

  subroutine close (nunit, stats)
! to release the unit number nunit from the list of unit numbers under
! the control of filehand
! the housekeeping data is written to the first block if stats = 'keep'
! if stats = 'scratch' the first block is not written
! the buffers associated with nunit are released for reuse
    integer, intent(in)          :: nunit
    character(len=7), intent(in) :: stats
    integer                      :: lunit, nbanki, ios, status
    logical                      :: ft

    lunit = junit(nunit)
    ft = rt(lunit)

    if (l2u(stats) /= 'SCRATCH') then
       call startw (nunit, 1)
       nbanki = nbank(lunit)
       call iwrita (nunit, nbanki)
       if (nbanki /= 0) call iwritb (nunit, ibank(:,lunit), 1, nbanki)
       call iwrita (nunit, nfinis(lunit))
       call endw (nunit)
    end if

    iunit(lunit) = 0
    junit(nunit) = 0
    nlist = nlist - 1

    if (l2u(stats) == 'SCRATCH') then
       close (nunit, status = 'delete', iostat=ios)
    else
       close (nunit, status = 'keep', iostat=ios)
       write (fo,'(a,i2)') 'closed direct access file on unit ', nunit
    end if
    if (ios /= 0) then
       write (fo,'(a,i4,a,i4)') 'Error closing unit ', nunit, &
            ', iostat = ', ios
       stop
    end if
    if (ft) then
       deallocate (buf(lunit)%b1, buf(lunit)%b2, buf(lunit)%b3, &
            stat=status)
    else
       deallocate (buf(lunit)%i1, buf(lunit)%i2, buf(lunit)%i3, &
            stat=status)
    end if
    if (status /= 0) then
       write (fo,'(a,i4)') 'FILEHAND: deallocation error = ', status
       stop
    end if
  end subroutine close

  subroutine delfil (nunit, nrec)
! Delete subfile: place entire file in overwrite bank
    integer, intent(in)     :: nunit  ! unit #
    integer, intent(in)     :: nrec   ! starting record of subfile
    integer                 :: lunit, nbanki

    lunit = junit(nunit)
    if (nrec /= 0) then
       nbanki = nbank(lunit)
       if (nbanki < ibnk) then
          nbanki = nbanki + 1
          ibank(nbanki,lunit) = nrec
          nbank(lunit) = nbanki
       else
          write (fo,'(a,i3,a,i4)') 'DEFIL: warning, no delete on &
               &unit ', nunit, ' at record ', nrec
          write (fo,'(a)') 'overwrite bank is full'
       end if
    else
       write (fo, '(a,i3,a)') 'DEFIL: warning, no delete on unit ',&
            nunit, ' record set zero'
    end if
  end subroutine delfil

  subroutine endovr (nunit)
! terminate overwriting part of a subfile
    integer, intent(in)     :: nunit
    integer                 :: lunit, ibuf1, nrec, ia, iia, n
    logical                 :: ft

    lunit = junit(nunit)
    ft = rt(lunit)          ! buffer type (real/integer)
    ibuf1 = ibuf(lunit)     ! buffer start location

! first test the buffer switch to find the current buffer
    if (ib(lunit)  >  0) then
! test if this is a new buffer not yet containing information
       if (ibuf1 == 0) then
! write other buffer to disc if a record number has been reserved
          if (ft) then
             buf(lunit)%b2(lbuff) = buf(lunit)%b3(lbuff)
             nrec = TRANSFER(buf(lunit)%b1(lbuff), n)
          else
             buf(lunit)%i2(lbuff) = buf(lunit)%i3(lbuff)
             nrec = buf(lunit)%i1(lbuff)
          end if
          if (nrec == 0) then
             write (fo, '(a,i6)') 'ENDOVR: warning ending a subfile to &
                  &which nothing has been written on unit ',  nunit
             return
          end if
          if (ft) then
             write (nunit, rec=nrec) buf(lunit)%b2
          else
             write (nunit, rec=nrec) buf(lunit)%i2
          end if
       else ! write bufer1 to disc if possible
          if (ft) then
             nrec = TRANSFER(buf(lunit)%b2(lbuff), n)
          else
             nrec = buf(lunit)%i2(lbuff)
          end if
          if (nrec == 0) then
             write (fo, '(a,i6)') 'ENDOVR: no record # available to &
                  &end subfile on unit ', nunit
             stop
          end if
!
! copy unedited part of buffer before writing to disc
          if (ft) then
             do ia = ibuf1 + 1, lbuff
                buf(lunit)%b1(ia) = buf(lunit)%b3(ia)
             end do
             write (nunit,rec=nrec) buf(lunit)%b1
          else
             do ia = ibuf1 + 1, lbuff
                buf(lunit)%i1(ia) = buf(lunit)%i3(ia)
             end do
             write (nunit,rec=nrec) buf(lunit)%i1
          end if
       end if
    else ! use bufer2
       ibuf1 = ibuf(lunit)
       if (ibuf1 == 0) then
          if (ft) then
             buf(lunit)%b1(lbuff) = buf(lunit)%b3(lbuff)
             nrec = TRANSFER(buf(lunit)%b2(lbuff), n)
          else
             buf(lunit)%i1(lbuff) = buf(lunit)%i3(lbuff)
             nrec = buf(lunit)%i2(lbuff)
          end if
          if (nrec == 0) then
             write (fo, '(a,i6)') 'ENDOVR: warning ending a subfile to &
                  &which nothing has been written on unit ',  nunit
             return
          end if
          if (ft) then
             write (nunit,rec=nrec) buf(lunit)%b1
          else
             write (nunit,rec=nrec) buf(lunit)%i1
          end if
       else
          if (ft) then
             nrec = TRANSFER(buf(lunit)%b1(lbuff), n)
          else
             nrec = buf(lunit)%i1(lbuff)
          end if
          if (nrec == 0) then
             write (fo, '(a,i6)') 'ENDOVR: no record # available to &
                  &end subfile on unit ', nunit
             stop
          end if
          if (ft) then
             do iia = ibuf1 + 1, lbuff
                buf(lunit)%b2(iia) = buf(lunit)%b3(iia)
             end do
             write (nunit,rec=nrec) buf(lunit)%b2
          else
             do iia = ibuf1 + 1, lbuff
                buf(lunit)%i2(iia) = buf(lunit)%i3(iia)
             end do
             write (nunit,rec=nrec) buf(lunit)%i2
          end if
       end if
    end if
    iover(lunit) = 0
  end subroutine endovr

  subroutine endw (nunit)
! Terminate the current subfile on unit nunit
    integer, intent(in)     :: nunit
    integer                 :: lunit, ibuf1, nrec, nfin, ia, iia
    integer                 :: nbanki, n
    real(wp)                :: r
    logical                 :: ft

    lunit = junit(nunit)
    ft = rt(lunit)
    ibuf1 = ibuf(lunit)
! first test the buffer switch to find the current buffer
    if (ib(lunit)  >  0) then
! test if this is a new buffer not yet containing information
       if (ibuf1 == 0) then
! write other buffer to disc if a record number has been reserved for it
          if (ft) then
             buf(lunit)%b2(lbuff) = TRANSFER(0,r)
             nrec = TRANSFER(buf(lunit)%b1(lbuff), n)
             if (nrec == 0) then
                write (fo, '(a,i5)') 'ENDW: warning ending a subfile to &
                     &which nothing has been written on unit ',  nunit
                return
             end if
             write (nunit,rec=nrec) buf(lunit)%b2
          else
             buf(lunit)%i2(lbuff) = 0
             nrec = buf(lunit)%i1(lbuff)
             if (nrec == 0) then
                write (fo, '(a,i5)') 'ENDW: warning ending a subfile to &
                     &which nothing has been written on unit ',  nunit
                return
             end if
             write (nunit,rec=nrec) buf(lunit)%i2
          end if
       else ! write bufer1 to disc if possible
          if (ft) then
             nrec = TRANSFER(buf(lunit)%b2(lbuff), n)
             if (nrec == 0) then
                write (fo, '(a,i6)') 'ENDW: no record # available to &
                     &end subfile on unit ', nunit
                stop
             end if

! fill unused part of buffer with zeroes before writing to disc
             do ia = ibuf1 + 1, lbuff
                buf(lunit)%b1(ia) = TRANSFER(0,r)
             end do
             write (nunit,rec=nrec) buf(lunit)%b1
          else
             nrec = buf(lunit)%i2(lbuff)
             if (nrec == 0) then
                write (fo, '(a,i6)') 'ENDW: no record # available to &
                     &end subfile on unit ', nunit
                stop
             end if
             do ia = ibuf1 + 1, lbuff
                buf(lunit)%i1(ia) = 0
             end do
             write (nunit,rec=nrec) buf(lunit)%i1
          end if
       end if

    else ! use bufer2
       ibuf1 = ibuf(lunit)
       if (ibuf1 == 0) then
          if (ft) then
             buf(lunit)%b1(lbuff) = TRANSFER(0, r)
             nrec = TRANSFER(buf(lunit)%b2(lbuff), n)
             if (nrec == 0) then
                write (fo, '(a,i5)') 'ENDW: warning ending a subfile to &
                     &which nothing has been written on unit ',  nunit
                return
             end if
             write (nunit, rec=nrec) buf(lunit)%b1
          else
             buf(lunit)%i1(lbuff) = 0
             nrec = buf(lunit)%i2(lbuff)
             if (nrec == 0) then
                write (fo, '(a,i5)') 'ENDW: warning ending a subfile to &
                     &which nothing has been written on unit ',  nunit
                return
             end if
             write (nunit, rec=nrec) buf(lunit)%i1
          end if
       else
          if (ft) then
             nrec = TRANSFER(buf(lunit)%b1(lbuff), n)
             if (nrec == 0) then
                write (fo, '(a,i6)') 'ENDW: no record # available to &
                     &end subfile on unit ', nunit
                stop
             end if
             do iia = ibuf1 + 1, lbuff
                buf(lunit)%b2(iia) = TRANSFER(0, r)
             end do
             write (nunit, rec=nrec) buf(lunit)%b2
          else 
             nrec = buf(lunit)%i1(lbuff)
             if (nrec == 0) then
                write (fo, '(a,i6)') 'ENDW: no record # available to &
                     &end subfile on unit ', nunit
                stop
             end if
             do iia = ibuf1 + 1, lbuff
                buf(lunit)%i2(iia) = 0
             end do
             write (nunit, rec=nrec) buf(lunit)%i2
          end if
       end if
    end if

! test the overwrite switch
! if it is on and the end of file has not been reached
! of the sub - file being overwritten enter the record
! number of the next available block in ibank
    if (iover(lunit) /= 0) then
       if (ft) then
          nfin = TRANSFER(buf(lunit)%b3(lbuff), n)
       else 
          nfin = buf(lunit)%i3(lbuff)
       end if
       if (nfin /= 0) then
          nbanki = nbank(lunit)
          if (nbanki < ibnk) then
             nbanki = nbanki + 1
             ibank(nbanki,lunit) = nfin
             nbank(lunit) = nbanki
          end if
       end if
    end if
  end subroutine endw

  subroutine initda
! initialise the list of unit numbers to zero

    iunit = 0      ! list values
    nlist = 0
    junit = 0      ! 99 unit #s
    call def_nipw
  end subroutine initda

  subroutine ireada (nunit, ia)
! move to next empty posn in buffer; switch to current buffer, read ia
    integer, intent(in)     :: nunit    ! unit #
    integer, intent(out)    :: ia       ! data read
    integer                 :: lunit, ibuf1, nrec

    lunit = junit(nunit)
    if (rt(lunit)) then      ! real buffer type
       call ireadar (nunit, ia)
       return
    end if
    ibuf1 = ibuf(lunit)  +  1
    if (ib(lunit)  >  0) then   ! buffer 1
       ia = buf(lunit)%i1(ibuf1)

! test if end of buffer; if so ia now contains the pointer to next block
! if a /= 0 there is a continuation block already in the other buffer.
! Test pointer in next buffer; if /= 0 read next continuation block to 
! overwrite current buffer and set the switch to use the other buffer.
! if ia = 0 the end has been reached of this sub - file.

       if (ibuf1 == lbuff) then ! end of buffer
          if (ia /= 0) then     ! next record number
             irec(lunit) = ia
             nrec = buf(lunit)%i2(lbuff)
             if (nrec /= 0) read (nunit,rec=nrec) buf(lunit)%i1
             ib(lunit) =  - 1
             ibuf1 = 1
             ia = buf(lunit)%i2(ibuf1)   ! required data
          else
             write (fo, '(a,i6)') 'IREADA: tried to read past end of &
                  &data on unit ', nunit
             stop
          end if
       end if
    else     ! use other buffer
       ia = buf(lunit)%i2(ibuf1)
       if (ibuf1 == lbuff) then     ! end of buffer
          if (ia /= 0) then
             irec(lunit) = ia
             nrec = buf(lunit)%i1(lbuff)
             if (nrec /= 0) read (nunit,rec=nrec) buf(lunit)%i2
             ib(lunit) = 1
             ibuf1 = 1
             ia = buf(lunit)%i1(ibuf1) ! required data
          else
             write (fo, '(a,i6)') 'IREADA: tried to read past end of &
                  &data on unit ', nunit
             stop
          end if
       end if
    end if
    ibuf(lunit) = ibuf1
  end subroutine ireada

  subroutine ireadar (nunit, ia)
! move to next empty posn in buffer; switch to current buffer, read ia
! assume real buffers and that data is on wp boundaries
    integer, intent(in)     :: nunit    ! unit #
    integer, intent(out)    :: ia       ! data read
    integer                 :: lunit, ibuf1, nrec, n

    lunit = junit(nunit)
    ibuf1 = ibuf(lunit)  +  1
    if (ib(lunit)  >  0) then   ! buffer 1
       ia = TRANSFER(buf(lunit)%b1(ibuf1), n)

! test if end of buffer; if so ia now contains the pointer to next block
! if a /= 0 there is a continuation block already in the other buffer.
! Test pointer in next buffer; if /= 0 read next continuation block to 
! overwrite current buffer and set the switch to use the other buffer.
! if ia = 0 the end has been reached of this sub - file.

       if (ibuf1 == lbuff) then ! end of buffer
          if (ia /= 0) then     ! next record number
             irec(lunit) = ia
             nrec = TRANSFER(buf(lunit)%b2(lbuff), n)
             if (nrec /= 0) read (nunit,rec=nrec) buf(lunit)%b1
             ib(lunit) =  - 1
             ibuf1 = 1
             ia = TRANSFER(buf(lunit)%b2(ibuf1), n)   ! required data
          else
             write (fo, '(a,i6)') 'IREADA: tried to read past end of &
                  &data on unit ', nunit
             stop
          end if
       end if
    else     ! use other buffer
       ia = TRANSFER(buf(lunit)%b2(ibuf1), n)
       if (ibuf1 == lbuff) then     ! end of buffer
          if (ia /= 0) then
             irec(lunit) = ia
             nrec = TRANSFER(buf(lunit)%b1(lbuff), n)
             if (nrec /= 0) read (nunit,rec=nrec) buf(lunit)%b2
             ib(lunit) = 1
             ibuf1 = 1
             ia = TRANSFER(buf(lunit)%b1(ibuf1), n) ! required data
          else
             write (fo, '(a,i6)') 'IREADA: tried to read past end of &
                  &data on unit ', nunit
             stop
          end if
       end if
    end if
    ibuf(lunit) = ibuf1
  end subroutine ireadar

  subroutine ireadb (nunit, ibb, i1, i2)
! read ni values from nunit into array ibb
! replace read(nunit) array(i1:i2) by call ireadb(nunit,iarray,i1,i2)
    integer, intent(in)     :: nunit     ! unit number
    integer, intent(in)     :: i1, i2    ! bounds
    integer, intent(out)    :: ibb(i2)   ! data read from file
    integer                 :: lunit, ibuf1, irec1, ib1, ia, ni, nb
    integer                 :: nloop, nl, nrec, nq

    lunit = junit(nunit)   ! unit #
    if (rt(lunit)) then
       call ireadbr (nunit, ibb, i1, i2)
       return
    end if
    ibuf1 = ibuf(lunit)    ! buffer position
    irec1 = irec(lunit)
    ib1 = ib(lunit)
    ia = i1 - 1
    ni = (i2 - ia)           ! # integers to be read
    nb = lbufm1 - ibuf1 ! space remaining in the buffer
    if (ni  >  nb) then ! data spreads at least into next block
       nloop = 1 + (ni - nb)/lbufm1
       records: do nl = 1, nloop
          nq = lbufm1 - ibuf1  ! # integers
          if (ib1  >  0) then ! decide which buffer to read first
             nrec = buf(lunit)%i1(lbuff)    ! next record
             ibb(ia+1:ia+nq) = buf(lunit)%i1(ibuf1+1:lbufm1)
             ia = ia + nq
! if there are two more continuation blocks overwrite this buffer.
!  proceed to read next block from the other buffer.
             if (nrec /= 0) then
                irec1 = nrec
                nrec = buf(lunit)%i2(lbuff) ! next record
                if (nrec /= 0) read (nunit,rec=nrec) buf(lunit)%i1
                ib1 =  - 1
             else if (ia < i2) then ! no continuation block => error
                write (fo,'(a,i6)') 'IREADB: read past end of data&
                     & on unit ', nunit
                stop
             end if
          else ! read from other buffer
             nrec = buf(lunit)%i2(lbuff)
             ibb(ia+1:ia+nq) = buf(lunit)%i2(ibuf1+1:lbufm1)
             ia = ia + nq
             if (nrec /= 0) then
                irec1 = nrec
                nrec = buf(lunit)%i1(lbuff)
                if (nrec /= 0) read (nunit,rec=nrec) buf(lunit)%i2
                ib1 = 1
             else if (ia < i2) then
                write (fo,'(a,i6)') 'IREADB: read past end of data&
                     & on unit ', nunit
                stop
             end if
          end if
          ibuf1 = 0
       end do records
    end if

! now read remaining values into b
    nq = i2 - ia
    if (ib1  >  0) then
       ibb(ia+1:i2) = buf(lunit)%i1(ibuf1+1:ibuf1+nq)
    else
       ibb(ia+1:i2) = buf(lunit)%i2(ibuf1+1:ibuf1+nq)
    end if
    ibuf(lunit) = ibuf1 + nq
    irec(lunit) = irec1
    ib(lunit) = ib1
  end subroutine ireadb

  subroutine ireadbr (nunit, ibb, i1, i2)
! read ni values from nunit into array ibb
! Assume real buffers
! NB: reads and writes must be identical in this case
! replace read(nunit) array(i1:i2) by call ireadb(nunit,iarray,i1,i2)
    integer, intent(in)     :: nunit     ! unit number
    integer, intent(in)     :: i1, i2    ! bounds
    integer, intent(out)    :: ibb(i2)   ! data read from file
    integer                 :: lunit, ibuf1, irec1, ib1, ia, ni, nb
    integer                 :: nloop, nl, nrec, niq, n, nq

    lunit = junit(nunit)   ! unit #
    ibuf1 = ibuf(lunit)    ! buffer position
    irec1 = irec(lunit)
    ib1 = ib(lunit)
    ia = i1 - 1
    ni = i2 - ia             ! # integers to be read
    niq = nwp(ni)            ! # equivalent wp values (dangerous!)

    nb = lbufm1 - ibuf1 ! space remaining in the buffer
    if (niq  >  nb) then  ! data spreads at least into next block
       nloop = 1 + (niq - nb)/lbufm1
       records: do nl = 1, nloop
          if (ib1  >  0) then ! decide which buffer to read first
             nrec = TRANSFER(buf(lunit)%b1(lbuff),n) ! next record
             nq = (lbufm1 - ibuf1) * nipw   ! # integers
             ibb(ia+1:ia+nq) = TRANSFER(buf(lunit)%b1(ibuf1+1:lbufm1),&
                  ibb,nq)
             ia = ia + nq
! if there are two more continuation blocks overwrite this buffer.
!  proceed to read next block from the other buffer.
             if (nrec /= 0) then
                irec1 = nrec
                nrec = TRANSFER(buf(lunit)%b2(lbuff),n) ! next record
                if (nrec /= 0) read (nunit,rec=nrec) buf(lunit)%b1
                ib1 =  - 1
             else if (ia < i2) then ! no continuation block => error
                write (fo,'(a,i6)') 'IREADB: read past end of data&
                     & on unit ', nunit
                stop
             end if
          else ! read from other buffer
             nrec = TRANSFER(buf(lunit)%b2(lbuff), n)
             nq = (lbufm1 - ibuf1) * nipw   ! # integers
             ibb(ia+1:ia+nq) = TRANSFER(buf(lunit)%b2(ibuf1+1:lbufm1),&
                  ibb,nq)
             ia = ia + nq
             if (nrec /= 0) then
                irec1 = nrec
                nrec = TRANSFER(buf(lunit)%b1(lbuff), n)
                if (nrec /= 0) read (nunit,rec=nrec) buf(lunit)%b2
                ib1 = 1
             else if (ia < i2) then
                write (fo,'(a,i6)') 'IREADB: read past end of data&
                     & on unit ', nunit
                stop
             end if
          end if
          ibuf1 = 0
       end do records
    end if

! now read remaining values into b
    nq = i2 - ia      ! # integers
    niq = nwp(nq)     ! # wp words
    if (ib1  >  0) then
       ibb(ia+1:i2) = TRANSFER(buf(lunit)%b1(ibuf1+1:ibuf1+niq), &
            ibb, nq)
    else
       ibb(ia+1:i2) = TRANSFER(buf(lunit)%b2(ibuf1+1:ibuf1+niq), &
            ibb, nq)
    end if
    ibuf(lunit) = ibuf1 + niq
    irec(lunit) = irec1
    ib(lunit) = ib1
  end subroutine ireadbr

  function nwp (n)
! # wp storage positions to hold n integers
    integer             :: nwp
    integer, intent(in) :: n       ! # integers
    if (nipw == 1) then
       nwp = n
    else
       nwp = (n + nipw - 1) / nipw
    end if
  end function nwp

  subroutine def_nipw
! find storage length associations:
    integer       :: i(2)
    real(wp)      :: r
    r = 1.0_wp
    nipw = SIZE(TRANSFER(r, i))
    write (fo,'(a,i4)') 'integer / working precision size = ', nipw
    if (nipw <= 0) then
       write (fo,'(a)') 'FILEHAND: nipw specification failure'
       stop
    end if
  end subroutine def_nipw
    
  subroutine ireadc (nunit, ic, i1, i2, j1, j2, idim, indic)
! Read into 2-D array ic from nunit
! read(nunit)ic(i1:i2,j1:j2) by call ireadc(nunit,ic,i1,i2,j1,j2,idim,0)
    integer, intent(in)     :: nunit       ! unit #
    integer, intent(in)     :: i1, i2      ! first index bounds
    integer, intent(in)     :: j1, j2      ! second index bounds
    integer, intent(in)     :: idim        ! first dimension of ic
    integer, intent(in)     :: indic       ! transpose switch (0,1)
    integer, intent(out)    :: ic(idim, j2)
    integer                 :: id(j2-j1+1)
    integer                 :: ja, jup, i, jj, j

    if (indic == 0) then ! test transpose switch, 0 => normal ordering
       do ja = j1, j2
          call ireadb (nunit, ic(:,ja), i1, i2)
       end do
    else ! read reversing indices
       jup = j2 - j1 + 1
       do i = i1, i2
          call ireadb (nunit, id, 1, jup)
          jj = 0
          do j = j1, j2
             jj = jj + 1
             ic(i,j) = id(jj)
          end do
       end do
    end if
  end subroutine ireadc

  subroutine iwrita (nunit, ia)
! Add ia into current buffer
! If buffer empty, initialise it and write the previous (if any) to disc
! If buffer full after adding ia, activate other buffer for next call
! Set counter ibuf1 to zero (Assumes that there is room in the buffer 
! for a an entry and that a record # is reserved for a block only when
! there is data to put into it)
    integer, intent(in)     :: nunit         ! unit number
    integer, intent(in)     :: ia            ! data to be written
    integer                 :: lunit, ibuf1

    lunit = junit(nunit)      ! logical unit number
    if (rt(lunit)) then
       call iwritar (nunit, ia)
       return
    end if
    ibuf1 = ibuf(lunit) + 1   ! buffer position
    if (ibuf1 == 1) then      ! this is a new buffer
       call newbuf (nunit)
    end if
    if (ib(lunit)  >  0) then   ! use first buffer
       buf(lunit)%i1(ibuf1) = ia
    else                        ! use other buffer
       buf(lunit)%i2(ibuf1) = ia
    end if

    if (ibuf1 == lbufm1) then ! reached end of buffer, switch to next
       ib(lunit) =  - ib(lunit)
       ibuf1 = 0
    end if
    ibuf(lunit) = ibuf1
  end subroutine iwrita

  subroutine iwritar (nunit, ia)
! Add ia into current buffer of a real-buffer file
! assume each write occurs at a wp boundary
! If buffer empty, initialise it and write the previous (if any) to disc
! If buffer full after adding ia, activate other buffer for next call
! Set counter ibuf1 to zero (Assumes that there is room in the buffer 
! for a an entry and that a record # is reserved for a block only when
! there is data to put into it)
    integer, intent(in)     :: nunit         ! unit number
    integer, intent(in)     :: ia            ! data to be written
    integer                 :: lunit, ibuf1
    real(wp)                :: r

    lunit = junit(nunit)      ! logical unit number
    ibuf1 = ibuf(lunit) + 1   ! buffer position
    if (ibuf1 == 1) then      ! this is a new buffer
       call newbuf (nunit)
    end if
    if (ib(lunit)  >  0) then   ! use first buffer
       buf(lunit)%b1(ibuf1) = TRANSFER(ia,r)
    else                        ! use other buffer
       buf(lunit)%b2(ibuf1) = TRANSFER(ia,r)
    end if

    if (ibuf1 == lbufm1) then ! reached end of buffer, switch to next
       ib(lunit) =  - ib(lunit)
       ibuf1 = 0
    end if
    ibuf(lunit) = ibuf1
  end subroutine iwritar

  subroutine iwritb (nunit, ibb, i1, i2)
! Copy array ibb into a buffer; buffer written to disc as filled
    integer, intent(in)     :: nunit        ! unit id
    integer, intent(in)     :: i1, i2       ! array bounds
    integer, intent(in)     :: ibb(i2)      ! data to be written
    integer                 :: lunit, ibuf1, ia, nb, ni, nloop
    integer                 :: nl, nq

    lunit = junit(nunit)      ! logical unit number
    if (rt(lunit)) then       ! real buffers
       call iwritbr (nunit, ibb, i1, i2)
       return
    end if
    ibuf1 = ibuf(lunit)       ! buffer location
    if (ibuf1 == 0) then ! new block; reserve disc block, write previous
       call newbuf (nunit)
    end if

    ia = i1 - 1           ! data start
    nb = lbufm1 - ibuf1   ! space in buffer
    ni = i2 - ia          ! # integers to write out
    if (ni  >  nb) then   ! data will spread into next block(s)
       nloop = 1 + (ni - nb)/lbufm1   ! # records
       records: do nl = 1, nloop
          nq = lbufm1 - ibuf1
          if (ib(lunit)  >  0) then    ! use first buffer
             buf(lunit)%i1(ibuf1+1:lbufm1) = ibb(ia+1:ia+nq)
             ib(lunit) =  - 1
          else            ! use other buffer
             buf(lunit)%i2(ibuf1+1:lbufm1) = ibb(ia+1:ia+nq)
             ib(lunit) = 1
          end if
          ia = ia + nq
          ibuf1 = 0
          if (ia < i2) call newbuf (nunit)
       end do records
    end if

! now write remainder of the data to the current buffer
    ni = i2 - ia
    if (ib(lunit)  >  0) then
       buf(lunit)%i1(ibuf1+1:ibuf1+ni) = ibb(ia+1:i2)
    else
       buf(lunit)%i2(ibuf1+1:ibuf1+ni) = ibb(ia+1:i2)
    end if
    ibuf1 = ibuf1 + ni

    if (ibuf1 == lbufm1) then ! end of buffer, set switch to next buffer
       ib(lunit) =  - ib(lunit)
       ibuf1 = 0
    end if
    ibuf(lunit) = ibuf1
  end subroutine iwritb

  subroutine iwritbr (nunit, ibb, i1, i2)
! Copy array ibb into a buffer; buffer written to disc as filled
! Assume real buffers and that writes begin at a wp boundary
    integer, intent(in)     :: nunit        ! unit id
    integer, intent(in)     :: i1, i2       ! array bounds
    integer, intent(in)     :: ibb(i2)      ! data to be written
    integer                 :: lunit, ibuf1, ia, nb, ni, nloop
    integer                 :: nl, niq, nq
    real(wp)                :: b(2)

    lunit = junit(nunit)      ! logical unit number
    ibuf1 = ibuf(lunit)       ! buffer location
    if (ibuf1 == 0) then ! new block; reserve disc block, write previous
       call newbuf (nunit)
    end if

    ia = i1 - 1           ! data start
    nb = lbufm1 - ibuf1   ! space in buffer
    ni = i2 - ia          ! # integers to write out
    niq = nwp(ni)         ! # equivalent wp values
    if (niq  >  nb) then  ! data will spread into next block(s)
       nloop = 1 + (niq - nb)/lbufm1   ! # records
       records: do nl = 1, nloop
          if (ib(lunit)  >  0) then    ! use first buffer
             nq = (lbufm1 - ibuf1) * nipw  ! # integers to write
             buf(lunit)%b1(ibuf1+1:lbufm1) = TRANSFER(ibb(ia+1:ia+nq),&
                  b, lbufm1-ibuf1)
             ia = ia + nq
             ib(lunit) =  - 1
             ibuf1 = 0
             if (ia < i2) then ! reserve a record for the next block
                call newbuf (nunit)
             end if
          else            ! use other buffer
             nq = (lbufm1 - ibuf1) * nipw  ! # integers to write
             buf(lunit)%b2(ibuf1+1:lbufm1) = TRANSFER(ibb(ia+1:ia+nq),&
                  b, lbufm1-ibuf1)
             ia = ia + nq
             ibuf1 = 0
             ib(lunit) = 1
             if (ia < i2) call newbuf (nunit)
          end if
       end do records
    end if

! now write remainder of the data to the current buffer
    ni = i2 - ia
    nq = nwp(ni)    ! # wp values to write
    if (ib(lunit)  >  0) then
       buf(lunit)%b1(ibuf1+1:ibuf1+nq) = TRANSFER(ibb(ia+1:i2), b, nq)
    else
       buf(lunit)%b2(ibuf1+1:ibuf1+nq) = TRANSFER(ibb(ia+1:i2), b, nq)
    end if
    ibuf1 = ibuf1 + nq

    if (ibuf1 == lbufm1) then ! end of buffer, set switch to next buffer
       ib(lunit) =  - ib(lunit)
       ibuf1 = 0
    end if
    ibuf(lunit) = ibuf1
  end subroutine iwritbr

  subroutine iwritc (nunit, ic, i1, i2, j1, j2, idim, indic)
! Write 2-D array c to unit nunit
! write(nunit)c(i1:i2,j1:j2) by call iwritc(nunit,ic,i1,i2,j1,j2,idim,0)
    integer, intent(in)     :: nunit        ! unit number
    integer, intent(in)     :: indic        ! transpose switch (=0,1)
    integer, intent(in)     :: idim         ! leading dimension of ic
    integer, intent(in)     :: i1, i2       ! first index bounds
    integer, intent(in)     :: j1, j2       ! second index bounds
    integer, intent(in)     :: ic(idim, j2) ! data to be written to disc
    integer                 :: id(j2-j1+1)
    integer                 :: ja, jup, i, j, jj

    if (indic == 0) then  ! test the transposing switch
       do ja = j1, j2
          call iwritb (nunit, ic(:,ja), i1, i2)
       end do
    else
       jup = j2 - j1 + 1
       do i = i1, i2
          jj = 0
          do j = j1, j2
             jj = jj + 1
             id(jj) = ic(i, j)
          end do
          call iwritb (nunit, id, 1, jup)
       end do
    end if
  end subroutine iwritc

  subroutine lookup (icatl, nrec, ibufi)
! Given the catalogue entry calculate record # and buffer position
    integer, intent(in)    :: icatl   ! catalogue entry
    integer, intent(out)   :: nrec    ! record number
    integer, intent(out)   :: ibufi   ! buffer position

    nrec = icatl / lbufm1
    ibufi = MOD(icatl, lbufm1)
    if (ibufi == 0) then ! correct to last position in the buffer
       ibufi = lbufm1
       nrec = nrec - 1
    end if
  end subroutine lookup

  subroutine moveb (nunit, nrec, nbuf)
! Skip backward to another point in a file currently being read.
! No read from disc if reqd block is already in a buffer or if block
! immediately following is in a buffer.
    integer, intent(in)     :: nunit    ! unit id
    integer, intent(in)     :: nrec     ! new record #   
    integer, intent(in)     :: nbuf
    integer                 :: lunit, nrec1, nrec2, n
    logical                 :: ft

    lunit = junit(nunit)
    nrec1 = irec(lunit)
    ft = rt(lunit)

    if (nrec /= nrec1) then
       if (ib(lunit)  >  0) then ! overwrite other buffer further forward
          if (ft) then
             read (nunit, rec=nrec) buf(lunit)%b2
             ib(lunit) =  - 1
             nrec2 = TRANSFER(buf(lunit)%b2(lbuff), n)
             if (nrec2 /= 0  .and.  nrec2 /= nrec1) then
                read (nunit, rec=nrec2) buf(lunit)%b1
             end if
          else
             read (nunit, rec=nrec) buf(lunit)%i2
             ib(lunit) =  - 1
             nrec2 = buf(lunit)%i2(lbuff)
             if (nrec2 /= 0  .and.  nrec2 /= nrec1) then
                read (nunit, rec=nrec2) buf(lunit)%i1
             end if
          end if
       else
          if (ft) then
             read (nunit, rec=nrec) buf(lunit)%b1
             ib(lunit) = 1
             nrec2 = TRANSFER(buf(lunit)%b1(lbuff), n)
             if (nrec2 /= 0  .and.  nrec2 /= nrec1) then
                read (nunit, rec=nrec2) buf(lunit)%b2
             end if
          else
             read (nunit, rec=nrec) buf(lunit)%i1
             ib(lunit) = 1
             nrec2 = buf(lunit)%i1(lbuff)
             if (nrec2 /= 0  .and.  nrec2 /= nrec1) then
                read (nunit, rec=nrec2) buf(lunit)%i2
             end if
          end if
       end if
    end if
    irec(lunit) = nrec
    ibuf(lunit) = nbuf - 1
  end subroutine moveb

  subroutine movef (nunit, nrec, nbuf)
! Skip forward to another point in a file currently being read.
! No read from disc if required block is already in a buffer.
    integer, intent(in)     :: nunit    ! unit id
    integer, intent(in)     :: nrec     ! new record
    integer, intent(in)     :: nbuf
    integer                 :: lunit, nrec1, nrec2, nrec3, n
    logical                 :: ft

    lunit = junit(nunit)
    nrec1 = irec(lunit)
    ft = rt(lunit)

! if current buffer is not the required one test the other.
! if that is then read a new record into the current buffer.
! if neither buffer contains the required one call startr.
! procedure is performed for either buffer being the current.
    if (nrec /= nrec1) then
       if (ib(lunit)  >  0) then
          if (ft) then
             nrec2 = TRANSFER(buf(lunit)%b1(lbuff), n)
             if (nrec == nrec2) then
                ib(lunit) =  - 1
                nrec3 = TRANSFER(buf(lunit)%b2(lbuff), n)
                if (nrec3 /= 0) read (nunit,rec=nrec3) buf(lunit)%b1
             else
                call startr (nunit, nrec, nbuf)
             end if
          else
             nrec2 = buf(lunit)%i1(lbuff)
             if (nrec == nrec2) then
                ib(lunit) =  - 1
                nrec3 = buf(lunit)%i2(lbuff)
                if (nrec3 /= 0) read (nunit,rec=nrec3) buf(lunit)%i1
             else
                call startr (nunit, nrec, nbuf)
             end if
          end if
       else
          if (ft) then
             nrec2 = TRANSFER(buf(lunit)%b2(lbuff), n)
             if (nrec == nrec2) then
                ib(lunit) = 1
                nrec3 = TRANSFER(buf(lunit)%b1(lbuff), n)
                if (nrec3 /= 0) read (nunit,rec=nrec3) buf(lunit)%b2
             else
                call startr (nunit, nrec, nbuf)
             end if
          else
             nrec2 = buf(lunit)%i2(lbuff)
             if (nrec == nrec2) then
                ib(lunit) = 1
                nrec3 = buf(lunit)%i1(lbuff)
                if (nrec3 /= 0) read (nunit,rec=nrec3) buf(lunit)%i2
             else
                call startr (nunit, nrec, nbuf)
             end if
          end if
       end if
    end if
    irec(lunit) = nrec
    ibuf(lunit) = nbuf - 1
  end subroutine movef

  subroutine newbuf (nunit)
! Before starting to fill a new buffer, reserve a disc block, store 
! record # in last position of other buffer. That buffer is then 
! written to disc to record given in present buffer. Last position of
! new buffer is set to 0 to signify this is the current buffer
    integer, intent(in)     :: nunit    ! unit id
    integer                 :: lunit, nfin, nbanki, nrec, n
    integer                 :: nr1, nr2
    real(wp)                :: r
    logical                 :: ft

! if opening first block of sub-file whose record # has been specified
! by call to startw, no action is needed
! bufer1 always used at the start of a sub-file
    lunit = junit(nunit)
    ft = rt(lunit)
    if (ft) then
       nr1 = TRANSFER(buf(lunit)%b1(lbuff), n)
       nr2 = TRANSFER(buf(lunit)%b2(lbuff), n)
    else
       nr1 = buf(lunit)%i1(lbuff)
       nr2 = buf(lunit)%i2(lbuff)
    end if
    if (ib(lunit) > 0 .and. nr1 == 0 .and. nr2 /= 0) return
!
! reserve record # for new block; test overwrite switch to use next 
! block in file being overwritten unless the end of file is reached
    if (iover(lunit)  >  0) then
       if (ft) then
          nfin = TRANSFER(buf(lunit)%b3(lbuff), n)
       else
          nfin = buf(lunit)%i3(lbuff)
       end if
       if (nfin /= 0) then
          if (ft) then
             read (nunit, rec=nfin) buf(lunit)%b3
          else
             read (nunit, rec=nfin) buf(lunit)%i3
          end if
       else
          iover(lunit) = 0
       end if
    end if

    if (iover(lunit) == 0) then ! test bank for subfiles to overwrite
       nbanki = nbank(lunit)
       if (nbanki  >  0) then
          nfin = ibank(nbanki, lunit)
          if (nfin == 0) then
             write (fo, '(a,i3,a,i3)') 'NEWBUF: record # obtained from &
                  &bank is zero on unit ', nunit, ' nbank = ', nbanki
             stop
          end if
          if (ft) then
             read (nunit, rec=nfin) buf(lunit)%b3
          else
             read (nunit, rec=nfin) buf(lunit)%i3
          end if
          nbanki = nbanki - 1
          nbank(lunit) = nbanki
          iover(lunit) = 1
       else ! since the bank is empty reserve a new record number
          nfin = nfinis(lunit)  +  1
          nfinis(lunit) = nfin
       end if
    end if

    if (ib(lunit)  >  0) then ! test buffer switch
       if (ft) then
          nrec = TRANSFER(buf(lunit)%b1(lbuff), n)
          buf(lunit)%b1(lbuff) = TRANSFER(0,r)
          buf(lunit)%b2(lbuff) = TRANSFER(nfin,r)
          if (nrec /= 0) then ! not starting a new subfile
             write (nunit, rec=nrec) buf(lunit)%b2
          end if
       else
          nrec = buf(lunit)%i1(lbuff)
          buf(lunit)%i1(lbuff) = 0
          buf(lunit)%i2(lbuff) = nfin
          if (nrec /= 0) then ! not starting a new subfile
             write (nunit, rec=nrec) buf(lunit)%i2
          end if
       end if
    else       ! use other buffer
       if (ft) then
          nrec = TRANSFER(buf(lunit)%b2(lbuff), n)
          buf(lunit)%b2(lbuff) = TRANSFER(0, r)
       else
          nrec = buf(lunit)%i2(lbuff)
          buf(lunit)%i2(lbuff) = 0
       end if
       if (nrec /= 0) then
          if (ft) then
             buf(lunit)%b1(lbuff) = TRANSFER(nfin, r)
             write (nunit, rec=nrec) buf(lunit)%b1
          else
             buf(lunit)%i1(lbuff) = nfin
             write (nunit, rec=nrec) buf(lunit)%i1
          end if
       else
          write (fo, '(a,i3)') 'NEWBUF: tried to write to zero record&
               & number on unit', nunit
          stop
       end if
    end if
  end subroutine newbuf

  subroutine newfil (nunit, nrec)
! Initiate writing a subfile on nunit to start at next available record
    integer, intent(in)     :: nunit  ! unit id
    integer, intent(out)    :: nrec   ! record number for start of file
    integer                 :: lunit, icatl
    real(wp)                :: r

    lunit = junit(nunit)
    if (rt(lunit)) then
       buf(lunit)%b1(lbuff) = TRANSFER(0, r)
       buf(lunit)%b2(lbuff) = TRANSFER(0, r)
    else
       buf(lunit)%i1(lbuff) = 0
       buf(lunit)%i2(lbuff) = 0
    end if
    ib(lunit) = 1
    ibuf(lunit) = 0
    call catlog (nunit, icatl)
    nrec = icatl / lbufm1
  end subroutine newfil

  subroutine open (nunit, filnam, stats, rtyp)
! Enter unit # in filehand list of unit #s; open the file
    integer, intent(in)          :: nunit  ! external unit # of file
    character(len=7), intent(in) :: filnam ! file name
    character(len=7), intent(in) :: stats  ! 'new', 'scratch' or 'old'
    logical, intent(in)          :: rtyp   ! true if real buffer
    integer                      :: nbanki, lunit, ios, status
    real(wp)                     :: r = 1.0_wp
    integer                      :: i = 1

    nlist = nlist + 1
    if (nlist  >  list) then
       write (fo, '(a,i3)') 'FILEHAND: # i/o units exceeds maximum = ',&
            list
       stop
    end if
    if (rtyp) then ! use real working precision buffers
       inquire (iolength=macdim) r ! wp length in storage units
    else           ! use integer buffers
       inquire (iolength=macdim) i ! integer length in storage units
    end if

    if (l2u(stats) == 'SCRATCH') then
       open (nunit, status = stats, access = 'direct', iostat=ios, &
            recl = macdim*lbuff)
       if (ios /= 0) then
          write (fo,'(a,i4,a,i4)') 'Error opening unit ', nunit,     &
               ', iostat = ', ios
          stop
       end if
       write (fo, '(a,a7,a,i7)') 'scratch file opened on unit ', &
            nunit, ', record length = ', macdim*lbuff
    else
       open (nunit, status = stats, file = filnam, access = 'direct', &
            recl = macdim*lbuff, iostat=ios)
       if (ios /= 0) then
          write (fo,'(a,i4,a,i4)') 'Error opening unit ', nunit,     &
               ', iostat = ', ios
          stop
       end if
       write (fo, '(a,a7,a,i3,a,i7)') 'direct access file ', filnam, &
            ' opened on unit ', nunit, ', record length = ',  &
            macdim*lbuff
    end if

! enter nunit in array iunit at first available vacancy
    do i = 1, list
       if (iunit(i) == 0) then
          iunit(i) = nunit
          junit(nunit) = i
          lunit = i
          if (rtyp) then
             rt(i) = .true.
             allocate (buf(i)%b1(lbuff), buf(i)%b2(lbuff), &
                  buf(i)%b3(lbuff), stat=status)
          else
             rt(i) = .false.
             allocate (buf(i)%i1(lbuff), buf(i)%i2(lbuff), &
                  buf(i)%i3(lbuff), stat=status)
          end if
          if (status /= 0) then
             write (fo,'(a,i4)') 'FILEHAND: allocation error = ', status
             stop
          end if
          exit
       end if
       if (i == list) then
          write (fo, '(a,i3)') 'FILEHAND: unable to place in the i/o &
               &list unit ', nunit
          stop
       end if
    end do
    if (l2u(stats) /= 'OLD') then   ! set housekeeping parameters
       nfinis(lunit) = 2
       nbank(lunit) = 0
       iover(lunit) = 0
    else                 ! read first block containing housekeeping data
       call startr (nunit, 1, 1)
       call ireada (nunit, nbank(lunit))
       nbanki = nbank(lunit)
       if (nbanki  >  ibnk) then
          write (fo, '(a,i4,a,i4)') 'FILEHAND: # deleted subfiles ',&
               nbanki, ' exceeds dimension ibnk ', ibnk
       end if
       if (nbanki /= 0) then
          call ireadb (nunit, ibank(:,lunit), 1, nbanki)
       end if
       call ireada (nunit, nfinis(lunit))  ! read last record on file
    end if
  end subroutine open

  subroutine overw (nunit, nrec, nbuf)
! Overwrite a subfile starting at record nrec and from position nbuf 
! in the buffer. Used to extend or replace an old subfile
    integer, intent(in)     :: nunit
    integer, intent(in)     :: nrec     ! record #
    integer, intent(in)     :: nbuf     ! buffer position
    integer                 :: lunit, n
    real(wp)                :: r

    lunit = junit(nunit)
    if (nrec /= 0) then
       if (rt(lunit)) then
          read (nunit,rec=nrec) buf(lunit)%b1 ! read record nrec
          iover(lunit) = 1
          buf(lunit)%b2(lbuff) = TRANSFER(nrec, n)
          ib(lunit) = 1
          ibuf(lunit) = nbuf - 1
          buf(lunit)%b3(lbuff) = buf(lunit)%b1(lbuff)
          buf(lunit)%b1(lbuff) = TRANSFER(0, r)
       else 
          read (nunit,rec=nrec) buf(lunit)%i1 ! read record nrec
          iover(lunit) = 1
          buf(lunit)%i2(lbuff) = nrec
          ib(lunit) = 1
          ibuf(lunit) = nbuf - 1
          buf(lunit)%i3(lbuff) = buf(lunit)%i1(lbuff)
          buf(lunit)%i1(lbuff) = 0
      end if
    else
       write (fo, '(a,i3)') 'OVERW: attempt to read zero record # from &
            &unit ', nunit
       stop
    end if
  end subroutine overw

  subroutine reada (nunit, a)
! move to next position in buffer; switch to current buffer, read a
    integer, intent(in)     :: nunit     ! unit id
    real(wp), intent(out)   :: a         ! data to read
    integer                 :: lunit, ibuf1, nrec, ia

    lunit = junit(nunit)
    if (.NOT.rt(lunit)) then 
       write (fo,'(a,i4)') 'READA: illegal read of integer buffer'
       stop
    end if
    ibuf1 = ibuf(lunit)  +  1
    if (ib(lunit)  >  0) then
       a = buf(lunit)%b1(ibuf1)
       if (ibuf1 == lbuff) then   ! at end of buffer
          ia = TRANSFER(a, ia)  ! pointer to next block
          if (ia /= 0) then ! continuation block in other buffer
             irec(lunit) = ia
             nrec = TRANSFER(buf(lunit)%b2(lbuff), ia)
             if (nrec /= 0) read (nunit,rec=nrec) buf(lunit)%b1
             ib(lunit) =  - 1
             ibuf1 = 1
             a = buf(lunit)%b2(ibuf1)
          else    ! have reached the end of this subfile
             write (fo,'(a,i5)') 'READA: tried to read past end of data&
                  & on unit ', nunit
             stop
          end if
       end if
    else         ! use other buffer
       a = buf(lunit)%b2(ibuf1)
       if (ibuf1 == lbuff) then
          ia = TRANSFER(a, ia)  ! pointer to next block
          if (ia /= 0) then
             irec(lunit) = ia
             nrec = TRANSFER(buf(lunit)%b1(lbuff), ia)
             if (nrec /= 0) read (nunit,rec=nrec) buf(lunit)%b2
             ib(lunit) = 1
             ibuf1 = 1
             a = buf(lunit)%b1(ibuf1)
          else
             write (fo, '(a,i6)') 'READA: tried to read past end of &
                  &data on unit ', nunit
             stop
          end if
       end if
    end if
    ibuf(lunit) = ibuf1
  end subroutine reada

  subroutine readb (nunit, b, i1, i2)
! Read ni values from nunit into array b
    integer, intent(in)     :: nunit      ! unit id
    integer, intent(in)     :: i1, i2     ! array bounds
    real(wp), intent(out)   :: b(i2)      ! array to read
    integer                 :: lunit, ibuf1, irec1, ib1, ia, ni, i
    integer                 :: nb, nloop, nrec, ii, nl, n

    lunit = junit(nunit)
    if (.NOT.rt(lunit)) then 
       write (fo,'(a,i4)') 'READB: illegal read of integer buffer'
       stop
    end if
    ibuf1 = ibuf(lunit)
    irec1 = irec(lunit)
    ib1 = ib(lunit)
    ia = i1 - 1
    ni = i2 - ia
    nb = lbufm1 - ibuf1 ! space remaining in the buffer
    if (ni  >  nb) then ! data spreads at least into next block
       nloop = 1 + (ni - nb)/lbufm1
       records: do nl = 1, nloop
          if (ib1  >  0) then    ! read bufer1 first
             nrec = TRANSFER(buf(lunit)%b1(lbuff), n)
             do i = ibuf1 + 1, lbufm1
                ia = ia + 1
                b(ia) = buf(lunit)%b1(i)
             end do
! if two more continuation blocks overwrite buffer.
             if (nrec /= 0) then
                irec1 = nrec
                nrec = TRANSFER(buf(lunit)%b2(lbuff), n)
                if (nrec /= 0) read (nunit,rec=nrec) buf(lunit)%b1
                ib1 =  - 1
             else if (ia < i2) then ! error if no continuation block
                write (fo, '(a,i5)') 'READB: tried to read past end of &
                     &data on unit ', nunit
                stop
             end if
          else         ! read from other buffer
             nrec = TRANSFER(buf(lunit)%b2(lbuff), n)
             do ii = ibuf1 + 1, lbufm1
                ia = ia + 1
                b(ia) = buf(lunit)%b2(ii)
             end do
             if (nrec /= 0) then
                irec1 = nrec
                nrec = TRANSFER(buf(lunit)%b1(lbuff), n)
                if (nrec /= 0) read (nunit, rec=nrec) buf(lunit)%b2
                ib1 = 1
             else if (ia < i2) then
                write (fo, '(a,i5)') 'READB: tried to read past end of &
                     &data on unit ', nunit
                stop
             end if
          end if
          ibuf1 = 0
       end do records
    end if

! now read remaining values into b
    if (ib1  >  0) then
       do i = ia + 1, i2
          ibuf1 = ibuf1 + 1
          b(i) = buf(lunit)%b1(ibuf1)
       end do
    else
       do ii = ia + 1, i2
          ibuf1 = ibuf1 + 1
          b(ii) = buf(lunit)%b2(ibuf1)
       end do
    end if
    ibuf(lunit) = ibuf1
    irec(lunit) = irec1
    ib(lunit) = ib1
  end subroutine readb

  subroutine readc (nunit, c, i1, i2, j1, j2, idim, indic)
! Read into the 2-D array c from nunit
    integer, intent(in)     :: nunit     ! unit id
    integer, intent(in)     :: indic     ! transpose switch
    integer, intent(in)     :: idim      ! first dimension of c
    integer, intent(in)     :: i1, i2    ! first index bounds
    integer, intent(in)     :: j1, j2    ! second index bounds
    real(wp), intent(out)   :: c(idim, j2)
    real(wp)                :: d(j2-j1+1)
    integer                 :: ja, jup, i, j, jj

    if (indic == 0) then ! transpose switch zero => read normal ordering
       do ja = j1, j2
          call readb (nunit, c(:,ja), i1, i2)
       end do
    else                ! else read reversing indices
       jup = j2 - j1 + 1
       do i = i1, i2
          call readb (nunit, d, 1, jup)
          jj = 0
          do j = j1, j2
             jj = jj + 1
             c(i, j) = d(jj)
          end do
       end do
    end if
  end subroutine readc

  subroutine readix (nunit)
! Initiate reading index subfile starting at record 2 on unit
    integer, intent(in)     :: nunit    ! unit id
    integer                 :: lunit, nrec2, n

    lunit = junit(nunit)
    if (rt(lunit)) then
       read (nunit, rec=2) buf(lunit)%b1
       nrec2 = TRANSFER(buf(lunit)%b1(lbuff), n)
       if (nrec2 /= 0) read (nunit, rec=nrec2) buf(lunit)%b2
    else
       read (nunit, rec=2) buf(lunit)%i1
       nrec2 = buf(lunit)%i1(lbuff)
       if (nrec2 /= 0) read (nunit, rec=nrec2) buf(lunit)%i2
    end if
    ibuf(lunit) = 0
    irec(lunit) = 2
    ib(lunit) = 1
  end subroutine readix

  subroutine reopen (nunit, filnam, rtyp)
! Enters unit # in filehand list of unit #s and opens the file
    integer, intent(in)          :: nunit   ! external unit #
    character(len=7), intent(in) :: filnam  ! file name
    logical, intent(in)          :: rtyp    ! true for real buffer
    integer                      :: i, nbanki, lunit, status

    nlist = nlist + 1
    if (nlist  >  list) then
       write (fo, '(a,i3)') 'REOPEN: # i/o units exceeds list = ', list
       stop
    end if
    write (fo, '(a,a7,a,i2)') 'direct access file ', filnam, ' reopen&
         &ed on unit ', nunit
    
! enter nunit in array iunit at first available vacancy
    do i = 1, list
       if (iunit(i) == 0) then
          iunit(i) = nunit
          junit(nunit) = i
          lunit = i
          if (rtyp) then
             rt(i) = .true.
             allocate (buf(i)%b1(lbuff), buf(i)%b2(lbuff), &
                  buf(i)%b3(lbuff), stat=status)
          else
             rt(i) = .false.
             allocate (buf(i)%i1(lbuff), buf(i)%i2(lbuff), &
                  buf(i)%i3(lbuff), stat=status)
          end if
          if (status /= 0) then
             write (fo,'(a,i4)') 'REOPEN: allocation error = ', status
             stop
          end if
          exit
       end if
       if (i == list) then
          write (fo, '(a,i3)') 'REOPEN: unable to place in the i/o &
               &list unit ', nunit
          stop
       end if
    end do

! read first block containing housekeeping data
    call startr (nunit, 1, 1)
    call ireada (nunit, nbank(lunit))
    nbanki = nbank(lunit)
    if (nbanki  >  ibnk) then
       write (fo, '(a,i4,a,i4)') 'REOPEN: # deleted subfiles ', &
            nbanki, ' exceeds dimension ibnk ', ibnk
       stop
    end if
    if (nbanki /= 0) call ireadb (nunit, ibank(:,lunit), 1, nbanki)
    call ireada (nunit, nfinis(lunit)) ! read last used record on file
  end subroutine reopen

  subroutine startr (nunit, nrec, nbuf)
! Initiate reading a subfile starting at element # nbuf, in record nrec
    integer, intent(in)     :: nunit   ! unit id
    integer, intent(in)     :: nrec    ! record number
    integer, intent(in)     :: nbuf    ! element position
    integer                 :: lunit, nrec2, n

    lunit = junit(nunit)
    if (rt(lunit)) then
       read (nunit, rec=nrec) buf(lunit)%b1
    else
       read (nunit, rec=nrec) buf(lunit)%i1
    end if
    ibuf(lunit) = nbuf - 1
    irec(lunit) = nrec
    ib(lunit) = 1
! read next block (if there is one) into second buffer
    if (rt(lunit)) then
       nrec2 = TRANSFER(buf(lunit)%b1(lbuff), n)
       if (nrec2 /= 0) read (nunit,rec=nrec2) buf(lunit)%b2
    else
       nrec2 = buf(lunit)%i1(lbuff)
       if (nrec2 /= 0) read (nunit,rec=nrec2) buf(lunit)%i2
    end if
  end subroutine startr

  subroutine startw (nunit, nrec)
! Initiate writing of subfile on unit starting in record # nrec
! continuing in next available records. When nrec = 0 subfile will start
! in the first available block as determined in newbuf
    integer, intent(in)     :: nunit   ! unit id
    integer, intent(in)     :: nrec    ! starting record #
    integer                 :: lunit
    real(wp)                :: r

    lunit = junit(nunit)
    if (rt(lunit)) then
       buf(lunit)%b1(lbuff) = TRANSFER(0, r)
       buf(lunit)%b2(lbuff) = TRANSFER(nrec, r)
    else
       buf(lunit)%i1(lbuff) = 0
       buf(lunit)%i2(lbuff) = nrec
    end if
    ib(lunit) = 1
    ibuf(lunit) = 0

! if nrec /= 0 switch off overwrite facility so specified record used
    if (nrec /= 0) then
       iover(lunit) = 0
       if (nrec  >  2) then
          write (fo, '(a)') 'STARTW: error, a new file has been started'
          write (fo, '(a,i4,a,i4)') 'unit # = ', nunit, ' record = ',&
               nrec
          stop
       end if
    end if
  end subroutine startw

  subroutine tclose (nunit)
! used for a read only file to release unit # from filehand list
    integer, intent(in)           :: nunit   ! unit id
    integer                       :: lunit

    lunit = junit(nunit)
    iunit(lunit) = 0
    junit(nunit) = 0
    nlist = nlist - 1
  end subroutine tclose

  subroutine writa (nunit, a)
! Add a into current buffer; if buffer empty then initialise it and
! write the previous (if any) to disc. If buffer is full after adding
! a then change the buffer switch to activate the other buffer next 
! call and set the counter ibuf1 to zero
    integer, intent(in)     :: nunit      ! unit id
    real(wp), intent(in)    :: a          ! data to be written
    integer                 :: lunit, ibuf1

    lunit = junit(nunit)
    if (.NOT.rt(lunit)) then 
       write (fo,'(a,i4)') 'WRITA: illegal write to integer buffer'
       stop
    end if
    ibuf1 = ibuf(lunit) + 1
    if (ibuf1 == 1) call newbuf (nunit)  ! this is a new buffer
    if (ib(lunit)  >  0) then         ! use first buffer
       buf(lunit)%b1(ibuf1) = a
    else                              ! use other buffer
       buf(lunit)%b2(ibuf1) = a
    end if
    if (ibuf1 == lbufm1) then   ! reached end of buffer
       ib(lunit) =  - ib(lunit)
       ibuf1 = 0
    end if
    ibuf(lunit) = ibuf1
  end subroutine writa

  subroutine writb (nunit, b, i1, i2)
! Copy array b into a buffer which is written to disc when filled
    integer, intent(in)     :: nunit     ! unit id
    integer, intent(in)     :: i1, i2    ! array bounds
    real(wp), intent(in)    :: b(i2)     ! array to be written
    integer                 :: lunit, ibuf1, i, ia, nb, ni, nloop
    integer                 :: ii, nl

    lunit = junit(nunit)
    if (.NOT.rt(lunit)) then 
       write (fo,'(a,i4)') 'WRITB: illegal write to integer buffer'
       stop
    end if
    ibuf1 = ibuf(lunit)
    if (ibuf1 == 0) then     ! a new block, reserve disc block
       call newbuf (nunit)
    end if
    ia = i1 - 1
    nb = lbufm1 - ibuf1
    ni = i2 - ia
    if (ni  >  nb) then   ! data will spread into next block(s)
       nloop = 1 + (ni - nb)/lbufm1   ! # records to be written
       records: do nl = 1, nloop
          if (ib(lunit)  >  0) then     ! start with bufer1
             do i = ibuf1 + 1, lbufm1
                ia = ia + 1
                buf(lunit)%b1(i) = b(ia)
             end do
             ib(lunit) =  - 1
             ibuf1 = 0
             if (ia < i2) then  ! more, reserce record for next block
                call newbuf (nunit)
             end if
          else             ! use other buffer
             do ii = ibuf1 + 1, lbufm1
                ia = ia + 1
                buf(lunit)%b2(ii) = b(ia)
             end do
             ibuf1 = 0
             ib(lunit) = 1
             if (ia < i2) then
                call newbuf (nunit)
             end if
          end if
       end do records
    end if

! now write remainder of the data to the current buffer
    if (ib(lunit)  >  0) then
       do i = ia + 1, i2
          ibuf1 = ibuf1 + 1
          buf(lunit)%b1(ibuf1) = b(i)
       end do
    else
       do ii = ia + 1, i2
          ibuf1 = ibuf1 + 1
          buf(lunit)%b2(ibuf1) = b(ii)
       end do
    end if
    if (ibuf1 == lbufm1) then   ! end of buffer, switch buffers
       ib(lunit) =  - ib(lunit)
       ibuf1 = 0
    end if
    ibuf(lunit) = ibuf1
  end subroutine writb

  subroutine writc (nunit, c, i1, i2, j1, j2, idim, indic)
! Write 2-D array to unit
    integer, intent(in)     :: nunit      ! unit id
    integer, intent(in)     :: indic      ! transpose switch
    integer, intent(in)     :: idim       ! first dimension of c
    integer, intent(in)     :: i1, i2     ! c first index bounds
    integer, intent(in)     :: j1, j2     ! c second index bounds
    real(wp), intent(in)    :: c(idim,j2) ! array to be written to disc
    real(wp)                :: d(j2-j1+1)
    integer                 :: ja, jup, i, j, jj

    if (indic == 0) then     ! c is not to be transposed
       do ja = j1, j2
          call writb (nunit, c(:,ja), i1, i2)
       end do
    else                     ! write transposed c to disk
       jup = j2 - j1 + 1
       do i = i1, i2
          jj = 0
          do j = j1, j2
             jj = jj + 1
             d(jj) = c(i,j)
          end do
          call writb (nunit, d, 1, jup)
       end do
    end if
  end subroutine writc

  subroutine writix (nunit)
! Initiate writing of an index to file on unit (starting in record 2)
    integer, intent(in)     :: nunit   ! unit id
    integer                 :: lunit
    real(wp)                :: r

    lunit = junit(nunit)
    if (rt(lunit)) then
       buf(lunit)%b1(lbuff) = TRANSFER(0, r)
       buf(lunit)%b2(lbuff) = TRANSFER(2, r)
    else
       buf(lunit)%i1(lbuff) = 0
       buf(lunit)%i2(lbuff) = 2
    end if
    ib(lunit) = 1 ! bufer1 is first buffer to be used in data transfer
    ibuf(lunit) = 0
    iover(lunit) = 0 ! switch off overwrite so record 2 will be used
  end subroutine writix

end module filehand
