module debug
! Time-stamp: "01/11/20 09:59:55 cjn"
  implicit none

  integer, save :: bug1, bug2, bug3, bug4, bug5, bug6, bug7, bug8, bug9

  private
  public set_debug_switches
  public bug1, bug2, bug3, bug4, bug5, bug6, bug7, bug8, bug9

contains
  subroutine set_debug_switches (iabug)
    integer, intent(in) :: iabug(9)

    bug1 = iabug(1)
    bug2 = iabug(2)
    bug3 = iabug(3)
    bug4 = iabug(4)
    bug5 = iabug(5)
    bug6 = iabug(6)
    bug7 = iabug(7)
    bug8 = iabug(8)
    bug9 = iabug(9)
  end subroutine set_debug_switches

end module debug
