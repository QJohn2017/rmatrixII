module bc_angular_integrals
! Time-stamp: "2005-03-11 16:06:51 cjn"
  use precisn, only: wp
  use io_units, only: fo, jbufiv2, jbuffv2
  use debug, only: bug7, bug9
  use startup, only: ljcomp
  implicit none

  real(wp), parameter       :: eps = 1.0e-5_wp
  integer, save             :: jbufiv, jbuffv

  private
  public angbc

contains
  subroutine angbc (nelc, npset, lpset, ispset, ippset, npocsh,       &
       npocob, npelsh, nnpfig, npnpq, lssetn, ncoumx, nset, lset,      &
       isset, ipset, noccsh, nocorb, nelcsh, nmefig, nnq, lssetm,      &
       mcoumx, mmq, chs, id1, id2)
! calculates the angular integral (weighting factor) for each pair of
! bound continuum configurations
    use filehand, only: open, newfil, catlog, iwrita, iwritb, endw, &
         writix, close, writa, writb
    use dim, only: ldim0, ldim1
    use startup, only: fsh
    use ang_aux, only: delp
    use surfacing_tables, only: surf_vec, surf_tab
    use sym_sets, only: get_ncset, get_ncfg
    use vpack, only: pckl2, pck3, pck14, pck124
    use statistics, only: coll_bc_stat, reset_bc_stat, report_bc_stat
    integer, intent(in)  :: nelc
    integer, intent(in)  :: npset
    integer, intent(in)  :: lpset(:), ispset(:), ippset(:)
    integer, intent(in)  :: npocsh(:)
    integer, intent(in)  :: npocob(:,:)
    integer, intent(in)  :: npelsh(:,:), nnpfig(:,:)
    integer, intent(in)  :: npnpq(:)
    integer, intent(in)  :: lssetn
    integer, intent(in)  :: ncoumx(:)
    integer, intent(in)  :: nset
    integer, intent(in)  :: lset(:), isset(:), ipset(:)
    integer, intent(in)  :: noccsh(:), nocorb(:,:)
    integer, intent(in)  :: nelcsh(:,:), nmefig(:,:)
    integer, intent(in)  :: nnq(:)
    integer, intent(in)  :: lssetm
    integer, intent(in)  :: mcoumx(:)
    integer, intent(in)  :: mmq(:)
    integer, intent(in)  :: chs
    integer, intent(in)  :: id1, id2

    integer              :: irsd(fsh), irse(fsh)
    real(wp)             :: bcd(ldim0,fsh), bce(ldim0,fsh)
    integer              :: lamdst(fsh), nlamd(fsh)
    real(wp)             :: bcdir(0:ldim1),bcex(0:ldim1)
    integer              :: lamest(fsh), nlame(fsh)
    integer              :: irsde(2*fsh), isre(fsh)
    integer              :: ivbci(chs+1,npset), ivbcr(chs+1,npset)
    real(wp)             :: bc(2*ldim0*fsh)
    integer              :: nsch(npset)

    character(len=7)     ::  filea, fileb, stats
    integer :: ndummy, isnp, lrgl, lrgpi, lrgs, lmin, lmax, jsn
    integer :: jch1, lj, jspin, jpi, jcsn, jncou, jnc
    integer :: n2shj, nonz, icsnp, inpcou, inpc, inpcq, n1shi
    integer :: ndcont, necont, ncont1, inc, incq, ncomx
    integer :: isig, lsigi, idelp, ik, idelps, k2i, imc
    integer :: imcq, irho, neli2, lrhoi, idirs, lameup, irs, isr
    integer :: idebug, id, ie, ilame, lamelo, k1ib, k1
    integer :: jmc, k2topi, n2shi, idelp1, jrho
    integer :: lrhoj, jncq, nd, ien, i, n, nterm
    integer :: nelnc, lcfgp, k1i, lamdlo, lamdup, ilamd
    integer :: irs1, k2ib, k2jb, idelp2, ilamdb, ilameb, idn, ne
    integer :: ncont, j, ii
    real(wp) :: facij1, facex, facdir, cup, bc1, facij2
    logical  :: chk_n1
    real(wp), pointer   :: surip(:), suri(:), surj(:)
    integer, pointer    :: indxip(:), indxi(:), indxj(:)
    integer, pointer    :: maxcom(:), mincom(:)
    integer, pointer    :: ncset(:), npcset(:)
    integer, pointer    :: ncfga(:,:), ncfgb(:,:)
    integer, pointer    :: npcfga(:,:), npcfgb(:,:)
    logical             :: real_typ

    write (fo,'(5x,a,/)') 'ANGBC: BC Integrals'
    jbufiv = jbufiv2
    jbuffv = jbuffv2
    if (bug7 >0) then
       if (bug7 == 1) then
          write (fo,'(a)') '    isnp  iebci     iebcr     ivbci     &
               &ivbcr'
          write (fo,'(a)') '    irsd      bcd           irse         bce'
       end if
    end if

    ncset => get_ncset(1)
    npcset => get_ncset(2)
    call get_ncfg (1, ncfga, ncfgb)
    call get_ncfg (2, npcfga, npcfgb)

! initialise writing to all output files
    filea = 'abci'
    fileb = 'abcr'
    stats = 'new'
    real_typ = .false.
    call open (jbufiv, filea, stats, real_typ)
    real_typ = .true.
    call open (jbuffv, fileb, stats, real_typ)
    call newfil (jbufiv, ndummy)
    call newfil (jbuffv, ndummy)

! loop over sets of configurations with the same lrgs, lrgl, lrgpi
    call reset_bc_stat
!    write (fo,'(/,a,/)') ' set     #-irdse   irdse-sum    irs1-sum &
!         &            bc-sum                bc1-sum'
    isnp_loop: do isnp = 1, npset
       lrgl = lpset(isnp)
       lrgpi = ippset(isnp)
       lrgs = ispset(isnp)
       if (bug7 == 1) write (fo,'(a,3i4)') 'lrgl, lrgpi, lrgs ', lrgl,&
            lrgpi, lrgs
! find range of continuum l
       lmin = 999
       lmax = 0
       do jsn = 1, nset
          lcfgp = lset(jsn)
          lmin = MIN(lmin, ABS(lcfgp-lrgl))
          lmax = MAX(lmax, lcfgp+lrgl)
       end do

! loop over continuum l
       jch1 = 0
       lj_loop: do lj = lmin, lmax
! loop over sets of n electron configurations
          jsn_loop: do jsn = 1, nset
             lcfgp = lset(jsn)
             jspin = isset(jsn)
             jpi = ipset(jsn)
             if (MOD(lj+jpi+lrgpi, 2) == 1) cycle
             if (lrgl >lcfgp+lj .or. lrgl < ABS(lcfgp-lj)) cycle
             if (ABS(lrgs-jspin) /= 1) cycle
             jch1 = jch1+1
! store the starting positions of the files in the indexing arrays
             call catlog (jbufiv, ivbci(jch1,isnp))
             call catlog (jbuffv, ivbcr(jch1,isnp))

! loop over N configurations belonging to set jsn

             jcsn_loop: do jcsn = 1, ncset(jsn)
                jncou = ncfgb(jcsn,jsn)
                jnc = ncfga(jcsn,jsn)
                n2shj = noccsh(jnc)
! loop over n+1 configurations belonging to set isnp
                nonz  =  0
                icsnp_loop: do icsnp = 1, npcset(isnp)
                   inpcou = npcfgb(icsnp,isnp) ! coupling index
                   inpc = npcfga(icsnp,isnp)   ! cfg index
                   inpcq = npnpq(inpc)
                   n1shi = npocsh(inpc)

! ndcont, necont will count the number of shell pairs which
! contribute to the direct, exchange integrals for the
! coupling of inpcou with jncou,lj.
                   ndcont = 0
                   necont = 0
                   ncont1 = 0
! bring an electron to the surface of the n+1 configuration
! from each shell in turn looking for a match of the residual
! n configuration with jnc
! if there is a match isig is defined.  irho=jrho and can
! take the value of each occupied shell in the n config.
                   chk_n1 = .true.
                   k1i_loop: do k1i = n1shi, 1, -1
                      inc = nnpfig(k1i,inpc)
                      if (inc /= jnc) cycle k1i_loop
                      incq = nnq(inc)
                      ncomx = ncoumx(incq)
                      call surf_vec (id1, inpcou, k1i, inpcq, indxip, &
                           surip)
                      isig = npocob(k1i,inpc)
                      lsigi = ljcomp(isig)
                      facij1 = SQRT(REAL(npelsh(k1i,inpc),wp))

! calculate delta p for the n+1 configuration
                      idelp = 0
                      do ik = k1i+1, n1shi
                         idelp = idelp + npelsh(ik,inpc)
                      end do
                      idelps = 1
                      if (MOD(idelp, 2) == 1) idelps = -1
                      k2i_loop: do k2i = n2shj, 1, -1
                         imc = nmefig(k2i,inc)
                         imcq = mmq(imc)
                         irho = nocorb(k2i,inc)
                         neli2 = nelcsh(k2i,inc)
                         lrhoi = ljcomp(irho)
                         idirs = 1
                         if (MOD((lrgl+lrhoi+lsigi), 2) == 1) idirs = -1
                         facex = facij1 * REAL(idelps*neli2,wp)
                         lameup  =  0
                         if (irho == isig) lameup  =  -1
                         facdir = facex * REAL(idirs,wp)
                         facex = - facex
                         irs = pck3(isig, irho, irho)
                         isr = pck3(irho, irho, isig)
                         if (nelc > 2) then
                            call surf_tab (id2, k2i, incq, mincom, &
                                 maxcom, indxi, suri)
                            call surf_vec (id2, jncou, k2i, incq, &
                                indxj, surj)
                         end if
                         idebug = 0
                         call bccup (nelc, indxip, surip, indxi, suri, &
                              indxj, surj, incq, lssetn, lcfgp, jspin, &
                              imcq, lssetm, lrhoi, lrhoi, lsigi, lj,   &
                              lrgl, lrgs, mcoumx(imcq),                &
                              mincom, maxcom, lamdlo, lamdup, lamelo,  &
                              lameup,  bcdir, bcex, idebug)

!      test whether any direct terms from these shells

                         if (lamdlo /= 999) then
                            ndcont = ndcont + 1
                            irsd(ndcont) = irs
                            id = 0
                            do ilamd = lamdlo, lamdup, 2
                               id = id + 1
                               bcd(id,ndcont) = bcdir(ilamd)*facdir
                            end do
                            nlamd(ndcont) = id
                            lamdst(ndcont) = pckl2(id, lamdlo)
                         end if

! test whether any exchange terms from these shells

                         if (lamelo /= 999) then
                            necont = necont + 1
                            irse(necont) = irs
                            isre(necont) = isr
                            ie = 0
                            do ilame = lamelo, lameup, 2
                               ie = ie + 1
                               bce(ie,necont) = bcex(ilame) * facex
                            end do
                            nlame(necont) = ie
                            lamest(necont) = pckl2(ie, lamelo)
                         end if

                      end do k2i_loop


! all shells completed: calculate 1-electron part if incou=jncou
                      if (lsigi == lj) then
                         cup = 0.0_wp
                         do ii = 1, SIZE(indxip)
                            if (indxip(ii) == jncou) then
                               cup = surip(ii)
                               exit
                            end if
                         end do
                         if (ABS(cup) >= eps) then
                            ncont1 = 1
                            bc1 = cup * facij1 * idelps
                            irs1 = isig
                         end if
                      end if
!jump to write the files
                      chk_n1 = .false.
                      exit k1i_loop
                   end do k1i_loop

! end of search for identical configurations.
! now try to find two identical n-1 configurations
                   if (chk_n1) then
                      k1 = -1
                      k1ib_loop: do k1ib = n1shi, 1, -1
                         inc = nnpfig(k1ib,inpc)
                         n2shi = noccsh(inc)
                         k1 = k1 + 1
                         k2topi = n2shi-k1
                         k2ib_loop: do k2ib = k2topi, 1, -1
                            imc = nmefig(k2ib,inc)
                            k2jb_loop: do k2jb = n2shj, 1, -1
                               jmc = nmefig(k2jb,jnc)
! test if n-1 configs are the same. if so irho, jrho, isig are defined
                               if (imc /= jmc) cycle k2jb_loop
                               isig = npocob(k1ib,inpc)
                               lsigi = ljcomp(isig)
                               facij1 = SQRT(REAL(npelsh(k1ib,inpc),wp))
                               idelp = 0
                               do ik = k1ib+1, n1shi
                                  idelp = idelp + npelsh(ik,inpc)
                               end do
                               idelp1 = 1
                               if (MOD(idelp, 2) == 1) idelp1 = -1

                               irho = nocorb(k2ib,inc)
                               lrhoi = ljcomp(irho)
                               jrho = nocorb(k2jb,jnc)
                               lrhoj = ljcomp(jrho)
                               facij2 = SQRT(REAL(nelcsh(k2ib,inc) * &
                                    nelcsh(k2jb,jnc),wp))
                               call delp (n2shi, nocorb(:,inc),        &
                                    nelcsh(:,inc), k2ib, irho, n2shj,  &
                                    nocorb(:,jnc), nelcsh(:,jnc), k2jb,&
                                    jrho, idelp2)
                               idirs = 1
                               if (MOD((lrgl+lsigi+lrhoj),2) == 1) &
                                    idirs = -1
                               facex = facij1 * facij2 * REAL(idelp1 * &
                                    idelp2,wp)
                               lameup  =  0
                               if (irho == isig) lameup  =  -1
                               facdir = facex * REAL(idirs,wp)
                               facex = -facex
                               irs = pck3(isig, jrho, irho)
                               isr = pck3(irho, jrho, isig)
                               incq = nnq(inc)
                               jncq = nnq(jnc)
                               imcq = mmq(imc)
                               call surf_vec (id1, inpcou, k1ib, inpcq,&
                                    indxip, surip)
                               if (nelc > 2) then
                                  call surf_tab (id2, k2ib, incq, mincom,&
                                      maxcom, indxi, suri)
                                  call surf_vec (id2, jncou, k2jb, jncq, &
                                      indxj, surj)
                               end if
                               idebug = 0
                               call bccup (nelc, indxip, surip, indxi, &
                                    suri, indxj, surj, incq, lssetn,   &
                                    lcfgp, jspin, imcq, lssetm, lrhoi, &
                                    lrhoj, lsigi, lj, lrgl, lrgs,      &
                                    mcoumx(imcq), mincom,&
                                    maxcom, lamdlo, lamdup, lamelo,    &
                                    lameup, bcdir, bcex, idebug)

!      test whether any direct terms from these shells
                               if (lamdlo /= 999) then
                                  ndcont = ndcont + 1
                                  irsd(ndcont) = irs
                                  id = 0
                                  do ilamdb = lamdlo, lamdup, 2
                                     id = id + 1
                                     bcd(id,ndcont) = bcdir(ilamdb) &
                                          * facdir
                                  end do
                                  nlamd(ndcont) = id
                                  lamdst(ndcont) = pckl2(id, lamdlo)
                               end if

! test whether any exchange terms from these shells
                               if (lamelo /= 999) then
                                  necont = necont + 1
                                  irse(necont) = irs
                                  isre(necont) = isr
                                  ie = 0
                                  do ilameb = lamelo,lameup, 2
                                     ie = ie+1
                                     bce(ie,necont) = bcex(ilameb) *&
                                          facex
                                  end do
                                  nlame(necont) = ie
                                  lamest(necont) = pckl2(ie, lamelo)
                               end if
                               exit k1ib_loop ! jump to write files
                            end do k2jb_loop
                         end do k2ib_loop
                      end do k1ib_loop
                   end if

! no interacting shells found.
! write the files.
! write ang integrals for current pair of b-c configs to files
                   if (bug7 == 3) then
                      do nd = 1, ndcont
                         id = nlamd(nd)
                         write (fo,'(1x,i5,i12,e15.5)') lamdst(nd), &
                              irsd(nd), bcd(1,nd)
                         do idn = 2, id
                            write (fo,'(18x,e15.5)') bcd(idn,nd)
                         end do
                      end do
                      do ne = 1, necont
                         ie = nlame(ne)
                         write (fo,'(1x,i5,23x,i12,e15.5)') lamest(ne),&
                              irse(ne), bce(1,ne)
                         do ien = 2, ie
                            write (fo,'(41x,e15.5)') bce(ien, ne)
                         end do
                      end do
                      if (ncont1 == 1) write (fo,'(52x,i5,e15.5)') irs1,&
                           bc1
                   end if

! combine direct and exchange and irsde and lamst
!**** nb. we don't yet take advantage of 2 electrons from one shell
!     nor do we prune out zero contributions.  leave latter for debugging.
                   i = 0
                   do n = 1, ndcont
                      irsde(n) = pck14(irsd(n), lamdst(n))
                      do id = 1, nlamd(n)
                         i = i + 1
                         bc(i) = bcd(id,n)
                      end do
                   end do
                   do n = 1, necont
                     irsde(ndcont+n) = pck14(isre(n), lamest(n))
                      do ie = 1, nlame(n)
                         i = i+1
                         bc(i) = bce(ie,n)
                      end do
                   end do
                   nterm = i
                   ncont = ndcont + necont
! write ang integrals for current pair of bound configurations to files
                   if (ncont+ncont1 /= 0) then
!                      call iwrita (jbufiv, jcsn) ! added header record
                      nonz  =  nonz + 1
                      nelnc = pck124(ncont1, ncont, icsnp)
                      call iwrita (jbufiv, nelnc)
                      if (ncont > 0) then
                         call iwritb (jbufiv, irsde, 1, ncont)
                         call writb (jbuffv, bc, 1, nterm)
                      end if
                      if (ncont1 > 0) then
                         call iwrita (jbufiv, irs1)
                         call writa (jbuffv, bc1)
                      end if
                      call coll_bc_stat (nterm, ncont, irsde, bc, &
                           ncont1, irs1, bc1)
                   end if
                end do icsnp_loop
                if (nonz < npcset(isnp)) call iwrita (jbufiv, 0)
             end do jcsn_loop
          end do jsn_loop
       end do lj_loop
       nsch(isnp) = jch1

! catalog final disk positions for each symmetry for eof checks in HAM
       jch1 = jch1 + 1
       call catlog (jbufiv, ivbci(jch1,isnp))
       call catlog (jbuffv, ivbcr(jch1,isnp))

    end do isnp_loop
    call report_bc_stat

    call endw (jbufiv)
    call writix (jbufiv)
    call iwrita (jbufiv, npset)

! additional symmetry information for ham: lpset, ippset, ispset
    call iwritb (jbufiv, lpset, 1, npset)   
    call iwritb (jbufiv, ippset, 1, npset)
    call iwritb (jbufiv, ispset, 1, npset)

    call iwritb (jbufiv, nsch, 1, npset)
    do i = 1, npset
       call iwritb (jbufiv, ivbci(:,i), 1, nsch(i)+1) ! modified for eof
    end do
    call endw (jbufiv)
    call endw (jbuffv)
    call writix (jbuffv)
    do i  =  1,npset
       call iwritb (jbuffv, ivbcr(:, i), 1, nsch(i)+1)
    end do
    call endw (jbuffv)

    if (bug9 > 0) then
       write (fo,'(/,a,i5)') 'Data on index of abci and abcr: &
            &npset = ', npset
       write (fo,'(//2x,a6,9(2x,i6)/(8x,9(2x,i6)))') 'nsch', &
            nsch(1:npset)
       write (fo,'(/,2x,a6)') 'ivbci'
       do j = 1, npset
          write (fo,'((8x,6(2x,i9)))') ivbci(1:nsch(j)+1,j)
       end do
       write (fo,'(/,2x,a6)') 'ivbcr'
       do j = 1, npset
          write (fo,'((8x,6(2x,i9)))') ivbcr(1:nsch(j)+1, j)
       end do
    end if

    stats = 'keep'
    call close (jbufiv, stats)
    call close (jbuffv, stats)
    write (fo,'(a)') 'ANGBC complete'
  end subroutine angbc

 subroutine bccup (nelc, indni, surni, indmi, surmi, indmj, surmj,   &
      vi, lsseti, lcfgp, jspin, vm, lssetm, lrhoi, lrhoj, lsigi, lj, &
      lrgl, lrgs, mcomx, mincoi, maxcoi, lamdmn, lamdmx, lamemn,     &
      lamemx, vval, exval, idebug)
! couple a bound configuration with a continuum after the interacting
! shells have been determined.  determine vval(lamd) and exval(lame)
! for given lrhoi,lrhoj,lsigi,lj.
    use angular_momentum, only: spinw, rmetab, dracah
    use dim, only: ldim1
    use symmetries, only: isran2
    use ang_aux, only: cupl2
    use ls_states, only: lvec, svec
    integer, intent(in)    :: nelc
    integer, intent(in)    :: lcfgp
    integer, intent(in)    :: jspin
    integer, intent(in)    :: lrhoi, lrhoj
    integer, intent(in)    :: lsigi, lj, lrgl, lrgs
    integer, intent(in)    :: mcomx
    integer, intent(out)   :: lamdmn, lamdmx
    integer, intent(out)   :: lamemn
    integer, intent(inout) :: lamemx
    integer, intent(in)    :: idebug
    integer, intent(in)    :: indni(:)
    real(wp), intent(in)   :: surni(:)
    integer, intent(in)    :: indmi(:)
    real(wp), intent(in)   :: surmi(:)     ! mincoi
    integer, intent(in)    :: indmj(:)
    real(wp), intent(in)   :: surmj(:)
    integer, intent(in)    :: vi
    integer, intent(in)    :: lsseti
    integer, intent(in)    :: vm
    integer, intent(in)    :: lssetm
    integer, intent(in)    :: mincoi(:), maxcoi(:)
    real(wp), intent(out)  :: vval(0:ldim1), exval(0:ldim1)
    real(wp)               :: spifac(isran2+4), ashel(isran2+4,0:ldim1)
    integer                :: lamd1, lamd2, lame1, incoux
    integer                :: lame2, lrgl2, lcfp21, lcfgp2, lrhoi2
    integer                :: lrhoj2, lsigi2, lj2, incou, ispin, lcfg
    integer                :: lcfg2, lcurl1, lcurl2, npoint
    integer                :: kap1, kap2, idrct, iscur1, iscur2, isp1
    integer                :: lamdlo, lamdup, lcurls, lamd, i1, i2
    integer                :: lam2, lsign, lcurl, lcur2, iscurl
    integer                :: lame, kaplo, kapup, kap, kapt2
    real(wp)               :: surf1i, prod, rac, d1, d2, ex1
    real(wp)               :: rac1, rac2, rac3, ex2, ex3
    integer, pointer       :: li(:), isi(:), lmi(:), ismi(:)

    li => lvec(vi, lsseti)
    isi => svec(vi, lsseti)
    lmi => lvec(vm, lssetm)
    ismi => svec(vm, lssetm)
! check parity
    if  (MOD(lrhoi+lrhoj+lsigi+lj,2) /= 0) then
       write (fo,'(2(a,i4))') 'bccup: parity violation, lrgl = ', &
            lrgl,' lrgs = ', lrgs
       stop
    end if

! lamdmn,lamdmx will hold the limits of direct lambda
! if lamdmn = 999, no direct integrals have been calculated this entry
! lamemn is initially set to 999 and acts as an indicator that
! an exchange integral has been calculated.
! lamemx <0 => no exchange integral should be calculated
! this happens when irho=isig or jrho=jsig
    lamdmn = 999
    lamdmx = 0
    lamemn = 999
    vval = 0.0_wp
    exval = 0.0_wp

! calculate limits on direct and exchange lambda
    lamd1 = MAX(ABS(lrhoi-lrhoj), ABS(lsigi-lj))
    lamd2 = MIN(lrhoi+lrhoj, lsigi+lj)
    lame1 = MAX(ABS(lsigi-lrhoj), ABS(lj-lrhoi))
    if (lamemx >= 0) then
       lame2 = MIN(lsigi+lrhoj, lj+lrhoi)
    else
       lame2 = -999
    end if
    lrgl2 = lrgl + lrgl
    lcfp21 = lcfgp + lcfgp + 1
    lcfgp2 = lcfgp + lcfgp
    lrhoi2 = lrhoi + lrhoi
    lrhoj2 = lrhoj + lrhoj
    lsigi2 = lsigi + lsigi
    lj2 = lj + lj

    incou_loop: do incoux = 1, SIZE(indni)
       incou = indni(incoux)
       surf1i = surni(incoux)
       if (ABS(surf1i) <= eps) cycle incou_loop
       ispin = isi(incou)
       if (ABS(ispin-jspin) <= 2) then
          lcfg = li(incou)
          lcfg2 = lcfg+lcfg
! when there are only 2 electrons the n-2 tree consists of 1 empty shell
          if (nelc == 2) then
             lcurl1 = 0
             lcurl2 = 0
             ashel(1,0) = 1.0_wp
             ashel(3,0) = 0.0_wp
             npoint = 1
          else
! calculate limits on top coupling of n-i tree
             lcurl1 = MAX(ABS(lcfg-lrhoi), ABS(lcfgp-lrhoj))
             lcurl2 = MIN(lcfg+lrhoi, lcfgp+lrhoj)
             if (lcurl1 <= lcurl2) then
                i1 = mincoi(incou)
                if (i1 == 0) then
                   npoint = 0
                else
                   i2 = maxcoi(incou)
                   call cupl2 (indmi(i1:i2), surmi(i1:i2), indmj, &
                        surmj, mcomx, lmi, ismi, ashel, npoint)
                end if
             end if
          end if

! if no entries in ashel go to end of loop
          if (npoint > 0) then
             prod = SQRT(REAL((lcfg+lcfg+1)*lcfp21,wp)) * surf1i
! calculate limits on top coupling of n-i tree
             kap1 = MAX(ABS(lcfg-lrhoj), ABS(lcfgp-lrhoi))
             kap2 = MIN(lcfg+lrhoj, lcfgp+lrhoi)

! calculate the spin at the top of the n-1 tree
! find the appropriate spin racah coefficient from the spinw table
             if (ispin == jspin) then
                idrct = 1
                iscur1 = ispin - 1
                if (iscur1 == 0) then
                   iscur1 = 2
                   iscur2 = 2
                   spifac(2) = spinw(3,ispin)
                else
                   iscur2 = ispin + 1
                   if (lrgs == iscur1) then
                      spifac(iscur1) = spinw(1,ispin)
                      spifac(iscur2) = spinw(2,ispin)
                   else
                      spifac(iscur1) = spinw(2,ispin)
                      spifac(iscur2) = spinw(3,ispin)
                   end if
                end if
             else
                isp1 = MIN(ispin, jspin)
                iscur1 = isp1 + 1
                iscur2 = iscur1
                spifac(iscur1) = spinw(4,isp1)
                idrct = -1
             end if

! revise direct lambda limits
             lamdlo = MAX(ABS(lcfg-lcfgp), lamd1)
             if (MOD(lamdlo+lamd1,2) /= 0) lamdlo = lamdlo + 1
             lamdup = MIN(lcfg+lcfgp, lamd2)
             if (lamdlo > lamdup) idrct = -1

             if (idrct == 1) then ! direct angular integral
                lcurls = 1
                if (MOD(lcurl1+lcfg+lcfgp,2) == 1) lcurls = -1
                lamdmn = MIN(lamdlo, lamdmn)
                lamdmx = MAX(lamdup, lamdmx)
                lamd_loop: do lamd = lamdlo, lamdup, 2
                   lam2 = lamd + lamd
                   call dracah (lcfg2, lcfgp2, lsigi2, lj2, lam2,&
                        lrgl2, rac)
                   d1 = prod * rac * rmetab(lamd,lrhoi,lrhoj) *  &
                        rmetab(lamd,lsigi,lj)
                   lsign = lcurls
                   lcurl_loop: do lcurl = lcurl1, lcurl2
                      lcur2 = lcurl + lcurl
                      call dracah (lcfg2, lcfgp2, lrhoi2, lrhoj2,&
                           lam2, lcur2, rac)
                      d2 = rac * d1 * lsign
                      lsign = - lsign
                      iscurl_loop: do iscurl = iscur1, iscur2, 2
                         vval(lamd) = vval(lamd) + ashel(iscurl,lcurl) &
                              * d2
                         if (idebug /= 0) then
                            write (fo,'(a,3i6,4f12.5)') 'lamd, &
                                 &iscurl,lcurl,ashel,d1,d2,vval', &
                                 lamd, iscurl, lcurl, &
                                 ashel(iscurl,lcurl), d1, d2,  &
                                 vval(lamd)
                            if (ashel(iscurl,lcurl) /= 0.0_wp) then
                               write (fo,'(a, 4i4)') 'lrhoi,lrhoj,&
                                    &lsigi,lj' , lrhoi, lrhoj, &
                                    lsigi, lj
                            end if
                         end if
                      end do iscurl_loop
                   end do lcurl_loop
                end do lamd_loop
             end if

             if (lame1 <= lame2) then ! exchange integral
                lamemn = lame1
                lamemx = lame2
                lame_loop: do lame = lame1, lame2, 2
                   lam2 = lame + lame
                   ex1 = prod * rmetab(lame,lrhoi,lj) * &
                        rmetab(lame,lrhoj,lsigi)
                   kaplo = MAX(ABS(lrgl-lame), kap1)
                   kapup = MIN(lrgl+lame, kap2)
                   do kap = kaplo, kapup
                      kapt2 = kap + kap
                      call dracah (kapt2, lcfg2, lam2, lsigi2, &
                           lrhoj2, lrgl2, rac1)
                      call dracah (kapt2, lcfgp2, lam2, lj2, &
                           lrhoi2, lrgl2, rac2)
                      ex2 = ex1 * (kap + kap + 1) * rac1 * rac2
                      do lcurl = lcurl1, lcurl2
                         lcur2 = lcurl + lcurl
                         call dracah (lcfg2, lrhoj2, lrhoi2, lcfgp2,&
                              kapt2, lcur2, rac3)
                         ex3 = ex2 * rac3
                         do iscurl = iscur1, iscur2, 2
                            exval(lame) = exval(lame) +      &
                                 ashel(iscurl,lcurl) * ex3 * &
                                 spifac(iscurl)
                         end do
                      end do
                   end do
                end do lame_loop
             end if

          end if   ! npoint > 0
       end if
    end do incou_loop
  end subroutine bccup
end module bc_angular_integrals
