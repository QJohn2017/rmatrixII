module multiplet_energies
! Time-stamp: "2005-02-11 17:12:03 cjn"
  use precisn, only: wp
  implicit none
! engy:
  real(wp), save     :: pdi2(3,5)    ! p-diagonal energies
  real(wp), save     :: ddi2(16,9)   ! d-diagonal energies
  real(wp), save     :: ddi4(16,9)
  real(wp), save     :: doff32, doff34, doff42(16), doff44(16)
  real(wp), save     :: doff52, doff54

  private
  public energy
  public pdi2, ddi2, ddi4
  public doff32, doff34, doff42, doff44, doff52, doff54

contains
  subroutine energy
! calculates energy terms taken from slater appendix 21
! note n=5 offdiagonal term disagrees in sign with slater
! table goes up to d electrons; f must be added
    real(wp)         :: pdenom, doff, ddenom
    integer          :: n, n1, nn1

! p-diagonal terms: Eav = F0(pp) - 2/25*F2(pp)  ! Slater p 286
    pdenom = 1.0_wp / REAL(25,wp)
    n = 2             ! P**2
    n1 = n - 1
    nn1 = (n * (n - 1)) / 2      ! # pairs
    pdi2(1,n1) = REAL(-nn1*2 +12,wp) * pdenom     ! 1S
    pdi2(2,n1) = REAL(-nn1*2 +3,wp) * pdenom      ! 1D
    pdi2(3,n1) = REAL(-nn1*2 -3,wp) * pdenom      ! 3P
    n = 3             ! P**3
    n1 = n - 1
    nn1 = (n * (n - 1)) / 2
    pdi2(1,n1) = REAL(-nn1*2 +6,wp) * pdenom      ! 2Po
    pdi2(2,n1) = REAL(-nn1*2,wp) * pdenom         ! 2Do
    pdi2(3,n1) = REAL(-nn1*2 -9,wp) * pdenom      ! 4So
    n = 4            ! P**4
    n1 = n - 1
    nn1 = (n * (n - 1)) / 2
    pdi2(1,n1) = REAL(-nn1*2 +12,wp) * pdenom
    pdi2(2,n1) = REAL(-nn1*2 +3,wp) * pdenom
    pdi2(3,n1) = REAL(-nn1*2 -3,wp) * pdenom
    n = 5            ! P**5
    n1=  n - 1
    nn1 = (n * (n - 1)) / 2
    pdi2(1,n1) = REAL(-nn1*2,wp) * pdenom
    pdi2(2:3,n1) = 0.0_wp
    n = 6            ! P**6
    n1 = n - 1
    nn1 = (n * (n - 1)) / 2
    pdi2(1,n1) = REAL(-nn1*2,wp) * pdenom
    pdi2(2:3,n1) = 0.0_wp

! d-diagonal terms: Eav(pair) = F0(dd) - 14/441*F2(dd) - 14/441*F4(dd)
!                   Slater p. 286
    ddenom = 1.0_wp / 441.0_wp
    n = 2              ! d**2
    n1 = n - 1
    nn1 = (n * (n - 1)) / 2
    ddi2(1,n1) = REAL(-nn1*14 +140,wp) * ddenom   ! 1S  F2(dd)
    ddi2(2,n1) = REAL(-nn1*14 -13,wp) * ddenom    ! !D
    ddi2(3,n1) = REAL(-nn1*14 +50,wp) * ddenom    ! 1G
    ddi2(4,n1) = REAL(-nn1*14 +77,wp) * ddenom    ! 3P
    ddi2(5,n1) = REAL(-nn1*14 -58,wp) * ddenom    ! 3F
    ddi2(6:16,n1) = 0.0_wp
    ddi4(1,n1) = REAL(-nn1*14 +140,wp) * ddenom   ! 1S F4(dd)
    ddi4(2,n1) = REAL(-nn1*14 +50,wp) * ddenom    ! 1D
    ddi4(3,n1) = REAL(-nn1*14 +15,wp) * ddenom    ! 1G
    ddi4(4,n1) = REAL(-nn1*14 -70,wp) * ddenom    ! 3P
    ddi4(5,n1) = REAL(-nn1*14 +5,wp) * ddenom     ! 3F
    ddi4(6:16,n1) = 0.0_wp
    n = 3              ! d**3
    n1 = n - 1
    nn1 = (n * (n - 1)) / 2
    ddi2(1,n1) = REAL(-nn1*14 +105,wp) * ddenom
    ddi2(2,n1) = REAL(-nn1*14 -12,wp) * ddenom
    ddi2(3,n1) = REAL(-nn1*14 +69,wp) * ddenom
    ddi2(4,n1) = REAL(-nn1*14 +123,wp) * ddenom
    ddi2(5,n1) = REAL(-nn1*14 -57,wp) * ddenom
    ddi2(6,n1) = REAL(-nn1*14 -12,wp) * ddenom
    ddi2(7,n1) = REAL(-nn1*14 +42,wp) * ddenom
    ddi2(8,n1) = REAL(-nn1*14 -93,wp) * ddenom
    ddi2(9:16,n1) = 0.0_wp
    ddi4(1,n1) = REAL(-nn1*14 +105,wp) * ddenom
    ddi4(2,n1) = REAL(-nn1*14 +30,wp) * ddenom
    ddi4(3,n1) = REAL(-nn1*14 -15,wp) * ddenom
    ddi4(4,n1) = REAL(-nn1*14 -45,wp) * ddenom
    ddi4(5,n1) = REAL(-nn1*14 +55,wp) * ddenom
    ddi4(6,n1) = REAL(-nn1*14 +30,wp) * ddenom
    ddi4(7,n1) = REAL(-nn1*14 -105,wp) * ddenom
    ddi4(8,n1) = REAL(-nn1*14 -30,wp) * ddenom
    ddi4(9:16,n1) = 0.0_wp
    n = 4              ! d**4
    n1 = n - 1
    nn1 = (n * (n - 1)) / 2
    ddi2(1,n1) = REAL(-nn1*14 +210,wp) * ddenom
    ddi2(2,n1) = REAL(-nn1*14 +219,wp) * ddenom
    ddi2(3,n1) = REAL(-nn1*14 +30,wp) * ddenom
    ddi2(4,n1) = REAL(-nn1*14 +21,wp) * ddenom
    ddi2(5,n1) = REAL(-nn1*14 +66,wp) * ddenom
    ddi2(6,n1) = REAL(-nn1*14 +138,wp) * ddenom
    ddi2(7,n1) = REAL(-nn1*14 +111,wp) * ddenom
    ddi2(8,n1) = REAL(-nn1*14 +84,wp) * ddenom
    ddi2(9,n1) = REAL(-nn1*14 +48,wp) * ddenom
    ddi2(10,n1) = REAL(-nn1*14 -51,wp) * ddenom
    ddi2(11,n1) = REAL(-nn1*14 +57,wp) * ddenom
    ddi2(12,n1) = REAL(-nn1*14 +39,wp) * ddenom
    ddi2(13,n1) = REAL(-nn1*14 +12,wp) * ddenom
    ddi2(14,n1) = REAL(-nn1*14 -24,wp) * ddenom
    ddi2(15,n1) = REAL(-nn1*14 -69,wp) * ddenom
    ddi2(16,n1) = REAL(-nn1*14 -105,wp) * ddenom
    ddi4(1,n1) = REAL(-nn1*14 +210,wp) * ddenom
    ddi4(2,n1) = REAL(-nn1*14 +30,wp) * ddenom
    ddi4(3,n1) = REAL(-nn1*14 +135,wp) * ddenom
    ddi4(4,n1) = REAL(-nn1*14 +70,wp) * ddenom
    ddi4(5,n1) = REAL(-nn1*14 +45,wp) * ddenom
    ddi4(6,n1) = REAL(-nn1*14 -30,wp) * ddenom
    ddi4(7,n1) = REAL(-nn1*14 -15,wp) * ddenom
    ddi4(8,n1) = REAL(-nn1*14,wp) * ddenom
    ddi4(9,n1) = REAL(-nn1*14 +20,wp) * ddenom
    ddi4(10,n1) = REAL(-nn1*14 +75,wp) * ddenom
    ddi4(11,n1) = REAL(-nn1*14 -55,wp) * ddenom
    ddi4(12,n1) = REAL(-nn1*14 -45,wp) * ddenom
    ddi4(13,n1) = REAL(-nn1*14 -30,wp) * ddenom
    ddi4(14,n1) = REAL(-nn1*14 -10,wp) * ddenom
    ddi4(15,n1) = REAL(-nn1*14 +15,wp) * ddenom
    ddi4(16,n1) = REAL(-nn1*14 -105,wp) * ddenom
    n = 5                 ! d**5
    n1 = n - 1
    nn1 = (n * (n - 1)) / 2
    ddi2(1,n1) = REAL(-nn1*14 +140,wp) * ddenom
    ddi2(2,n1) = REAL(-nn1*14 +320,wp) * ddenom
    ddi2(3,n1) = REAL(-nn1*14 +104,wp) * ddenom
    ddi2(4,n1) = REAL(-nn1*14 -85,wp) * ddenom
    ddi2(5,n1) = REAL(-nn1*14 +167,wp) * ddenom
    ddi2(6,n1) = REAL(-nn1*14 -58,wp) * ddenom
    ddi2(7,n1) = REAL(-nn1*14 -112,wp) * ddenom
    ddi2(8,n1) = REAL(-nn1*14 +23,wp) * ddenom
    ddi2(9,n1) = REAL(-nn1*14 +113,wp) * ddenom
    ddi2(10,n1) = REAL(-nn1*14 +86,wp) * ddenom
    ddi2(11,n1) = REAL(-nn1*14 +59,wp) * ddenom
    ddi2(12,n1) = REAL(-nn1*14 +23,wp) * ddenom
    ddi2(13,n1) = REAL(-nn1*14 -76,wp) * ddenom
    ddi2(14,n1) = REAL(-nn1*14 -22,wp) * ddenom
    ddi2(15,n1) = REAL(-nn1*14 -85,wp) * ddenom
    ddi2(16,n1) = REAL(-nn1*14 -175,wp) * ddenom
    ddi4(1,n1) = REAL(-nn1*14 +140,wp) * ddenom
    ddi4(2,n1) = REAL(-nn1*14 -100,wp) * ddenom
    ddi4(3,n1) = REAL(-nn1*14 +20,wp) * ddenom
    ddi4(4,n1) = REAL(-nn1*14 +125,wp) * ddenom
    ddi4(5,n1) = REAL(-nn1*14 -15,wp) * ddenom
    ddi4(6,n1) = REAL(-nn1*14 +110,wp) * ddenom
    ddi4(7,n1) = REAL(-nn1*14 +35,wp) * ddenom
    ddi4(8,n1) = REAL(-nn1*14 -40,wp) * ddenom
    ddi4(9,n1) = REAL(-nn1*14 -55,wp) * ddenom
    ddi4(10,n1) = REAL(-nn1*14 -40,wp) * ddenom
    ddi4(11,n1) = REAL(-nn1*14 -25,wp) * ddenom
    ddi4(12,n1) = REAL(-nn1*14 -5,wp) * ddenom
    ddi4(13,n1) = REAL(-nn1*14 +50,wp) * ddenom
    ddi4(14,n1) = REAL(-nn1*14 -85,wp) * ddenom
    ddi4(15,n1) = REAL(-nn1*14 -50,wp) * ddenom
    ddi4(16,n1) = REAL(-nn1*14 -175,wp) * ddenom
    n = 6                  ! d**6
    n1 = n - 1
    nn1 = (n * (n - 1)) / 2
    ddi2(1,n1) = REAL(-nn1*14 +210,wp) * ddenom
    ddi2(2,n1) = REAL(-nn1*14 +219,wp) * ddenom
    ddi2(3,n1) = REAL(-nn1*14 +30,wp) * ddenom
    ddi2(4,n1) = REAL(-nn1*14 +21,wp) * ddenom
    ddi2(5,n1) = REAL(-nn1*14 +66,wp) * ddenom
    ddi2(6,n1) = REAL(-nn1*14 +138,wp) * ddenom
    ddi2(7,n1) = REAL(-nn1*14 +111,wp) * ddenom
    ddi2(8,n1) = REAL(-nn1*14 +84,wp) * ddenom
    ddi2(9,n1) = REAL(-nn1*14 +48,wp) * ddenom
    ddi2(10,n1) = REAL(-nn1*14 -51,wp) * ddenom
    ddi2(11,n1) = REAL(-nn1*14 +57,wp) * ddenom
    ddi2(12,n1) = REAL(-nn1*14 +39,wp) * ddenom
    ddi2(13,n1) = REAL(-nn1*14 +12,wp) * ddenom
    ddi2(14,n1) = REAL(-nn1*14 -24,wp) * ddenom
    ddi2(15,n1) = REAL(-nn1*14 -69,wp) * ddenom
    ddi2(16,n1) = REAL(-nn1*14 -105,wp) * ddenom
    ddi4(1,n1) = REAL(-nn1*14 +210,wp) * ddenom
    ddi4(2,n1) = REAL(-nn1*14 +30,wp) * ddenom
    ddi4(3,n1) = REAL(-nn1*14 +135,wp) * ddenom
    ddi4(4,n1) = REAL(-nn1*14 +70,wp) * ddenom
    ddi4(5,n1) = REAL(-nn1*14 +45,wp) * ddenom
    ddi4(6,n1) = REAL(-nn1*14 -30,wp) * ddenom
    ddi4(7,n1) = REAL(-nn1*14 -15,wp) * ddenom
    ddi4(8,n1) = REAL(-nn1*14,wp) * ddenom
    ddi4(9,n1) = REAL(-nn1*14 +20,wp) * ddenom
    ddi4(10,n1) = REAL(-nn1*14 +75,wp) * ddenom
    ddi4(11,n1) = REAL(-nn1*14 -55,wp) * ddenom
    ddi4(12,n1) = REAL(-nn1*14 -45,wp) * ddenom
    ddi4(13,n1) = REAL(-nn1*14 -30,wp) * ddenom
    ddi4(14,n1) = REAL(-nn1*14 -10,wp) * ddenom
    ddi4(15,n1) = REAL(-nn1*14 +15,wp) * ddenom
    ddi4(16,n1) = REAL(-nn1*14 -105,wp) * ddenom
    n = 7                 ! d**7
    n1 = n - 1
    nn1 = (n * (n - 1)) / 2
    ddi2(1,n1) = REAL(-nn1*14 +105,wp) * ddenom
    ddi2(2,n1) = REAL(-nn1*14 -12,wp) * ddenom
    ddi2(3,n1) = REAL(-nn1*14 +69,wp) * ddenom
    ddi2(4,n1) = REAL(-nn1*14 +123,wp) * ddenom
    ddi2(5,n1) = REAL(-nn1*14 -57,wp) * ddenom
    ddi2(6,n1) = REAL(-nn1*14 -12,wp) * ddenom
    ddi2(7,n1) = REAL(-nn1*14 +42,wp) * ddenom
    ddi2(8,n1) = REAL(-nn1*14 -93,wp) * ddenom
    ddi2(9:16,n1) = 0.0_wp
    ddi4(1,n1) = REAL(-nn1*14 +105,wp) * ddenom
    ddi4(2,n1) = REAL(-nn1*14 +30,wp) * ddenom
    ddi4(3,n1) = REAL(-nn1*14 -15,wp) * ddenom
    ddi4(4,n1) = REAL(-nn1*14 -45,wp) * ddenom
    ddi4(5,n1) = REAL(-nn1*14 +55,wp) * ddenom
    ddi4(6,n1) = REAL(-nn1*14 +30,wp) * ddenom
    ddi4(7,n1) = REAL(-nn1*14 -105,wp) * ddenom
    ddi4(8,n1) = REAL(-nn1*14 -30,wp) * ddenom
    ddi4(9:16,n1) = 0.0_wp
    n = 8                 ! d**8
    n1 = n - 1
    nn1 = (n * (n - 1)) / 2
    ddi2(1,n1) = REAL(-nn1*14 +140,wp) * ddenom
    ddi2(2,n1) = REAL(-nn1*14 -13,wp) * ddenom
    ddi2(3,n1) = REAL(-nn1*14 +50,wp) * ddenom
    ddi2(4,n1) = REAL(-nn1*14 +77,wp) * ddenom
    ddi2(5,n1) = REAL(-nn1*14 -58,wp) * ddenom
    ddi2(6:16,n1) = 0.0_wp
    ddi4(1,n1) = REAL(-nn1*14 +140,wp) * ddenom
    ddi4(2,n1) = REAL(-nn1*14 +50,wp) * ddenom
    ddi4(3,n1) = REAL(-nn1*14 +15,wp) * ddenom
    ddi4(4,n1) = REAL(-nn1*14 -70,wp) * ddenom
    ddi4(5,n1) = REAL(-nn1*14 +5,wp) * ddenom
    ddi4(6:16,n1) = 0.0_wp
    n = 9               ! d**9
    n1 = n - 1
    nn1 = (n * (n - 1)) / 2
    ddi2(1,n1) = REAL(-nn1*14,wp) * ddenom
    ddi4(1,n1) = REAL(-nn1*14,wp) * ddenom
    n = 10              ! d**10
    n1 = n - 1
    nn1 = (n * (n - 1)) / 2
    ddi2(1,n1) = REAL(-nn1*14,wp) * ddenom
    ddi4(1,n1) = REAL(-nn1*14,wp) * ddenom

! off diagonal elements: Slater 294-295
! these are for terms with differing seniority
! the numbering corresponds to array isen given in bconqn
    doff = SQRT(21.0_wp) * ddenom       ! d**3
    doff32 = 27.0_wp * doff
    doff34 = -15.0_wp * doff

    doff = SQRT(21.0_wp) * ddenom       ! d**4
    doff42(1) = 54.0_wp * doff
    doff44(1) = -30.0_wp * doff
    doff = SQRT(2.0_wp) * ddenom
    doff42(2) = 108.0_wp * doff
    doff44(2) = -60.0_wp * doff
    doff = SQRT(11.0_wp) * ddenom
    doff42(3) = 36.0_wp * doff
    doff44(3) = -20.0_wp * doff
    doff = SQRT(14.0_wp) * ddenom
    doff42(4) = 36.0_wp * doff
    doff44(4) = -20.0_wp * doff
    doff42(5) = 108.0_wp * ddenom
    doff44(5) = -60.0_wp * ddenom

! transposed part of the interaction matrix:
! other member of seniority pair refers back to first member
    doff42(6) = doff42(1)
    doff44(6) = doff44(1)
    doff42(7) = doff42(2)
    doff44(7) = doff44(2)
    doff42(8) = 0.0_wp
    doff44(8) = 0.0_wp
    doff42(9) = doff42(3)
    doff44(9) = doff44(3)
    doff42(10) = 0.0_wp
    doff44(10) = 0.0_wp
    doff42(11) = doff42(4)
    doff44(11) = doff44(4)
    doff42(12) = 0.0_wp
    doff44(12) = 0.0_wp
    doff42(13) = doff42(5)
    doff44(13) = doff44(5)
    doff42(14) = 0.0_wp
    doff44(14) = 0.0_wp
    doff42(15) = 0.0_wp
    doff44(15) = 0.0_wp
    doff42(16) = 0.0_wp
    doff44(16) = 0.0_wp

! note the n=5 off diagonal disagree in sign with slater
! taken from the results of the code without the table
    doff = SQRT(14.0_wp) * ddenom      ! d**5
    doff52 = -54.0_wp * doff
    doff54 = 30.0_wp * doff
  end subroutine energy
end module multiplet_energies
