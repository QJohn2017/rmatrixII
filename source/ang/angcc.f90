module cc_angular_integrals
! Time-stamp: "2005-03-11 15:30:01 cjn"
  use precisn, only: wp
  use io_units, only: fo, jbufiv, jbuffv, jbufie, jbuffe
  use debug, only: bug7, bug9
  use angular_momentum, only: spinw, dracah, rme
  use startup, only: ljcomp
  implicit none

  real(wp), parameter       :: eps = 1.0e-5_wp

  private
  public angcc

contains
  subroutine angcc (nelc, nset, lset, isset, ipset, noccsh, nocorb, &
       nelcsh, nmefig, nnq, lsset, mcoumx, mmq, id)
! calculate the direct angular integral over target configurations.
! the configurations are arranged in sets according to l, s and pi
    use filehand, only: open, newfil, catlog, iwrita, lookup, endw, &
         writix, iwritb, iwritc, close, writa
    use dim, only: ldim1
    use symmetries, only: isran2
    use surfacing_tables, only: surf_vec
    use sym_sets, only: get_ncset, get_ncfg
    integer, intent(in)  :: nelc
    integer, intent(in)  :: nset
    integer, intent(in)  :: lset(:), isset(:), ipset(:)
    integer, intent(in)  :: noccsh(:)
    integer, intent(in)  :: nocorb(:,:)
    integer, intent(in)  :: nelcsh(:,:), nmefig(:,:)
    integer, intent(in)  :: nnq(:)
    integer, intent(in)  :: lsset
    integer, intent(in)  :: mcoumx(:)
    integer, intent(in)  :: mmq(:)
    integer, intent(in)  :: id

    character(len=7)     :: filea, fileb, filec, filed, stats
    integer              :: ivcont((nset*(nset+1))/2+1)
    integer              :: iecont((nset*(nset+1))/2+1)
    integer              :: lamij(nset,nset)
    real(wp)             :: exshel(isran2+4), ashel(isran2+4,0:ldim1)
    integer              :: nines = 9999
    integer  :: ndummy, isjs, is, lcfg, lcfg2, ispin, ipi, js
    integer  :: idrct, lamlom, lamupm, lcfgp2, jspin, jpi, lam1
    integer  :: lam2, isp1, iscur1, iscur2, isp0, lcfgs, ics
    integer  :: icou, ic, icq, jcslo, jcs, jcou, jc, jcq
    integer  :: nme, ki, kj, ksheli, kshelj, irho, isig, nsheli, nshelj
    integer  :: lrho, lsig, lrho2, lsig2, lmmax, lmmin, lmmins, nlm, irs
    integer  :: nmq, mcomx, idelp, ik, ishel
    integer  :: jshel, idelps, lsigs, lamlo, lamup, lam, lamt2, lsign
    integer  :: lmval, lmval2, irsl, kaplo, kapup, kap
    integer  :: kap21, kap2, ism, ispct, irsks, nsh
    integer  :: inr, lcfgp, npoint, jk, ilrgs
    integer  :: nrecv, nrece, ibufv, ibufe, i
    real(wp) :: fac1, fac2, facdir, facex, vshel, rac, ashsum, vshval
    real(wp) :: exsval
    logical  :: interact
    integer, pointer  :: indxi(:), indxj(:)
    real(wp), pointer :: suri(:), surj(:)
    integer, pointer  :: ncset(:), ncfga(:,:), ncfgb(:,:)
    logical           :: real_typ

    if (bug7 == 1) then
       write (fo,'(/,a,/)') 'subroutine angcc'
       write (fo,'(a,/,a,/)') '      is    js   iecont    ivcont lamij',&
            '    irls     vshval       irsks      exsval'
    end if

    ncset => get_ncset(1)
    call get_ncfg (1, ncfga, ncfgb)

! initialise writing to all output files
    filea = 'accdi'
    fileb = 'accdr'
    filec = 'accei'
    filed = 'accer'
    stats = 'new'

    real_typ = .false.
    call open (jbufiv, filea, stats, real_typ)
    call open (jbufie, filec, stats, real_typ)
    real_typ = .true.
    call open (jbuffv, fileb, stats, real_typ)
    call open (jbuffe, filed, stats, real_typ)

    call newfil (jbufiv, ndummy)
    call newfil (jbuffv, ndummy)
    call newfil (jbufie, ndummy)
    call newfil (jbuffe, ndummy)

    isjs = 0
    symmetry: do is = 1, nset    ! loop over SLpi values
       lcfg = lset(is)             ! Li value
       lcfg2 = lcfg + lcfg
       ispin = isset(is)           ! Si-value
       ipi = ipset(is)             ! Pi-value
       js_loop: do js = is, nset
          isjs = isjs + 1
          call catlog (jbufiv, ivcont(isjs))
          call catlog (jbufie, iecont(isjs))

          lamij(is,js) = -999   ! limits of lambda for direct integrals
          lamij(js,is) = -999
          idrct = 1          ! reset if no integrals between sets is, js
          lamlom = 999       ! min lambda for is, js
          lamupm = 0         ! max lambda for is, js

          lcfgp = lset(js)          ! Lj
          lcfgp2 = lcfgp + lcfgp
          jspin = isset(js)         ! Sj
          jpi = ipset(js)           ! Pj
          if (ispin /= jspin) then      ! spins unequal
             idrct = -1                 ! no direct integrals
             if (ABS(ispin-jspin) /= 2) then ! spin difference /= 2
                if (bug7 == 1) then
                   call lookup (ivcont(isjs), nrecv, ibufv)
                   call lookup (iecont(isjs), nrece, ibufe)
                   write (fo,'(7(4X,I5))') is, js, nrece, ibufe, nrecv,&
                        ibufv, lamij(is,js)
                end if
                Cycle js_loop
             end if
          end if

! calculate lambda limits using triangular relation and parity rule
          lam1 = ABS(lcfg - lcfgp)    ! |Li - Lj|
          lam2 = lcfg + lcfgp         !  Li + Lj
          if (MOD(lam1 + ipi + jpi,2) /= 0) then ! |Li - Lj|+Pi+Pj odd
             lam1 = lam1 + 1
             lam2 = lam2-1
             if (lam1 > lam2) idrct = -1 ! no direct integrals
          end if

! calculate spin limits of top of n-1 electron tree and parameters to 
! access the table of spin racah coefficients spinw
          if (ispin == jspin) then    ! spins equal
             isp1 = ispin
             iscur1 = ispin - 1
             iscur2 = iscur1 + 2
             isp0 = 0
             if (iscur1 == 0) iscur1 = iscur2
          else                        ! unequal spins
             isp1 = MIN(ispin, jspin)
             iscur1 = isp1 + 1
             iscur2 = iscur1
             isp0 = 3
          end if

! calculate factors in ang integral which are independent of the shells
          fac1 = ((lcfg2 + 1) * (lcfgp2 + 1))**0.5_wp
          lcfgs = 1
          if (MOD(lcfg+lcfgp, 2) == 1) lcfgs = -1

! calculate intraction between each cfg in set is with each in set js
          i_cfgs: do ics = 1, ncset(is)
             icou = ncfgb(ics,is)
             ic = ncfga(ics, is)
             icq = nnq(ic)
             if (is == js) then ! symmetry in limits if is = js
                jcslo = ics
             else
                jcslo = 1
             end if
             j_cfgs: do jcs = jcslo, ncset(js)
                jcou = ncfgb(jcs,js)
                jc = ncfga(jcs,js)
                jcq = nnq(jc)
                if (ic /= jc) then

! test whether configs ic,jc can interact
! thus the n-1 electron configuration must be the same if
! an electron is removed from ksheli in ic and kshelj in jc
! this also determines the interacting shells
                   interact = .false.
                   ic_shells: do ki = 1, noccsh(ic) ! ic shells
                      nme = nmefig(ki,ic)   ! n-1 cfg, removed ki
                      do kj = 1, noccsh(jc)       ! jc shells
                         if (nme == nmefig(kj,jc)) then ! same cfg
                            ksheli = ki
                            kshelj = kj
                            interact = .true.
                            exit ic_shells
                         end if
                      end do
                   end do ic_shells
                   if (.NOT.interact) then  ! update files
                      if (idrct == 1) then
                         call iwrita (jbufiv, nines)
                         call writa (jbuffv, 0.0_wp)
                      end if
                      call iwrita (jbufie, nines)
                      call writa (jbuffe, 0.0_wp)
                      cycle j_cfgs
                   end if

! find interacting shells krho and ksig
                   irho = nocorb(ksheli,ic)    
                   isig = nocorb(kshelj,jc)
! find number of electrons in the interacting shells
                   nsheli = nelcsh(ksheli,ic)
                   nshelj = nelcsh(kshelj,jc)
! find angular momentum of the shells
                   lrho = ljcomp(irho)
                   lsig = ljcomp(isig)

                   lrho2 = lrho + lrho
                   lsig2 = lsig + lsig

! calculate the l limits of the top of the n-1 electron tree
! the parity of the n-1 system is ipi+lrho
                   lmmax = MIN(lcfg+lrho, lcfgp+lsig)
                   lmmin = MAX(ABS(lcfg-lrho), ABS(lcfgp-lsig))
                   if (lmmin > lmmax) then
                      if (idrct == 1) then
                         call iwrita (jbufiv, nines)
                         call writa (jbuffv, 0.0_wp)
                      end if
                      call iwrita (jbufie, nines)
                      call writa (jbuffe, 0.0_wp)
                      cycle j_cfgs
                   end if
                   lmmins = 1
                   if (MOD(lmmin+ipi+lrho, 2) == 1) lmmins = -1
                   nlm = lmmax - lmmin + 1
                   irs = 10000 * irho + 100 * isig

! obtain the coupling from the surfacing coefficients
                   if (nelc == 1) then   ! no surfacing coeffs
                      lmmax = 0          ! n-1 cfg -> 1S
                      lmmin = 0
                      ashel(1,0) = 1.0_wp
                      ashel(3,0) = 0.0_wp
                   else
                      nmq = mmq(nme)      ! equiv cfg
                      mcomx = mcoumx(nmq) ! # couplings

                      call surf_vec (id, icou, ksheli, icq, indxi, &
                           suri)
                      call surf_vec (id, jcou, kshelj, jcq, indxj, &
                           surj)

                      call cuple (indxi, suri, indxj, surj,        &
                           mcoumx(nmq), nmq, lsset, ashel, npoint)
                   end if

! calculate delta p
                   idelp = 0
                   if (irho < isig) then
                      do ik = ksheli+1, noccsh(ic)
                         ishel = nocorb(ik,ic)
                         if (ishel >  isig) exit
                         idelp = idelp + nelcsh(ik,ic)
                      end do
                   else
                      do jk = kshelj+1, noccsh(jc)
                         jshel = nocorb(jk,jc)
                         if (jshel > irho) exit
                         idelp = idelp + nelcsh(jk,jc)
                      end do
                   end if

! calculate the external factors of the angular integral
                   idelps = 1
                   if (MOD(idelp, 2) == 1) idelps = -1
                   lsigs = 1
                   if (MOD(lrho+lsig, 2) == 1) lsigs = -1
                   fac2 = idelps * lsigs * fac1 * &
                        (nsheli * nshelj)**0.5_wp
                   facdir = fac2 * lcfgs
                   facex = lsigs * fac2 * ((lrho2+1) * (lsig2+1))**0.5_wp

                   if (idrct == 1) then ! calculate the direct term
! reset lambda to obey triangular relation with lrho,lsig
                      lamlo = MAX(lam1, ABS(lrho-lsig))
                      lamup = MIN(lam2, lrho+lsig)

! if not possible shells will not interact
                      if (lamlo > lamup) then
                         call iwrita (jbufiv, nines)
                         call writa (jbuffv, 0.0_wp)
                      else
                         lam_loop: do lam = lamlo, lamup, 2
                            lamt2 = lam + lam
                            vshel = 0.0_wp
                            lsign = lmmins
                            do lmval = lmmin, lmmax
                               lmval2 = lmval + lmval
                               call dracah (lcfg2, lcfgp2, lrho2, lsig2,&
                                    lamt2, lmval2, rac)
                               rac = rac * lsign
                               lsign = - lsign
                               ashsum = 0.0_wp
                               do ism = iscur1, iscur2, 2
                                  ashsum = ashsum + ashel(ism,lmval)
                               end do
                               vshel = vshel + ashsum * rac
                            end do
! store the direct integral in vsh and rho,sig,lam in irhsgl
                            irsl = irs + lam
                            vshval = rme(lrho,lsig,lam) * vshel * facdir
                            if (bug7 == 1) write (fo,'(1x,i8,e15.5)') &
                                 irsl, vshval
                            call iwrita (jbufiv, irsl)
                            call writa (jbuffv, vshval)
                         end do lam_loop
                         lamlom = MIN(lamlom, lamlo)
                         lamupm = MAX(lamupm, lamup)
                      end if
                   end if

! exchange integral
                   kaplo = MAX(ABS(lcfg-lsig), ABS(lcfgp-lrho))
                   kapup = MIN(lcfg+lsig, lcfgp+lrho)
                   kap_loop: do kap = kaplo, kapup
                      kap21 = kap + kap + 1
                      kap2 = kap + kap
                      do ism = iscur1, iscur2, 2
                         exshel(ism) = 0.0_wp
                         do lmval = lmmin, lmmax
                            lmval2 = lmval + lmval
                            call dracah (lcfg2, lsig2, lrho2, lcfgp2,&
                                 kap2, lmval2, rac)
                            exshel(ism) = exshel(ism) + rac * &
                                 ashel(ism,lmval)
                         end do
                      end do
! loop over allowed lrgs and sum over curly s (n-1)spin
                      ispct = isp0 + 1
                      ilrgs_loop: do ilrgs = iscur1, iscur2, 2
                         ispct = ispct - 1
                         exsval = 0.0_wp
                         do ism = iscur1, iscur2, 2
                            ispct = ispct + 1
                            exsval = exsval + spinw(ispct,isp1) * &
                                 exshel(ism)
                         end do
! store the exchange integral in exsh and rho,sig,ism,kap in irhsk
                         irsks = irs + 4 * kap + ilrgs
                         exsval = exsval * kap21 * facex
                         if (bug7 == 1) write (fo,'(1x,23x,i8,e15.5)')&
                              irsks, exsval
                         call iwrita (jbufie, irsks)
                         call writa (jbuffe, exsval)
                      end do ilrgs_loop
                   end do kap_loop

                else  ! case of ic = jc; all shells can contribute
                   nsh = noccsh(ic)
                   kshell_loop: do ksheli = 1, nsh
                      irho = nocorb(ksheli,ic)
                      inr = 10000 * nsh + 100 * irho
                      nsheli = nelcsh(ksheli,ic)

                      lrho = ljcomp(irho)
                      lrho2 = lrho + lrho
! calculate the l limits of the top of the n-1 electron tree
                      lmmax = MIN(lcfg+lrho, lcfgp+lrho)
                      lmmin = MAX(ABS(lcfg-lrho), ABS(lcfgp-lrho))
                      if (lmmin > lmmax) then
                         if (idrct == 1) then
                            irsl = - inr - 99
                            call iwrita (jbufiv, irsl)
                            call writa (jbuffv, 0.0_wp)
                         end if
                         irsks = - inr- 9
                         call iwrita (jbufie, irsks)
                         call writa (jbuffe, 0.0_wp)
                         cycle kshell_loop
                      end if
                      lmmins = 1
                      if (MOD(lmmin+ipi+lrho, 2) == 1) lmmins = -1

! obtain the coupling from the surfacing coefficients
                      if (nelc == 1) then ! no surfacing coefficients
                         lmmax  =  0
                         lmmin = 0
                         ashel(1,0) = 1.0_wp
                         ashel(3,0) = 0.0_wp
                      else
                         nme = nmefig(ksheli,ic)
                         nmq = mmq(nme)
                         mcomx = mcoumx(nmq)
                         call surf_vec (id, icou, ksheli, icq, indxi, &
                              suri)
                         call surf_vec (id, jcou, ksheli, icq, indxj, &
                              surj)
                         call cuple (indxi, suri, indxj, surj,        &
                              mcoumx(nmq), nmq, lsset, ashel, npoint)
                      end if
! calculate the external factor of the angular integral
                      fac2 = fac1 * nsheli
                      facex = fac2 * (lrho2 + 1)
                      facdir = fac2 * lcfgs

                      if (idrct == 1) then ! calculate the direct term
! reset lambda to obey triangular relation with lrho,lsig
                         lamlo = lam1
                         lamup = MIN(lam2, lrho2)

! if not possible shells will not interact
                         if (lamlo > lamup) then
                            irsl = - inr - 99
                            call iwrita (jbufiv, irsl)
                            call writa (jbuffv, 0.0_wp)
                         else
                            lam1_loop: do lam = lamlo, lamup, 2
                               lamt2 = lam + lam
                               vshel = 0.0_wp
                               lsign = lmmins
                               do lmval = lmmin, lmmax
                                  lmval2 = lmval+lmval
                                  call dracah (lcfg2, lcfgp2, lrho2, &
                                       lrho2, lamt2, lmval2, rac)
                                  rac = rac * lsign
                                  lsign = - lsign
                                  ashsum = 0.0_wp
                                  do ism = iscur1, iscur2, 2
                                     ashsum = ashsum + ashel(ism,lmval)
                                  end do
                                  vshel = vshel + ashsum * rac
                               end do
! store the direct integral in vsh and -nsh,rho,lam in irhsgl
                               irsl = - inr - lam
                               vshval = rme(lrho,lrho,lam) * vshel * &
                                    facdir
                               if (bug7 == 1) write (fo,'(1x,i8,e15.5)')&
                                    irsl, vshval
                               call iwrita (jbufiv, irsl)
                               call writa (jbuffv, vshval)
                            end do lam1_loop
                            lamlom = MIN(lamlom, lamlo)
                            lamupm = MAX(lamupm, lamup)
                         end if
                      end if

! exchange integral
                      kaplo = MAX(ABS(lcfg-lrho), ABS(lcfgp-lrho))
                      kapup = MIN(lcfg+lrho, lcfgp+lrho)
                      kap1_loop: do kap = kaplo, kapup
                         kap21 = kap + kap + 1
                         kap2 = kap + kap
                         do ism = iscur1, iscur2, 2
                            exshel(ism) = 0.0_wp
                            do lmval = lmmin, lmmax
                               lmval2 = lmval + lmval
                               call dracah (lcfg2, lrho2, lrho2, lcfgp2,&
                                    kap2, lmval2, rac)
                               exshel(ism) = exshel(ism) + rac * &
                                    ashel(ism,lmval)
                            end do
                         end do
! loop over allowed lrgs and sum over curly s (n-1)spin
                         ispct = isp0 + 1
                         do ilrgs = iscur1, iscur2, 2
                            ispct = ispct - 1
                            exsval = 0.0_wp
                            do ism = iscur1, iscur2, 2
                               ispct = ispct + 1
                               exsval = exsval + spinw(ispct,isp1) * &
                                    exshel(ism)
                            end do
! store the exchange integral in exsh and -nsh,rho,ism,kap in irhsk
                            irsks = - inr - 4 * kap - ilrgs
                            exsval = exsval * kap21 * facex
                            if (bug7 == 1) write (fo,'(1x,23x,i8,&
                                 &e15.5)') irsks, exsval
                            call iwrita (jbufie, irsks)
                            call writa (jbuffe, exsval)
                         end do
                      end do kap1_loop
                   end do kshell_loop
                end if
             end do j_cfgs
          end do i_cfgs

          if (idrct == 1) lamij(is,js) = lamlom * 100 + lamupm

          if (bug7 == 1) then
             call lookup (ivcont(isjs), nrecv, ibufv)
             call lookup (iecont(isjs), nrece, ibufe)
             write (fo,'(7(4x,i5))') is, js, nrece, ibufe, nrecv, ibufv, &
                  lamij(is, js)
          end if
       end do js_loop
    end do symmetry

    isjs = isjs + 1
    call catlog (jbufiv, ivcont(isjs))
    call catlog (jbufie, iecont(isjs))
    call endw (jbufiv)
    call endw (jbuffv)
    call endw (jbufie)
    call endw (jbuffe)

! write index to integer files
    call writix (jbufiv)
    call iwrita (jbufiv, nset)
    call iwritb (jbufiv, ncset, 1, nset)
    call iwritc (jbufiv, lamij, 1, nset, 1, nset, nset, 0)
    call iwritb (jbufiv, ivcont, 1, isjs)
! add additional records of N-electron symmetries: lset, ipset, isset
    call iwritb (jbufiv, lset, 1, nset)
    call iwritb (jbufiv, ipset, 1, nset)
    call iwritb (jbufiv, isset, 1, nset)
    call endw (jbufiv)
! delay ending jbufiv file until after call putset in main
    call writix (jbufie)
    call iwrita (jbufie, nset)
    call iwritb (jbufie, iecont, 1, isjs)
    call endw (jbufie)

    if (bug9 > 0) then
       write (fo,'(/,a,i5)') 'data on index of accei, nset = ', nset
       write (fo,'(//2x,a6,9(2x,i6)/(8x,9(2x,i6)))') 'iecont', &
            (iecont(i), i=1,isjs)
    end if

    stats = 'keep'
    call close (jbuffv, stats)
    call close (jbufie, stats)
    call close (jbufiv, stats)
    call close (jbuffe, stats)
    
    if (bug9 > 0) then
       write (fo,'(/,a,i5)') 'data on index of accdi, nset = ', nset
       write (fo,'(//2x,a6,9(2x,i6)/(8x,9(2x,i6)))') 'ncset', &
            ncset(1:nset)
       write (fo,'(//2x,a6,9(2x,i6)/(8x,9(2x,i6)))') 'lamij', &
            lamij(1:nset,1:nset)
       write (fo,'(//2x,a6,9(2x,i6)/(8x,9(2x,i6)))') 'ivcont', &
            ivcont(1:isjs)
    end if
  end subroutine angcc

  subroutine cuple (indmi, surmi, indmj, surmj, mcomx, vc, lsset, ash,&
       ipoint)
! sum over the intermediate l.
    use ls_states, only: lvec, svec
    integer, intent(in)     :: mcomx
    integer, intent(in)     :: indmi(:)
    integer, intent(in)     :: indmj(:)
    integer, intent(out)    :: ipoint
    real(wp), intent(in)    :: surmi(:)
    real(wp), intent(in)    :: surmj(:)
    integer, intent(in)     :: vc      ! q.# column
    integer, intent(in)     :: lsset   ! q.# set index
    real(wp), intent(out)   :: ash(:,0:)
    real(wp)                :: t(mcomx), tj, cup
    integer                 :: imc, lmval, ismval, j
    integer, pointer        :: lvm(:), ipm(:)


    lvm => lvec(vc, lsset)
    ipm => svec(vc, lsset)
    ash = 0.0_wp ! coupling coefficients
! set pointer to indicate if there are any nonzero coupling coefficients
    ipoint = 0
    t = 0.0_wp

! IBM essl routine
!    call dsctr (SIZE(indmi), surmi, indmi, t)

    do imc = 1, SIZE(indmi)
       t(indmi(imc)) = surmi(imc)
    end do
    do imc = 1, SIZE(indmj)
       j = indmj(imc)
       tj = t(j)
       if (tj == 0.0_wp) cycle
       cup = tj * surmj(imc)
       if (ABS(cup) <= eps) cycle
       ipoint = 1
       lmval = lvm(j)
       ismval = ipm(j)
       ash(ismval,lmval) = ash(ismval,lmval) + cup
    end do
  end subroutine cuple

end module cc_angular_integrals
