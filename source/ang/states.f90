module LS_states
! maintain list of quantum numbers defining LS coupled states
! Time-stamp: "2005-03-11 14:49:24 cjn"
  use io_units, only: fo
  implicit none

  integer, save                      :: m(3) = 0
  integer, save                      :: n(3) = 0
  integer, pointer, save             :: lval1(:,:)
  integer, pointer, save             :: ispval1(:,:)
  integer, pointer, save             :: lval2(:,:)
  integer, pointer, save             :: ispval2(:,:)
  integer, pointer, save             :: lval3(:,:)
  integer, pointer, save             :: ispval3(:,:)

  private
  public allocate_lsvals, extend_lsvals
  public reallocate_lsvals, del_lsvals
  public lvals, svals, lvec, svec

contains

  subroutine reallocate_lsvals (p, q, set)
 ! Extend dimensions of lsvalus to size p, q
    integer, intent(in)   :: p        ! new first dimension
    integer, intent(in)   :: q        ! new second dimension
    integer, intent(in)   :: set      ! lsvals set

    m(set) = p                       ! update size value
    select case (set)
    case (1)
       call realloc (lval1, p, q)
       call realloc (ispval1, p, q)
    case (2)
       call realloc (lval2, p, q)
       call realloc (ispval2, p, q)
    case (3)
       call realloc (lval3, p, q)
       call realloc (ispval3, p, q)
    end select
  end subroutine reallocate_lsvals

  subroutine realloc (x, m, n)
! extend pointer array
    integer, pointer     :: x(:,:)
    integer, intent(in)  :: n, m
    integer, pointer     :: temp(:,:)
    integer              :: i, j, status

    i = SIZE(x, DIM=1)
    j = SIZE(x, DIM=2)
    allocate (temp(m,n), stat=status)
    if (status /= 0) then
       write (fo,'(a,i4)') 'realloc: allocation error = ', status
       stop
    end if
    temp(1:i,1:j) = x
    deallocate (x, stat=status)
    if (status /= 0) then
       write (fo,'(a,i4)') 'realloc: deallocation error = ', status
       stop
    end if
    x => temp
  end subroutine realloc

  function lvals(q)
! provide pointer to L-table
    integer, pointer       :: lvals(:,:)
    integer, intent(in)    :: q
    select case (q)
    case (1)
       lvals => lval1
    case (2)
       lvals => lval2
    case (3)
       lvals => lval3
    end select
  end function lvals

  function svals(q)
! provide pointer to S-table
    integer, pointer       :: svals(:,:)
    integer, intent(in)    :: q
    select case (q)
    case (1)
       svals => ispval1
    case (2)
       svals => ispval2
    case (3)
       svals => ispval3
    end select
  end function svals

  function lvec(p, q)
! pointer to a vector of L-values
    integer, pointer    :: lvec(:)
    integer, intent(in) :: p
    integer, intent(in) :: q

    select case (q)
    case (1)
       lvec => lval1(:,p)
    case (2)
       lvec => lval2(:,p)
    case (3)
       lvec => lval3(:,p)
    end select
  end function lvec

  function svec(p, q)
! pointer to a vector of S-values
    integer, pointer    :: svec(:)
    integer, intent(in) :: p
    integer, intent(in) :: q

    select case (q)
    case (1)
       svec => ispval1(:,p)
    case (2)
       svec => ispval2(:,p)
    case (3)
       svec => ispval3(:,p)
    end select
  end function svec

  subroutine del_lsvals (set)
    integer, intent(in)   :: set
    integer               :: status
    select case (set)
    case (1)
       deallocate (lval1, ispval1, stat=status)
    case (2)
       deallocate (lval2, ispval2, stat=status)
    case (3)
       deallocate (lval3, ispval3, stat=status)
    end select
!   if (status /= 0) then
!      write (fo,'(a,2i6)') 'del_lsvals: deallocation error = ', &
!           status, set
!      stop
!   end if
  end subroutine del_lsvals

  subroutine allocate_lsvals (p, q, set)
    integer, intent(in)   :: p
    integer, intent(in)   :: q
    integer, intent(in)   :: set
    integer               :: status

    m(set) = p
    n(set) = q
    select case (set)
    case (1)
       allocate (lval1(p,q), ispval1(p,q), stat=status)
    case (2)
       allocate (lval2(p,q), ispval2(p,q), stat=status)
    case (3)
       allocate (lval3(p,q), ispval3(p,q), stat=status)
    end select
    if (status /= 0) then
       write (fo,'(a,i6)') 'allocate_lsvals: allocation error = ', &
            status
       stop
    end if
  end subroutine allocate_lsvals

  subroutine extend_lsvals (dim, q)
    integer, intent(in)   :: dim
    integer, intent(in)   :: q
    integer               :: mo, no, j, status, mt, nt
    integer, allocatable  :: tmp1(:,:), tmp2(:,:)
    integer, pointer      :: lval(:,:), sval(:,:)

    mo = m(q)
    no = n(q)
    write (fo,'(a,4i4)') 'extend_lsvals: q, dim, mo, no = ', q, dim, &
         mo, no
    allocate (tmp1(mo,no), tmp2(mo,no), stat=status)
    if (status /= 0) call prt_error(1, status)
    if (dim == 1) then
       mt = 2 * mo
       m(q) = mt
       nt = no
    else
       nt = 2 * no
       n(q) = nt
       mt = mo
    end if

    select case (q)
    case (1)
       tmp1 = lval1
       tmp2 = ispval1
       deallocate(lval1, ispval1, stat=status)
       if (status /= 0) call prt_error(2, status)
       allocate (lval1(mt,nt), ispval1(mt,nt), stat=status)
       if (status /= 0) call prt_error(1, status)
       do j = 1, no
          lval1(1:mo, j) = tmp1(:, j)
          ispval1(1:mo, j) = tmp2(:, j)
       end do
    case (2)
       tmp1 = lval2
       tmp2 = ispval2
       deallocate(lval2, ispval2, stat=status)
       if (status /= 0) call prt_error(2, status)
       allocate (lval2(mt,nt), ispval2(mt,nt), stat=status)
       if (status /= 0) call prt_error(1,status)
       do j = 1, no
          lval2(1:mo, j) = tmp1(:, j)
          ispval2(1:mo, j) = tmp2(:, j)
       end do
    case (3)
       tmp1 = lval3
       tmp2 = ispval3
       deallocate(lval3, ispval3, stat=status)
       if (status /= 0) call prt_error(2, status)
       allocate (lval3(mt,nt), ispval3(mt,nt), stat=status)
       if (status /= 0) call prt_error(1,status)
       do j = 1, no
          lval3(1:mo, j) = tmp1(:, j)
          ispval3(1:mo, j) = tmp2(:, j)
       end do
    end select
    deallocate(tmp1, tmp2, stat=status)
    if (status /= 0) call prt_error(2,status)
  end subroutine extend_lsvals

  subroutine prt_error (p, status)
    integer, intent(in)    :: p
    integer, intent(in)    :: status    ! error code
    if (p == 1) then
       write (fo,'(a,i6)') 'extend_lsvals: allocation error = ', &
            status
    else
       write (fo,'(a,i6)') 'extend_lsvals: deallocation error = ', &
            status
    end if
    stop
  end subroutine prt_error
end module LS_states
