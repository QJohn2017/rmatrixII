module precisn
! define precision-related paramters
! Time-stamp: "98/03/25 08:30:46 cjn"
  implicit none
  public
  integer, parameter    :: ibyte = 4
  integer, parameter    :: rbyte = 8
  integer, parameter    :: zbyte = 16
  integer, parameter    :: sp=selected_real_kind(7)
  integer, parameter    :: wp=selected_real_kind(15)
  integer, parameter    :: ep1=selected_real_kind(21)
  integer, parameter    :: ep = MAX(ep1, wp)
!  real(wp), parameter   :: acc8 = epsilon(1.0_wp)
!  real(wp), parameter   :: acc16 = epsilon(1.0_ep)
!  real(wp), parameter   :: fpmax = huge(1.0_wp) * acc8
!  real(wp), parameter   :: fpmin = tiny(1.0_wp) / acc8
!  real(wp), save        :: fplmax
!  real(wp), save        :: fplmin
  real(wp), parameter   :: acc8 = 2.0e-16_wp
  real(wp), parameter   :: acc16 = 3.0e-33_ep
  real(wp), parameter   :: fpmax = 1.0e60_wp
  real(wp), parameter   :: fpmin = 1.0e-60_wp
  real(wp), parameter   :: fplmax = 140.0_wp
  real(wp), parameter   :: fplmin = -140.0_wp
end module precisn
