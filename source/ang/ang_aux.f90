module ang_aux
! auxilliary routines used to form angular coefficients
! Time-stamp: "01/11/23 11:51:38 cjn"
  use precisn, only: wp
  implicit none

  real(wp), parameter     :: eps = 1.0e-5_wp

  private
  public delp, cupl2

contains
  subroutine delp (nocci, nocori, nelci, ki, irho, noccj, &
       nocorj, nelcj, kj, jrho, isign)
! calculates (-1)**delta p
    integer, intent(in)  :: nocci, noccj
    integer, intent(in)  :: ki, kj
    integer, intent(in)  :: irho, jrho
    integer, intent(in)  :: nocori(:)
    integer, intent(in)  :: nelci(:), nelcj(:)
    integer, intent(in)  :: nocorj(:)
    integer, intent(out) :: isign
    integer              :: idelp, ik, jk, ishel, jshel

    idelp = 0
    if (irho < jrho) then
       do ik = ki+1, nocci
          ishel = nocori(ik)
          if (ishel > jrho) then
             isign = 1
             if (MOD(idelp,2) == 1) isign = -1
             return
          end if
          idelp = idelp + nelci(ik)
       end do
    else
       do jk = kj+1, noccj
          jshel = nocorj(jk)
          if (jshel > irho) then
             isign = 1
             if (MOD(idelp,2) == 1) isign = -1
             return
          end if
          idelp = idelp + nelcj(jk)
       end do
    end if
    isign = 1
    if (MOD(idelp,2) == 1) isign = -1
  end subroutine delp

  subroutine cupl2 (indmi, surmi, indmj, surmj, mcomx, lvm, ipm, ash,&
       ipoint)
! sum over the intermediate l.
    integer, intent(in)     :: mcomx
    integer, intent(in)     :: indmi(:)
    integer, intent(in)     :: indmj(:)
    integer, intent(out)    :: ipoint
    real(wp), intent(in)    :: surmi(:)
    real(wp), intent(in)    :: surmj(:)
    integer, intent(in)     :: lvm(:)
    integer, intent(in)     :: ipm(:)
    real(wp), intent(out)   :: ash(:,0:)
    real(wp)                :: t(mcomx)
    integer                 :: imc, im, jm, j

    ash = 0.0_wp ! coupling coefficients
    t = 0.0_wp

! use IBM essl sparse routines
!    call dsctr(SIZE(indmi), surmi, indmi, t)
    do imc = 1, SIZE(indmi)
       t(indmi(imc)) = surmi(imc)
    end do

    do imc = 1, SIZE(indmj)
       j = indmj(imc)
       if (t(j) /= 0) then
          im = ipm(j)
          jm = lvm(j)
          ash(im,jm) = ash(im,jm) + t(j) * surmj(imc)
       end if
    end do

    ipoint = 0   ! nonzero coefficient pointer
    do imc = 1, SIZE(indmj)
       j = indmj(imc)
       if (t(j) /= 0) then
          im = ipm(j)
          jm = lvm(j)
          if (ABS(ash(im,jm)) > eps) then
             ipoint = 1
             exit
          end if
       end if
    end do

!    jm_l: do jm = 0, UBOUND(ash, dim=2)
!       do im = 1, SIZE(ash, dim=1)
!          if (ABS(ash(im,jm)) > eps) then
!             ipoint = 1
!             exit jm_l
!          end if
!       end do
!    end do jm_l
  end subroutine cupl2

end module ang_aux
