module bb_angular_integrals
! Time-stamp: "2005-03-11 16:18:37 cjn"
  use precisn, only: wp
  use io_units, only: fo
  implicit none

  real(wp), save             :: eps = 1.0e-5_wp
  integer, save              :: necfgs    ! # unique ccfgs
  integer, allocatable, save :: necfg(:)  ! ccfg indicies
  integer, allocatable, save :: lecfg(:)  ! ccfg final indicies
  integer, allocatable, save :: nelems(:) ! nelem values

  private
  public angbb, wr_abbi

contains

  subroutine angbb (nelc, npset, lpset, ispset, ippset, neqsen, nshqn, &
       npocsh, npocob, npelsh, nnpfig, npnpq, lssetn, ncoumx,  &
       noccsh, nocorb, nelcsh, nmefig, nnq, lssetm, mcoumx, mmq, id1,  &
       id2, typ)
! calculates the angular integral (weighting factor) for each pair of
! bound bound configurations
    use dim, only: ldim0, ldim1
    use filehand, only: open, readix, ireada, ireadb, newfil, catlog, &
         lookup, iwrita, endw, writix, iwritb, close
    use multiplet_energies, only: energy, pdi2, ddi2, ddi4, doff32, &
         doff34, doff42, doff44, doff52, doff54
    use bb_aux, only: bbcup, cup1, wrbb, jbufiv, jbuffv, filea, fileb,&
         tgt
    use startup, only: multco, ljcomp, fsh
    use ang_aux, only: delp
    use debug, only: bug7, bug8, bug9
    use surfacing_tables, only: surf_vec, surf_tab
    use sym_sets, only: get_ncset, get_ncfg
    use vpack, only: pck, pck2, pckl2
!    use oo_timer, only: tmr, new, delete, report_time
!    use omp_lib, only: omp_get_num_threads, omp_get_thread_num
    integer, intent(in)  :: nelc        ! # electrons
    integer, intent(in)  :: npset       ! # SLpi cfg sets
    integer, intent(in)  :: lpset(:)    ! cfg set L values
    integer, intent(in)  :: ispset(:)   ! cfg set S values
    integer, intent(in)  :: ippset(:)   ! cfg set Pi values
    integer, intent(in)  :: neqsen(:,:), nshqn(:,:)
    integer, intent(in)  :: npocsh(:)
    integer, intent(in)  :: npocob(:,:)
    integer, intent(in)  :: npelsh(:,:), nnpfig(:,:)
    integer, intent(in)  :: npnpq(:)
    integer, intent(in)  :: lssetn
    integer, intent(in)  :: ncoumx(:)
    integer, intent(in)  :: noccsh(:), nocorb(:,:)
    integer, intent(in)  :: nelcsh(:,:), nmefig(:,:)
    integer, intent(in)  :: nnq(:)
    integer, intent(in)  :: lssetm
    integer, intent(in)  :: mcoumx(:)
    integer, intent(in)  :: mmq(:)
    integer, intent(in)  :: id1    ! first surfacing table
    integer, intent(in)  :: id2    ! second surfacing table
    integer, intent(in)  :: typ

    integer              :: irsd(fsh*fsh), irse(fsh*fsh)
    real(wp)             :: bbd(ldim0,fsh*fsh), bbe(ldim0,fsh*fsh)
    integer              :: k1k2d(fsh*fsh), k1k2e(fsh*fsh)
    integer              :: lamdst(fsh*fsh), nlamd(fsh*fsh)
    real(wp)             :: bbdir(0:ldim1)
    integer              :: lamest(fsh*fsh), nlame(fsh*fsh)
    real(wp)             :: bbex(0:ldim1), bb1(fsh)
    integer              :: isre(fsh*fsh), irs1(fsh)
    integer              :: ivbbi(npset), ivbbr(npset)
    integer              :: nqni(fsh),nqnj(fsh)
    character(len=7)     :: stats
    integer :: lrgl, lrgpi, lrgs, isnp
    integer :: nonz, ncfgs, nelem, icsnp
    integer :: inpcou, inpc, inpcq, i, kk, k1, k2, isig, irho, ndcont
    integer :: necont, n1sh, nshqni, inc, incq, k1i, ncomx
    integer :: neli1, n2sh, k2top, k2i
    integer :: irs, k1k2, neli, neli2, lrhoi, imc, imcq, idirs
    integer :: isr, idebug, lamdlo, id, ie, lamelo
    integer :: ncont1, jnpc, nqnii, nqnjj
    integer :: nqsum, jnc, jsig, lsigj
    integer :: ndummy, lsigi, lameup, lamdup, ilamd, ilame, ecfgs
    integer :: jcsnp, jnpcou, jnpcq, nshqnj, ilamda, status
    integer :: nocc1i, nocc1j, k1ia, k1ja, idelps, k2ia, ilamea
    integer :: npoci, npocj, k1ib, noci, k2ib, k2topi, k1jb, k1j, nocj
    integer :: k2topj, k2jb, jmc, jrho, idelp1, lrhoj, idelp2, jncq, iiq
    integer :: ilamdb, ilameb, nrecvi, nrecvr, ibufvi, ibufvr, iq, ncsf
    real(wp) :: facex, facdir, facij1, cup, facij2
    real(wp), pointer :: surip(:), suri(:), surjp(:), surj(:)
    integer, pointer  :: indxip(:), indxi(:), indxjp(:), indxj(:)
    integer, pointer  :: mincom(:), maxcom(:), mincomp(:), maxcomp(:)
    integer, pointer  :: npcset(:), ncfga(:,:), ncfgb(:,:)
    logical           :: real_typ
    integer :: c0, c1, cr
    real(wp) :: t0, t1
!    type(tmr) :: dtime, otime
    integer   :: nthr, nthd

    call cpu_time(t0)
    call system_clock (count=c0)

! id2 becomes 4 if csf index out of range
    if (bug7 == 3) then
       write (fo,'(5x,a,/)') 'subroutine angbb'
       write (fo,'(a)') '    isnp      ivbbi     ivbbr'
       write (fo,'(a)') '    irsd      bbd           irse         bbe'
    end if

    npcset => get_ncset (typ)
    call get_ncfg (typ, ncfga, ncfgb)

! initialise writing to all output files
    stats = 'new'
    real_typ = .false.
    call open (jbufiv, filea, stats, real_typ)
    real_typ = .true.
    call open (jbuffv, fileb, stats, real_typ)
    call energy
    call newfil (jbufiv, ndummy)
    call newfil (jbuffv, ndummy)

! loop over sets of configurations with the same lrgs, lrgl, lrgpi
    symmetries: do isnp = 1, npset
       lrgl = lpset(isnp)     ! L
       lrgpi = ippset(isnp)   ! pi
       lrgs = ispset(isnp)    ! S
       if (bug7 == 3) write (fo,'(a,3i3)') 'Symmetry: s l pi = ', &
            lrgs, lrgl, lrgpi
! store the starting position of the files in the indexing arrays.
       call catlog (jbufiv, ivbbi(isnp)) ! next avail posn
       call catlog (jbuffv, ivbbr(isnp))

       nonz = 0
       ncfgs = npcset(isnp)  ! # configurations
! Define loop limits for non-equivalent ccfgs
       call equiv_ccfgs (isnp, ncfgs)

!********** diagonal elements *********
! 1-electron contribution from each occupied shell
!       call new(dtime)
!$OMP PARALLEL &
!$OMP& DEFAULT (none) &
!$OMP& PRIVATE(iq, icsnp, nelem, inpcou, inpc, inpcq, ndcont, necont,  &
!$OMP&         n1sh, nshqni, k2, nqni, k1i, k1, inc, incq, ncomx, isig,&
!$OMP&         neli1, irs1, bb1, lsigi, indxip, surip, n2sh, k2top,    &
!$OMP&         k2i, irho, irs, k1k2, irsd, k1k2d, neli, bbd, nlamd,    &
!$OMP&         lamdst, neli2, lrhoi, imc, imcq, idirs, facex, lameup,  &
!$OMP&         facdir, isr, mincom, maxcom, indxi, suri, idebug,       &
!$OMP&         lamdlo, lamelo, bbdir, bbex, id, ilamd, irse, isre, ie, &
!$OMP&         ilame, bbe, nlame, lamest, i, kk, ecfgs, k1k2e, ncont1, &
!$OMP&         lamdup, nthr, nthd)& 
!$OMP& SHARED(ncfgs, necfg, nelems, ncfga, ncfgb, npnpq, isnp, nshqn, &
!$OMP&        multco, nnpfig, ncoumx, npocob, npelsh, nelc, id1,       &
!$OMP&        noccsh, nocorb, pdi2, ddi2, ddi4, nmefig, ljcomp, lrgl,  &
!$OMP&        nelcsh, mmq, lecfg, nnq, lssetm, lrgs, lssetn, npocsh,   &
!$OMP&        mcoumx, id2, nonz, necfgs)

!       nthd = omp_get_num_threads()
!       nthr = omp_get_thread_num()
!       call bindthreadstoprocessors(nthr)
!       write (fo,'(2(a,i6),a)') 'thread ', nthr, ' of ', nthd, ' bound'
!$OMP DO                          &
!$OMP& SCHEDULE(DYNAMIC,1)
       ccfgs: do  iq = 1, necfgs  ! loop on unique ccfs
          icsnp = necfg(iq)
          ecfgs = lecfg(iq)
          nelem = nelems(icsnp)
          inpcou = ncfgb(icsnp,isnp)
          inpc = ncfga(icsnp,isnp)
          inpcq = npnpq(inpc)

          ndcont = 0
          necont = 0
          n1sh = npocsh(inpc)

! unpack the shell qn ready for special treatment of the pairs of
! electrons from the same shell
          nshqni = nshqn(inpcou,inpcq)
          do k2 = 1, n1sh-1
             nqni(k2) = MOD(nshqni, multco(k2+1)) / multco(k2)
          end do
          nqni(n1sh) = nshqni / multco(n1sh)

          k1i_loop: do k1i = n1sh, 1, -1 ! loop down through shells
             k1 = n1sh - k1i
             inc = nnpfig(k1i,inpc)
             incq = nnq(inc)
             ncomx = ncoumx(incq)        ! # couplings
             isig = npocob(k1i,inpc)     ! orb seq
             neli1 = npelsh(k1i,inpc)    ! occupancy
             irs1(k1i) = pck2(isig, isig)
             bb1(k1i) = REAL(neli1,wp)
             if (nelc <= 1) cycle k1i_loop ! one-electron systems
             lsigi = ljcomp(isig)                ! L-value
             call surf_vec (id1, inpcou, k1i, inpcq, indxip, surip)
             n2sh = noccsh(inc)
             k2top = n2sh - k1

             k2i_loop: do k2i = k2top, 1, -1  ! loop down through shells
                irho = nocorb(k2i,inc)
                if (irho == isig .and. lsigi < 3) then
! use tables of energy terms
                   irs = pck(isig, isig, isig, isig)
                   k1k2 = 100 * k1i + k2i

                   ndcont = ndcont + 1
                   irsd(ndcont) = irs
                   k1k2d(ndcont) = k1k2
                   neli = neli1 - 1
                   bbd(1,ndcont) = REAL((neli1*neli)/2,wp)
                   if (lsigi == 0) then
                      nlamd(ndcont) = 1
                      lamdst(ndcont) = 1
                   else if (lsigi == 1) then
                      nlamd(ndcont) = 2
                      lamdst(ndcont) = 2
                      bbd(2,ndcont) = pdi2(nqni(k1i),neli)
                   else
                      nlamd(ndcont) = 3
                      lamdst(ndcont) = 3
                      bbd(2,ndcont) = ddi2(nqni(k1i),neli)
                      bbd(3,ndcont) = ddi4(nqni(k1i),neli)
                   end if
                   cycle k2i_loop
                end if
                neli2 = nelcsh(k2i,inc)
                lrhoi = ljcomp(irho)
                imc = nmefig(k2i,inc)
                imcq = mmq(imc)
                idirs = 1
                if (MOD((lrgl+lrhoi+lsigi),2) == 1) idirs = -1
                facex = neli1 * neli2
                lameup = 0
                if (irho == isig) then ! both electrons from f-shell
                   lameup = -1
                   facex = 0.5_wp * facex
                end if
                facdir = facex * idirs
                facex = -facex
! pack rhoi, rhoj, sigi, sigj into irs
                irs = pck(isig, isig, irho, irho)
                isr = pck(irho, isig, isig, irho)
                k1k2 = 100 * k1i + k2i

                   if (nelc > 2) call surf_tab (id2, k2i, incq, mincom, &
                        maxcom, indxi, suri)
                   idebug = 0
                   call bbcup (nelc, indxip, surip, indxip, surip, indxi,&
                        suri, indxi, suri, incq, lssetn, incq, lssetn,   &
                        mincom, maxcom, mincom, maxcom, imcq, lssetm,    &
                        lrhoi, lrhoi, lsigi, lsigi, lrgl, lrgs,          &
                        mcoumx(imcq), lamdlo, lamdup, lamelo, lameup,    &
                        bbdir, bbex, idebug)

                if (lamdlo /= 999) then ! there are direct terms
                   ndcont = ndcont + 1
                   irsd(ndcont) = irs
                   k1k2d(ndcont) = k1k2
                   id = 0
                   do ilamd = lamdlo, lamdup, 2
                      id = id + 1
                      bbd(id,ndcont) = bbdir(ilamd) * facdir
                   end do
                   nlamd(ndcont) = id
                   lamdst(ndcont) = pckl2(id, lamdlo)
                end if

                if (lamelo /= 999) then  ! there are exchange terms
                   necont = necont + 1
                   irse(necont) = irs
                   isre(necont) = isr
                   k1k2e(necont) = k1k2
                   ie = 0
                   do ilame = lamelo, lameup, 2
                      ie = ie + 1
                      bbe(ie,necont) = bbex(ilame) * facex
                   end do
                   nlame(necont) = ie
                   lamest(necont) = pckl2(ie, lamelo)
                end if

             end do k2i_loop
          end do k1i_loop

! all shells completed.  write the files
!$OMP CRITICAL (WRB)
          ncont1 = n1sh
          call wrbb (nelem, nonz, ndcont, nlamd, lamdst, irsd, bbd,&
               necont, nlame, lamest, irse, bbe, isre, ncont1, &
               irs1, bb1)
!$OMP END CRITICAL (WRB)

! Now deal with equivalent coupled configs:
          do icsnp = necfg(iq)+1, ecfgs   ! ecfgs=0 if there are none
             nelem = nelems(icsnp)
             inpcou = ncfgb(icsnp,isnp)
             inpc = ncfga(icsnp,isnp)
             inpcq = npnpq(inpc)
             do i = 1, ndcont    ! direct labels
                kk = k1k2d(i)
                k1 = kk/100
                k2 = MOD(kk, 100)
                isig = npocob(k1,inpc)
                irho = npocob(k2,inpc)
                irsd(i) = pck(isig, isig, irho, irho)
             end do
             do i = 1, necont    ! exchange labels
                kk = k1k2e(i)
                k1 = kk / 100
                k2 = MOD(kk, 100)
                isig = npocob(k1,inpc)
                irho = npocob(k2,inpc)
                irse(i) = pck(isig, isig, irho, irho)
                isre(i) = pck(irho, isig, isig, irho)
             end do
             do i = 1, ncont1       ! one-electron labels
                isig = npocob(i,inpc)
                irs1(i) = pck2(isig, isig)
             end do
!$OMP CRITICAL (WRB)
             call wrbb (-nelem, nonz, ndcont, nlamd, lamdst, irsd,&
                  bbd, necont, nlame, lamest, irse, bbe, isre, &
                  ncont1, irs1, bb1)
!$OMP END CRITICAL (WRB)
          end do
       end do ccfgs
!$OMP END DO
!$OMP END PARALLEL
!       call delete(dtime)
!       call report_time ('angbb diagonal elements', dtime)


! ********** off-diagonal elements **********
       ncsf = npcset(isnp)
!       call new (otime)
!$OMP PARALLEL &
!$OMP& DEFAULT (none) &
!$OMP& PRIVATE(nelem, icsnp, jcsnp, inpcou, inpc, inpcq, jnpcou, jnpc, &
!$OMP&         jnpcq, ndcont, necont, ncont1, n1sh, nshqni, k2, nqni,  &
!$OMP&         nshqnj, nqnj, k1i, k1, inc, incq, ncomx, isig, neli1,   &
!$OMP&         lsigi, indxip, surip, indxjp, surjp, n2sh, k2top, k2i,  &
!$OMP&         irho, lameup, facex, nqnii, nqnjj, nqsum, nlamd, lamdst,&
!$OMP&         irs, irsd, bbd, imc, imcq, neli2, lrhoi, idirs, facdir, &
!$OMP&         isr, indxi, suri, mincom, maxcom, lamdlo, lamdup, nthr, &
!$OMP&         lamelo, bbdir, bbex, ilamd, id, irse, isre, ie, ilame,  &
!$OMP&         nlame, lamest, bbe, k1ia, nocc1j, k1ja, jnc, jsig, nthd,&
!$OMP&         lsigj, bb1, irs1, facij1, idelps, k2ia, cup, npoci,     &
!$OMP&         npocj, k1ib, noci, k2topi, k2ib, k1jb, k2topj, k1j,     &
!$OMP&         nocj, jmc, k2jb, idelp1, idelp2, indxj, surj, mincomp,  &
!$OMP&         maxcomp, idebug, jncq, jrho, facij2, lrhoj, nocc1i)     &
!$OMP& SHARED(ncsf, npcset, isnp, ncfga, ncfgb, npnpq, npocsh, nshqn,  &
!$OMP&        multco, nnpfig, nnq, ncoumx, npocob, npelsh, ljcomp, id1,&
!$OMP&        noccsh, nocorb, neqsen, doff32, doff34, doff42, doff52,  &
!$OMP&        doff54, lrgl, id2, nonz, nelc, eps, lrgs, nmefig, nelcsh,&
!$OMP&        lssetn, lssetm, mmq, doff44, mcoumx)

!       nthd = omp_get_num_threads()
!       nthr = omp_get_thread_num()
!       call bindthreadstoprocessors(nthr)
!       write (fo,'(2(a,i6),a)') '*thread ', nthr, ' of ', nthd, ' bound'
!$OMP DO                  &
!$OMP& SCHEDULE(STATIC)
       i_csfs: do icsnp = 1, npcset(isnp)
          inpcou = ncfgb(icsnp,isnp)
          inpc = ncfga(icsnp,isnp)
          inpcq = npnpq(inpc)
          j_csfs: do jcsnp = icsnp+1, npcset(isnp)
             nelem = triangular_index (ncsf, jcsnp, icsnp)
             jnpcou = ncfgb(jcsnp,isnp)
             jnpc = ncfga(jcsnp,isnp)
             jnpcq = npnpq(jnpc)
             ndcont = 0 ! # shell pairs contributing to direct integrals
             necont = 0 ! # shell pairs contributing to exchangs ints
             ncont1 = 0 ! # 1-electron sshell pairs off-diagonal (0/1)

! If cfgs identical, sum over all pairs of shells in N+1-electron cfg
! and in the N-electron since the couplings are different, ncont1=0.
             if (inpc  == jnpc) then ! Identical cfgs

! Unpack shell quantum #s; use energy expressions for pairs of
! electrons from the same shell
                n1sh = npocsh(inpc)
                nshqni = nshqn(inpcou,inpcq)
                do k2 = 1, n1sh-1
                   nqni(k2) = MOD(nshqni, multco(k2+1)) / multco(k2)
                end do
                nqni(n1sh) = nshqni / multco(n1sh)

                if (inpcou /= jnpcou) then  ! couplings differ
                   nshqnj = nshqn(jnpcou,inpcq)
                   do k2 = 1, n1sh-1
                      nqnj(k2) = MOD(nshqnj,multco(k2+1)) / multco(k2)
                   end do
                   nqnj(n1sh) = nshqnj / multco(n1sh)
                end if

                k1i1_loop: do k1i = n1sh, 1, -1
                   k1 = n1sh - k1i
                   inc = nnpfig(k1i,inpc)
                   incq = nnq(inc)
                   ncomx = ncoumx(incq)
                   isig = npocob(k1i,inpc)
                   neli1 = npelsh(k1i,inpc)
                   lsigi = ljcomp(isig)
                   call surf_vec (id1, inpcou, k1i, inpcq, indxip, &
                        surip)
                   call surf_vec (id1, jnpcou, k1i, inpcq, indxjp, &
                        surjp)
                   n2sh = noccsh(inc)
                   k2top = n2sh - k1
                   k2i1_loop: do k2i = k2top, 1, -1
                      irho = nocorb(k2i,inc)
! if 2 electrons from same shell check if there can be a nonzero term
                      lameup = 0
                      facex = 1.0_wp
                      if (irho == isig ) then
                         nqnii = nqni(k1i)
                         nqnjj = nqnj(k1i)
                         nqsum = nqnii+nqnjj
                         if (lsigi == 2 .and. neli1 > 2 .and.         &
                              neli1 < 8 .and. neqsen(inpcou,inpcq) == &
                              neqsen(jnpcou,inpcq) .and. nqnii /=     &
                              nqnjj) then

                            if (neli1 == 3 .or. neli1 == 7) then
                               ndcont = ndcont + 1
                               nlamd(ndcont) = 2
                               lamdst(ndcont) = pckl2(2, 2)
                               irs = pck(isig, isig, isig, isig)
                               irsd(ndcont) = irs
                               bbd(1,ndcont) = doff32
                               bbd(2,ndcont) = doff34
                            else if (neli1 == 4 .or. neli1 == 6) then
                               ndcont = ndcont + 1
                               nlamd(ndcont) = 2
                               lamdst(ndcont) = pckl2(2, 2)
                               irs = pck(isig, isig, isig, isig)
                               irsd(ndcont) = irs
                               bbd(1,ndcont) = doff42(nqnii)
                               bbd(2,ndcont) = doff44(nqnii)
                            else if (neli1 == 5 .and. nqsum == 11) then
                               ndcont = ndcont + 1
                               nlamd(ndcont) = 2
                               lamdst(ndcont) = pckl2(2, 2)
                               irs = pck(isig, isig, isig, isig)
                               irsd(ndcont) = irs
                               bbd(1,ndcont) = doff52
                               bbd(2,ndcont) = doff54
                            end if
                            
                         end if
                         cycle k2i1_loop
                      end if

                      imc = nmefig(k2i,inc)
                      imcq = mmq(imc)
                      neli2 = nelcsh(k2i,inc)
                      lrhoi = ljcomp(irho)
                      idirs = 1
                      if (MOD((lrgl+lrhoi+lsigi),2) == 1) idirs = -1
                      facex = neli1 * neli2 * facex
                      facdir = facex * idirs
                      facex = - facex
                      irs = pck(isig, isig, irho, irho)
                      isr = pck(irho, isig, isig, irho)
                      if (nelc > 2) call surf_tab (id2, k2i, incq, &
                           mincom, maxcom, indxi, suri)
                      if (inpc == 999) then
                         idebug = 0
                      else
                         idebug = 0
                      end if
                      call bbcup (nelc, indxip, surip, indxjp, surjp, &
                           indxi, suri, indxi, suri, incq, lssetn,    &
                           incq, lssetn, mincom, maxcom, mincom,      &
                           maxcom, imcq, lssetm, lrhoi, lrhoi, lsigi, &
                           lsigi, lrgl, lrgs, mcoumx(imcq), lamdlo,   &
                           lamdup, lamelo, lameup, bbdir, bbex, idebug)

                      if (lamdlo /= 999) then    ! direct terms exist
                         ndcont = ndcont + 1
                         irsd(ndcont) = irs
                         id = 0
                         do ilamd = lamdlo, lamdup, 2
                            id = id + 1
                            bbd(id,ndcont) = bbdir(ilamd)*facdir
                         end do
                         nlamd(ndcont) = id
                         lamdst(ndcont) = pckl2(id, lamdlo)
                      end if

                      if (lamelo /= 999) then ! exchange terms exist
                         necont = necont + 1
                         irse(necont) = irs
                         isre(necont) = isr
                         ie = 0
                         do ilame = lamelo, lameup, 2
                            ie = ie+1
                            bbe(ie,necont) = bbex(ilame) * facex
                         end do
                         nlame(necont) = ie
                         lamest(necont) = pckl2(ie, lamelo)
                      end if

                   end do k2i1_loop
                end do k1i1_loop

!$OMP CRITICAL (WRB)
! all shells completed.   write the files
                call wrbb (nelem, nonz, ndcont, nlamd, lamdst, irsd,&
                     bbd, necont, nlame, lamest, irse, bbe, isre,   &
                     ncont1, irs1, bb1)
!$OMP END CRITICAL (WRB)
                cycle j_csfs
             end if ! end of treatment of n+1 configs the same
             
! N+1-configurations are not equal
! surface 1 electron from each shell in turn on each side to look for
! equal N-electron configurations
             nocc1i = npocsh(inpc)
             k1ia_loop: do k1ia = nocc1i, 1, -1
                inc = nnpfig(k1ia,inpc)
                nocc1j = npocsh(jnpc)
                k1ja_loop: do k1ja = nocc1j, 1, -1
                   jnc = nnpfig(k1ja,jnpc)

! test whether n electron configurations the same.  if so
! sigi, sigj defined by k1ia,k1ja.  loop over all shells in n config.
! a 1-electron term is possible if lsigi=lsigj
                   if (inc == jnc) then
                      isig = npocob(k1ia,inpc)
                      lsigi = ljcomp(isig)
                      jsig = npocob(k1ja,jnpc)
                      lsigj = ljcomp(jsig)

! treat H-like case separately
                      if (nelc == 1) then
!                      if (lsigi /= lsigj) then
!                         write (fo,'(a)') 'angbb: H-case error'
!                         stop
!                      end if
                         ncont1 = 1
                         bb1(1) = 1.0_wp
                         irs1(1) = pck2(jsig, isig)
!$OMP CRITICAL (WRB)
                         call wrbb (nelem, nonz, ndcont, nlamd, lamdst,&
                              irsd, bbd, necont, nlame, lamest, irse,  &
                              bbe, isre, ncont1, irs1, bb1)
!$OMP END CRITICAL (WRB)
                         cycle j_csfs
                      end if
                      incq = nnq(inc)
                      ncomx = ncoumx(incq)
                      call surf_vec (id1, inpcou, k1ia, inpcq, indxip, &
                           surip)
                      call surf_vec (id1, jnpcou, k1ja, jnpcq, indxjp, &
                           surjp)
                      facij1 = SQRT(REAL(npelsh(k1ia,inpc) * &
                           npelsh(k1ja,jnpc),wp))
                      call delp (nocc1i, npocob(:,inpc),         &
                           npelsh(:,inpc), k1ia, isig, nocc1j,   &
                           npocob(:,jnpc), npelsh(:,jnpc), k1ja, &
                           jsig, idelps)

                      n2sh = noccsh(inc)

                      k2ia_loop: do k2ia = n2sh, 1, -1
                         imc = nmefig(k2ia,inc)
                         imcq = mmq(imc)
                         irho = nocorb(k2ia,inc)
                         neli2 = nelcsh(k2ia,inc)
                         lrhoi = ljcomp(irho)
                         idirs = 1
                         if (MOD((lrgl+lrhoi+lsigj),2) == 1) idirs = -1
                         facex = facij1 * REAL(idelps * neli2,wp)
                         lameup = 0
                         if (irho == isig .or. irho == jsig) lameup = -1
                         facdir = facex * REAL(idirs,wp)
                         facex = - facex
                         irs = pck(jsig, isig, irho, irho)
                         isr = pck(irho, isig, jsig, irho)
                         if (nelc > 2) call surf_tab (id2, k2ia, incq, &
                              mincom, maxcom, indxi, suri)
                         idebug = 0
                         call bbcup (nelc, indxip, surip, indxjp,     &
                              surjp, indxi, suri, indxi, suri,        &
                              incq, lssetn, incq, lssetn, mincom,     &
                              maxcom, mincom, maxcom, imcq, lssetm,   &
                              lrhoi, lrhoi, lsigi, lsigj, lrgl, lrgs, &
                              mcoumx(imcq), lamdlo, lamdup, lamelo,   &
                              lameup, bbdir, bbex, idebug)

                         if (lamdlo /= 999) then ! direct terms
                            ndcont = ndcont + 1
                            irsd(ndcont) = irs
                            id = 0
                            do ilamda = lamdlo, lamdup, 2
                               id = id + 1
                               bbd(id,ndcont) = bbdir(ilamda) * facdir
                            end do
                            nlamd(ndcont) = id
                            lamdst(ndcont) = pckl2(id, lamdlo)
                         end if

                         if (lamelo /= 999) then ! exchange terms
                            necont = necont + 1
                            irse(necont) = irs
                            isre(necont) = isr
                            ie = 0
                            do ilamea = lamelo, lameup, 2
                               ie = ie + 1
                               bbe(ie,necont) = bbex(ilamea) * facex
                            end do
                            nlame(necont) = ie
                            lamest(necont) = pckl2(ie, lamelo)
                         end if
                      end do k2ia_loop

! all shells completed.  calculate 1-electron part
                      if (lsigi == lsigj) then
                         call cup1 (indxip, surip, indxjp, surjp,   &
                              ncomx, cup)
                         if (ABS(cup) >= eps) then
                            ncont1 = 1
                            bb1(1) = cup * facij1 * idelps
                            irs1(1) = pck2(jsig, isig)
                         end if
                      end if
!$OMP CRITICAL (WRB)
                      call wrbb (nelem, nonz, ndcont, nlamd, lamdst,&
                           irsd, bbd, necont, nlame, lamest, irse,  &
                           bbe, isre, ncont1, irs1, bb1)
!$OMP END CRITICAL (WRB)
                      cycle j_csfs
                   end if
                end do k1ja_loop
             end do k1ia_loop
! end of search for identical N-electron configurations.

! Search for two identical N-1 configurations (no 1-electron part)
             npoci = npocsh(inpc)
             npocj = npocsh(jnpc)
             k1ib_loop: do k1ib = npoci, 1, -1
                inc = nnpfig(k1ib,inpc)
                noci = noccsh(inc)
                k1i = npoci - k1ib
                k2topi = noci - k1i
                k2ib_loop: do k2ib = k2topi, 1, -1
                   imc = nmefig(k2ib,inc)
                   if (imc <= 0) cycle
                   l1jb_loop: do k1jb = npocj, 1, -1
                      jnc = nnpfig(k1jb,jnpc)
                      nocj = noccsh(jnc)
                      k1j = npocj - k1jb
                      k2topj = nocj - k1j
                      k2jb_loop: do k2jb = k2topj, 1, -1
                         jmc = nmefig(k2jb,jnc)
                         if (jmc <= 0) cycle

! If N-1 cfgs are identical, irho, jrho, isig, jsig are defined
                         if (imc == jmc) then
                            isig = npocob(k1ib,inpc)
                            lsigi = ljcomp(isig)
                            jsig = npocob(k1jb,jnpc)
                            lsigj = ljcomp(jsig)
                            facij1 = SQRT(REAL(npelsh(k1ib,inpc) * &
                                 npelsh(k1jb,jnpc),wp))
                            call delp (npoci, npocob(:,inpc),      &
                                 npelsh(:,inpc), k1ib, isig,       &
                                 npocj, npocob(:,jnpc),            &
                                 npelsh(:,jnpc), k1jb, jsig, idelp1)
                            irho = nocorb(k2ib,inc)
                            lrhoi = ljcomp(irho)
                            jrho = nocorb(k2jb,jnc)
                            lrhoj = ljcomp(jrho)
                            facij2 = SQRT(REAL(nelcsh(k2ib,inc) * &
                                 nelcsh(k2jb,jnc),wp))
                            call delp (noci, nocorb(:,inc),       &
                                 nelcsh(:,inc), k2ib, irho, nocj, &
                                 nocorb(:,jnc), nelcsh(:,jnc),    &
                                 k2jb, jrho, idelp2)
                            idirs = 1
                            if (MOD((lrgl+lrhoi+lsigj),2) == 1) &
                                 idirs = -1
                            facex = facij1 * facij2 * REAL(idelp1 * &
                                 idelp2,wp)
                            lameup = 0
                            if (irho == isig .and. jrho == jsig) then
                               lameup = -1
                               facex = 0.5_wp * facex
                            else if (irho == isig .or. jrho == jsig) &
                                 then
                               lameup = -1
                            end if
                            facdir = facex * REAL(idirs,wp)
                            facex = -facex
                            irs = pck(jsig, isig, jrho, irho)
                            isr = pck(jrho, isig, jsig, irho)

                            incq = nnq(inc)
                            jncq = nnq(jnc)
                            imcq = mmq(imc)
                            call surf_vec (id1, inpcou, k1ib, inpcq, &
                                 indxip, surip)
                            call surf_vec (id1, jnpcou, k1jb, jnpcq, &
                                 indxjp, surjp)
                            if (nelc > 2) then
                               call surf_tab (id2, k2ib, incq, mincom, &
                                    maxcom, indxi, suri)
                               call surf_tab (id2, k2jb, jncq, mincomp,&
                                    maxcomp, indxj, surj)
                            end if
                            idebug = 0
                            call bbcup (nelc, indxip, surip, indxjp,   &
                                 surjp, indxi, suri, indxj, surj,      &
                                 incq, lssetn, jncq, lssetn, mincom,   &
                                 maxcom, mincomp, maxcomp, imcq,       &
                                 lssetm, lrhoi, lrhoj, lsigi, lsigj,   &
                                 lrgl, lrgs, mcoumx(imcq), lamdlo,     &
                                 lamdup, lamelo, lameup, bbdir, bbex,  &
                                 idebug)

! test whether any direct terms from these shells
                            if (lamdlo /= 999) then
                               ndcont = ndcont + 1
                               irsd(ndcont) = irs
                               id = 0
                               do ilamdb = lamdlo, lamdup, 2
                                  id = id + 1
                                  bbd(id,ndcont) = bbdir(ilamdb) *facdir
                               end do
                               nlamd(ndcont) = id
                               lamdst(ndcont) = pckl2(id, lamdlo)
                            end if
! test whether any exchange terms from these shells
                            if (lamelo /= 999) then
                               necont = necont + 1
                               irse(necont) = irs
                               isre(necont) = isr
                               ie = 0
                               do ilameb = lamelo, lameup, 2
                                  ie = ie + 1
                                  bbe(ie,necont) = bbex(ilameb) * &
                                       facex
                               end do
                               nlame(necont) = ie
                               lamest(necont) = pckl2(ie, lamelo)
                            end if
                            exit k1ib_loop
                         end if
                      end do k2jb_loop
                   end do l1jb_loop
                end do k2ib_loop
             end do k1ib_loop
! no interacting shells found.  write the files.
!$OMP CRITICAL (WRB)
             call wrbb (nelem, nonz, ndcont, nlamd, lamdst, irsd, &
                  bbd, necont, nlame, lamest, irse, bbe, isre,    &
                  ncont1, irs1, bb1)
!$OMP END CRITICAL (WRB)
          end do j_csfs
       end do i_csfs
!$OMP END DO
!$OMP END PARALLEL
!       call delete (otime)
!       call report_time ('angbb off_diagonal', otime)

       nelem = ncsf * (ncsf+1) / 2
       deallocate (necfg, lecfg, nelems, stat=status)
       if (status /= 0) then
          write (fo,'(a,i6)') 'angbb: deallocation error = ', status
          stop
       end if
       if (nonz < nelem+1) call iwrita (jbufiv, 0)
       if (bug7 == 3) then  ! check location of output data
          call lookup (ivbbi(isnp), nrecvi, ibufvi) ! rec #; buff posn
          call lookup(ivbbr(isnp), nrecvr, ibufvr)
          write (fo,'(9(2x,i5))') isnp, nrecvi, ibufvi, nrecvr, ibufvr
       end if
    end do symmetries

    call endw (jbufiv)     ! end integer data file

    call writix (jbufiv)   ! index
! additional symmetry information for ham:   lpset, ippset, ispset
    call iwrita (jbufiv, npset)
    call iwritb (jbufiv, lpset, 1, npset)
    call iwritb (jbufiv, ippset, 1, npset)
    call iwritb (jbufiv, ispset, 1, npset)

    call iwritb (jbufiv, npcset, 1, npset)
    call iwritb (jbufiv, ivbbi, 1, npset)
    call endw (jbufiv)

    call endw (jbuffv)
    call writix (jbuffv)
    call iwritb (jbuffv, ivbbr, 1, npset)
    call endw (jbuffv)
    if (bug9 == 1) then
       write (fo,'(/,a,/)') 'Data on index of ' // TRIM(filea) // &
            ' and ' // TRIM(fileb)
       write (fo,'(a,i4)') 'npset = ', npset
       write (fo,'(a,5i6/(10x,5i6))') ' npcset = ', npcset(1:npset)
       write (fo,'(/2x,a6,6(2x,i9)/(8x,6(2x,i9)))') 'ivbbi', &
            ivbbi(1:npset)
       write (fo,'(/2x,a6,6(2x,i9)/(8x,6(2x,i9)))') 'ivbbr', &
            ivbbr(1:npset)
    end if

    stats = 'keep'
    call close (jbufiv, stats)
    call close (jbuffv, stats)
    call cpu_time (t1)
    write (fo,'(a,f16.4,a)') 'ANGBB CPU time     = ', t1 - t0, ' secs'
    call system_clock (count=c1, count_rate=cr)
    write (fo,'(a,f16.4,a)') 'ANGBB Elapsed time = ', REAL(c1-c0,wp) / &
         REAL(cr,wp), ' secs'
    write (fo,'(a)') 'ANGBB completed'
  contains
    subroutine equiv_ccfgs (isnp, ncfgs)
! Obtain information to loop on equivalent coupling cfgs
! for each unique coupled cfg
      integer, intent(in)    :: isnp   ! symmetry sequence
      integer, intent(in)    :: ncfgs  ! total # coupled cfgs
      integer                :: status, cpg0, pcq0, icsnp, cpg, pcq, ie
      integer                :: nelem, cfg

! use upper limit on array sizes:
      allocate (necfg(ncfgs), lecfg(ncfgs), nelems(ncfgs), stat=status)
      if (status /= 0) then
         write (fo,'(a,i6)') 'equiv_ccfgs: allocation error = ', status
         stop
      end if
      cpg0 = 0
      pcq0 = 0
      ie = 0
      nelem = - ncfgs
      ecfgs: do icsnp = 1, ncfgs
         nelem = nelem + ncfgs - icsnp + 2
         nelems(icsnp) = nelem
         cpg = ncfgb(icsnp,isnp) ! coupling
         cfg = ncfga(icsnp,isnp) ! cfg
         pcq = npnpq(cfg)        ! packed quantum #s of cfg
         if (pcq /= pcq0 .or. cpg /= cpg0) then ! new unique ccfg
            ie = ie + 1           ! nonequiv ecfg
            necfg(ie) = icsnp
            lecfg(ie) = 0
            cpg0 = cpg           ! set new coupling
            pcq0 = pcq           ! quantum #s of equivalent cfg
         else
            lecfg(ie) = icsnp
         end if
      end do ecfgs
      necfgs = ie                ! # unique ccfgs
    end subroutine equiv_ccfgs

  end subroutine angbb

  subroutine wr_abbi(npset, lpset, ispset, ippset)
! write an index to abbi in the special case that there are
! no bound terms in a run which includes exchange  
! can happen at high total L
    use filehand, only: open, iwrita, endw, writix, iwritb, close
    use bb_aux, only: jbufiv, filea
    integer, intent(in)  :: npset       ! # SLpi cfg sets
    integer, intent(in)  :: lpset(:)    ! cfg set L values
    integer, intent(in)  :: ispset(:)   ! cfg set S values
    integer, intent(in)  :: ippset(:)   ! cfg set Pi values
    integer              :: ndummy
    integer              :: npcset(npset)
    integer              :: ivbbi(npset)
    character(len=7)     :: stats
    logical              :: real_typ
    stats = 'new'
    real_typ = .false.
    call open (jbufiv, filea, stats, real_typ)
    call writix (jbufiv)   ! index
! additional symmetry information for ham:   lpset, ippset, ispset
    call iwrita (jbufiv, npset)
    call iwritb (jbufiv, lpset, 1, npset)
    call iwritb (jbufiv, ippset, 1, npset)
    call iwritb (jbufiv, ispset, 1, npset)
    npcset = 0 ! zero configurations in each total symmetry
    ivbbi = 0 ! not really necessary 
    call iwritb (jbufiv, npcset, 1, npset)
    call iwritb (jbufiv, ivbbi, 1, npset)
    call endw (jbufiv)
    stats = 'keep'
    call close (jbufiv, stats)
  end subroutine wr_abbi

  pure function triangular_index (n, row, col)
! Find triangular index corresponding to row and col
    integer               :: triangular_index
    integer, intent(in)   :: n    ! triangle array dimension
    integer, intent(in)   :: row  ! row index
    integer, intent(in)   :: col  ! column index
    triangular_index = (n*(n+1) - (n-col+1) * (n-col+2)) / 2 + &
         row - col + 1
  end function triangular_index

end module bb_angular_integrals

       
